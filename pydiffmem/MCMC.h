#include "mcmc/SamplerFactory.hpp"
#include "mcmc/Sampler.hpp"
#include "mcmc/SamplingTrace.hpp"
#include "mcmc/MESampler.hpp"

namespace mcmc {
	SamplingTraceCallback make_sampling_trace_cb(size_t (*cb)(SamplingValues &, void *), void *userdata) {
		return [cb, userdata](SamplingValues vals) mutable -> size_t {
			if( PyErr_CheckSignals() == -1 && PyErr_ExceptionMatches(PyExc_KeyboardInterrupt) != 0 ) {
				PyErr_Clear();
				throw interrupted_exception();
			}

			return cb(vals, userdata);
		};
	}
}

