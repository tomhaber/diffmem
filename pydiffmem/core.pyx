from __future__ import absolute_import
cimport core_defs as cpp
from eigen cimport *

from cython.operator cimport dereference as deref
from libcpp.memory cimport shared_ptr, unique_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport bool

import numpy as np
cimport numpy as np

np.import_array()

NA = cpp.valueOfNA()
def isNA(double x):
    return cpp.isNA(x)

def InitDiffMEM(nthreads = -1, noInitTBB = False):
    cdef cpp.DiffMEM_Options opts
    opts.nthreads = nthreads
    opts.noInitTBB = noInitTBB
    cpp.InitDiffMEM(opts)
InitDiffMEM()

cdef class PluginManager:
    @staticmethod
    def loadPlugin(name):
        cpp.PluginManager.instance().loadPlugin(name.encode('ascii'))

    @staticmethod
    def unloadPlugin(name):
        cpp.PluginManager.instance().unloadPlugin(name.encode('ascii'))

    @staticmethod
    def isPluginLoaded(name):
        return cpp.PluginManager.instance().isPluginLoaded(name.encode('ascii'))

cdef class LikelihoodModel:
    cdef cpp.shared_ptr[cpp.LikelihoodModel] model
    def __cinit__(self, name, dict data={}):
        cdef cpp.PyDict d = cpp.PyDict(data)
        self.model = cpp.move(cpp.ModelFactory.instance().createLikelihoodModel(name.encode('ascii'), d))

    def numberOfObservations(self):
        return deref(self.model).numberOfObservations()

    def numberOfSigmas(self):
        return deref(self.model).numberOfSigmas()

    def numberOfTaus(self):
        return deref(self.model).numberOfTaus()

    def transsigma(self, np.ndarray tau):
        sigma = np.empty([self.numberOfSigmas()], dtype=np.double, order='F')
        deref(self.model).transsigma(Map[ConstVecd](tau), Map[Vecd](sigma))
        return sigma

    def transtau(self, np.ndarray sigma):
        tau = np.empty([self.numberOfTaus()], dtype=np.double, order='F')
        deref(self.model).transtau(Map[ConstVecd](sigma), Map[Vecd](tau))
        return tau

    def logpdf(self, np.ndarray tau, np.ndarray u):
        return deref(self.model).logpdf(Map[ConstVecd](tau), Map[ConstMatrixd](u))

    def logpdfGradTau(self, np.ndarray tau, np.ndarray u):
        gradtau = np.empty([np.size(tau)], dtype=np.double, order='F')
        deref(self.model).logpdfGradTau(Map[ConstVecd](tau), Map[ConstMatrixd](u), Map[Vecd](gradtau))
        return gradtau

    def logpdfGrad(self, np.ndarray tau, np.ndarray u, np.ndarray s):
        gradu = np.empty([np.size(u)], dtype=np.double, order='F')
        deref(self.model).logpdfGrad(Map[ConstVecd](tau), Map[ConstMatrixd](u), Map[ConstMatrixd](s), Map[Vecd](gradu))
        return gradu

    def logpdf(self, np.ndarray psi, np.ndarray tau, StructuralModel sm, grad = False, hess = False):
        cdef int N = np.size(psi)
        if hess:
            g = np.empty([N], dtype=np.double, order='F')
            H = np.empty([N, N], dtype=np.double, order='F')
            ll = deref(self.model).logpdf(Map[ConstVecd](psi), Map[ConstVecd](tau),
                    deref(sm.model), Map[Vecd](g), Map[Matrixd](H))
            return ll, g, H
        elif grad:
            g = np.empty([N], dtype=np.double, order='F')
            ll = deref(self.model).logpdf(Map[ConstVecd](psi), Map[ConstVecd](tau),
                    deref(sm.model), Map[Vecd](g))
            return ll, g
        else:
            return deref(self.model).logpdf(Map[ConstVecd](psi), Map[ConstVecd](tau),
                    deref(sm.model))

    def logpdfGradTau(self, np.ndarray psi, np.ndarray tau, StructuralModel sm):
        cdef int N = np.size(tau)
        g = np.empty([N], dtype=np.double, order='F')
        ll = deref(self.model).logpdfGradTau(Map[ConstVecd](psi), Map[ConstVecd](tau),
                    deref(sm.model), Map[Vecd](g))
        return ll, g

    @staticmethod
    def list_available():
        return cpp.ModelFactory.instance().listLikelihoodModels()

cdef class StructuralModel:
    cdef cpp.shared_ptr[cpp.StructuralModel] model
    def __cinit__(self, name, dict data={}):
        cdef cpp.PyDict d = cpp.PyDict(data)
        self.model = cpp.move( cpp.ModelFactory.instance().createStructuralModel(name.encode('ascii'), d) )

    def numberOfObservations(self):
        return deref(self.model).numberOfObservations()

    def numberOfParameters(self):
        return deref(self.model).numberOfParameters()

    def transphi(self, np.ndarray psi):
        phi = psi.copy()
        deref(self.model).transphi(Map[ConstVecd](psi), Map[Vecd](phi))
        return phi

    def transpsi(self, np.ndarray phi):
        psi = phi.copy()
        deref(self.model).transpsi(Map[ConstVecd](phi), Map[Vecd](psi))
        return psi

    def dtranspsi(self, np.ndarray phi):
        dphi = phi.copy()
        deref(self.model).dtranspsi(Map[ConstVecd](phi), Map[Vecd](dphi))
        return dphi

    def evalU(self, np.ndarray psi, np.ndarray t):
        u = np.empty([self.numberOfObservations(), np.size(t)], dtype=np.double, order='F')
        deref(self.model).evalU(Map[ConstVecd](psi), Map[ConstVecd](t), Map[Matrixd](u))
        return u

    def evalSensitivity(self, np.ndarray psi, np.ndarray t):
        cdef int numberOfSensitivities = self.numberOfObservations() * (np.size(psi) + 1);
        s = np.empty([numberOfSensitivities, np.size(t)], dtype=np.double, order='F')
        deref(self.model).evalSens(Map[ConstVecd](psi), Map[ConstVecd](t), Map[Matrixd](s))
        return s

    def evalFD(self, np.ndarray psi, np.ndarray t):
        cdef int numberOfSensitivities = self.numberOfObservations() * (np.size(psi) + 1);
        s = np.empty([numberOfSensitivities, np.size(t)], dtype=np.double, order='F')
        cpp.evalFD(deref(self.model), Map[ConstVecd](psi), Map[ConstVecd](t), Map[Matrixd](s))
        return s

    @staticmethod
    def list_available():
        return cpp.ModelFactory.instance().listStructuralModels()

cdef class Individual:
    cdef cpp.Individual individual

    def __cinit__(self, int id_, StructuralModel sm, LikelihoodModel lm, **kwargs):
        self.individual = cpp.move( cpp.Individual(id_,
                cpp.move(sm.model), cpp.move(lm.model),
                Map[ConstMatrixd](kwargs['A']),
                Map[ConstMatrixd](kwargs['B'])) )

    def numberOfBetas(self):
        return self.individual.numberOfBetas()

    def numberOfObservations(self):
        return self.individual.numberOfObservations()

    def numberOfRandom(self):
        return self.individual.numberOfRandom()

    def numberOfMeasurements(self):
        return self.individual.numberOfMeasurements()

    def numberOfParameters(self):
        return self.individual.numberOfParameters()

    def numberOfSigmas(self):
        return self.individual.numberOfSigmas()

    def numberOfTaus(self):
        return self.individual.numberOfTaus()

    def toParameter(self, np.ndarray beta, np.ndarray eta):
        phi = np.empty([self.numberOfParameters()], dtype=np.double, order='F')
        self.individual.toParameter(Map[ConstVecd](beta), Map[ConstVecd](eta), Map[Vecd](phi))
        return phi

    def logpdf(self, np.ndarray phi, np.ndarray tau, grad = False, hess = False):
        cdef int N = np.size(phi)
        if hess:
            g = np.empty([N], dtype=np.double, order='F')
            H = np.empty([N, N], dtype=np.double, order='F')
            ll = self.individual.logpdf(Map[ConstVecd](phi), Map[ConstVecd](tau), Map[Vecd](g), Map[Matrixd](H))
            return ll, g, H
        elif grad:
            g = np.empty([N], dtype=np.double, order='F')
            ll = self.individual.logpdf(Map[ConstVecd](phi), Map[ConstVecd](tau), Map[Vecd](g))
            return ll, g
        else:
            return self.individual.logpdf(Map[ConstVecd](phi), Map[ConstVecd](tau))

    def logpdfGradTau(self, np.ndarray phi, np.ndarray tau):
        cdef int N = np.size(phi)
        g = np.empty([N], dtype=np.double, order='F')
        ll = self.individual.error(Map[ConstVecd](phi), Map[ConstVecd](tau), Map[Vecd](g))
        return ll, g

cdef class ConstIndividual:
    cdef const cpp.Individual *individual

    @staticmethod
    cdef setup(const cpp.Individual *ptr):
        x = ConstIndividual()
        x.individual = ptr
        return x

    def __cinit__(self):
        self.individual = NULL

    def numberOfBetas(self):
        return self.individual.numberOfBetas()

    def numberOfObservations(self):
        return self.individual.numberOfObservations()

    def numberOfRandom(self):
        return self.individual.numberOfRandom()

    def numberOfMeasurements(self):
        return self.individual.numberOfMeasurements()

    def numberOfParameters(self):
        return self.individual.numberOfParameters()

    def numberOfSigmas(self):
        return self.individual.numberOfSigmas()

    def numberOfTaus(self):
        return self.individual.numberOfTaus()

    def toParameter(self, np.ndarray beta, np.ndarray eta):
        phi = np.empty([self.numberOfParameters()], dtype=np.double, order='F')
        self.individual.toParameter(Map[ConstVecd](beta), Map[ConstVecd](eta), Map[Vecd](phi))
        return phi

    def logpdf(self, np.ndarray phi, np.ndarray tau, grad = False, hess = False):
        cdef int N = np.size(phi)
        if hess:
            g = np.empty([N], dtype=np.double, order='F')
            H = np.empty([N, N], dtype=np.double, order='F')
            ll = self.individual.logpdf(Map[ConstVecd](phi), Map[ConstVecd](tau), Map[Vecd](g), Map[Matrixd](H))
            return ll, g, H
        elif grad:
            g = np.empty([N], dtype=np.double, order='F')
            ll = self.individual.logpdf(Map[ConstVecd](phi), Map[ConstVecd](tau), Map[Vecd](g))
            return ll, g
        else:
            return self.individual.logpdf(Map[ConstVecd](phi), Map[ConstVecd](tau))

    def logpdfGradTau(self, np.ndarray phi, np.ndarray tau):
        cdef int N = np.size(phi)
        g = np.empty([N], dtype=np.double, order='F')
        ll = self.individual.error(Map[ConstVecd](phi), Map[ConstVecd](tau), Map[Vecd](g))
        return ll, g

cdef class Population:
    cdef cpp.Population population

    def __cinit__(self):
        self.population = cpp.Population()

    def add(self, Individual ind):
        return self.population.add( cpp.move(ind.individual) )

    def numberOfIndividuals(self):
        return self.population.numberOfIndividuals()

    def numberOfBetas(self):
        return self.population.numberOfBetas()

    def numberOfRandom(self):
        return self.population.numberOfRandom()

    def numberOfParameters(self):
        return self.population.numberOfParameters()

    def numberOfSigmas(self):
        return self.population.numberOfSigmas()

    def numberOfTaus(self):
        return self.population.numberOfTaus()

    def numberOfMeasurements(self):
        return self.population.numberOfMeasurements()

    def numberOfObservations(self):
        return self.population.numberOfObservations()

    def evalU(self, np.ndarray beta, np.ndarray etas, np.ndarray t, individuals=None):
        if individuals is None:
            N = self.numberOfIndividuals() * self.numberOfObservations()
            u = np.empty([N, np.size(t)], dtype=np.double, order='F')
            self.population.evalU(Map[ConstVecd](beta), Map[ConstMatrixd](etas), Map[ConstVecd](t), Map[Matrixd](u))
        else:
            if not isinstance(individuals, np.ndarray):
                individuals = np.array(individuals, dtype=np.int, order='F')

            N = len(individuals) * self.numberOfObservations()
            u = np.empty([N, np.size(t)], dtype=np.double, order='F')
            self.population.evalU(Map[ConstVecd](beta), Map[ConstVeci](individuals), Map[ConstMatrixd](etas), Map[ConstVecd](t), Map[Matrixd](u))
        return u

    def evalSensitivity(self, np.ndarray beta, np.ndarray etas, np.ndarray t, individuals=None):
        cdef int numberOfSensitivities = self.numberOfObservations() * (etas.shape[0] + 1);
        if individuals is None:
            N = self.numberOfIndividuals() * numberOfSensitivities
            s = np.empty([N, np.size(t)], dtype=np.double, order='F')
            self.population.evalSens(Map[ConstVecd](beta), Map[ConstMatrixd](etas), Map[ConstVecd](t), Map[Matrixd](s))
        else:
            if not isinstance(individuals, np.ndarray):
                individuals = np.array(individuals, dtype=np.int, order='F')

            N = len(individuals) * numberOfSensitivities
            s = np.empty([N, np.size(t)], dtype=np.double, order='F')
            self.population.evalSens(Map[ConstVecd](beta), Map[ConstVeci](individuals), Map[ConstMatrixd](etas), Map[ConstVecd](t), Map[Matrixd](s))
        return s

cdef class MEModel:
    cdef cpp.MEModel *model

    def __cinit__(self, Population pop, **kwargs):
        self.model = new cpp.MEModel(pop.population,
                Map[ConstVecd](kwargs['beta']), Map[ConstVecb](kwargs['estimated']),
                Map[ConstMatrixd](kwargs['omega']), Map[ConstMatrixb](kwargs['cov_model']),
                Map[ConstVecd](kwargs['sigma']), Map[ConstVecb](kwargs['sigma_model']))

    def __dealloc__(self):
        del self.model

    def numberOfIndividuals(self):
        return self.model.numberOfIndividuals()

    def numberOfBetas(self):
        return self.model.numberOfBetas()

    def numberOfRandom(self):
        return self.model.numberOfRandom()

    def numberOfSigmas(self):
        return self.model.numberOfSigmas()

    def numberOfTaus(self):
        return self.model.numberOfTaus()

    def set(self, np.ndarray beta, np.ndarray omega, np.ndarray sigma):
        return self.model.set(Map[ConstVecd](beta), Map[ConstMatrixd](omega), Map[ConstVecd](sigma))

    def empiricalBayesEstimate(self):
        etas = np.empty([self.numberOfRandom(), self.numberOfIndividuals()], dtype=np.double, order='F')
        self.model.empiricalBayesEstimate(Map[Matrixd](etas))
        return etas

    def beta(self):
        return ndarray_copy( self.model.beta() )

    def omega(self):
        return ndarray_copy( self.model.omega() )

    def sigma(self):
        return ndarray_copy( self.model.sigma() )

    def tau(self):
        return ndarray_copy( self.model.tau() )

cdef class ConstMEModel:
    cdef const cpp.MEModel *model

    @staticmethod
    cdef setup(const cpp.MEModel *ptr):
        x = ConstMEModel()
        x.model = ptr
        return x

    def __cinit__(self):
        self.model = NULL

    def numberOfIndividuals(self):
        return self.model.numberOfIndividuals()

    def numberOfBetas(self):
        return self.model.numberOfBetas()

    def numberOfRandom(self):
        return self.model.numberOfRandom()

    def numberOfSigmas(self):
        return self.model.numberOfSigmas()

    def numberOfTaus(self):
        return self.model.numberOfTaus()

    def beta(self):
        return ndarray_copy( self.model.beta() )

    def omega(self):
        return ndarray_copy( self.model.omega() )

    def sigma(self):
        return ndarray_copy( self.model.sigma() )

    def tau(self):
        return ndarray_copy( self.model.tau() )

cdef class Random:
    cdef cpp.Random rng
    def __cinit__(self, seed = None):
        if seed is None:
            self.rng = cpp.Random()
        else:
            self.rng = cpp.Random(seed)

cdef class Distribution:
    cdef cpp.shared_ptr[cpp.Distribution] ptr
    def __cinit__(self, name, **kwargs):
        cdef cpp.PyDict d = cpp.PyDict(kwargs)
        self.ptr = cpp.move( cpp.ModelFactory.instance().createDistribution(name.encode('ascii'), d) )

    def numberOfDimensions(self):
        return deref(self.ptr).numberOfDimensions()

    def logpdf(self, np.ndarray x, grad = False, hess = False):
        cdef int N = np.size(x)
        if hess:
            g = np.empty([N], dtype=np.double, order='F')
            H = np.empty([N, N], dtype=np.double, order='F')
            ll = deref(self.ptr).logpdf(Map[ConstVecd](x), Map[Vecd](g), Map[Matrixd](H))
            return ll, g, H
        elif grad:
            g = np.empty([N], dtype=np.double, order='F')
            ll = deref(self.ptr).logpdf(Map[ConstVecd](x), Map[Vecd](g))
            return ll, g
        else:
            return deref(self.ptr).logpdf(Map[ConstVecd](x))

    def sample(self, nsamples=1, Random r = None, **kwargs):
        cdef cpp.PyDict d = cpp.PyDict(kwargs)
        cdef int N = deref(self.ptr).numberOfDimensions()
        cdef unique_ptr[cpp.DistributionSampler] sampler = cpp.move(deref(self.ptr).sampler(d))
        s = np.empty([N, nsamples], dtype=np.double, order='F')

        if r is None:
            r = get_rng(kwargs)

        deref(sampler).sample(r.rng, Map[Matrixd](s))
        return s

    @staticmethod
    def list_available():
        return cpp.ModelFactory.instance().listDistributions()

cdef class MEDistribution:
    cdef cpp.MEDistribution *ptr
    def __cinit__(self, MEModel m, Distribution prior):
        self.mem = m
        self.ptr = new cpp.MEDistribution(m.model[0], deref(prior.ptr))

    def __dealloc__(self):
        del self.ptr

    def logpdf(self, np.ndarray x, grad = False, hess = False):
        cdef int N = np.size(x)
        if hess:
            g = np.empty([N], dtype=np.double, order='F')
            H = np.empty([N, N], dtype=np.double, order='F')
            ll = self.ptr.logpdf(Map[ConstVecd](x), Map[Vecd](g), Map[Matrixd](H))
            return ll, g, H
        elif grad:
            g = np.empty([N], dtype=np.double, order='F')
            ll = self.ptr.logpdf(Map[ConstVecd](x), Map[Vecd](g))
            return ll, g
        else:
            return self.ptr.logpdf(Map[ConstVecd](x))

def saem(MEModel mem, **kwargs):
    cdef cpp.Random rng
    if 'seed' in kwargs:
        rng = cpp.Random( kwargs['seed'] )

    cdef cpp.SAEM_Options opts
    if 'K1' in kwargs:
        opts.K1 = kwargs['K1']
    if 'K2' in kwargs:
        opts.K2 = kwargs['K2']
    if 'burnin' in kwargs:
        opts.burnin = kwargs['burnin']
    if 'thinning' in kwargs:
        opts.thinning = kwargs['thinning']
    if 'trace' in kwargs:
        opts.trace = make_meoptimization_trace(kwargs['trace'])
    opts.validate()

    cdef cpp.SAEM* saem = new cpp.SAEM(mem.model[0], opts)
    try:
        saem.optimize(rng)
    finally:
        del saem

cdef bool optimizationtrace_helper(cpp.OptimizationState state, cpp.OptimizationValues & vals, void*f):
    cdef dict d = { 'iteration' : vals.iteration, 'x' : ndarray_copy(vals.x), 'value' : vals.value, 'gradNorm' : vals.gradNorm }
    if state == cpp.Init:
        d['state'] = 'init'
    elif state == cpp.Iter:
        d['state'] = 'iter'
    elif state == cpp.Interrupt:
        d['state'] = 'interrupt'
    elif state == cpp.Done:
        d['state'] = 'done'
    else:
        d['state'] = 'unknown'

    return (<object>f)(d)

cdef cpp.OptimizationTraceCallback make_optimization_trace(f):
    return cpp.make_optimization_trace_cb(optimizationtrace_helper, <void*>f)

cdef bool meoptimizationtrace_helper(cpp.OptimizationState istate, size_t iteration, const cpp.MEModel & model, void*f):
    if istate == cpp.Init:
        state = 'init'
    elif istate == cpp.Iter:
        state = 'iter'
    elif istate == cpp.Interrupt:
        state = 'interrupt'
    elif istate == cpp.Done:
        state = 'done'
    else:
        state = 'unknown'

    cm = ConstMEModel.setup(&model)
    return (<object>f)(state, iteration, cm)

cdef cpp.MEOptimizationTraceCallback make_meoptimization_trace(f):
    return cpp.make_meoptimization_trace_cb(meoptimizationtrace_helper, <void*>f)

cdef bfgs(cpp.Function& func, np.ndarray phi, args):
    cdef cpp.BFGS_Options opts
    opts.fTol = args.get("fTol", 1e-8)
    opts.xTol = args.get("xTol", 1e-32)
    opts.gradTol = args.get("gradTol", 1e-8)
    opts.maxiters = args.get("maxiters", 1000)
    opts.m = args.get("m", 10)
    if 'trace' in args:
        opts.trace = make_optimization_trace(args['trace'])

    cdef cpp.BFGS* bfgs = new cpp.BFGS(func, Map[ConstVecd](phi), opts)
    cdef int iters = 0
    try:
        iters = bfgs.optimize()
        return {
            'par' : ndarray_copy(bfgs.optimum()),
            'value' : -bfgs.value(),
            'convergence' : bfgs.isConverged(),
            'iters' : iters
        }
    finally:
        del bfgs

cdef pso(cpp.Function& func, np.ndarray phi, args):
    cdef cpp.PSO_Options opts
    opts.numParticles = args.get("numParticles", 20)
    opts.numEpochs = args.get("numEpochs", 1000)
    opts.c1 = args.get("c1", 2.0)
    opts.c2 = args.get("c2", 2.0)
    opts.w_start = args.get("w_start", 0.95)
    opts.w_end = args.get("w_end", 0.4)
    opts.w_varyfor = args.get("w_varyfor", 1.0)
    opts.V_max = args.get("V_max", 100.0)
    cdef double sigma = args["sigma"]
    if 'trace' in args:
        opts.trace = make_optimization_trace(args['trace'])

    cdef cpp.ParticleSwarmOptimization* pso = new cpp.ParticleSwarmOptimization(func, opts)
    try:
        pso.optimize(Map[ConstVecd](phi), sigma)
        return {
            'par' : ndarray_copy(pso.optimum()),
            'value' : -pso.value()
        }
    finally:
        del pso

cdef neldermead(cpp.Function& func, np.ndarray phi, args):
    cdef cpp.NelderMead_Options opts
    opts.fTol = args.get("fTol", 1e-8)
    opts.alpha = args.get("alpha", -1.0)
    opts.beta = args.get("beta", 0.5)
    opts.gamma = args.get("gamma", 2.0)
    opts.usualDelta = args.get("usualDelta", 5.0e-2)
    opts.zeroDelta = args.get("zeroDelta", 0.25e-3)
    opts.maxiters = args.get("maxiters", 1000)
    if 'trace' in args:
        opts.trace = make_optimization_trace(args['trace'])

    cdef cpp.NelderMead* neldermead = new cpp.NelderMead(func, Map[ConstVecd](phi), opts)
    cdef int iters = 0
    try:
        iters = neldermead.optimize()
        return {
            'par' : ndarray_copy(neldermead.optimum()),
            'value' : -neldermead.value(),
            'convergence' : neldermead.isConverged(),
            'iters' : iters
        }
    finally:
        del neldermead

cdef pattern(cpp.Function& func, np.ndarray phi, args):
    cdef int N = phi.size
    cdef cpp.Pattern_Options opts
    opts.fTol = args.get("fTol", 1e-6)
    opts.xTol = args.get("xTol", 1e-6)
    opts.meshTol = args.get("meshTol", 1e-6)
    opts.initialMeshSize = args.get("initialMeshSize", 1.0)
    opts.expansion = args.get("expansion", 2.0)
    opts.contraction = args.get("contraction", 0.5)
    opts.maxMeshSize = args.get("maxMeshSize", float("inf"))
    opts.maxEvals = args.get("maxEvals", 2000*N)
    opts.maxIters = args.get("maxIters", 100*N)
    if 'trace' in args:
        opts.trace = make_optimization_trace(args['trace'])

    method = args.get("method", "Positive2N")
    if method == "Positive2N":
        opts.method = cpp.Positive2N
    elif method == "PositiveNp1":
        opts.method = cpp.PositiveNp1
    elif method == "Cross4N":
        opts.method = cpp.Cross4N
    else:
        raise NameError("unknown poll method for patternsearch: " + method)

    cdef cpp.PatternSearch* search = new cpp.PatternSearch(func, Map[ConstVecd](phi), opts)
    cdef int iters = 0
    try:
        iters = search.optimize()
        return {
            'par' : ndarray_copy(search.optimum()),
            'value' : -search.value(),
            'convergence' : search.isConverged(),
            'iters' : iters,
	    'nevals' : search.numberOfEvals(),
	    'meshsize' : search.meshSize()
        }
    finally:
        del search


def optim(Distribution dist, np.ndarray x, **kwargs):
    algo = kwargs.get("algo", "BFGS")
    if algo == "BFGS":
        return bfgs(deref(dist.ptr), x, kwargs)
    elif algo == "PSO":
        return pso(deref(dist.ptr), x, kwargs)
    elif algo == "NelderMead":
        return neldermead(deref(dist.ptr), x, kwargs)
    elif algo == "Pattern":
        return pattern(deref(dist.ptr), x, kwargs)
    else:
        raise NameError("unknown algorithm")

cdef size_t samplingtrace_helper(cpp.SamplingValues & vals, void*f):
    cdef dict d = { 'iteration' : vals.iteration, 'x' : ndarray_copy(vals.x), 'value' : vals.value }
    res = (<object>f)(d)
    if res is None:
        return 1
    return res

cdef cpp.SamplingTraceCallback make_sampling_trace(f):
    return cpp.make_sampling_trace_cb(samplingtrace_helper, <void*>f)

cdef class Sampler:
    cdef cpp.unique_ptr[cpp.Sampler] ptr
    cdef cpp.shared_ptr[cpp.Distribution] dist

    def __cinit__(self, name, Distribution dist, kwargs):
        cdef cpp.PyDict d = cpp.PyDict(kwargs)
        self.dist = dist.ptr
        self.ptr = cpp.move( cpp.SamplerFactory.instance().createSampler(name.encode('ascii'), self.dist, d) )
        if 'trace' in kwargs:
            deref(self.ptr).setTrace( make_sampling_trace(kwargs['trace']) )

    def reset(self):
        deref(self.ptr).reset()

    def reset(self, np.ndarray x):
        deref(self.ptr).reset(Map[ConstVecd](x))

    def warmup(self, Random r, warmup):
        deref(self.ptr).warmup(r.rng, warmup)

    def sample(self, Random r, nsamples):
        dim = deref(self.dist).numberOfDimensions()
        samples = np.empty([dim, nsamples], order='F', dtype=np.double)
        deref(self.ptr).sample(r.rng, Map[Matrixd](samples))
        return samples

rng = Random()
def get_rng(kwargs):
    if 'seed' in kwargs:
        return Random(kwargs['seed'])
    else:
        global rng
        return rng

def sample(Distribution dist, nsamples, warmup=0, **kwargs):
    rng = get_rng(kwargs)
    name = kwargs.get("type_", "MH")
    sampler = Sampler(name, dist, kwargs)
    sampler.warmup(rng, warmup)
    return sampler.sample(rng, nsamples)

