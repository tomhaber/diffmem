#include "Except.h"
#include "Init.hpp"
#include "Measurements.hpp"
#include "ModelFactory.hpp"
#include "PluginManager.hpp"
#include "Dict.hpp"
#include "Random.hpp"
#include "eigen_cpp.h"

[[noreturn]] static void throwPythonError() {
	 PyObject *type, *value, *traceback;
	 PyErr_Fetch(&type, &value, &traceback);
	 PyErr_Clear();

	 std::string message = "Python error: ";
	 if (type) {
		 PyObject *o = PyObject_Repr(type);
		 PyObject *pyStr = PyUnicode_AsEncodedString(o, "ascii", "Error ~");
		 message += PyBytes_AS_STRING(pyStr);
		 Py_XDECREF(o);
		 Py_XDECREF(pyStr);
	 }
	 if (value) {
		 PyObject *o = PyObject_Repr(value);
		 PyObject *pyStr = PyUnicode_AsEncodedString(o, "ascii", "Error ~");
		 message += ": ";
		 message += PyBytes_AS_STRING(pyStr);
		 Py_XDECREF(o);
		 Py_XDECREF(pyStr);
	 }
	 Py_XDECREF(type);
	 Py_XDECREF(value);
	 Py_XDECREF(traceback);

	 throw std::runtime_error( message );
 }

class PyDict final : public Dict {
	public:
		PyDict() : dict(NULL) {
		}
		PyDict(PyObject *dict) : dict(dict) {
			Py_XINCREF(dict);
		}
		PyDict(const PyDict & other) {
			dict = other.dict;
			Py_XINCREF(dict);
		}
		PyDict & operator =(const PyDict & other) {
			dict = other.dict;
			Py_XINCREF(dict);
			return *this;
		}
		~PyDict() {
			Py_XDECREF(dict);
		}

	public:
		bool exists(const std::string & name) const override {
			PyObject *obj = PyDict_GetItemString( dict, name.c_str() );
			if( obj == NULL )
				return false;

			return true;
		}

		Vecd getNumericVector(const std::string & name) const override {
			PyObject *obj = PyDict_GetItemString( dict, name.c_str() );
			if( obj == NULL )
				throw missingkey_error(name);

			if( ! PyArray_Check(obj) )
				throw std::runtime_error("Excepting an numeric array");

			PyArrayObject *arr = (PyArrayObject*)obj;
			if( PyArray_TYPE(arr) != NPY_DOUBLE )
				throw std::runtime_error("Excepting an numeric double array");
			if( PyArray_NDIM(arr) != 1 )
				throw std::runtime_error("Excepting a numeric vector");
			return Map<Vecd>(arr);
		}

		Matrixd getNumericMatrix(const std::string & name) const override {
			PyObject *obj = PyDict_GetItemString( dict, name.c_str() );
			if( obj == NULL )
				throw missingkey_error(name);

			if( ! PyArray_Check(obj) )
				throw std::runtime_error("Excepting a numeric array");

			PyArrayObject *arr = (PyArrayObject*)obj;
			if( PyArray_TYPE(arr) != NPY_DOUBLE )
				throw std::runtime_error("Excepting a numeric double array");
			if( PyArray_NDIM(arr) == 1 )
				throw std::runtime_error("Excepting a numeric matrix");
			return Map<Matrixd>(arr);
		}

		Veci getIntegerVector(const std::string & name) const override {
			PyObject *obj = PyDict_GetItemString( dict, name.c_str() );
			if( obj == NULL )
				throw missingkey_error(name);

			if( ! PyArray_Check(obj) )
				throw std::runtime_error("Excepting a numeric array");

			PyArrayObject *arr = (PyArrayObject*)obj;
			if( PyArray_TYPE(arr) != NPY_INT64 )
				throw std::runtime_error("Excepting a numeric integer array");
			if( PyArray_NDIM(arr) != 1 )
				throw std::runtime_error("Excepting a numeric vector");
			return Map<Veci>(arr);
		}

		Matrixi getIntegerMatrix(const std::string & name) const override {
			PyObject *obj = PyDict_GetItemString( dict, name.c_str() );
			if( obj == NULL )
				throw missingkey_error(name);

			if( ! PyArray_Check(obj) )
				throw std::runtime_error("Excepting a numeric array");

			PyArrayObject *arr = (PyArrayObject*)obj;
			if( PyArray_TYPE(arr) != NPY_INT64 )
				throw std::runtime_error("Excepting a numeric integer array");
			if( PyArray_NDIM(arr) == 2 )
				throw std::runtime_error("Excepting a numeric matrix");
			return Map<Matrixi>(arr);
		}

		Vecb getBooleanVector(const std::string & name) const override {
			PyObject *obj = PyDict_GetItemString( dict, name.c_str() );
			if( obj == NULL )
				throw missingkey_error(name);

			if( ! PyArray_Check(obj) ) {
				throw std::runtime_error("Excepting a numeric array");
			}

			PyArrayObject *arr = (PyArrayObject*)obj;
			if( PyArray_TYPE(arr) != NPY_UINT8 )
				throw std::runtime_error("Excepting a numeric bool (uint8) array");
			if( PyArray_NDIM(arr) == 1 )
				throw std::runtime_error("Excepting a numeric vector");
			return Map<Vecb>(arr);
		}

		Matrixb getBooleanMatrix(const std::string & name) const override {
			PyObject *obj = PyDict_GetItemString( dict, name.c_str() );
			if( obj == NULL )
				throw missingkey_error(name);

			if( ! PyArray_Check(obj) )
				throw std::runtime_error("Excepting a numeric array");

			PyArrayObject *arr = (PyArrayObject*)obj;
			if( PyArray_TYPE(arr) != NPY_UINT8 )
				throw std::runtime_error("Excepting a numeric bool (uint8) array");
			if( PyArray_NDIM(arr) == 2 )
				throw std::runtime_error("Excepting a numeric matrix");
			return Map<Matrixb>(arr);
		}

		std::string getString(const std::string & name) const override {
			PyObject *obj = PyDict_GetItemString( dict, name.c_str() );
			if( obj == NULL )
				throw missingkey_error(name);

			std::string s;
			if( PyUnicode_Check(obj) ) {
				PyObject *pyStr = PyUnicode_AsEncodedString(obj, "ascii", "Error ~");
				s = PyBytes_AS_STRING(pyStr);
				Py_XDECREF(pyStr);
			} else {
				s = PyBytes_AS_STRING(obj);
			}

			if( PyErr_Occurred() ) {
				throwPythonError();
			}

			return s;
		}

		int getInteger(const std::string & name) const override {
			PyObject *obj = PyDict_GetItemString( dict, name.c_str() );
			if( obj == NULL )
				throw missingkey_error(name);

			long x = PyInt_AsLong(obj);
			if( PyErr_Occurred() ) {
				throwPythonError();
			}

			return x;
		}

		double getNumeric(const std::string & name) const override {
			PyObject *obj = PyDict_GetItemString( dict, name.c_str() );
			if( obj == NULL )
				throw missingkey_error(name);

			double x = PyFloat_AsDouble(obj);
			if( PyErr_Occurred() ) {
				throwPythonError();
			}

			return x;
		}

		std::shared_ptr<Dict> getDict(const std::string & name) const override {
			PyObject *obj = PyDict_GetItemString( dict, name.c_str() );
			if( obj == NULL )
				throw missingkey_error(name);

			return nullptr;
		}

	private:
		 PyObject *dict;
};

#include "LikelihoodModel.hpp"
#include "StructuralModel.hpp"
#include "Individual.hpp"

inline void evalFD(StructuralModel & model, ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd sens) {
	model.StructuralModel::evalSens(psi, t, sens);
}

#include "Population.hpp"
#include "MEModel.hpp"
#include "SAEM.hpp"
#include "Distribution.hpp"
#include "MEDistribution.hpp"
#include <OptimizationTrace.hpp>

OptimizationTraceCallback make_optimization_trace_cb(bool (*cb)(OptimizationState, OptimizationValues &, void *), void *userdata) {
	return [cb, userdata](OptimizationState state, OptimizationValues vals) mutable -> bool {
		if( PyErr_CheckSignals() == -1 && PyErr_ExceptionMatches(PyExc_KeyboardInterrupt) != 0 ) {
			PyErr_Clear();
			throw interrupted_exception();
		}

		return cb(state, vals, userdata);
	};
}

MEOptimizationTraceCallback make_meoptimization_trace_cb(bool (*cb)(OptimizationState, size_t, const MEModel &, void *), void *userdata) {
	return [cb, userdata](OptimizationState state, size_t iteration, const MEModel & model) mutable -> bool {
		if( PyErr_CheckSignals() == -1 && PyErr_ExceptionMatches(PyExc_KeyboardInterrupt) != 0 ) {
			PyErr_Clear();
			throw interrupted_exception();
		}

		return cb(state, iteration, model, userdata);
	};
}

#include <BFGS.hpp>
#include <NelderMead.hpp>
#include <PatternSearch.hpp>
#include <ParticleSwarmOptimization.hpp>
