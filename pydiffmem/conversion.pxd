cimport numpy as np
from libcpp cimport bool

cdef api np.ndarray[double, ndim=2] ndarray_double_C(double *data, long rows, long cols, long outer_stride, long inner_stride)
cdef api np.ndarray[double, ndim=2] ndarray_double_F(double *data, long rows, long cols, long outer_stride, long inner_stride)
cdef api np.ndarray[double, ndim=2] ndarray_copy_double_C(const double *data, long rows, long cols, long outer_stride, long inner_stride)
cdef api np.ndarray[double, ndim=2] ndarray_copy_double_F(const double *data, long rows, long cols, long outer_stride, long inner_stride)

cdef api np.ndarray[int, ndim=2] ndarray_int_C(int *data, long rows, long cols, long outer_stride, long inner_stride)
cdef api np.ndarray[int, ndim=2] ndarray_int_F(int *data, long rows, long cols, long outer_stride, long inner_stride)
cdef api np.ndarray[int, ndim=2] ndarray_copy_int_C(const int *data, long rows, long cols, long outer_stride, long inner_stride)
cdef api np.ndarray[int, ndim=2] ndarray_copy_int_F(const int *data, long rows, long cols, long outer_stride, long inner_stride)

cdef api np.ndarray[char, ndim=2] ndarray_bool_C(bool *data, long rows, long cols, long outer_stride, long inner_stride)
cdef api np.ndarray[char, ndim=2] ndarray_bool_F(bool *data, long rows, long cols, long outer_stride, long inner_stride)
cdef api np.ndarray[char, ndim=2] ndarray_copy_bool_C(const bool *data, long rows, long cols, long outer_stride, long inner_stride)
cdef api np.ndarray[char, ndim=2] ndarray_copy_bool_F(const bool *data, long rows, long cols, long outer_stride, long inner_stride)
