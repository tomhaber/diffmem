from eigen cimport *

from cython.operator cimport dereference as deref
from libcpp.memory cimport shared_ptr, unique_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport bool

cdef extern from "DiffMEM.h":
    cdef struct DiffMEM_Options "diffmem::Options":
        int nthreads
        bool noInitTBB
    cdef void InitDiffMEM "diffmem::init" (const DiffMEM_Options &)

    cdef double valueOfNA "math::valueOfNA" ()
    cdef bool isNA "math::isNA" (double)

    cdef cppclass PyDict:
        PyDict() except +
        PyDict(dict) except +

    cdef cppclass Random:
        Random() except +
        Random(unsigned long) except +

    cdef cppclass Function:
        pass

    cdef cppclass DistributionSampler:
        void reset() except +
        void sample(Random &, Map[Matrixd]) except +

    cdef cppclass Distribution(Function):
        int numberOfDimensions()
        double logpdf(Map[ConstVecd]&) except +
        double logpdf(Map[ConstVecd]&, Map[Vecd]) except +
        double logpdf(Map[ConstVecd]&, Map[Vecd], Map[Matrixd]) except +
        unique_ptr[DistributionSampler] sampler(const PyDict &) except +

    cdef cppclass ModelFactory:
        @staticmethod
        ModelFactory & instance()
        shared_ptr[StructuralModel] createStructuralModel(const string &, const PyDict &) except +
        shared_ptr[LikelihoodModel] createLikelihoodModel(const string &, const PyDict &) except +
        shared_ptr[Distribution] createDistribution(const string &, const PyDict &) except +
        vector[string] listDistributions() except +
        vector[string] listStructuralModels() except +
        vector[string] listLikelihoodModels() except +

    cdef cppclass PluginManager:
        @staticmethod
        PluginManager & instance()
        void loadPlugin(const string &) except +
        void unloadPlugin(const string &) except +
        bool isPluginLoaded(const string &) except +

    cdef cppclass LikelihoodModel:
        int numberOfObservations()
        int numberOfSigmas()
        int numberOfTaus()
        void transsigma(Map[ConstVecd]&, Map[Vecd]) except +
        void transtau(Map[ConstVecd]&, Map[Vecd]) except +
        double logpdf(Map[ConstVecd]&, Map[ConstMatrixd]&) except +
        double logpdfGradTau(Map[ConstVecd]&, Map[ConstMatrixd]&, Map[Vecd]&) except +
        double logpdfGrad(Map[ConstVecd]&, Map[ConstMatrixd]&, Map[ConstMatrixd]&, Map[Vecd]&) except +

        double logpdf(Map[ConstVecd]&, Map[ConstVecd]&, StructuralModel&) except +
        double logpdf(Map[ConstVecd]&, Map[ConstVecd]&, StructuralModel &, Map[Vecd]) except +
        double logpdf(Map[ConstVecd]&, Map[ConstVecd]&, StructuralModel&, Map[Vecd], Map[Matrixd]) except +
        double logpdfGradTau(Map[ConstVecd]&, Map[ConstVecd]&, StructuralModel&, Map[Vecd]) except +

    cdef cppclass StructuralModel:
        int numberOfObservations()
        int numberOfParameters()
        void transphi(Map[ConstVecd]&, Map[Vecd]) except +
        void transpsi(Map[ConstVecd]&, Map[Vecd]) except +
        void dtranspsi(Map[ConstVecd]&, Map[Vecd]) except +
        void evalU(Map[ConstVecd]&, Map[ConstVecd]&, Map[Matrixd]) except +
        void evalSens(Map[ConstVecd]&, Map[ConstVecd]&, Map[Matrixd]) except +

    void evalFD(StructuralModel &, Map[ConstVecd]&, Map[ConstVecd]&, Map[Matrixd]) except +

    cdef cppclass Individual:
        Individual()
        Individual(int, shared_ptr[StructuralModel], shared_ptr[LikelihoodModel], Map[ConstMatrixd]&, Map[ConstMatrixd]&) except +
        int numberOfBetas()
        int numberOfParameters()
        int numberOfSigmas()
        int numberOfTaus()
        int numberOfRandom()
        int numberOfObservations()
        int numberOfMeasurements()
        void toParameter(Map[ConstVecd]&, Map[ConstVecd]&, Map[Vecd]&) except +
        double logpdf(Map[ConstVecd]&, Map[ConstVecd]&) except +
        double logpdf(Map[ConstVecd]&, Map[ConstVecd]&, Map[Vecd]) except +
        double logpdf(Map[ConstVecd]&, Map[ConstVecd]&, Map[Vecd], Map[Matrixd]) except +
        double error(Map[ConstVecd]&, Map[ConstVecd]&, Map[Vecd]) except +

    cdef cppclass Population:
        Population()
        void add(Individual &&) except +
        int numberOfIndividuals()
        int numberOfBetas()
        int numberOfParameters()
        int numberOfSigmas()
        int numberOfTaus()
        int numberOfRandom()
        int numberOfObservations()
        int numberOfMeasurements()
        const Individual & subject(int)
        void evalU(Map[ConstMatrixd]&, Map[ConstVecd]&, Map[Matrixd]) except +
        void evalU(Map[ConstVeci]&, Map[ConstMatrixd]&, Map[ConstVecd]&, Map[Matrixd]) except +
        void evalSens(Map[ConstMatrixd]&, Map[ConstVecd]&, Map[Matrixd]) except +
        void evalSens(Map[ConstVeci]&, Map[ConstMatrixd]&, Map[ConstVecd]&, Map[Matrixd]) except +
        void evalU(Map[ConstVecd]&, Map[ConstMatrixd]&, Map[ConstVecd]&, Map[Matrixd]) except +
        void evalU(Map[ConstVecd]&, Map[ConstVeci]&, Map[ConstMatrixd]&, Map[ConstVecd]&, Map[Matrixd]) except +
        void evalSens(Map[ConstVecd]&, Map[ConstMatrixd]&, Map[ConstVecd]&, Map[Matrixd]) except +
        void evalSens(Map[ConstVecd]&, Map[ConstVeci]&, Map[ConstMatrixd]&, Map[ConstVecd]&, Map[Matrixd]) except +

    cdef cppclass MEModel:
        MEModel(const Population&,
                Map[ConstVecd]&, Map[ConstVecb]&,
                Map[ConstMatrixd]&, Map[ConstMatrixb]&,
                Map[ConstVecd]&, Map[ConstVecb]&) except +

        int numberOfIndividuals()
        int numberOfBetas()
        int numberOfRandom()
        int numberOfSigmas()
        int numberOfTaus()

        void set(Map[ConstVecd]&, Map[ConstMatrixd]&, Map[ConstVecd]&) except +
        void empiricalBayesEstimate(Map[Matrixd]) except +

        Vecd beta() except +
        Matrixd omega() except +
        Vecd sigma() except +
        Vecd tau() except +

    cdef cppclass MEDistribution:
        MEDistribution(MEModel &, Distribution &) except +
        int numberOfDimensions()
        Vecd generateInitial()
        double logpdf(Map[ConstVecd]&) except +
        double logpdf(Map[ConstVecd]&, Map[Vecd]) except +
        double logpdf(Map[ConstVecd]&, Map[Vecd], Map[Matrixd]) except +

    ctypedef enum OptimizationState : Init "OptimizationState::Init", Iter "OptimizationState::Iter", Interrupt "OptimizationState::Interrupt", Done "OptimizationState::Done"
    cdef struct OptimizationValues:
        size_t iteration
        Vecd x
        double value
        double gradNorm
    ctypedef bool (*OptimizationTraceFunc)(OptimizationState state, OptimizationValues & vals, void *userdata)

    ctypedef bool (*MEOptimizationTraceFunc)(OptimizationState state, size_t iteration, const MEModel & model, void *userdata)

    cdef cppclass OptimizationTraceCallback:
        pass
    cdef OptimizationTraceCallback make_optimization_trace_cb(OptimizationTraceFunc, void*)

    cdef cppclass MEOptimizationTraceCallback:
        pass
    cdef MEOptimizationTraceCallback make_meoptimization_trace_cb(MEOptimizationTraceFunc, void*)

    cdef cppclass BFGS_Options "BFGS::Options":
        double xTol
        double fTol
        double gradTol
        int m
        int maxiters
        OptimizationTraceCallback trace

    cdef cppclass BFGS:
        BFGS(Function &, Map[ConstVecd] &, const BFGS_Options &) except +
        int optimize() except +
        bool isConverged()
        Vecd optimum()
        double value()

    cdef cppclass PSO_Options "ParticleSwarmOptimization::Options":
        size_t numParticles
        size_t numEpochs
        double c1
        double c2
        double w_start
        double w_end
        double w_varyfor
        double V_max
        OptimizationTraceCallback trace

    cdef cppclass ParticleSwarmOptimization:
        ParticleSwarmOptimization(Function &, const PSO_Options &) except +
        void optimize(Map[ConstVecd] &, double) except +
        Vecd optimum()
        double value()

    cdef cppclass NelderMead_Options "NelderMead::Options":
        double fTol
        double alpha
        double beta
        double gamma
        double usualDelta
        double zeroDelta
        int maxiters
        OptimizationTraceCallback trace

    cdef cppclass NelderMead:
        NelderMead(Function &, Map[ConstVecd] &, const NelderMead_Options &) except +
        int optimize() except +
        bool isConverged()
        Vecd optimum()
        double value()

    ctypedef enum PollMethod : Positive2N "PatternSearch::Positive2N", PositiveNp1 "PatternSearch::PositiveNp1", Cross4N "PatternSearch::Cross4N"
    cdef cppclass Pattern_Options "PatternSearch::Options":
        double fTol
        double xTol
        double meshTol
        double initialMeshSize
        double expansion
        double contraction
        double maxMeshSize
        int maxEvals
        int maxIters
        PollMethod method
        OptimizationTraceCallback trace

    cdef cppclass PatternSearch:
        PatternSearch(Function &, Map[ConstVecd] &, const Pattern_Options &) except +
        int optimize() except +
        bool isConverged()
        Vecd optimum()
        double value()
        int numberOfEvals()
        double meshSize()

    cdef cppclass SAEM_Options "SAEM::Options":
        int K1;
        int K2;
        int burnin;
        int thinning;
        MEOptimizationTraceCallback trace
        void validate() except +

    cdef cppclass SAEM:
        SAEM(MEModel&, SAEM_Options&) except +
        void optimize(Random&) except +

cdef extern from "<utility>" namespace "std" nogil:
    cdef shared_ptr[LikelihoodModel] move(shared_ptr[LikelihoodModel])
    cdef shared_ptr[StructuralModel] move(shared_ptr[StructuralModel])
    cdef shared_ptr[Distribution] move(shared_ptr[Distribution])
    cdef unique_ptr[DistributionSampler] move(unique_ptr[DistributionSampler])
    cdef Individual move(Individual)

cdef extern from "MCMC.h" namespace "mcmc":
    cdef struct SamplingValues "mcmc::SamplingValues":
        size_t iteration
        Vecd x
        double value
    ctypedef size_t (*SamplingTraceFunc)(SamplingValues & vals, void *userdata)

    cdef cppclass SamplingTraceCallback:
        pass
    cdef SamplingTraceCallback make_sampling_trace_cb(SamplingTraceFunc, void*)

    cdef cppclass SamplerFactory "mcmc::SamplerFactory":
        @staticmethod
        SamplerFactory & instance()
        unique_ptr[Sampler] createSampler(const string &, const shared_ptr[Distribution]&, const PyDict &) except +

    cdef cppclass Sampler "mcmc::Sampler" (DistributionSampler):
        void reset(Map[ConstVecd]&) except +
        void warmup(Random &, size_t) except +
        void setTrace(SamplingTraceCallback trace) except +

cdef extern from "<utility>" namespace "std" nogil:
    cdef unique_ptr[Sampler] move(unique_ptr[Sampler])
