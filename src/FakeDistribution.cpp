/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <FakeDistribution.hpp>
#include <Dict.hpp>
#include <ModelFactory.hpp>

static auto create_weights(const Dict &d) {
	auto v = d.getNumericVector("weights");
	return std::vector<double>(begin(v), end(v));
}

FakeDistribution::FakeDistribution(const Dict &d) : ZMMvNDistribution(d), weights_(create_weights(d)) {
	multiplier = d.getIntegerDefault("multiplier", 1);
}

void FakeDistribution::waitRandomly(ConstRefVecd &x) const {
	union v {
		double d;
		uint64_t i;
	};
	v v;
	v.d = x.sum();
	Random r(v.i);
	int pos = stats::UniformDiscrete::sample(r, 0, weights_.size() - 1);

	auto duration = weights_[pos];
	volatile size_t remaining = duration * multiplier;
	while (remaining > 0) remaining--;
}

REGISTER_DISTRIBUTION(Fake, FakeDistribution);
