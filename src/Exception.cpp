/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define __STDC_WANT_LIB_EXT1__ 1
#include "Exception.hpp"
#include "scope_exit.hpp"
#include <string>
#include <cstring>

#if (_POSIX_C_SOURCE >= 200112L || _XOPEN_SOURCE >= 600) && ! _GNU_SOURCE
#	define STRERROR_XSI
#endif

std::string errnoStr(int err) {
	int savedErrno = errno;
	auto guard = on_scope_exit([savedErrno]{
		errno = savedErrno;
	});

	char buf[1024];
	buf[0] = '\0';

	std::string str;

#if defined(__STDC_LIB_EXT1__) || defined(_MSC_VER) || defined(STRERROR_XSI)
#if defined(__STDC_LIB_EXT1__) || defined(_MSC_VER)
	int r = strerror_s(buf, sizeof(buf), err);
#else
	int r = strerror_r(err, buf, sizeof(buf));
#endif

	if (r != 0) {
		str = stringAppend("Unknown error ", err, " (strerror_r failed with error ", errno, ")");
	} else {
		str.assign(buf);
	}
#else
	str.assign( strerror_r(err, buf, sizeof(buf)) );
#endif

	return buf;
}
