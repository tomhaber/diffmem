/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <MEModel.hpp>
#include <ConditionalMEDistribution.hpp>
#include <Function.hpp>
#include <NelderMead.hpp>
#include <BFGS.hpp>
#include <LocalMatrix.hpp>
#include <tbb/blocked_range.h>
#include <tbb/parallel_reduce.h>
#include <tbb/parallel_for.h>
#include "math/number.hpp"
#include "stats/mvnormal.hpp"

static void maskVector(const Vecd & x, const Vecb & mask, Vecd & xMasked) {
	for(int i = 0; i < x.size(); i++)
		xMasked[i] = ( mask[i] ) ? x[i] : 0.0;
}

static void maskMatrix(const Matrixd & omega, const Matrixb & mask, Matrixd & omegaMasked) {
	for(int i = 0; i < omega.rows(); i++)
		for(int j = 0; j < omega.cols(); j++)
			omegaMasked(i, j) = ( mask(i, j) ) ? omega(i, j) : 0.0;
}

MEModel::MEModel(const Population & population, const Vecd & beta, const Vecb & estimated,
		const Matrixd & omega, const Matrixb & cov_model, const Vecd & sigma, const Vecb & sigma_model)
	: beta_(beta), omega_(omega), sigma_(sigma), population(population), covariance_model(cov_model), sigma_model(sigma_model) {

	finalize(estimated);
}

void MEModel::finalize(const Vecb & estimated) {
	Require( population.numberOfIndividuals() > 1, "Mixed Effect Models should contain more than 1 subject" );
	Require( beta_.size() == estimated.size(), "estimated vector should have same length as beta" );

	int nrandom = population.numberOfRandom();
	int nbetas = population.numberOfBetas();

	Require( nbetas == beta_.size(), "beta vector has wrong size" );
	Require( nrandom == covariance_model.rows()
			&& nrandom == covariance_model.cols(), "covariance model should be of size nrandom x nrandom" );

	Require( population.numberOfSigmas() == sigma_model.size(), "sigma model should be of correct size" );
	tau_.resize( population.numberOfTaus() );
	population.transtau(sigma_, tau_);

	computeMasks(estimated);
}

void MEModel::set(const Vecd & beta, const Matrixd & omega, const Vecd & sigma) {
	beta_ = beta;
	omega_ = omega;

	sigma_ = sigma;
	population.transtau(sigma_, tau_);
}

Vecd MEModel::betaMasked() const {
	Vecd beta(nfixed + nrandom);
	int idx = 0;
	for(int i = 0; i < beta_.size(); ++i)
		if(fixed[i] || random[i])
			beta[idx++] = beta_[i];
	return beta;
}

void MEModel::computeMasks(const Vecb & estimated) {
	int nbetas = population.numberOfBetas();
	Matrixd sumXf = Matrixd::Zero( nbetas, nbetas );
	for(int i = 0; i < numberOfIndividuals(); ++i) {
		const Matrixd & xf = population.subject(i).betaToRandomXf();
		sumXf.noalias() += xf.transpose() * xf;
	}

	fixed.resize( beta_.size() );
	random.resize( beta_.size() );
	for(int i = 0; i < beta_.size(); i++) {
		if( estimated[i] ) {
			const bool estimatable = (sumXf.col(i).sum() > 1e-16);
			random[i] = estimatable;
			fixed[i] = ! estimatable;
		} else {
			random[i] = fixed[i] = false;
		}
	}

	nfixed = fixed.count();
	nrandom = random.count();
}

class BetaFunction : public Function {
	public:
		BetaFunction(const MEModel & model, const Vecd & beta, const Matrixd & eta, const Vecb & mask)
			: model(model), beta_orig(beta), eta(eta), mask(mask) {
		}

	public:
		double operator()(ConstRefVecd & x) const override {
			Vecd beta( mask.size() );

			int idx = 0;
			for(int i = 0; i < beta.size(); ++i)
				beta[i] = mask[i] ? x[idx++] : beta_orig[i];

			return -model.evaluateLikelihood(beta, eta);
		}

	private:
		const MEModel & model;
		const Vecd & beta_orig;
		const Matrixd & eta;
		const Vecb & mask;
};

double MEModel::evaluateLikelihood(ConstRefVecd & betaMasked, ConstRefMatrixd & etas) const {
	LOCAL_VECTOR(Vecd, beta, beta_.size());
	int idx = 0;
	for(int i = 0; i < beta.size(); ++i)
		beta[i] = (fixed[i] || random[i]) ? betaMasked[idx++] : beta_[i];

	const int N = numberOfIndividuals();
	return tbb::parallel_reduce(
		tbb::blocked_range<int>(0, N),
		0.,
		[&](const tbb::blocked_range<int> & r, double ll)-> double {
			for(int id = r.begin(); id != r.end(); ++id) {
				const Individual & ind = population.subject(id);
				LOCAL_VECTOR(Vecd, phi, ind.numberOfParameters());
				ind.toParameter(beta, etas.col(id), phi);
				ll += ind.logpdf(phi, tau_);
			}
			return ll;
		},
		std::plus<double>()
	);
}

double MEModel::evaluateLikelihood(ConstRefVecd & betaMasked, ConstRefMatrixd & etas, Cholesky<Matrixd> & chol_omega, ConstRefVecd & tau) const {
	LOCAL_VECTOR(Vecd, beta, beta_.size());
	int idx = 0;
	for(int i = 0; i < beta.size(); ++i)
		beta[i] = (fixed[i] || random[i]) ? betaMasked[idx++] : beta_[i];

	const int N = numberOfIndividuals();
	return tbb::parallel_reduce(
		tbb::blocked_range<int>(0, N),
		0.,
		[&](const tbb::blocked_range<int> & r, double ll)-> double {
			for(int id = r.begin(); id != r.end(); ++id) {
				const Individual & ind = population.subject(id);
				LOCAL_VECTOR(Vecd, phi, ind.numberOfParameters());
				ind.toParameter(beta, etas.col(id), phi);
				ll += ind.logpdf(phi, tau) + stats::MVNormal::logpdf<true>(etas.col(id), chol_omega);
			}
			return ll;
		},
		std::plus<double>()
	) + N * stats::MVNormal::logproportions(chol_omega);
}


void MEModel::solveBeta(const Matrixd & S, const Matrixd & eta, double stepsize) {
	// Optimize for beta's without random effects
	if( nfixed != 0 )
		estimateBetaFixed(eta, stepsize);

	// Solve for beta's (given omega) with random effects
	if( nrandom != 0 )
		estimateBetaRandom(S);
}

void MEModel::estimateBetaFixed(const Matrixd & eta, double stepsize) {
	BetaFunction func(*this, beta_, eta, fixed);

	Vecd x;
	sliceRows(beta_, fixed, x);

	NelderMead::Options opts;
	opts.maxiters = 100;
	NelderMead opt(func, x, opts);
	opt.optimize();
	// check opt.isConverged()? solution does not have to be optimal, only better

	int idx = 0;
	for(int i = 0; i < beta_.size(); ++i) {
    if( fixed[i] )
      beta_[i] = (1 - stepsize) * beta_[i] + stepsize * opt.optimum()[idx++];
	}

//	Vecd tmp = beta_;
//	sliceIntoRows(opt.optimum(), fixed, tmp);
//	beta_ = (1 - stepsize) * beta_ + stepsize * tmp;
}

class IndividualLikelihoodFunction : public Function {
	public:
		template <typename BT, typename ST>
		IndividualLikelihoodFunction(ZMMvNDistribution & mvn,
				const Individual & subject, const MatrixBase<BT> & beta, const MatrixBase<ST> & tau)
			: ll(subject, beta, tau), mvn(mvn) {}

	public:
		double operator()(ConstRefVecd & x) const override {
			return -(ll.logpdf(x) + mvn.logpdf(x));
		}

		double operator()(ConstRefVecd & x, RefVecd grad) const override {
			double lp = 0.0;
			LOCAL_VECTOR(Vecd, grad_prior, x.size());
			lp += mvn.logpdf(x, grad_prior);

			lp += ll.logpdf(x, grad);
			grad = -(grad + grad_prior);
			return -lp;
		}

	private:
		IndividualLikelihoodEta ll;
		const ZMMvNDistribution & mvn;
};

void MEModel::empiricalBayesEstimate(RefMatrixd etas) const {
	ZMMvNDistribution mvn(omega_);
	Require(etas.rows() == omega_.cols(), "etas has wrong number of rows:", etas.rows(), "instead of", omega_.cols());
	Require(etas.cols() == numberOfIndividuals(), "etas has wrong number of columns:", etas.cols(), "instead of", numberOfIndividuals());

	tbb::parallel_for(
		tbb::blocked_range<int>(0, numberOfIndividuals()),
		[&](const tbb::blocked_range<int> & r) {
			LOCAL_VECTOR(Vecd, phi, population.numberOfParameters());
			for(int id = r.begin(); id != r.end(); ++id) {
				const Individual & ind = population.subject(id);
				IndividualLikelihoodFunction indll(mvn, ind, beta_, tau_);

				auto eta = etas.col(id);
				eta.setZero();

				BFGS bfgs(indll, eta);
				bfgs.optimize();
				Assert(bfgs.isConverged(), "empiricalBayesEstimate: more bfgs iterations needed!");
				if( ! bfgs.isConverged() )
					eta.fill( math::NA() );
				else
					eta = bfgs.optimum();
			}
		}
	);
}

void MEModel::updateSigma(const Vecd & err) {
	population.updateTau(tau_, err, population.numberOfMeasurements(), 0.001);

	population.transsigma(tau_, sigma_);
	{
		for(int i = 0; i < sigma_.size(); i++)
			sigma_(i) = math::cutoff( sigma_(i) );
		maskVector(sigma_, sigma_model, sigma_);
	}
	population.transtau(sigma_, tau_);
}

void MEModel::updateOmega(const Matrixd & S0, const Matrixd & S1, const Matrixd & mu) {
	Matrixd tmp = (S1 + (mu*mu.transpose()) - 2.0 * (mu * S0.transpose())) / double(numberOfIndividuals());

	for(int i = 0; i < tmp.rows(); i++)
		tmp(i,i) = math::cutoff( tmp(i,i) );
	maskMatrix(tmp, covariance_model, tmp);

	omega_.compute(tmp);
}

void MEModel::estimateBetaRandom(const Matrixd & S) {
	Vecd sumBeta = Vecd::Zero( nrandom );
	Matrixd sumXf = Matrixd::Zero( nrandom, nrandom );

	Matrixd omega_inv;
	chol_inv(omega_, omega_inv);

	Matrixd xf( numberOfRandom(), nrandom );
	for(int i = 0; i < numberOfIndividuals(); ++i) {
		sliceCols( population.subject(i).betaToRandomXf(), random, xf );

		sumBeta.noalias() += xf.transpose() * omega_inv * S.col(i);
		sumXf.noalias() += xf.transpose() * omega_inv * xf;
	}

	sumBeta = sumXf.colPivHouseholderQr().solve(sumBeta);
	sliceIntoRows(sumBeta, random, beta_);
}

void MEModel::initializeMu(Matrixd & mu) {
	mu.resize( numberOfRandom(), numberOfIndividuals() );
	for(int i = 0; i < numberOfIndividuals(); ++i)
		mu.col(i) = population.subject(i).betaToRandomXf() * beta_;
}

void MEModel::updateMuEta(const Matrixd & BInvPhi, Matrixd & mu, Matrixd & eta) {
	for(int i = 0; i < numberOfIndividuals(); ++i) {
		mu.col(i) = population.subject(i).betaToRandomXf() * beta_;
		eta.col(i) = BInvPhi.col(i) - mu.col(i);
	}
}

void MEModel::updateEta(ConstRefVecd & old_beta, ConstRefVecd & new_beta, Matrixd & eta) const {
	Assert( old_beta.size() == new_beta.size() );

	LOCAL_VECTOR(Vecd, delta_beta, beta_.size());
	int idx = 0;
	for(int i = 0; i < delta_beta.size(); ++i) {
		if(fixed[i] || random[i]) {
			delta_beta[i] = (new_beta[idx] - old_beta[idx]);
			idx++;
		} else
			delta_beta[i] = beta_[i];
	}

	for(int i = 0; i < numberOfIndividuals(); ++i)
		eta.col(i) -= population.subject(i).betaToRandomXf() * delta_beta;
}

std::unique_ptr<ConditionalMEDistribution> MEModel::createCondDistribution() const {
	return std::make_unique<ConditionalMEDistribution>(population, beta_, omega_, tau_);
}

void MEModel::updateCondDistribution(ConditionalMEDistribution *distribution) const {
	distribution->update(beta_, omega_, tau_);
}

void MEModel::updateCondDistribution(ConditionalMEDistribution *distribution, ConstRefVecd &betaMasked,
		Cholesky<Matrixd> &chol_omega, ConstRefVecd &tau) const {

	LOCAL_VECTOR(Vecd, beta, beta_.size());
	int idx = 0;
	for(int i = 0; i < beta.size(); ++i)
		beta[i] = (fixed[i] || random[i]) ? betaMasked[idx++] : beta_[i];

	distribution->update(beta, chol_omega, tau);
}
