/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "BFGS.hpp"
#include "math/number.hpp"

static double maxdiff(const Vecd & x, const Vecd & y) {
	Assert(x.size() == y.size(), "Vectors should have same length");

	double res = 0.0;
	for(int i = 0; i < x.size(); ++i) {
		const double d = std::abs(x[i] - y[i]);
		if( d > res ) res = d;
	}

	return res;
}

static double normInf(const Vecd & x) {
	double res = 0.0;
	for(int i = 0; i < x.size(); ++i) {
		const double d = std::abs(x[i]);
		if( d > res ) res = d;
	}

	return res;
}

BFGSstate::BFGSstate(int dim, int m) {
	dx = Matrixd::Zero(dim, m);
	dgrad = Matrixd::Zero(dim, m);
	alpha = Vecd::Zero(m);
	rho = Vecd::Zero(m);
	numOfIter = 0;
}

// q = Hk * grad
void BFGSstate::multH(const Vecd & grad, Vecd & q) {
	q = grad;

	if(numOfIter == 0) //H = I
		return;

	const int m = dx.cols();
	const int bound = numOfIter < m ? numOfIter - 1 : m - 1;
	int i = numOfIter % m;

	int count = 0;
	do {
		i = (i - 1 + m) % m;
		alpha[i] = rho[i] * dot(dx.col(i), q);
		q -= alpha[i] * dgrad.col(i);
	} while(++count < bound);

	// q = H0k * q;

	i = numOfIter < m ? 0 : numOfIter % m;
	count = 0;
	do {
		i = (i + 1) % m;
		const double beta = rho[i] * dot(dgrad.col(i), q);
		q += (alpha[i] - beta) * dx.col(i);
	} while(++count < bound);

	// q = Hk * gr
}

void BFGSstate::estimateH(Matrixd &H) const {
	// S = dx and Y = dgrad
	const int m = dx.cols();
	const int n = dx.rows();

	Matrixd R(m, m);
	for(size_t k = 0; k < m; ++k) {
		for(size_t l = k; l < m; ++l)
			R(k, l) = dot(dx.col(k), dgrad.col(l));
	}

	Matrixd RinvS = R.triangularView<Eigen::Upper>().solve(dx.transpose());
	Matrixd D = R.diagonal().asDiagonal();
	D.noalias() += dgrad.transpose()*dgrad;
	H.noalias() = Matrixd::Identity(n, n) + RinvS.transpose() * D * RinvS - dgrad * RinvS - (dgrad * RinvS).transpose();
}

BFGS::BFGS(const Function & func, ConstRefVecd & xInit, const Options & options)
	: func(func), options(options), state(xInit.size(), options.m) {

	x = xInit;
	grad = Vecd::Zero(x.size());
	fVal = func(x, grad);

	gradOld = grad;
	xOld = Vecd::Zero(x.size());
	fValOld = math::NaN();

	xLoc = gradLoc = Vecd::Zero(x.size());
}

int BFGS::optimize() {
	int i = 0;
	double gradNorm;

	if( options.trace ) {
		gradNorm = grad.norm();
		options.trace(OptimizationState::Init, OptimizationValues(i, x, fVal, gradNorm));
	}

	for(; i < options.maxiters; i++ ) {
		if( options.trace && options.trace(OptimizationState::Interrupt, OptimizationValues(i, x, fVal, gradNorm)) )
			break;

		if( step() )
			break;

		if( options.trace ) {
			gradNorm = grad.norm();
			options.trace(OptimizationState::Iter, OptimizationValues(i, x, fVal, gradNorm));
		}
	}

	if( options.trace )
		options.trace(OptimizationState::Done, OptimizationValues(i, x, fVal, gradNorm));
	return i;
}

bool BFGS::step() {
	// Determine the L-BFGS search direction
	state.multH(grad, descDir);

	// Initial alpha
	const double alphaInit = 1.0 / grad.norm();

	// Determine the distance of movement along the search line
	const double alpha = linesearch(descDir, alphaInit);

	// Maintain a record of previous position
	xOld = x;
	gradOld = grad;
	fValOld = fVal;

	// Update current position
	x -= alpha * descDir;
	fVal = func(x, grad);

	state.push(x, grad, xOld, gradOld);

	return isConverged();
}

double BFGS::linesearch(const Vecd & dd, double alphaInit) {
	const double c1 = 1e-4;
	const double c2 = 0.9;

	double ddProj = -grad.dot(dd);
	double ddProjLoc;

	double alpha = alphaInit;
	for(size_t iter = 0; iter < 200; ++iter) {
		// new guess for phi(alpha)
		xLoc = x - alpha * dd;
		const double fValLoc = func(xLoc);

		// decrease condition invalid --> shrink interval
		if( fValLoc	 > fVal + c1 * alpha * ddProj ) {
			alpha *= 0.5;
		} else {
			// valid decrease --> test strong wolfe condition
			func(xLoc, gradLoc);
			ddProjLoc = -dot(dd, gradLoc);

			// curvature condition invalid ?
			if( ddProjLoc < c2 * ddProj ) {
				// increase interval
				alpha *= 2.1;
			} else {
				// both condition are valid
				break;
			}
		}
	}

	return alpha;
}

bool BFGS::isConverged() const {
	if(maxdiff(x, xOld) < options.xTol)
		return true;

	// Absolute Tolerance
	//if( std::abs( fVal - fValOld ) < options.ftol )
	if(std::abs(fVal - fValOld) / (std::abs(fVal) + options.fTol) < options.fTol)
		return true;

#if 0
	if( normInf(grad) < options.gradtol )
#else
	if(grad.norm() < options.gradTol)
#endif
		return true;

	return false;
}
