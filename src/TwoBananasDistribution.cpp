/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <TwoBananasDistribution.hpp>
#include <Dict.hpp>
#include <ModelFactory.hpp>

TwoBananasDistribution::TwoBananasDistribution() {
	x_ = make_vector<double>({
		0.0000000, 0.2105263, 0.4210526, 0.6315789,
		0.8421053, 1.0526316, 1.2631579,  1.4736842,
		1.6842105, 1.8947368, 2.1052632, 2.3157895,
		2.5263158, 2.7368421,  2.9473684, 3.1578947,
		3.3684211, 3.5789474, 3.7894737, 4.0000000
	});

	y_ = make_vector<double>({
		0.00000000, 0.04123115, 0.08076229, 0.11866351,
		0.15500203, 0.18984227,  0.22324600, 0.25527246,
		0.28597843, 0.31541836, 0.34364444, 0.37070674,
		0.39665322, 0.42152990, 0.44538089, 0.46824847,
		0.49017320, 0.51119394,  0.53134797, 0.55067104
	});
}

TwoBananasDistribution::TwoBananasDistribution(const Dict &d) : TwoBananasDistribution() {
	if(d.exists("x"))
		x_ = d.getNumericVector("x");

	if(d.exists("y"))
		y_ = d.getNumericVector("y");
}

REGISTER_DISTRIBUTION(TwoBananas, TwoBananasDistribution);
