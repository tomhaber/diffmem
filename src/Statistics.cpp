/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable: 4267 )
#endif

#include "Statistics.hpp"
#include <unsupported/Eigen/FFT>

double Statistics::mean(const Vecd & y) {
	return y.sum() / y.size();
}

double Statistics::variance(const Vecd & y) {
	if(y.size() == 1)
		return 0.0;

	const double mu(mean(y));
	double sum_sq_diff = 0.0;
	for(int i = 0; i < y.size(); ++i)
		sum_sq_diff += (y[i] - mu) * (y[i] - mu);
	return sum_sq_diff / (y.size() - 1);
}

double Statistics::sd(const Vecd &y) {
	if( y.size() == 1 )
		return 0.0;

	return sqrt( variance(y) );
}

/// Round up to next higher power of 2 (return x if it's already a power
/// of 2).
static inline int nextpow2(int x) {
	if(x < 0)
		return 0;

	--x;
	x |= x >> 1;
	x |= x >> 2;
	x |= x >> 4;
	x |= x >> 8;
	x |= x >> 16;
	return x + 1;
}

/**
 * Write autocorrelation estimates for every lag for the specified
 * input sequence into the specified result using the specified
 * FFT engine.  The return vector be resized to the same length as
 * the input sequence with lags given by array index.
 *
 * The implementation involves a fast Fourier transform,
 * followed by a normalization, followed by an inverse transform.
 */
void Statistics::autocorrelation(const Vecd & y, Vecd & ac) {
	Eigen::FFT<double> fft;

	const int N = y.size();
	const int M = nextpow2(N);
	const int nfft = 2 * M;

	// centered_signal = y - mean(y) padded with zeroes
	Eigen::VectorXd centered(nfft);
	centered.setZero(nfft);
	centered.head(N) = y.array() - Statistics::mean(y);

	Eigen::VectorXcd freqvec;
	fft.fwd(freqvec, centered);
	freqvec = freqvec.cwiseProduct( freqvec.conjugate() );

	fft.inv(centered, freqvec);
	ac = centered.head(N).array() / centered[0];
}

void Statistics::autocovariance(const Vecd &y, Vecd &acov) {
	Statistics::autocorrelation(y, acov);

	const double var = Statistics::variance(y) * (y.size() - 1) / y.size();
	for(int i = 0; i < y.size(); i++)
		acov[i] *= var;
}

double Statistics::effectiveSampleSize(const Vecd & y, size_t nSamplesKept) {
	Vecd acov;
	autocovariance(y, acov);

	int nSamples = y.size();
	if(nSamplesKept == -1)
		nSamplesKept = nSamples;

	double var = acov[0] * nSamplesKept / (nSamplesKept - 1);
	double var_plus = var * (nSamples - 1) / nSamples;

	double rho_hat = 0.0;
	double rho_hat_sum = 0.0;

	int t;
	for(t = 1; (t < nSamples && rho_hat >= 0); t++) {
		rho_hat = 1 - (var - acov[t]) / var_plus;
		if(rho_hat >= 0.0)
			rho_hat_sum += rho_hat;
	}
	t = t - 1;

	double ess = nSamples;
	if(t > 1) {
		ess /= 1 + 2 * rho_hat_sum;
	}

	return ess;
}

#ifdef _MSC_VER
#pragma warning( pop )
#endif
