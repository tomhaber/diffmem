/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "NelderMead.hpp"

NelderMead::NelderMead(const Function & func, ConstRefVecd & xInit, const Options & options)
	: func(func), options(options) {

	state.init(xInit, func, options.usualDelta, options.zeroDelta);
	state.getBest(&x, &fVal);
}

int NelderMead::optimize() {
	int i = 0;

	if( options.trace )
		options.trace(OptimizationState::Init, OptimizationValues(i, x, fVal));

	for( ; i < options.maxiters; ++i ) {
		if( options.trace && options.trace(OptimizationState::Interrupt, OptimizationValues(i, x, fVal)) )
			break;

		if( step() )
			break;

		if( options.trace )
			options.trace(OptimizationState::Iter, OptimizationValues(i, x, fVal));
	}

	if( options.trace )
		options.trace(OptimizationState::Done, OptimizationValues(i, x, fVal));
	return i;
}

void NelderMeadState::init(ConstRefVecd & x, const Function & func, double usualDelta, double zeroDelta) {
	const int dimension = x.size();
	points.resize(dimension, dimension+1);
	values.resize(dimension+1);

	points.col(0) = x;
    values[0] = func(x);

    for(int k = 1; k <= dimension; ++k) {
		points.col(k) = x;
		points(k - 1, k) +=
				(x[k - 1] != 0.0) ?
						(usualDelta * x[k - 1]) : zeroDelta;

		values[k] = func( points.col(k) );
	}

    centroid = points.rowwise().sum();
    updateIndices();
}

void NelderMeadState::updateIndices() {
	if( values[0] > values[1] ) {
		worst = 0;
		near_worst = 1;
	} else {
		worst = 1;
		near_worst = 0;
	}

	best = near_worst;
	for(int i = 2; i < values.size(); ++i) {
		if( values[i] < values[best] ) {
			best = i;
		}

		if( values[i] > values[worst] ) {
			near_worst = worst;
			worst = i;
		} else if( values[i] > values[near_worst] ) {
			near_worst = i;
		}
	}
}

double NelderMeadState::tryMove(const Function & func, double fac) {
	const double fac1 = (1.0f - fac) / points.rows();
	const double fac2 = fac1 - fac;

	tmp = fac1 * centroid - fac2 * points.col(worst);
	double val = func(tmp);
	if( val < values[worst] ) {
		values[worst] = val;
		centroid += tmp;
		centroid -= points.col(worst);
		points.col(worst) = tmp;
	}

	return val;
}

void NelderMeadState::shrink(const Function & func) {
	points = ((points.colwise() - points.col(best)) / 2).colwise() + points.col(best);
	for(int i = 0; i < points.cols(); ++i)
		values[i] = func( points.col(i) );

	centroid = points.rowwise().sum();
}

bool NelderMead::step() {
	const double fWorst = state.getWorst();
	const double fBest = state.getBest();
	const double fNearWorst = state.getNearWorst();

	// Compute a reflection
	double y_try = state.tryMove(func, options.alpha);
	if( y_try < fBest ) {
		// Compute an expansion
		state.tryMove(func, options.gamma);
	} else if( y_try > fNearWorst ) {
		// New point is not better than near worst
		y_try = state.tryMove(func, options.beta);

		if( y_try > fWorst )
			state.shrink(func);
	}

	state.updateIndices();
	state.getBest(&x, &fVal);

	return isConverged();
}

bool NelderMead::isConverged() const {
	const double fWorst = state.getWorst();
	const double fBest = state.getBest();
	return ( 2.0*std::abs(fWorst - fBest) / (std::abs(fWorst) + std::abs(fBest) + options.fTol) < options.fTol );
}
