/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "PointwiseLikelihoodModel.hpp"

double PointwiseLikelihoodModel::logpdf(ConstRefVecd &tau, ConstRefMatrixd &u) const {
	double ll = logProportions(tau);
	for(int i = 0; i < u.cols(); ++i) {
		ll += logProportionalPdf(tau, i, u.col(i));
	}
	return ll;
}

double PointwiseLikelihoodModel::logpdfGradTau(ConstRefVecd &tau, ConstRefMatrixd &u, RefVecd gradTau) const {
	Vecd tmp(tau.size());
	gradTau = Vecd::Zero(tau.size());

	double ll = logProportions(tau);
	for(int i = 0; i < u.cols(); ++i) {
		ll += logpdfGradTau(tau, i, u.col(i), tmp);
		gradTau += tmp;
	}
	return ll;
}

void PointwiseLikelihoodModel::logpdfGrad(ConstRefVecd &tau, ConstRefMatrixd &u, ConstRefMatrixd &s,
		RefVecd grad) const {
	const int n = u.rows();
	const int numberOfSensitivities = n * grad.size();
	Vecd gradU(n);

	grad.setZero();
	for(int i = 0; i < u.cols(); ++i) {
		auto si = s.block(0, i, numberOfSensitivities, 1);
		logpdfGradU(tau, i, u, gradU);
		grad.noalias() += ConstMapMatrixd(si.data(), n, grad.size()).transpose() * gradU;
	}
}

void PointwiseLikelihoodModel::logpdfHess(ConstRefVecd &tau, ConstRefMatrixd &u, ConstRefMatrixd &s,
		RefVecd grad, RefMatrixd hess) const {
	const int n = u.rows();
	const int numberOfSensitivities = n * grad.size();
	Vecd gradU(n);
	Matrixd hessU(n, n);

	grad.setZero();
	hess.setZero();
	for(int i = 0; i < u.cols(); ++i) {
		auto si = s.block(0, i, numberOfSensitivities, 1);
		logpdfHessU(tau, i, u, gradU, hessU);

		auto z = ConstMapMatrixd(si.data(), n, grad.size());
		grad.noalias() += z.transpose() * gradU;
		hess.noalias() += z.transpose() * hessU * z;
	}
}
