/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <Distribution.hpp>
#include <FiniteDifference.hpp>
#include <mcmc/SamplerFactory.hpp>
#include <Dict.hpp>

double Distribution::logpdf(ConstRefVecd & x, RefVecd grad) const {
	FiniteDifference::computeGradient(
		[this](ConstRefVecd & x) {
			return logpdf(x);
		}, x, grad, 1e-6, 1e-8);
	return logpdf(x);
}

double Distribution::logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const {
	auto f = [this](ConstRefVecd &x) {
		return logpdf(x);
	};

	// XXX could be merged into one FD
	FiniteDifference::computeGradient(f, x, grad, 1e-6, 1e-8);
	FiniteDifference::computeHessian(f, x, H, 2, 1e-6, 1e-8);
	return logpdf(x);
}

std::unique_ptr<DistributionSampler> Distribution::sampler(const Dict &dict) const {
	std::string type = dict.getString("type");
	return mcmc::SamplerFactory::instance().createSampler(type, shared_from_this(), dict);
}
