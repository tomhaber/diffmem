/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "NormalDistribution.hpp"
#include "DistributionSampler.hpp"
#include "Random.hpp"
#include "stats/mvnormal.hpp"
#include "ModelFactory.hpp"
#include "Dict.hpp"

double MvNDistribution::logpdf(ConstRefVecd & x) const {
	return stats::MVNormal::logpdf<true>(x, mu, chol_omega);
}

double MvNDistribution::logpdf(ConstRefVecd & x, RefVecd grad) const {
	stats::MVNormal::logpdf_dx(x, mu, chol_omega, grad);
	return 0.5 * (x - mu).transpose() * grad;
}

double MvNDistribution::logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const {
	chol_inv(chol_omega, H);
	H = -H;
	return MvNDistribution::logpdf(x, grad);
}

void MvNDistribution::sample(Random & rng, RefVecd s) const {
	stats::MVNormal::sample(rng, mu, chol_omega, s);
}

std::unique_ptr<DistributionSampler> MvNDistribution::sampler(const Dict &) const {
	return make_basic_sampler(shared_from_base<const MvNDistribution>());
}

void MvNDistribution::set(const Vecd & mu, const Matrixd & omega) {
	Require(mu.size() == omega.cols(),
			"Mismatch between size of mu ({}) and omega ({})\n", mu.size(), omega.cols());
	Require(omega.rows() == omega.cols(),
			"Omega is not square ({}, {})\n", omega.rows(), omega.cols());
	this->mu = mu;
	chol_omega.compute( omega );
	Require( chol_omega.info() == Eigen::Success, "Cholesky decomposition of omega failed" );
}

double ZMMvNDistribution::logpdf(ConstRefVecd & x) const {
	return stats::MVNormal::logpdf<true>(x, chol_omega);
}

double ZMMvNDistribution::logpdf(ConstRefVecd & x, RefVecd grad) const {
	stats::MVNormal::logpdf_dx(x, chol_omega, grad);
	return 0.5 * x.transpose() * grad;
}

double ZMMvNDistribution::logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const {
	chol_inv(chol_omega, H);
	H = -H;
	return ZMMvNDistribution::logpdf(x, grad);
}

void ZMMvNDistribution::sample(Random & rng, RefVecd s) const {
	stats::MVNormal::sample(rng, Vecd::Zero(chol_omega.rows()), chol_omega, s);
}

std::unique_ptr<DistributionSampler> ZMMvNDistribution::sampler(const Dict &) const {
	return make_basic_sampler(shared_from_base<const ZMMvNDistribution>());
}

void ZMMvNDistribution::set(ConstRefMatrixd & omega) {
	Require(omega.rows() == omega.cols(),
			"Omega is not square ({}, {})\n", omega.rows(), omega.cols());
	chol_omega.compute( omega );
	Require( chol_omega.info() == Eigen::Success, "Cholesky decomposition of omega failed" );
}

ZMMvNDistribution::ZMMvNDistribution(const Dict &d) {
	set(d.getNumericMatrix("omega"));
}

MvNDistribution::MvNDistribution(const Dict &d) {
	set(d.getNumericVector("mu"), d.getNumericMatrix("omega"));
}

static DistributionType MvN__regged( "mvn",
	[](const Dict & d) -> std::unique_ptr<Distribution> {
		if( d.exists("mu") ) {
			return std::make_unique<MvNDistribution>(d);
		} else {
			return std::make_unique<ZMMvNDistribution>(d);
		}
	}
);
