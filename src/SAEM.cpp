/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "SAEM.hpp"
#include "ConditionalMEDistribution.hpp"
#include "Random.hpp"
#include "DictCpp.hpp"
#include "mcmc/ConditionalMESampler.hpp"
#include <tbb/parallel_for.h>
#include <parallel_reduce_sum.hpp>

SAEM::SAEM(MEModel & model, const Options & options)
	: model(model), options(options), N(model.numberOfIndividuals()) {

	S0 = Matrixd::Zero( model.numberOfRandom(), N );
	S1 = Matrixd::Zero( model.numberOfRandom(), model.numberOfRandom() );
	S2 = Vecd::Zero( model.numberOfTaus() );

	distribution = model.createCondDistribution();
	sampler = std::make_unique<mcmc::ConditionalMESampler>(*distribution,
		DictCpp{"type", "MH", "proposal", "MH", "always_adaptive", 1}
	);
}

void SAEM::warmup(Random & rng) {
	int numIterations = options.numberOfWarmupIterations();
	sampler->warmup(rng, numIterations*options.thinning);
}

void SAEM::step(Random & rng, int iteration) {
	MapVecd samples(eta.data(), distribution->numberOfDimensions());
	sampler->sample(rng, samples, options.thinning);

	const double gamma = options.stepsize(iteration);
	if( gamma > 0.0 ) {
		Vecd err = parallel_reduce_sum(0, N,
				[this](int id, Vecd &err_id) {
					err_id.resize(model.numberOfTaus());
					distribution->errorIndividual(id, eta.col(id), err_id);
				},
				Vecd::Zero(model.numberOfTaus()));

		BInvPhi = mu + eta;
		S0 = (1.0 - gamma) * S0 + gamma * BInvPhi;
		S1 = (1.0 - gamma) * S1 + gamma * (BInvPhi * BInvPhi.transpose());
		S2 = (1.0 - gamma) * S2 + gamma * err;

		model.solveBeta(S0, eta, gamma);
		model.updateMuEta(BInvPhi, mu, eta);
		model.updateOmega(S0, S1, mu);
		model.updateSigma(S2);

		/*{
			auto beta = model.beta();
			auto omega = model.omega();
			auto sigma = model.sigma();
			std::cout << "beta = " << beta.transpose() << std::endl;
			std::cout << "omega = " << omega << std::endl;
			std::cout << "sigma = " << sigma << std::endl;
		}*/
	}

	model.updateCondDistribution( distribution.get() );
	sampler->reset(samples);
}

void SAEM::optimize(Random & rng) {
	model.initializeMu(mu);
	eta.setZero(model.numberOfRandom(), N);

	warmup(rng);

	int iter = options.numberOfWarmupIterations();

	if( options.trace )
		options.trace(OptimizationState::Init, iter, model);

	for(; iter < options.numberOfIterations(); iter++ ) {
		if( options.trace && options.trace(OptimizationState::Interrupt, iter, model) )
			break;

		step(rng, iter);

		if( options.trace )
			options.trace(OptimizationState::Iter, iter, model);
	}

	if( options.trace )
		options.trace(OptimizationState::Done, iter, model);
}
