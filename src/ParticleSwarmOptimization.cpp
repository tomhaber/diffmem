/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ParticleSwarmOptimization.hpp"
#include <tbb/parallel_for.h>
#include <tbb/combinable.h>
#include "math/number.hpp"
#include "stats/normal.hpp"
#include "stats/uniform.hpp"

using U = stats::Uniform;
using N = stats::Normal;

ParticleSwarmOptimization::ParticleSwarmOptimization(const Function & problem, Options const & options) : func(problem), options(options) {
	xFitness = math::posInf();
}

void ParticleSwarmOptimization::optimize(ConstRefVecd & lb, ConstRefVecd & ub) {
	initParticles(lb, ub);
	optimize();
}

void ParticleSwarmOptimization::optimize(ConstRefVecd & x, double sigma) {
	initParticles(x, sigma);
	optimize();
}

void ParticleSwarmOptimization::optimize() {
	size_t e = 0;

	if( options.trace )
		options.trace(OptimizationState::Init, OptimizationValues(e, xBest, xFitness));

	double w = options.w_start;
	auto vary_its = static_cast<size_t>( std::floor(options.numEpochs * options.w_varyfor) );
	double w_dec = (options.w_start - options.w_end) / vary_its;

	for(; e < options.numEpochs; ++e) {
		if( options.trace && options.trace(OptimizationState::Interrupt, OptimizationValues(e, xBest, xFitness)) )
			break;

		step();
		update(options.c1, options.c2, w);

		if( e < vary_its )
			w = w - w_dec;

		if( options.trace )
			options.trace(OptimizationState::Iter, OptimizationValues(e, xBest, xFitness));
	}

	if( options.trace )
		options.trace(OptimizationState::Done, OptimizationValues(e, xBest, xFitness));
}

struct Best {
	explicit Best(double fitness, int id = -1) : fitness(fitness), id(id) {}
	void operator <<(const Best & other) {
		if( other.fitness < fitness ) {
			fitness = other.fitness;
			id = other.id;
		}
	}

	double fitness;
	int id;
};

void ParticleSwarmOptimization::step() {
	tbb::combinable<Best> c( [this]{ return Best(xFitness); } );
	tbb::parallel_for(0, static_cast<int>(options.numParticles),
		[this, &c](int id) {
			Particle & p = particles[id];
			double fitness = func( p.x );

			if( fitness < p.fitnessBest ) {
				p.xBest = p.x;
				p.fitnessBest = fitness;

				c.local() << Best(fitness, id);
			}
		}
	);

	Best best = c.combine(
		[]( const Best & x, const Best & y ) {
			return (x.fitness < y.fitness) ? x : y;
		}
	);

	if( best.id != -1 ) {
		xBest = particles[best.id].x;
		xFitness = best.fitness;
	}
}

void ParticleSwarmOptimization::update(double c1, double c2, double w) {
	const int dim = xBest.size();
	const double V_max = options.V_max;

	auto prng = rng.split(options.numParticles);
	tbb::parallel_for(0, static_cast<int>(options.numParticles),
		[&](int id) {
			Random rng_id = prng[id];

			Particle & p = particles[id];
			for(int i = 0; i < dim; ++i) {
				const double r1 = U::sample(rng_id);
				const double r2 = U::sample(rng_id);
				p.v[i] = w * p.v[i] + c1 * r1 * (xBest[i] - p.x[i]) + c2 * r2 * (p.xBest[i] - p.x[i]);
				if( p.v[i] > V_max ) p.v[i] = V_max;
				if( p.v[i] < -V_max ) p.v[i] = -V_max;
			}

			p.x += p.v;
		}
	);
}

void ParticleSwarmOptimization::initParticles(ConstRefVecd & x, double sigma) {
	const int dim = x.size();

	particles.resize(options.numParticles);
	tbb::combinable<Best> c( [this]{ return Best(xFitness); } );

	auto prng = rng.split(options.numParticles);
	tbb::parallel_for(0, static_cast<int>(particles.size()),
		[&](int id) {
			Particle & p = particles[id];
			Random rng_id = prng[id];

			p.x = x + generate_random<Vecd>(N::generator(rng_id, 0.0, sigma), dim);
			p.v = generate_random<Vecd>(U::generator(rng_id, -1, 1), dim);

			double fitness = func( p.x );
			p.fitnessBest = fitness;
			p.xBest = p.x;
			c.local() << Best(fitness, id);
	});

	Best best = c.combine(
		[]( const Best & x, const Best & y ) {
			return (x.fitness < y.fitness) ? x : y;
		}
	);

	Require(best.id != -1, "cannot evaluate function in initial region");
	xBest = particles[best.id].x;
	xFitness = best.fitness;
}

void ParticleSwarmOptimization::initParticles(ConstRefVecd & lb, ConstRefVecd & ub) {
	const int dim = lb.size();
	const int nrandom_per_particle = dim * 2;

	particles.resize(options.numParticles);
	tbb::combinable<Best> c( [this]{ return Best(xFitness); } );

	auto prng = rng.split(options.numParticles);
	tbb::parallel_for(0, static_cast<int>(particles.size()),
		[&](int id) {
			Particle & p = particles[id];
			Random rng_id = prng[id];

			p.x = U::sample<Vecd>(rng_id, lb, ub);
			p.v = generate_random<Vecd>(U::generator(rng_id, -1, 1), dim);

			double fitness = func( p.x );
			p.fitnessBest = fitness;
			p.xBest = p.x;
			c.local() << Best(fitness, id);
	});

	Best best = c.combine(
		[]( const Best & x, const Best & y ) {
			return (x.fitness < y.fitness) ? x : y;
		}
	);

	Require(best.id != -1, "cannot evaluate function in initial region");
	xBest = particles[best.id].x;
	xFitness = best.fitness;
}
