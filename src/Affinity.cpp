/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Affinity.hpp"
#include "Error.hpp"
#include <iostream>
#include <sstream>

constexpr size_t cpuset_nbits(size_t setsize = sizeof(cpu_set_t)) {
	return (8 * (setsize));
}

constexpr size_t cpuset_size(size_t ncpus) {
	return CPU_ALLOC_SIZE(ncpus);
}

Affinity::Affinity() {
	*this = Affinity::active();
}

size_t Affinity::findMaxCPUs() {
	size_t ncpus;

	constexpr size_t kMaxNCPUs = 16*1024;
	for(ncpus = cpuset_nbits(); ncpus < kMaxNCPUs; ncpus *= 2) {
		cpu_set_t *mask = CPU_ALLOC(ncpus);
		if(mask == nullptr)
			throw std::runtime_error("unable to allocate cpu set");

		const size_t setsize = CPU_ALLOC_SIZE(ncpus);
		CPU_ZERO_S(setsize, mask);
		const int err = sched_getaffinity(0, setsize, mask);
		CPU_FREE(mask);

		if(err == 0)
			break;

		if(errno != EINVAL)
			throw std::runtime_error("unknown error occurred calling sched_getaffinity");
	}

	return ncpus;
}

Affinity Affinity::active() {
	cpu_set_t *mask;
	size_t setsize;

	constexpr size_t kMaxNCPUs = 16*1024;
	for(size_t ncpus = cpuset_nbits(); ncpus < kMaxNCPUs; ncpus *= 2) {
		mask = CPU_ALLOC(ncpus);
		if(mask == nullptr)
			throw std::runtime_error("unable to allocate cpu set");

		setsize = CPU_ALLOC_SIZE(ncpus);
		CPU_ZERO_S(setsize, mask);
		const int err = sched_getaffinity(0, setsize, mask);
		if(err == 0)
			break;

		CPU_FREE(mask);
		mask = nullptr;

		if(errno != EINVAL)
			throw std::runtime_error("unknown error occurred calling sched_getaffinity");
	}

	return {mask, setsize};
}

Affinity::Affinity(size_t ncpus) {
	mask = CPU_ALLOC(ncpus);
	if(mask == nullptr)
		throw std::runtime_error("unable to allocate cpu set");

	setsize = CPU_ALLOC_SIZE(ncpus);
	CPU_ZERO_S(setsize, mask);
}

Affinity::Affinity(const std::string & list) {
	*this = Affinity::active();
	fromString(list);
}

Affinity::~Affinity() {
	CPU_FREE(mask);
	mask = nullptr;
}

size_t Affinity::maxcpus() const {
	return cpuset_nbits(setsize);
}

size_t Affinity::numcpus() const {
	return CPU_COUNT_S(setsize, mask);
}

void Affinity::add(size_t id) {
	if(id >= maxcpus())
		throw std::invalid_argument("id out of range");

	CPU_SET_S(id, setsize, mask);
}

bool Affinity::has(size_t id) const {
	return (CPU_ISSET_S(id, setsize, mask) != 0);
}

std::string Affinity::toString() const {
	const size_t ncpus = maxcpus();

	std::stringstream ss;
	bool first = true;
	for(size_t i = 0; i < ncpus; i++) {
		if( has(i) ) {
			size_t length = 0;
			for(size_t j = i + 1; j < ncpus && has(j); j++)
				length++;

			if(!first)
				ss << ",";
			else
				first = false;

			if( length == 0 ) ss << i;
			else if( length == 1) ss << i << "," << i+1;
			else ss << i << "-" << i+length;

			i = i + length;
		}
	}

	return ss.str();
}

void Affinity::clear() {
	CPU_ZERO_S(setsize, mask);
}

void Affinity::fromString(const std::string &s) {
	clear();

	std::string::size_type lastPos = 0;
	std::string::size_type pos(s.find_first_of(",-"));

	bool isrange = false;
	size_t prev_idx = 0;
	while(true) {
		if(pos == std::string::npos)
			pos = s.size();

		const std::string w(s.substr(lastPos, pos - lastPos));

		size_t len;
		size_t idx = std::stoi(w, &len);

		if(len != pos - lastPos)
			throw std::invalid_argument("malformed integer range string");

		if(!isrange)
			prev_idx = idx;

		for(; prev_idx <= idx; prev_idx++)
			add(prev_idx);

		if(pos >= s.size())
			break;

		isrange = ( s[pos] == '-' );

		lastPos = pos + 1;
		pos = s.find_first_of(",-", lastPos);
	}
}

void Affinity::set(pid_t pid) {
	checkUnixError(sched_setaffinity(pid, setsize, mask));
}

Affinity Affinity::pin(size_t idx, pid_t pid) {
	idx = (idx % numcpus());

	size_t mapped_idx = 0;
	while(!has(mapped_idx))
		mapped_idx++;

	while(idx != 0) {
		do {
			mapped_idx++;
		} while(!has(mapped_idx));

		--idx;
	}

	Affinity target(maxcpus());
	target.add(mapped_idx);
	target.set(pid);
	return target;
}