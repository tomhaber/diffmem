/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Timer.cpp
 *
 *  Created on: Apr 2, 2015
 *      Author: thaber
 */
#include "Timer.hpp"

inline static void pl_tic(pl_time_t & ts) {
#ifdef CHRONO_CPP11
	ts = std::chrono::high_resolution_clock::now();
#elif defined(WIN32) || defined(WIN64)
	if( QueryPerformanceCounter(&ts) == 0 ) {
		ts.HighPart = ~0;
		ts.LowPart = timeGetTime();
	}
#elif defined(POSIX_TIMERS)
	if( clock_gettime(CLOCK_REALTIME, &ts) != 0 ) {
		struct timeval tv;
		gettimeofday(&tv, NULL);
		ts.tv_sec = tv.tv_sec;
		ts.tv_nsec = tv.tv_usec * 1000;
	}
#else
	gettimeofday(&ts, NULL);
#endif
}

void Timer::tic() {
	pl_tic(ts);
}

double Timer::toc(std::ostream & ostr) {
	double t = toq();
	ostr << "elapsed time: " << t << std::endl;
	return t;
}

double Timer::toq() {
	pl_time_t te;
	pl_tic(te);

#ifdef CHRONO_CPP11
	std::chrono::duration<double> dur = te - ts;
	return dur.count();
#elif defined(WIN32) || defined(WIN64)
	if( ts.HighPart != ~0 ) {
		LARGE_INTEGER ElapsedMicroseconds;
		LARGE_INTEGER Frequency;

		QueryPerformanceFrequency(&Frequency);

		ElapsedMicroseconds.QuadPart = te.QuadPart - ts.QuadPart;

		//
		// We now have the elapsed number of ticks, along with the
		// number of ticks-per-second. We use these values
		// to convert to the number of elapsed microseconds.
		// To guard against loss-of-precision, we convert
		// to microseconds *before* dividing by ticks-per-second.
		//

		//ElapsedMicroseconds.QuadPart *= 1000000;
		ElapsedMicroseconds.QuadPart /= Frequency.QuadPart;
		return double(ElapsedMicroseconds.QuadPart);
	} else {
		return (te.LowPart - ts.LowPart) * 1.0e-3;
	}
#elif defined(POSIX_TIMERS)
	double elapsedTime;
    elapsedTime = (te.tv_sec - ts.tv_sec);
    elapsedTime += (te.tv_nsec - ts.tv_nsec) * 1.0e-9;
	return elapsedTime;
#else
	double elapsedTime;
    elapsedTime = (te.tv_sec - ts.tv_sec);
    elapsedTime += (te.tv_usec - ts.tv_usec) * 1.0e-6;
	return elapsedTime;
#endif
}

double Timer::now() {
	tic();

#ifdef CHRONO_CPP11
	std::chrono::duration<double> dur = ts.time_since_epoch();
	return dur.count();
#elif defined(WIN32) || defined(WIN64)
	if( ts.HighPart != ~0 ) {
		LARGE_INTEGER Frequency;
		QueryPerformanceFrequency(&Frequency);

		//
		// We now have the elapsed number of ticks, along with the
		// number of ticks-per-second. We use these values
		// to convert to the number of elapsed microseconds.
		// To guard against loss-of-precision, we convert
		// to microseconds *before* dividing by ticks-per-second.
		//

		//ElapsedMicroseconds.QuadPart *= 1000000;
		ts.QuadPart /= ts.QuadPart;
		return double(ts.QuadPart);
	} else {
		return ts.LowPart * 1.0e-3;
	}
#elif defined(POSIX_TIMERS)
	return ts.tv_sec + ts.tv_nsec / 1.0e-9;
#else
	return ts.tv_sec + ts.tv_usec / 1.0e-6;
#endif
}

