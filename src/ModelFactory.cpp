/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ModelFactory.hpp"
#include "LikelihoodModel.hpp"
#include "StructuralModel.hpp"
#include "Distribution.hpp"
#include "PluginManager.hpp"

ModelFactory::ModelFactory() {
	PluginManager::instance();
}

ModelFactory & ModelFactory::instance() {
	static ModelFactory inst;
	return inst;
}

ModelFactory::StructuralMap::iterator ModelFactory::registerStructuralModel(const std::string & name, StructuralConstr constructor) {
	auto r = structuralModels.insert( {name, constructor} );
	if(!r.second)
		throwException<std::runtime_error>("Unable to register StructuralModel: {}", name);
	return r.first;
}

ModelFactory::StructuralMap::iterator ModelFactory::registerStructuralModel(std::string && name, StructuralConstr constructor) {
	auto r = structuralModels.emplace( std::move(name), constructor );
	if(!r.second)
		throwException<std::runtime_error>("Unable to register StructuralModel: {}", name);
	return r.first;
}

void ModelFactory::unregisterStructuralModel(ModelFactory::StructuralMap::iterator it) {
	if( it != structuralModels.end() )
		structuralModels.erase(it);
}

ModelFactory::LikelihoodMap::iterator ModelFactory::registerLikelihoodModel(const std::string & name, LikelihoodConstr constructor) {
	auto r = likelihoodModels.insert( {name, constructor} );
	if(!r.second)
		throwException<std::runtime_error>("Unable to register LikelihoodModel: {}", name);
	return r.first;
}

ModelFactory::LikelihoodMap::iterator ModelFactory::registerLikelihoodModel(std::string && name, LikelihoodConstr constructor) {
	auto r = likelihoodModels.emplace( std::move(name), constructor );
	if(!r.second)
		throwException<std::runtime_error>("Unable to register LikelihoodModel: {}", name);
	return r.first;
}

void ModelFactory::unregisterLikelihoodModel(ModelFactory::LikelihoodMap::iterator it) {
	if( it != likelihoodModels.end() )
		likelihoodModels.erase(it);
}

ModelFactory::DistributionMap::iterator ModelFactory::registerDistribution(const std::string & name, DistributionConstr constructor) {
	auto r = distributions.insert( {name, constructor} );
	if(!r.second)
		throwException<std::runtime_error>("Unable to register Distribution: {}", name);
	return r.first;
}

ModelFactory::DistributionMap::iterator ModelFactory::registerDistribution(std::string && name, DistributionConstr constructor) {
	auto r = distributions.emplace( std::move(name), constructor );
	if(!r.second)
		throwException<std::runtime_error>("Unable to register Distribution: {}", name);
	return r.first;
}

void ModelFactory::unregisterDistribution(ModelFactory::DistributionMap::iterator it) {
	if( it != distributions.end() )
		distributions.erase(it);
}

std::shared_ptr<StructuralModel> ModelFactory::createStructuralModel(const std::string & name, const Dict & dict) const {
	auto it = structuralModels.find(name);
	if( it == structuralModels.end() )
		throwException<std::invalid_argument>("Unknown StructuralModel: {}", name);

	auto constr = it->second;
	return constr(dict);
}

std::shared_ptr<LikelihoodModel> ModelFactory::createLikelihoodModel(const std::string & name, const Dict & dict) const {
	auto it = likelihoodModels.find(name);
	if( it == likelihoodModels.end() )
		throwException<std::invalid_argument>("Unknown LikelihoodModel: {}", name);

	auto constr = it->second;
	return constr(dict);
}

std::shared_ptr<Distribution> ModelFactory::createDistribution(const std::string & name, const Dict & dict) const {
	auto it = distributions.find(name);
	if( it == distributions.end() )
		throwException<std::invalid_argument>("Unknown Distribution: {}", name);

	auto constr = it->second;
	return constr(dict);
}

template <typename C>
std::vector<std::string> listKeys(C &c) {
	std::vector<std::string> list;
	list.reserve(c.size());
	for(const auto & i : c) {
		list.push_back(i.first);
	}
	return list;
}

std::vector<std::string> ModelFactory::listStructuralModels() const {
	return listKeys(structuralModels);
}

std::vector<std::string> ModelFactory::listLikelihoodModels() const {
	return listKeys(likelihoodModels);
}

std::vector<std::string> ModelFactory::listDistributions() const {
	return listKeys(distributions);
}
