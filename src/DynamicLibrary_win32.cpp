/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <DynamicLibrary.hpp>

DynamicLibrary::DynamicLibrary(const std::string & name) {
	open(name);
}

DynamicLibrary::DynamicLibrary(DynamicLibrary && lib) noexcept {
	libname = std::move(lib.libname);
	handle = lib.handle;
	lib.handle = nullptr;
}

DynamicLibrary & DynamicLibrary::operator =(DynamicLibrary && lib) noexcept {
	libname = std::move(lib.libname);
	handle = lib.handle;
	lib.handle = nullptr;
	return *this;
}

DynamicLibrary::~DynamicLibrary() {
	close();
}

void DynamicLibrary::open(const std::string & name) {
	handle = LoadLibrary( name.c_str() );
	
	if( handle == nullptr ) {
		char *err = 0;

		DWORD m;
		m = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
					  FORMAT_MESSAGE_FROM_SYSTEM | 
					  FORMAT_MESSAGE_IGNORE_INSERTS,
					  NULL, 			/* Instance */
					  GetLastError(),   /* Message Number */
					  0,   				/* Language */
					  err,  			/* Buffer */
					  0,    			/* Min/Max Buffer size */
					  NULL);  			/* Arguments */

		if( m != 0 ) {
			DynamicLibraryException ex(err);

			// Free error string if allocated
			LocalFree(err);

			throw ex;
		} else
			throw DynamicLibraryException();
	}

	libname = name;
}

void *DynamicLibrary::procAddress(const char *name) {
	void *addr = GetProcAddress(handle, name);
	if( addr == nullptr ) {
		char *err = 0;

		DWORD m;
		DWORD e = GetLastError();
		m = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
					  FORMAT_MESSAGE_FROM_SYSTEM | 
					  FORMAT_MESSAGE_IGNORE_INSERTS,
					  NULL, 			/* Instance */
					  GetLastError(),   /* Message Number */
					  0,   				/* Language */
					  err,  			/* Buffer */
					  0,    			/* Min/Max Buffer size */
					  NULL);  			/* Arguments */

		if( m != 0 ) {
			DynamicLibraryException ex(err);

			// Free error string if allocated
			LocalFree(err);

			throw ex;
		} else
			throw DynamicLibraryException();
	}

	return addr;
}

void DynamicLibrary::close() {
	if( handle != nullptr )
		FreeLibrary(handle);
}
