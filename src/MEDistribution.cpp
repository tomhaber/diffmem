/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "MEDistribution.hpp"
#include "LocalMatrix.hpp"
#include "math/number.hpp"

MEDistribution::MEDistribution(const MEModel &model, Distribution &prior)
		: model(model), prior(prior) {
	const int ncoeffs = Cholesky<Matrixd>::numberOfCoefficients(model.numberOfRandom());
	const int N = model.numberOfBetas() + ncoeffs + model.numberOfTaus();
	Require(N == prior.numberOfDimensions(),
			"number of parameters of model differs from prior");
}

int MEDistribution::numberOfDimensions() const {
	return model.numberOfIndividuals() * model.numberOfRandom() + model.numberOfBetas()
			+ Cholesky<Matrixd>::numberOfCoefficients(model.numberOfRandom())
			+ model.numberOfTaus();
}

Vecd MEDistribution::generateInitial() const {
	Vecd x( numberOfDimensions() );
	const int netas = model.numberOfIndividuals() * model.numberOfRandom();

	x.head( netas ) = Vecd::Zero( netas );
	x.segment( netas, model.numberOfBetas() ) = model.betaMasked();
	const auto & chol_omega = model.cholOmega();
	auto coeffs = x.segment( netas + model.numberOfBetas(), chol_omega.numberOfCoefficients());
	chol_omega.extractCoefficients( coeffs );
	x.tail( model.numberOfTaus() ) = model.tau();
	return x;
}

double MEDistribution::logpdf(ConstRefVecd &x) const {
	const int netas = model.numberOfIndividuals() * model.numberOfRandom();
	ConstMapMatrixd etas(x.data(), model.numberOfRandom(), model.numberOfIndividuals());

	auto beta = x.segment(netas, model.numberOfBetas());
	auto tau = x.tail(model.numberOfTaus());

	Cholesky<Matrixd> chol_omega;
	const int ncoeffs = Cholesky<Matrixd>::numberOfCoefficients(model.numberOfRandom());
	chol_omega.fromCoefficients(x.segment(netas + model.numberOfBetas(), ncoeffs));

	double lp = prior.logpdf(x.tail(x.size() - netas));
	if(math::isNegInf(lp))
		return lp;

	lp += model.evaluateLikelihood(beta, etas, chol_omega, tau);
	return lp;
}
