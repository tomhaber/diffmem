/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Population.hpp"
#include "ModelFactory.hpp"
#include "math/number.hpp"
#include <tbb/parallel_for.h>
#include <typeinfo>

void Population::add(Individual && ind) {
	if( numberOfIndividuals() > 0 ) {
		const int nrandom = subjects[0].numberOfRandom();
		const int nbetas = subjects[0].numberOfBetas();
		const int No = subjects[0].numberOfObservations();

		Require( nbetas == ind.numberOfBetas(), "All subjects should be use the same number of random effects" );
		Require( nrandom == ind.numberOfRandom(), "All subjects should be use the same number of random effects" );
		Require( No == ind.numberOfObservations(), "Measurements have incorrect dimension.");

		const StructuralModel & newsm = *ind.structuralModel;
		const StructuralModel & othersm = *subjects[0].structuralModel;
		Require( typeid(newsm) == typeid(othersm), "structuralModel types should match" );

		const LikelihoodModel & newlm = *ind.likelihoodModel;
		const LikelihoodModel & otherlm = *subjects[0].likelihoodModel;
		Require( typeid(newlm) == typeid(otherlm), "likelihoodModel types should match" );
	}

	subjects.emplace_back( std::move(ind) );
}

void Population::evalU(ConstRefMatrixd & psis, ConstRefVecd & t, RefMatrixd us) const {
	const int N = numberOfObservations();
	const int numind = numberOfIndividuals();

	Require( psis.cols() == numind, "number of psis (" , psis.cols(), ") does not match number of individuals (", numind, ")" );
	Require( psis.rows() == numberOfParameters(), "number of psis does not match number of parameters" );
	Require( us.cols() == t.size(), "dimension of u does not match number of time points" );
	Require( us.rows() == numind * N, "dimension of u (", us.rows(), ") does not match number of individuals (", (numind * N), ")" );

	tbb::parallel_for(0, numind,
		[this, N, &psis, &t, &us](int i) {
			auto psi = psis.col(i);
			auto u = us.block(N*i, 0, N, t.size());
			try {
				subjects[i].evalU(psi, t, u);
			} catch( model_error & ) {
				u.fill( math::NaN() );
			}
		}
	);
}

void Population::evalU(ConstRefVeci & indices, ConstRefMatrixd & psis, ConstRefVecd & t, RefMatrixd us) const {
	const int N = numberOfObservations();
	const int numind = indices.size();

	Require( psis.cols() == numind, "number of psis (" , psis.cols(), ") does not match number of individuals (", numind, ")" );
	Require( psis.rows() == numberOfParameters(), "number of psis does not match number of parameters" );
	Require( us.cols() == t.size(), "dimension of u does not match number of time points" );
	Require( us.rows() == numind * N, "dimension of u (", us.rows(), ") does not match number of individuals (", (numind * N), ")" );

	tbb::parallel_for(0, indices.size(),
		[this, N, &psis, &t, &us, &indices](int ix) {
			const auto i = indices[ix];
			auto psi = psis.col(ix);
			auto u = us.block(N*ix, 0, N, t.size());
			try {
				subjects[i].evalU(psi, t, u);
			} catch( model_error & ) {
				u.fill( math::NaN() );
			}
		}
	);
}

void Population::evalSens(ConstRefMatrixd & psis, ConstRefVecd & t, RefMatrixd ss) const {
	const int N = numberOfObservations();
	const int numberOfSensitivities = N * (psis.rows() + 1);
	const int numind = numberOfIndividuals();

	Require( psis.cols() == numind, "number of psis (", psis.cols(), ") does not match number of individuals (", numind, ")" );
	Require( psis.rows() == numberOfParameters(), "number of psis does not match number of parameters" );
	Require( ss.cols() == t.size(), "dimension of u does not match number of time points" );
	Require( ss.rows() == numind * numberOfSensitivities, "dimension of u (", ss.rows(), ") does not match number of individuals (", (numind*numberOfSensitivities), ")" );

	tbb::parallel_for(0, numind,
		[this, numberOfSensitivities, &psis, &t, &ss](int i) {
			auto psi = psis.col(i);
			auto s = ss.block(numberOfSensitivities*i, 0, numberOfSensitivities, t.size());
			try {
				subjects[i].evalSens(psi, t, s);
			} catch( model_error & ) {
				s.fill( math::NaN() );
			}
		}
	);
}

void Population::evalSens(ConstRefVeci & indices, ConstRefMatrixd & psis, ConstRefVecd & t, RefMatrixd ss) const {
	const int N = numberOfObservations();
	const int numberOfSensitivities = N * (psis.rows() + 1);
	const int numind = indices.size();

	Require( psis.cols() == numind, "number of psis (", psis.cols(), ") does not match number of individuals (", numind, ")" );
	Require( psis.rows() == numberOfParameters(), "number of psis does not match number of parameters" );
	Require( ss.cols() == t.size(), "dimension of u does not match number of time points" );
	Require( ss.rows() == numind * numberOfSensitivities, "dimension of u (", ss.rows(), ") does not match number of individuals (", (numind*numberOfSensitivities), ")" );

	tbb::parallel_for(0, indices.size(),
		[this, numberOfSensitivities, &psis, &t, &ss, &indices](int ix) {
			const auto i = indices[ix];
			auto psi = psis.col(i);
			auto s = ss.block(numberOfSensitivities*i, 0, numberOfSensitivities, t.size());
			try {
				subjects[i].evalSens(psi, t, s);
			} catch( model_error & ) {
				s.fill( math::NaN() );
			}
		}
	);
}

void Population::evalU(ConstRefVecd & beta, ConstRefMatrixd & etas, ConstRefVecd & t, RefMatrixd us) const {
	const int N = numberOfObservations();
	const int numind = numberOfIndividuals();

	Require( etas.cols() == numind, "number of etas (" , etas.cols(), ") does not match number of individuals (", numind, ")" );
	Require( etas.rows() == numberOfRandom(), "number of etas does not match number of random effects" );
	Require( us.cols() == t.size(), "dimension of u does not match number of time points" );
	Require( us.rows() == numind * N, "dimension of u (", us.rows(), ") does not match number of individuals (", (numind * N), ")" );

	tbb::parallel_for(0, numind,
		[this, N, &beta, &etas, &t, &us](int i) {
			auto eta = etas.col(i);
			auto u = us.block(N*i, 0, N, t.size());
			try {
				subjects[i].evalU(beta, eta, t, u);
			} catch( model_error & ) {
				u.fill( math::NaN() );
			}
		}
	);

}

void Population::evalU(ConstRefVecd & beta, ConstRefVeci & indices, ConstRefMatrixd & etas, ConstRefVecd & t, RefMatrixd us) const {
	const int N = numberOfObservations();
	const int numind = indices.size();

	Require( etas.cols() == numind, "number of etas (" , etas.cols(), ") does not match number of individuals (", numind, ")" );
	Require( etas.rows() == numberOfRandom(), "number of etas does not match number of random effects" );
	Require( us.cols() == t.size(), "dimension of u does not match number of time points" );
	Require( us.rows() == numind * N, "dimension of u (", us.rows(), ") does not match number of individuals (", (numind * N), ")" );

	tbb::parallel_for(0, indices.size(),
		[this, N, &beta, &etas, &t, &us, &indices](int ix) {
			const auto i = indices[ix];
			auto eta = etas.col(ix);
			auto u = us.block(N*ix, 0, N, t.size());
			try {
				subjects[i].evalU(beta, eta, t, u);
			} catch( model_error & ) {
				u.fill( math::NaN() );
			}
		}
	);
}

void Population::evalSens(ConstRefVecd & beta, ConstRefMatrixd & etas, ConstRefVecd & t, RefMatrixd ss) const {
	const int N = numberOfObservations();
	const int numberOfSensitivities = N * (etas.rows() + 1);
	const int numind = numberOfIndividuals();

	Require( etas.cols() == numind, "number of etas (" , etas.cols(), ") does not match number of individuals (", numind, ")" );
	Require( etas.rows() == numberOfRandom(), "number of etas does not match number of random effects" );
	Require( ss.cols() == t.size(), "dimension of u does not match number of time points" );
	Require( ss.rows() == numind * numberOfSensitivities, "dimension of u (", ss.rows(), ") does not match number of individuals (", (numind*numberOfSensitivities), ")" );

	tbb::parallel_for(0, numind,
		[this, numberOfSensitivities, &beta, &etas, &t, &ss](int i) {
			auto eta = etas.col(i);
			auto s = ss.block(numberOfSensitivities*i, 0, numberOfSensitivities, t.size());
			try {
				subjects[i].evalSens(beta, eta, t, s);
			} catch( model_error & ) {
				s.fill( math::NaN() );
			}
		}
	);
}

void Population::evalSens(ConstRefVecd & beta, ConstRefVeci & indices, ConstRefMatrixd & etas, ConstRefVecd & t, RefMatrixd ss) const {
	const int N = numberOfObservations();
	const int numberOfSensitivities = N * (etas.rows() + 1);
	const int numind = indices.size();

	Require( etas.cols() == numind, "number of etas (" , etas.cols(), ") does not match number of individuals (", numind, ")" );
	Require( etas.rows() == numberOfRandom(), "number of etas does not match number of random effects" );
	Require( ss.cols() == t.size(), "dimension of u does not match number of time points" );
	Require( ss.rows() == numind * numberOfSensitivities, "dimension of u (", ss.rows(), ") does not match number of individuals (", (numind*numberOfSensitivities), ")" );

	tbb::parallel_for(0, indices.size(),
		[this, numberOfSensitivities, &beta, &etas, &t, &ss, &indices](int ix) {
			const auto i = indices[ix];
			auto eta = etas.col(i);
			auto s = ss.block(numberOfSensitivities*i, 0, numberOfSensitivities, t.size());
			try {
				subjects[i].evalSens(beta, eta, t, s);
			} catch( model_error & ) {
				s.fill( math::NaN() );
			}
		}
	);
}

int Population::numberOfMeasurements() const {
	int N = 0;
	for(int i = 0; i < numberOfIndividuals(); ++i)
		N += subjects[i].numberOfMeasurements();
	return N;
}

int Population::numberOfRandom() const {
	Require( numberOfIndividuals() > 1, "Population should contain multiple individuals" );
	return subjects[0].numberOfRandom();
}

int Population::numberOfBetas() const {
	Require( numberOfIndividuals() > 1, "Population should contain multiple individuals" );
	return subjects[0].numberOfBetas();
}

int Population::numberOfParameters() const {
	Require( numberOfIndividuals() > 1, "Population should contain multiple individuals" );
	return subjects[0].numberOfParameters();
}

int Population::numberOfTaus() const {
	Require( numberOfIndividuals() > 1, "Population should contain multiple individuals" );
	return subjects[0].numberOfTaus();
}

int Population::numberOfSigmas() const {
	Require( numberOfIndividuals() > 1, "Population should contain multiple individuals" );
	return subjects[0].numberOfSigmas();
}

int Population::numberOfObservations() const {
	Require( numberOfIndividuals() > 1, "Population should contain multiple individuals" );
	return subjects[0].numberOfObservations();
}

void Population::transsigma(ConstRefVecd & tau, RefVecd sigma) const {
	Require( numberOfIndividuals() > 1, "Population should contain multiple individuals" );
	subjects[0].transsigma(tau, sigma);
}

void Population::transtau(ConstRefVecd & sigma, RefVecd tau) const {
	Require( numberOfIndividuals() > 1, "Population should contain multiple individuals" );
	subjects[0].transtau(sigma, tau);
}

void Population::updateTau(RefVecd tau, ConstRefVecd & gradTau, int N, double alpha) const {
	Require( numberOfIndividuals() > 1, "Population should contain multiple individuals" );
	subjects[0].updateTau(tau, gradTau, N, alpha);
}
