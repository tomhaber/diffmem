/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "math/pqgamma.hpp"
#include "math/number.hpp"
#include "math/constants.hpp"
#include <cstddef>

namespace math {

	template <bool regularized>
	double pgamma_ser(double a, double x) {
		const std::size_t itmax = 32;
		const double eps = 4*std::numeric_limits<double>::epsilon();
		if(x < eps)
			return 0.0;

		double xx(1.0 / a), n(a), sum(xx);
		std::size_t i = 0;
		do {
			++n;
			++i;
			xx *= x / n;
			sum += xx;
		} while(std::abs(xx) > eps * std::abs(sum) && i < itmax);

		if(regularized)
			return std::exp(-x + a * std::log(x) - std::lgamma(a)) * sum;
		return std::exp(-x + a * std::log(x)) * sum;
	  }

	template <bool regularized>
	double qgamma_cf(double a, double x) {
		const double itmax = 32;
		const double eps = 4*std::numeric_limits<double>::epsilon();
		const double min = 4*std::numeric_limits<double>::min();

		// set up for evaluating continued fraction by modied Lentz's method
		double del, ai, bi(x + 1.0 - a), ci(1.0 / min), di(1.0 / bi), h(di), i(0.0);
		do { // iterate
			++i;
			ai = -i * (i - a);
			bi += 2.0;

			di = ai * di + bi;
			if(std::abs(di) < min)
				di = min;

			ci = bi + ai / ci;
			if(std::abs(ci) < min)
				ci = min;

			di = 1.0 / di;
			del = di * ci;
			h *= del;
		} while((std::abs(del - 1.0) > eps) && i < itmax);

		if(regularized)
			return std::exp(-x + a * std::log(x) - std::lgamma(a)) * h;
		return std::exp(-x + a * std::log(x)) * h;
	}

	template <bool regularized>
	double pgamma_impl(double a, double x) {
		if(x == 0.0)
			return 0.0;

		if(x < 0.0 || a <= 0.0)
			return math::NaN();

		if(regularized) {
			if(x < a + 1.0)
				return pgamma_ser<regularized>(a, x);
			return 1.0 - qgamma_cf<regularized>(a, x);
		} else {
			if(x < a + 1.0)
				return pgamma_ser<regularized>(a, x);
			return std::tgamma(a) - qgamma_cf<regularized>(a, x);
		}
	}

	template <bool regularized>
	double qgamma_impl(double a, double x) {
		if(x == 0.0)
			return 0.0;

		if(x < 0.0 || a <= 0.0)
			return math::NaN();

		//double v = regularized ? 1.0 : std::tgamma(a);
		if(regularized) {
			if(x < a + 1.0)
				return 1.0 - pgamma_ser<regularized>(a, x);
			return qgamma_cf<regularized>(a, x);
		} else {
			if(x < a + 1.0)
				return std::tgamma(a) - pgamma_ser<regularized>(a, x);
			return qgamma_cf<regularized>(a, x);
		}
	}

	double pgamma(double x, double a) {
		return pgamma_impl<true>(a, x);
	}

	double qgamma(double x, double a) {
		return qgamma_impl<true>(a, x);
	}
}
