/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "math/integration.hpp"
#include "cubature.h"
#include "math/constants.hpp"
#include "stats/normal.hpp"
#include <tbb/parallel_for.h>
#include "parallel_reduce_sum.hpp"
#include "GaussianQuadrature.hpp"

template <typename Derived>
auto clamp(const MatrixBase<Derived> & v) {
	using Scalar = typename Derived::Scalar;
	return v.unaryExpr([](Scalar v){ return (v < 0.0) ? 0.0 : v;});
}

template <typename Derived>
auto log(const MatrixBase<Derived> & v) {
	using Scalar = typename Derived::Scalar;
	return v.unaryExpr([](Scalar v){ return std::log(v);});
}

template <typename Derived>
auto exp(const MatrixBase<Derived> & v) {
	using Scalar = typename Derived::Scalar;
	return v.unaryExpr([](Scalar v){ return std::exp(v);});
}

namespace math {
	extern "C"
	int fun(unsigned dim, size_t npt, const double *x_, void *data, unsigned fdim, double *retval_) {
		details::f_helper *f = static_cast<details::f_helper *>(data);
		ConstMapMatrixd x(x_, dim, npt);
		MapMatrixd retval(retval_, fdim, npt);

		try {
			tbb::parallel_for(0, x.cols(), [&](int i) {
				f->operator()(x.col(i), retval.col(i));
			});
		} catch(...) {
			return EXIT_FAILURE;
		}

		return EXIT_SUCCESS;
	}

	struct normal_helper {
		details::f_helper *f;
		bool log;
	};

	extern "C"
	int fun_normal(unsigned dim, size_t npt, const double *x_, void *data, unsigned fdim, double *retval_) {
		normal_helper *n = static_cast<normal_helper *>(data);
		details::f_helper *f = n->f;

		ConstMapMatrixd x(x_, dim, npt);
		MapMatrixd retval(retval_, fdim, npt);

		try {
			if(n->log) {
				tbb::parallel_for(0, x.cols(), [&](int i) {
					f->operator()(x.col(i), retval.col(i));		// log(f(x))

					const double norm_x = -0.5 * x.col(i).squaredNorm();
					for(int j = 0; j < fdim; ++j)
						retval(j,i) = std::exp(retval(j,i) + norm_x);
				});
			} else {
				tbb::parallel_for(0, x.cols(), [&](int i) {
					f->operator()(x.col(i), retval.col(i));		// f(x)
					retval.col(i) *= std::exp(-0.5 * x.col(i).squaredNorm());
				});
			}
		} catch(...) {
			return EXIT_FAILURE;
		}

		return EXIT_SUCCESS;
	}

	double cubature(details::f_helper f, ConstRefVecd &lb, ConstRefVecd &ub,
			double reltol, double abstol, size_t maxevals) {
		double val, err;

		Require(lb.size() == ub.size());
		int rv = hcubature_v(1, fun, &f, lb.size(), lb.data(), ub.data(),
				maxevals, abstol, reltol, ERROR_INDIVIDUAL, &val, &err);
		checkPosixError(rv);

		return val;
	}

	void cubature(details::f_helper f, ConstRefVecd &lb, ConstRefVecd &ub,
			RefVecd res, double reltol, double abstol, size_t maxevals) {
		const int fdim = res.size();
		Vecd err(fdim);

		Require(lb.size() == ub.size());
		int rv = hcubature_v(fdim, fun, &f, lb.size(), lb.data(), ub.data(),
				maxevals, abstol, reltol, ERROR_INDIVIDUAL, res.data(), err.data());
		checkPosixError(rv);
	}

	double cubature_normal(details::f_helper f, size_t dim, bool log_, double q, double reltol, double abstol, size_t maxevals) {
		double val, err;

		Vecd lb(dim), ub(dim);
		const double qnorm = stats::Normal::icdf(q);
		lb.fill(-qnorm);
		ub.fill(qnorm);

		normal_helper n{&f, log_};
		int rv = hcubature_v(1, fun_normal, &n, lb.size(), lb.data(), ub.data(),
				maxevals, abstol, reltol, ERROR_INDIVIDUAL, &val, &err);
		checkPosixError(rv);

		if(log_)
			return std::log(val < 0.0 ? 0.0 : val) - 0.5 * math::log2Pi() * dim;
		else
			return val / std::pow(math::sqrt2Pi(), dim);
	}

	void cubature_normal(details::f_helper f, size_t dim, bool log_, RefVecd res, double q, double reltol, double abstol, size_t maxevals) {
		const int fdim = res.size();
		Vecd err(fdim);

		Vecd lb(dim), ub(dim);
		const double qnorm = stats::Normal::icdf(q);
		lb.fill(-qnorm);
		ub.fill(qnorm);

		normal_helper n{&f, log_};
		int rv = hcubature_v(fdim, fun_normal, &n, lb.size(), lb.data(), ub.data(),
				maxevals, abstol, reltol, ERROR_INDIVIDUAL, res.data(), err.data());
		checkPosixError(rv);

		if(log_)
			res = log(clamp(res)).array() - 0.5 * math::log2Pi() * dim;
		else
			res /= std::pow(math::sqrt2Pi(), dim);
	}

	double quadrature(details::f_helper f, ConstRefVecd &lb, ConstRefVecd &ub, size_t n) {
		Require(lb.size() == ub.size());
		const int dims = lb.size();

		GaussianQuadrature::NDGaussQuad ndgq(dims, GaussianQuadrature::findGaussLegendreTable(n));
		double s = parallel_reduce_sum<size_t>(0, ndgq.count(), [&](size_t i, Vecd &x) {
			x.resize(dims);
			double w = ndgq.x(i, x);
			for(int j = 0; j < dims; ++j) {
				const double A = 0.5 * (ub[j] - lb[j]);
				const double B = 0.5 * (ub[j] + lb[j]);
				x[j] = B + A * x[j];
			}

			double fx;
			f(x, MapVecd(&fx, 1));
			return w * fx;
		}, 0.0);

		for(int j = 0; j < dims; ++j)
			s *= 0.5 * (ub[j] - lb[j]);
		return s;
	}

	void quadrature(details::f_helper f, ConstRefVecd &lb, ConstRefVecd &ub, size_t n, RefVecd res) {
		Require(lb.size() == ub.size());
		const int dims = lb.size();
		const int fdims = res.size();

		Vecd zero = Vecd::Zero(fdims);

		GaussianQuadrature::NDGaussQuad ndgq(dims, GaussianQuadrature::findGaussLegendreTable(n));
		res = parallel_reduce_sum<size_t>(0, ndgq.count(), [&](size_t i, Vecd &val) {
			Vecd x(dims);
			double w = ndgq.x(i, x);

			for(int j = 0; j < dims; ++j) {
				const double A = 0.5 * (ub[j] - lb[j]);
				const double B = 0.5 * (ub[j] + lb[j]);
				x[j] = B + A * x[j];
			}

			val.resize(fdims);
			f(x, val);
			val *= w;
		}, zero);

		double s = 1.0;
		for(int j = 0; j < dims; ++j)
			s *= 0.5 * (ub[j] - lb[j]);
		res *= s;
	}

	double quadrature_normal(details::f_helper f, size_t dim, bool log_, size_t n) {
		GaussianQuadrature::NDGaussQuad ndgq(dim, GaussianQuadrature::findGaussHermiteTable(n));
		double res = parallel_reduce_sum<size_t>(0, ndgq.count(), [&](size_t i, Vecd &x) {
			x.resize(dim);
			double w = ndgq.x(i, x);

			const double sqrt2_d = std::pow(math::sqrt2(), dim);
			x *= math::sqrt2();
			w *= sqrt2_d;

			double fx;
			f(x, MapVecd(&fx, 1));
			return log_ ? w * std::exp(fx) : w * fx;
		}, 0.0);

		if(log_)
			return std::log(res < 0.0 ? 0.0 : res) - 0.5 * math::log2Pi() * dim;
		else
			return res / std::pow(math::sqrt2Pi(), dim);
	}

	void quadrature_normal(details::f_helper f, size_t dim, bool log_, size_t n, RefVecd res) {
		const int fdims = res.size();
		Vecd zero = Vecd::Zero(fdims);

		GaussianQuadrature::NDGaussQuad ndgq(dim, GaussianQuadrature::findGaussHermiteTable(n));
		res = parallel_reduce_sum<size_t>(0, ndgq.count(), [&](size_t i, Vecd &val) {
			Vecd x(dim);
			double w = ndgq.x(i, x);

			const double sqrt2_d = std::pow(math::sqrt2(), dim);
			x *= math::sqrt2();
			w *= sqrt2_d;

			val.resize(fdims);
			f(x, val);

			if(log_)
				val = w * exp(val);
			else
				val *= w;
		}, zero);

		if(log_)
			res = log(clamp(res)).array() - 0.5 * math::log2Pi() * dim;
		else
			res /= std::pow(math::sqrt2Pi(), dim);
	}

	double mc_normal(details::f_helper f, size_t dim, bool log_, Random &rng, size_t n) {
		auto prng = rng.split(n);
		double res = parallel_reduce_sum<size_t>(0, n, [&](size_t i, Vecd &x) {
			Random rng_i = prng[i];
			x = generate_random<Vecd>(stats::Normal::generator(rng_i), dim);

			double fx;
			f(x, MapVecd(&fx, 1));
			return log_ ? std::exp(fx) : fx;
		}, 0.0) / n;

		return log_ ? std::log(res < 0.0 ? 0.0 : res) : res;
	}

	void mc_normal(details::f_helper f, size_t dim, bool log_, Random &rng, size_t n, RefVecd res) {
		const int fdims = res.size();
		Vecd zero = Vecd::Zero(fdims);

		auto prng = rng.split(n);
		res = parallel_reduce_sum<size_t>(0, n, [&](size_t i, Vecd &val) {
			Random rng_i = prng[i];
			Vecd x = generate_random<Vecd>(stats::Normal::generator(rng_i), dim);

			val.resize(fdims);
			f(x, val);

			if(log_)
				val = exp(val);
		}, zero);

 		res /= n;
		if(log_)
			res = log(clamp(res));
	}
}
