/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "math/inv_phi.hpp"
#include "math/polynomial.hpp"
#include "math/log1m.hpp"
#include "math/number.hpp"
#include "math/constants.hpp"

namespace math {
	template <class T>
	static T invPhi_impl(T p) {
		// ALGORITHM AS241  APPL. STATIST. (1988) VOL. 37, NO. 3

		static const T A[] = {
			3.387132872796366608L,
			133.14166789178437745L,
			1971.5909503065514427L,
			13731.693765509461125L,
			45921.953931549871457L,
			67265.770927008700853L,
			33430.575583588128105L,
			2509.0809287301226727L
		};
		static const T B[] = {
			1.0L,
			42.313330701600911252L,
			687.1870074920579083L,
			5394.1960214247511077L,
			21213.794301586595867L,
			39307.89580009271061L,
			28729.085735721942674L,
			5226.495278852854561L
		};

		static const T C[] = {
			1.42343711074968357734L,
			4.6303378461565452959L,
			5.7694972214606914055L,
			3.64784832476320460504L,
			1.27045825245236838258L,
			0.24178072517745061177L,
			0.0227238449892691845833L,
			7.7454501427834140764e-4L
		};
		static const T D[] = {
			1.0L,
			2.05319162663775882187L,
			1.6763848301838038494L,
			0.68976733498510000455L,
			0.14810397642748007459L,
			0.0151986665636164571966L,
			5.475938084995344946e-4L,
			1.05075007164441684324e-9L
		};

		static const T E[] = {
			6.6579046435011037772L,
			5.4637849111641143699L,
			1.7848265399172913358L,
			0.29656057182850489123L,
			0.026532189526576123093L,
			0.0012426609473880784386L,
			2.71155556874348757815e-5L,
			2.01033439929228813265e-7L
		};

		static const T F[] = {
			1.0L,
			0.59983220655588793769L,
			0.13692988092273580531L,
			0.0148753612908506148525L,
			7.868691311456132591e-4L,
			1.8463183175100546818e-5L,
			1.4215117583164458887e-7L,
			2.04426310338993978564e-15L
		};

		T x;
		const T q = p - 0.5;
		if( std::abs(q) < .425 ) {
			const T r = .180625 - q * q;
			x = evaluate_polynomial(A, r) * q / evaluate_polynomial(B, r);
		} else { /* closer than 0.075 from {0,1} boundary */
			T r = std::sqrt( - std::log( (q > 0) ? (1 - p) : p ) );
			if( r <= 5. ) {
				r -= 1.6;
				x = evaluate_polynomial(C, r) / evaluate_polynomial(D, r);
			} else {
				r -= 5.;
				x = evaluate_polynomial(E, r) / evaluate_polynomial(F, r);
			}

			x = (q < 0.0) ? -x : x;
		}

		return x;
	}

	double inv_Phi(double p) {
		check_bounded(p, 0.0, 1.0);

		if( p == 0.0 )
			return math::negInf();

		if( p == 1.0 )
			return math::posInf();

		return invPhi_impl(p);
	}

}
