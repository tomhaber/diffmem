/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "math/digamma.hpp"
#include "math/polynomial.hpp"
#include "math/number.hpp"
#include "math/constants.hpp"

namespace math {

	template <class T>
	static T digamma_impl_large(T x) {
		static const T P[] = {
			 0.08333333333333L,
			-0.00833333333333L,
			 0.00396825396825L,
			-0.00416666666666L,
			 0.00757575757575L,
			-0.02109279609279L,
			 0.08333333333333L,
			-0.44325980392156L
		};

		x -= 1;
		T result = std::log(x);
		result += 1 / (2 * x);
		T z = 1 / (x*x);
		result -= z * evaluate_polynomial(P, z);
		return result;
	}

	template <class T>
	static T digamma_impl(T x) {
	   // Now the approximation, we use the form:
	   //
	   // digamma(x) = (x - root) * (Y + R(x-1))
	   //
	   // Where root is the location of the positive root of digamma,
	   // Y is a constant, and R is optimised for low absolute error
	   // compared to Y.
	   static const float Y = 0.99558162689208984F;

	   static const T root1 = T(1569415565) / 1073741824uL;
	   static const T root2 = (T(381566830) / 1073741824uL) / 1073741824uL;
	   static const T root3 = 0.9016312093258695918615325266959189453125e-19L;

	   static const T P[] = {
		  0.25479851061131551L,
		  -0.32555031186804491L,
		  -0.65031853770896507L,
		  -0.28919126444774784L,
		  -0.045251321448739056L,
		  -0.0020713321167745952L
	   };
	   static const T Q[] = {
		  1.0L,
		  2.0767117023730469L,
		  1.4606242909763515L,
		  0.43593529692665969L,
		  0.054151797245674225L,
		  0.0021284987017821144L,
		  -0.55789841321675513e-6L
	   };

	   T g = x - root1;
	   g -= root2;
	   g -= root3;
	   T r = evaluate_polynomial(P, T(x-1)) / evaluate_polynomial(Q, T(x-1));
	   T result = g * Y + g * r;
	   return result;
	}

	double digamma(double x) {
		static double euler_mascheroni = 0.57721566490153286060;

		double result = 0.0;

		if( x == 0.0 )
			return math::NaN();

		// Check for negative arguments and use reflection:
		if( x < 0 ) {
			x = 1 - x;
			// Argument reduction for tan:
			double remainder = x - std::floor(x);
			// Shift to negative if > 0.5:
			if( remainder > 0.5 ) {
				remainder -= 1;
			}

			// check for evaluation at a negative pole
			if( remainder == 0.0 )
				return math::NaN();

			result = math::pi() / std::tan(math::pi() * remainder);
		}

		//  Use approximation for small argument.
		if( x <= 0.000001 ) {
			result += -euler_mascheroni - 1.0 / x + 1.6449340668482264365 * x;
		} else if( x >= 10.0 ) {
			//  Use approximation for large argument.
			result += digamma_impl_large(x);
		} else {
			// If x > 2 reduce to the interval [1,2]:
			while( x > 2 ) {
				x -= 1;
				result += 1 / x;
			}

			// If x < 1 use recurrence to shift to > 1:
			if( x < 1 ) {
				result = -1 / x;
				x += 1;
			}

			result += digamma_impl(x);
		}

		return result;
	}

}
