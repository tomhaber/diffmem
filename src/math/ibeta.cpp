/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

 #include <algorithm>
#include "math/ibeta.hpp"
#include "math/number.hpp"
#include "math/constants.hpp"

#define STOP 1.0e-8
#define TINY 1.0e-30

namespace math {
	double ibeta(double a, double b, double x) {
		if(x < 0.0 || x > 1.0)
			return math::NaN();

		/*The continued fraction converges nicely for x < (a+1)/(a+b+2)*/
		if(x > (a + 1.0) / (a + b + 2.0))
			return (1.0 - ibeta(b, a, 1.0 - x)); /*Use the fact that beta is symmetrical.*/

		/*Find the first part before the continued fraction.*/
		const double lbeta_ab = std::lgamma(a) + std::lgamma(b) - std::lgamma(a + b);
		const double front = std::exp(std::log(x) * a + std::log(1.0 - x) * b - lbeta_ab) / a;

		/*Use Lentz's algorithm to evaluate the continued fraction.*/
		double f = 1.0, c = 1.0, d = 0.0;

		int i, m;
		for(i = 0; i <= 200; ++i) {
			m = i / 2;

			double numerator;
			if(i == 0) {
				numerator = 1.0; /*First numerator is 1.0.*/
			} else if(i % 2 == 0) {
				numerator = (m * (b - m) * x) / ((a + 2.0 * m - 1.0) * (a + 2.0 * m)); /*Even term.*/
			} else {
				numerator = -((a + m) * (a + b + m) * x) / ((a + 2.0 * m) * (a + 2.0 * m + 1)); /*Odd term.*/
			}

			/*Do an iteration of Lentz's algorithm.*/
			d = 1.0 + numerator * d;
			if(std::fabs(d) < TINY)
				d = TINY;
			d = 1.0 / d;

			c = 1.0 + numerator / c;
			if(std::fabs(c) < TINY)
				c = TINY;

			const double cd = c * d;
			f *= cd;

			/*Check for stop.*/
			if(std::fabs(1.0 - cd) < STOP)
				return front * (f - 1.0);
		}

		return math::NaN(); /*Needed more loops, did not converge.*/
	}

	double inv_ibeta(double a, double b, double p) {
		if(p < 0.0 || p > 1.0)
			return math::NaN();

		if(p == 0.0)
			return 0.0;
		if( p == 1.)
			return 1.0;

		double x;
		if((a >= 1.0) && (b >= 1.0)) {
			const double pp = (p < 0.5) ? p : 1 - p;
			const double t = std::sqrt(-2*std::log(pp));
			x = (2.30753 + t * 0.27061) / (1.0 + t * (0.99229 + t * 0.04481)) - t;
			if(p < 0.5)
				x = -x;

			const double al = ((x * x) - 3.0) / 6.0;
			const double h = 2.0 / (1.0 / (2.0 * a - 1.0) + 1.0 / (2.0 * b - 1.0));
			const double w = (x * std::sqrt(al + h) / h) - (1.0 / (2.0 * b - 1) - 1.0 / (2.0 * a - 1.0)) * (al + 5.0 / 6.0 - 2.0 / (3.0 * h));
			x = a / (a + b * std::exp(2.0 * w));
		} else {
			const double lna = std::log(a / (a + b));
			const double lnb = std::log(b / (a + b));
			const double t = std::exp(a * lna) / a;
			const double u = std::exp(b * lnb) / b;
			const double w = t + u;
			x = (p < t / w) ?
					std::pow(a * w * p, 1 / a) :
					1 - std::pow(b * w * (1 - p), 1 / b);
		}

		const double afac = -std::lgamma(a) - std::lgamma(b) + std::lgamma(a+b);
		const double a1 = a - 1.0;
		const double b1 = b - 1.0;

		double t, u;
		for(size_t i = 0; i < 10; ++i) {
			if(x == 0.0 || x == 1.0)
				return x;

			const double err = ibeta(a, b, x) - p;
			t = std::exp(a1 * std::log(x) + b1 * std::log(1 - x) + afac);
			u = err / t;

			t = u / (1.0 - 0.5*std::min(1.0, u*(a1/x - b1/(1-x))));
			x -= t;

			if( x <= 0.0)
				x = 0.5 * (x + t);
			if(x >= 1.0)
				x = 0.5 * (x+t+1);
			if((std::abs(t) < STOP * x) && (i > 0))
				break;
		}

		return x;
	}
}