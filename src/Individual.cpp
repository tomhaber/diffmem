/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Individual.hpp"
#include "LocalMatrix.hpp"
#include "math/number.hpp"

void Individual::init() {
	Require( structuralModel, "structuralModel should not be null" );
	Require( likelihoodModel, "likelihoodModel should not be null" );

	Require(structuralModel->numberOfObservations() == likelihoodModel->numberOfObservations(),
			"structural and likelihood models should match ({:d} != {:d})",
			structuralModel->numberOfObservations(), likelihoodModel->numberOfObservations());

	Require(A.rows() == B.rows(), "number of rows in A ({:d}) and B ({:d}) should match", A.rows(), B.rows());

	B_identity = (B.rows() == B.cols()) && B.isIdentity(1e-10);

	if( B_identity ) {
		BinvA = A;
		Binv.setIdentity(B.rows(), B.cols());
	} else {
		BinvA = B.colPivHouseholderQr().solve(A);
		Binv = (B.transpose() * B).ldlt().solve(B.transpose());
	}
}

double Individual::logpdf(ConstRefVecd & phi, ConstRefVecd & tau) const {
	try {
		return likelihoodModel->logpdf(phi, tau, *structuralModel);
	} catch(model_error & /*ex*/) {
		return math::negInf();
	}
}

double Individual::logpdf(ConstRefVecd & phi, ConstRefVecd & tau, RefVecd grad) const {
	const int N = phi.size();

	try {
		grad.resize(N);
		return likelihoodModel->logpdf(phi, tau, *structuralModel, grad);
	} catch(model_error & /*ex*/) {
		grad = Vecd::Zero(N);
		return math::negInf();
	}
}

double Individual::logpdf(ConstRefVecd & phi, ConstRefVecd & tau, RefVecd grad, RefMatrixd H) const {
	const int N = phi.size();

	try {
		grad.resize(N);
		H.resize(N,N);

		return likelihoodModel->logpdf(phi, tau, *structuralModel, grad, H);
	} catch(model_error & /*ex*/) {
		grad = Vecd::Zero(N);
		H = Matrixd::Zero(N, N);
		return math::negInf();
	}
}

double Individual::error(ConstRefVecd & phi, ConstRefVecd & tau, RefVecd err) const {
	try {
		return likelihoodModel->logpdfGradTau(phi, tau, *structuralModel, err);
	} catch(model_error & /*ex*/) {
		err.setZero();
		return math::negInf();
	}
}

void Individual::evalU(ConstRefVecd & beta, ConstRefVecd & eta, ConstRefVecd & t, RefMatrixd u) const {
	LOCAL_VECTOR(Vecd, phi, numberOfParameters());
	toParameter(beta, eta, phi);

	LOCAL_VECTOR(Vecd, psi, phi.size());
	structuralModel->transpsi(phi, psi);

	structuralModel->evalU(psi, t, u);
}

void Individual::evalSens(ConstRefVecd & beta, ConstRefVecd & eta, ConstRefVecd & t, RefMatrixd s) const {
	LOCAL_VECTOR(Vecd, phi, numberOfParameters());
	toParameter(beta, eta, phi);

	LOCAL_VECTOR(Vecd, psi, phi.size());
	structuralModel->transpsi(phi, psi);

	structuralModel->evalSens(psi, t, s);
}
