/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "PatternSearch.hpp"
#include "parallel_reduce_sum.hpp"

PatternSearch::PatternSearch(const Function & func, ConstRefVecd & xInit, const Options & options)
	: func(func), options(options) {

	x = xInit;
	fVal = func(x);

	xOld = Vecd::Zero(x.size());
	fValOld = math::NaN();

	meshsize = options.initialMeshSize;
	nevals = 0;
}

int PatternSearch::optimize(const Options & options) {
	this->options = options;
	meshsize = options.initialMeshSize;
	return optimize();
}

int PatternSearch::optimize() {
	nevals = 0;

	const int N = x.size();
	if( options.method == Positive2N ) {
		directions.resize( N, 2 * N );
		directions.block(0, 0, N, N) = Matrixd::Identity(N, N);
		directions.block(0, N, N, N) = -Matrixd::Identity(N, N);
	} else if( options.method == PositiveNp1 ) {
		directions.resize( N, N + 1 );
		directions.block(0, 0, N, N) = Matrixd::Identity(N, N);
		directions.block(0,N, N, 1) = -Vecd::Ones(N);
	} else if( options.method == Cross4N ) {
		directions.resize( N, 4*N );
		directions.block(0, 0, N, N) = Matrixd::Identity(N, N);
		directions.block(0, N, N, N) = (1 + options.expansion) * Matrixd::Identity(N, N);
		directions.block(0, 2*N, N, 2*N) = -directions.block(0, 0, N, 2*N);
	} else {
		throw std::runtime_error("PatternSearch: unknown search method");
	}

	int i = 0;
	if( options.trace )
		options.trace(OptimizationState::Init, OptimizationValues(i, x, fVal));

	for( ; i < options.maxIters && nevals < options.maxEvals; ++i) {
		if( step() )
			break;

		if( options.trace )
			options.trace(OptimizationState::Iter, OptimizationValues(i, x, fVal));
	}

	if( options.trace )
		options.trace(OptimizationState::Done, OptimizationValues(i, x, fVal));
	return i;
}

namespace {
	struct Best {
		Best(double fitness, int id = -1) : fitness(fitness), id(id) {}
		void operator +=(const Best & other) {
			if( other.fitness < fitness ) {
				fitness = other.fitness;
				id = other.id;
			}
		}

		double fitness;
		int id;
	};
}

bool PatternSearch::step() {
	int maxeval = std::min(directions.cols(), options.maxEvals - nevals);

	Best best = parallel_reduce_sum(0, maxeval,
		[this](size_t i, Vecd & xTest) {
			xTest = x + meshsize * directions.col(i);
			const double fitness = func(xTest);
			return Best(fitness, i);
		},
		Best(fVal)
	);
	nevals += maxeval;

	if( best.id != -1 ) {
		xOld = x;
		fValOld = fVal;

		x += meshsize * directions.col(best.id);
		fVal = best.fitness;

		meshsize = std::min(options.maxMeshSize, options.expansion*meshsize);
	} else {
		meshsize = options.contraction * meshsize;
	}

	return isConverged( best.id == -1 );
}

bool PatternSearch::isConverged(bool refined) const {
	const double deltaX = (x - xOld).norm();
	const double deltaF = std::abs(fVal - fValOld);

	if( (meshsize < options.meshTol) && (deltaF < options.fTol || deltaX < options.xTol) )
		return true;

	if( !refined && (meshsize < options.xTol || meshsize < options.fTol) && (deltaF < options.fTol || deltaX < options.xTol) )
		return true;

    return false;
}
