/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ConditionalMEDistribution.hpp"
#include "Population.hpp"
#include "mcmc/RandomInit.hpp"
#include "mcmc/ConditionalMESampler.hpp"
#include "Random.hpp"
#include <tbb/parallel_for.h>

ConditionalMEDistribution::ConditionalMEDistribution(const Population & population,
		const Vecd & beta, const Cholesky<Matrixd> & chol_omega, const Vecd & tau) : mvn(chol_omega) {

	subjects.reserve(population.numberOfIndividuals());
	for(int i = 0; i < population.numberOfIndividuals(); ++i) {
		const Individual & ind = population.subject(i);
		subjects.emplace_back( std::make_shared<ConditionalIndividualDistribution>(mvn, ind, beta, tau) );
	}
}

void ConditionalMEDistribution::errorPopulation(ConstRefMatrixd & etas, Vecd & err) {
	Vecd tmp;
	for(int i = 0; i < numberOfIndividuals(); ++i) {
		errorIndividual(i, etas.col(i), tmp);
		if( i == 0 )
			err = tmp;
		else
			err += tmp;
	}
}

void ConditionalMEDistribution::update(ConstRefVecd & beta, ConstRefMatrixd & omega, ConstRefVecd & tau) {
	mvn.set(omega);

	tbb::parallel_for(0, numberOfIndividuals(),
		[this, &beta, &tau](int id) {
			subjects[id]->update(beta, tau);
		}
	);
}

void ConditionalMEDistribution::update(ConstRefVecd & beta, const Cholesky<Matrixd> & chol_omega, ConstRefVecd & tau) {
	mvn.set(chol_omega);

	tbb::parallel_for(0, numberOfIndividuals(),
		[this, &beta, &tau](int id) {
			subjects[id]->update(beta, tau);
		}
	);
}

void ConditionalMEDistribution::errorIndividual(int id, ConstRefVecd & eta, Vecd & err) {
	subjects[id]->error(eta, err);
}

double ConditionalMEDistribution::logpdf(ConstRefVecd & x) const {
	ConstMapMatrixd etas(x.data(), subjects[0]->numberOfDimensions(), numberOfIndividuals());

	double ll = 0.0;
	for(size_t i = 0; i < subjects.size(); ++i) {
		ll += subjects[i]->logpdf(etas.col(i));
		if( math::isNegInf(ll) )
			break;
	}
	return ll;
}

double ConditionalMEDistribution::logpdf(ConstRefVecd & x, RefVecd grad) const {
	ConstMapMatrixd etas(x.data(), subjects[0]->numberOfDimensions(), numberOfIndividuals());

	LOCAL_VECTOR(Vecd, tmp, grad.size());
	double ll = 0.0;
	for(size_t i = 0; i < subjects.size(); ++i) {
		ll += subjects[i]->logpdf(etas.col(i), tmp);
		if( math::isNegInf(ll) ) {
			grad.setZero();
			break;
		}

		grad += tmp;
	}
	return ll;
}

double ConditionalMEDistribution::logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const {
	ConstMapMatrixd etas(x.data(), subjects[0]->numberOfDimensions(), numberOfIndividuals());

	LOCAL_VECTOR(Vecd, tmp_grad, grad.size());
	LOCAL_MATRIX(Matrixd, tmp_H, H.rows(), H.cols());
	double ll = 0.0;
	for(size_t i = 0; i < subjects.size(); ++i) {
		ll += subjects[i]->logpdf(etas.col(i), tmp_grad, tmp_H);
		if( math::isNegInf(ll) ) {
			grad.setZero();
			H.setZero();
			break;
		}

		grad += tmp_grad;
		H += tmp_H;
	}
	return ll;
}

std::unique_ptr<DistributionSampler> ConditionalMEDistribution::sampler(const Dict &dict) const {
	return std::make_unique<mcmc::ConditionalMESampler>(*this, dict);
}

ConditionalIndividualDistribution::ConditionalIndividualDistribution(
		ZMMvNDistribution & mvn, const Individual & subject, const Vecd & beta, const Vecd & tau)
	: ll(subject, beta, tau), distribution(ll, mvn) {
}

void ConditionalIndividualDistribution::update(ConstRefVecd & beta, ConstRefVecd & tau) {
	ll.setBeta(beta);
	ll.setTau(tau);
}

void ConditionalIndividualDistribution::error(ConstRefVecd & eta, Vecd & err) {
	ll.error(eta, err);
}
