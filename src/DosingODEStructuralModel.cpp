/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "DosingODEStructuralModel.hpp"
#include "ModelFactory.hpp"

#include "models/PK.hpp"
#include "models/PKPD.hpp"
#include "models/Cana.hpp"

REGISTER_STRUCTURAL_MODEL(Cana, DosingODEStructuralModel<Cana>);
REGISTER_STRUCTURAL_MODEL(DosingPKPD, DosingODEStructuralModel<PKPD>);
REGISTER_STRUCTURAL_MODEL(PK, DosingODEStructuralModel<PK>);

namespace details {

	Dosing constructDosingScheme(const Dict & dictionary) {
		Vecd time = dictionary.getNumericVector("time");
		const int N = time.size();

		Vecd amount = dictionary.getNumericVector("amt");
		Require( amount.size() == N, "number of dosing amounts ({:d}) should equal number of dosing events ({:d})", amount.size(), N);

		Veci cmt;
		if( dictionary.exists("cmt") ) {
			 cmt = dictionary.getIntegerVector("cmt");
			 Require( cmt.size() == N, "number of dosing compartments ({:d}) should equal number of dosing events ({:d})", cmt.size(), N);
		} else {
			cmt = Veci::Zero(N);
		}

		Dosing scheme;
		if( dictionary.exists("addl") ) {
			Veci addl = dictionary.getIntegerVector("addl");
			Require( addl.size() == N, "number of additional dosing events ({:d}) should equal number of dosing events ({:d})", addl.size(), N);

			Vecd rate = dictionary.getNumericVector("rate");
			Require( rate.size() == N, "number of dosing rates ({:d}) should equal number of dosing events ({:d})", rate.size(), N);

			for(int i = 0; i < N; ++i)
				scheme.addEvent(time[i], amount[i], addl[i], rate[i], cmt[i]);
		} else {
			for(int i = 0; i < N; ++i)
				scheme.addEvent(time[i], amount[i], cmt[i]);
		}

		return scheme;
	}

}
