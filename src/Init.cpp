/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Init.hpp"
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <climits>
#include <tbb/task_scheduler_init.h>
#include <tbb/task_scheduler_observer.h>

#if TBB_INTERFACE_VERSION <= 7002
#define TBB_PREVIEW_TASK_ARENA 1
#endif

#include <tbb/task_arena.h>
#include "Affinity.hpp"

#ifdef _MSC_VER
#pragma warning( push )
#pragma warning( disable: 4996 )
#endif

class pinning_observer: public tbb::task_scheduler_observer {
	public:
		pinning_observer(int pinning_step = 1) : pinning_step(pinning_step) {
			affinity = Affinity::active();
		}

		void on_scheduler_entry(bool /*unused*/) override {
			size_t idx =
#if TBB_INTERFACE_VERSION <= 7002
					tbb::task_arena::current_slot();
#elif TBB_INTERFACE_VERSION <= 9005
					tbb::task_arena::current_thread_index();
#else
					tbb::this_task_arena::current_thread_index();
#endif

			affinity.pin(idx * pinning_step);
		}

	private:
		Affinity affinity;
		const int pinning_step;
};

class TBBHelper {
	public:
		TBBHelper()
			: init(tbb::task_scheduler_init::deferred), default_threads(-1) {
		}

		~TBBHelper() {
			// Always disable observation before observers destruction
			pinner.observe(false);
		}

		void initialize(int default_threads) {
			this->default_threads = default_threads;

			if( init.is_active() )
				init.terminate();

			init.initialize(default_threads);
		}

	public:
		void setNumThreads(int nThreads = -1) {
			if( init.is_active() )
				init.terminate();

			init.initialize( (nThreads == -1) ? default_threads : nThreads );
		}

		void setPinning(bool enable) {
			pinner.observe(enable);
		}

	private:
		pinning_observer pinner;
		tbb::task_scheduler_init init;
		int default_threads;
};

static TBBHelper tbbHelper;

// Returns the value of the flag, or nullptr if the parsing failed.
static const char *ParseFlagValue(const char* str, const char* flag, bool def_optional) {
	// str and flag must not be nullptr.
	if( str == nullptr || flag == nullptr )
		return nullptr;

	const size_t flag_len = strlen(flag);
	if( strncmp(str, flag, flag_len) != 0 )
		return nullptr;

	// Skips the flag name.
	const char* flag_end = str + flag_len;

	// When def_optional is true, it's OK to not have a "=value" part.
	if( def_optional && (flag_end[0] == '\0') ) {
		return flag_end;
	}

	// If def_optional is true and there are more characters after the
	// flag name, or if def_optional is false, there must be a '=' after
	// the flag name.
	if( flag_end[0] != '=' )
		return nullptr;

	// Returns the string after "=".
	return flag_end + 1;
}

static bool ParseBoolFlag(const char* str, const char* flag, bool* value) {
	// Gets the value of the flag as a string.
	const char* const value_str = ParseFlagValue(str, flag, true);

	// Aborts if the parsing failed.
	if( value_str == nullptr )
		return false;

	// Converts the string value to a bool.
	*value = !(*value_str == '0' || *value_str == 'f' || *value_str == 'F');
	return true;
}

static bool ParseIntFlag(const char *str, const char *flag, int *value) {
	// Gets the value of the flag as a string.
	const char* const value_str = ParseFlagValue(str, flag, false);

	// Aborts if the parsing failed.
	if( value_str == nullptr )
		return false;

	char* end = nullptr;
	const long long_value = strtol(value_str, &end, 10);

	// Has strtoul() consumed all characters in the string?
	if( *end != '\0' ) {
		// No - an invalid character was encountered.
		printf("WARNING: %s is expected to be a 32-bit integer\n", value_str);
		fflush(stdout);
		return false;
	}

	const int result = static_cast<int>(long_value);
	if( long_value == LONG_MAX || // The parsed value overflows. (strtol() returns LONG_MAX when the input overflows.)
		long_value == LONG_MIN || // The parsed value underflows. (strtol() returns LONG_MIN when the input underflows.)
					result != long_value // The parsed value overflows as a uint32_t.
									) {
		printf("WARNING: %s is expected to be a 32-bit integer, but actually overflows.\n", value_str);
		fflush(stdout);
		return false;
	}

	// Sets *value to the value of the flag.
	*value = result;
	return true;
}

static bool SkipPrefix(const char* prefix, const char** pstr) {
	const size_t prefix_len = strlen(prefix);
	if( strncmp(*pstr, prefix, prefix_len) == 0 ) {
		*pstr += prefix_len;
		return true;
	}
	return false;
}

static bool HasFlagPrefix(const char **str) {
  return ( SkipPrefix("--", str) || SkipPrefix("-", str) );
}

namespace diffmem {

	static void ParseCommandLineFlags(Options & opts, int* argc, char *argv[], bool removeFlags) {
		for(int i = 1; i < *argc; i++) {
			bool remove_flag = false;
			const char *arg = argv[i];
			if( HasFlagPrefix(&arg) &&
					(ParseIntFlag(arg, "nthreads", &opts.nthreads) ||
					 ParseBoolFlag(arg, "no_init_tbb", &opts.noInitTBB) ||
					 ParseBoolFlag(arg, "pincores", &opts.pincores)) ) {
			  remove_flag = true;
			}

			if( remove_flag ) {
			  // Shift the remainder of the argv list left by one.  Note
			  // that argv has (*argc + 1) elements, the last one always being
			  // nullptr.  The following loop moves the trailing nullptr element as
			  // well.
			  for (int j = i; j != *argc; j++) {
				argv[j] = argv[j + 1];
			  }

			  // Decrements the argument count.
			  (*argc)--;

			  // We also need to decrement the iterator as we just removed
			  // an element.
			  i--;
			}
		}
	}

	void init(const Options & opts) {
		if( ! opts.noInitTBB ) {
			tbbHelper.initialize(opts.default_threads);
			tbbHelper.setNumThreads(opts.nthreads);
			tbbHelper.setPinning(opts.pincores);
		}
	}

	void init(int *argc, char *argv[], bool removeFlags) {
		Options opts;
		ParseCommandLineFlags(opts, argc, argv, removeFlags);
		init(opts);
	}

	void setNumThreads(int n) {
		tbbHelper.setNumThreads(n);
	}

	Options::Options() {
		char *env;
		env = getenv("TBB_NUM_THREADS");
		if( env != 0 ) {
			default_threads = atoi(env);
		} else {
			default_threads = tbb::task_scheduler_init::default_num_threads();
		}

		env = getenv("TBB_PIN_CORE");
		if( env != 0 )
			pincores = atoi(env) != 0;
	}

}

extern "C" {
	void initDiffMEM(int *argc, char *argv[], bool removeFlags) {
		diffmem::init(argc, argv, removeFlags);
	}

	void setNumThreads(int n) {
		diffmem::setNumThreads(n);
	}
}

#ifdef _MSC_VER
#pragma warning( pop )
#endif
