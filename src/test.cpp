/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ODEStructuralModel.hpp"
#include "ModelFactory.hpp"
#include "models/Dummy.hpp"
REGISTER_STRUCTURAL_MODEL(Dummy, ODEStructuralModel<Dummy>);

#include "Distribution.hpp"
#include "stats/lognormal.hpp"
#include "stats/gamma.hpp"
#include "stats/beta.hpp"
#include "stats/uniform.hpp"

class /*DIFFMEM_EXPORT*/ MyPrior : public Distribution {
	public:
		MyPrior(const Dict &) {}

	public:
		int numberOfDimensions() const {
			return 7;
		}

		double logpdf(ConstRefVecd &x) const {
			auto basem = x[0];
			auto tau = x[1];
			auto ke = x[2];
			auto kout = x[3];
			auto basep = x[4];
			auto imax = x[5];
			auto b = x[6];

			using namespace stats;

			double lp = 0.0;
			lp += Lognormal::logpdf<false>(basem, 3.61, 0.002);
			lp += Gamma::logpdf<false>(tau, 54.441, 24.476);
			lp += Lognormal::logpdf<false>(ke, -1.15, 0.514);
			lp += Lognormal::logpdf<false>(kout, 1.93, 6.84);
			lp += Gamma::logpdf<false>(basep, 2.022, 1.186);
			lp += Beta::logpdf<false>(imax, 19.574, 174.532);
			lp += Uniform::logpdf<false>(b, -10, 10);
			return lp;
		}

		double logpdf(ConstRefVecd &x, RefVecd grad) const {
			auto basem = x[0];
			auto tau = x[1];
			auto ke = x[2];
			auto kout = x[3];
			auto basep = x[4];
			auto imax = x[5];
			auto b = x[6];

			using namespace stats;

			grad.resize(7);
			grad[0] = Lognormal::logpdf_dx(basem, 3.61, 0.002);
			grad[1] = Gamma::logpdf_dx(tau, 54.441, 24.476);
			grad[2] = Lognormal::logpdf_dx(ke, -1.15, 0.514);
			grad[3] = Lognormal::logpdf_dx(kout, 1.93, 6.84);
			grad[4] = Gamma::logpdf_dx(basep, 2.022, 1.186);
			grad[5] = Beta::logpdf_dx(imax, 19.574, 174.532);
			grad[6] = Uniform::logpdf_dx(b, -10, 10);
			return logpdf(x);
		}

		double logpdf(ConstRefVecd &x, RefVecd grad, RefMatrixd H) const {
			notYetImplemented("MyPrior::sample");
		}
};

REGISTER_DISTRIBUTION(MyPrior, MyPrior);
