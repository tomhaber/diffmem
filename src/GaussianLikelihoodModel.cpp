/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "GaussianLikelihoodModel.hpp"
#include "LocalMatrix.hpp"
#include "ModelFactory.hpp"
#include "math/constants.hpp"

GaussianLikelihoodModel::GaussianLikelihoodModel(const Dict &dict) {
	measurements = Measurements(dict.getNumericVector("t"), dict.getNumericMatrix("y"));
}

void GaussianLikelihoodModel::transsigma(ConstRefVecd &tau_, RefVecd sigma_) const {
	const int dim = measurements.dim();
	Cholesky<Matrixd> chol;
	chol.fromCoefficients(tau_);

	auto sigma = MapMatrixd(sigma_.data(), dim, dim);
	sigma = chol.reconstructedMatrix();
}

void GaussianLikelihoodModel::transtau(ConstRefVecd &sigma_, RefVecd tau_) const {
	const int dim = measurements.dim();
	auto sigma = ConstMapMatrixd(sigma_.data(), dim, dim);
	Cholesky<Matrixd> chol(sigma);
	Require(chol.info() == Eigen::Success, "failed to compute cholesky of sigma");
	chol.extractCoefficients(tau_);
}

double GaussianLikelihoodModel::logProportions(ConstRefVecd &tau) const {
	const int dim = measurements.dim();
	Cholesky<Matrixd> chol_omega;
	chol_omega.fromCoefficients(tau);
	return (-0.5 * logDeterminant(chol_omega) - 0.5 * math::log2Pi() * chol_omega.rows()) * measurements.size();
}

double GaussianLikelihoodModel::logpdf(ConstRefVecd &tau, ConstRefMatrixd &u) const {
	const int dim = measurements.dim();
	Cholesky<Matrixd> chol_omega;
	chol_omega.fromCoefficients(tau);

	LOCAL_VECTOR(Vecd, d, dim);

	double ll = logProportions(tau);
	for(int i = 0; i < measurements.size(); ++i) {
		d = (measurements.value(i) - u.col(i)).cwiseProduct(measurements.weight(i));
		chol_omega.matrixL().solveInPlace(d);
		ll -= 0.5 * d.squaredNorm();
	}
	return ll;
}

double GaussianLikelihoodModel::logProportionalPdf(ConstRefVecd &tau, int i, ConstRefVecd &u) const {
	const int dim = measurements.dim();
	Cholesky<Matrixd> chol_omega;
	chol_omega.fromCoefficients(tau);

	LOCAL_VECTOR(Vecd, d, dim);
	d = (measurements.value(i) - u).cwiseProduct(measurements.weight(i));
	chol_omega.matrixL().solveInPlace(d);
	return -0.5 * d.squaredNorm();
}

double GaussianLikelihoodModel::logpdfGradTau(ConstRefVecd &tau, ConstRefMatrixd &u, RefVecd gradTau) const {
	const int dim = measurements.dim();
	Cholesky<Matrixd> chol_omega;
	chol_omega.fromCoefficients(tau);

	LOCAL_VECTOR(Vecd, d, dim);
	LOCAL_MATRIX(Matrixd, tmp, dim, dim);
	tmp.setZero();

	double ll = 0.0;
	for(int i = 0; i < measurements.size(); ++i) {
		d = (measurements.value(i) - u.col(i)).cwiseProduct(measurements.weight(i));
		tmp.noalias() += d * d.transpose();
		chol_omega.matrixL().solveInPlace(d);
		ll += d.squaredNorm();
	}

	tri2vec<Eigen::Lower>(tmp, gradTau);
	return -0.5 * ll;
}

double GaussianLikelihoodModel::logpdfGradTau(ConstRefVecd &tau, int i, ConstRefVecd &u, RefVecd gradTau) const {
	const int dim = measurements.dim();
	Cholesky<Matrixd> chol_omega;
	chol_omega.fromCoefficients(tau);

	LOCAL_VECTOR(Vecd, d, dim);
	d = (measurements.value(i) - u).cwiseProduct(measurements.weight(i));
	tri2vec<Eigen::Lower>(d * d.transpose(), gradTau);

	chol_omega.matrixL().solveInPlace(d);
	return -0.5 * d.squaredNorm();
}

void GaussianLikelihoodModel::logpdfGradU(ConstRefVecd &tau, int i, ConstRefVecd &u, RefVecd gradU) const {
	const int dim = measurements.dim();
	Cholesky<Matrixd> chol_omega;
	chol_omega.fromCoefficients(tau);

	LOCAL_VECTOR(Vecd, d, dim);
	d = (measurements.value(i) - u).cwiseProduct(measurements.weight(i));
	gradU.noalias() = chol_omega.solve(d);
}

void GaussianLikelihoodModel::logpdfHessU(ConstRefVecd &tau, int i, ConstRefVecd &u, RefVecd gradU, RefMatrixd hessU) const {
	const int dim = measurements.dim();
	Cholesky<Matrixd> chol_omega;
	chol_omega.fromCoefficients(tau);

	LOCAL_VECTOR(Vecd, d, dim);
	d = (measurements.value(i) - u).cwiseProduct(measurements.weight(i));
	gradU.noalias() = chol_omega.solve(d);
	chol_inv(chol_omega, hessU);
	hessU = -hessU;
}

void GaussianLikelihoodModel::updateTau(RefVecd tau_, ConstRefVecd &gradTau_, int N, double alpha) const {
	const int dim = measurements.dim();
	LOCAL_MATRIX(Matrixd, tmp, dim, dim);
	vec2tri<Eigen::Lower>(gradTau_, tmp);
	tmp /= N;

	Cholesky<Matrixd> chol_tmp(tmp);
	chol_tmp.extractCoefficients(tau_);
}

REGISTER_LIKELIHOOD_MODEL(Gaussian, GaussianLikelihoodModel);
