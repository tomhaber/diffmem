/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "HillStructuralModel.hpp"
#include "LikelihoodModel.hpp"
#include "Measurements.hpp"
#include "ModelFactory.hpp"
#include "math/sqr.hpp"

void HillStructuralModel::transphi(ConstRefVecd & psi, RefVecd phi) const {
	phi = psi.unaryExpr([](double x) { return std::log(x); });
}

void HillStructuralModel::transpsi(ConstRefVecd & phi, RefVecd psi) const {
	psi = phi.unaryExpr([](double x) { return std::exp(x); });
}

void HillStructuralModel::dtranspsi(ConstRefVecd & phi, RefVecd dphi) const {
	dphi = phi.unaryExpr([](double x) { return std::exp(x); });
}

void HillStructuralModel::evalU(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd u) const {
	const double E0 = psi(0);
	const double Emax = psi(1);
	const double ED50 = psi(2);
	const double gamma = psi(3);
	const double ED50_gamma = std::pow(ED50, gamma);

	for(int i = 0; i < t.size(); ++i) {
		const auto x_gamma = std::pow(t[i], gamma);
		u(0, i) = E0 + (Emax * x_gamma) / (ED50_gamma + x_gamma);
	}
}

void HillStructuralModel::evalSens(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd sens) const {
	const double E0 = psi(0);
	const double Emax = psi(1);
	const double ED50 = psi(2);
	const double gamma = psi(3);
	const double ED50_gamma = std::pow(ED50, gamma);

	for(int i = 0; i < t.size(); ++i) {
		const auto x = t[i];
		const auto x_gamma = std::pow(x, gamma);
		sens(0, i) = E0 + (Emax * x_gamma) / (ED50_gamma + x_gamma);
		sens(1, i) = 1.0;
		sens(2, i) = (x_gamma) / (ED50_gamma + x_gamma);
		sens(3, i) = -gamma * ED50_gamma * (Emax * x_gamma) / (math::sqr(ED50_gamma + x_gamma) * ED50);
		sens(4, i) = (x != 0.0) ? (ED50_gamma * (Emax * x_gamma) * (std::log(x) - std::log(ED50)) / math::sqr(ED50_gamma + x_gamma)) : 0.0;
	}
}

REGISTER_STRUCTURAL_MODEL(Hill, HillStructuralModel);
