/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "SEIRStructuralModel.hpp"
#include "ModelFactory.hpp"
#include "ode/ODE.hpp"
#include "ode/ODEIntegrator.hpp"
#include "LocalMatrix.hpp"

#define S 0
#define E 1
#define I 2
#define R 3

class SEIR : public ode::ODEBase {
	public:
		SEIR(int nAges, double q,	const Matrixd & w_pde, const Vecd & c_pde, const Vecd & mu_pde, double B_pde,	const Vecd & e_pde,
				const Vecd & g_pde, const Vecd & delta_pde,	const Vecd & cov_pde,	const Vecd & y_init_pde) : nAges(nAges), q(q), B_pde(B_pde), w_pde(w_pde),
				c_pde(c_pde),	mu_pde(mu_pde),	e_pde(e_pde),	g_pde(g_pde),	delta_pde(delta_pde),	cov_pde(cov_pde),	y_init_pde(y_init_pde) {}

	public:
		int numberOfEquations() const { return nAges*4; }
		bool isStiff() const { return false; }
		void initial(RefVecd y0) {
			y0 = y_init_pde;
		}

		void ddt(const double t, const RefVecd & y, RefVecd ddt_y) {
			LOCAL_VECTOR(Vecd, l, nAges);

			l = (q*365.) * w_pde.block(0, 0, nAges, nAges) *
					y.segment(I*nAges, nAges);

			double P_prev = 0.0;

			ddt_y[nAges*S + 0] = B_pde - (l[0] + c_pde[0] + mu_pde[0])*y[nAges*S + 0];
			for (int i = 1; i < nAges; ++i)
				ddt_y[nAges*S + i] = (1. - cov_pde[i - 1] * P_prev)*c_pde[i - 1] * y[nAges*S + (i - 1)]
											- (l[i] + c_pde[i] + mu_pde[i])*y[nAges*S + i];

			ddt_y[nAges*E + 0] = l[0] * y[nAges*S + 0] - (e_pde[0] + c_pde[0] + mu_pde[0])*y[nAges*E + 0];
			for (int i = 1; i < nAges; ++i)
				ddt_y[nAges*E + i] = l[i] * y[nAges*S + i] + c_pde[i - 1] * y[nAges*E + (i - 1)]
									- (e_pde[i] + c_pde[i] + mu_pde[i])*y[nAges*E +i];

			ddt_y[nAges*I + 0] = e_pde[0] * y[nAges*E + 0] - (g_pde[0] + c_pde[0] + mu_pde[0])*y[nAges*I + 0];
			for (int i = 1; i < nAges; ++i)
				ddt_y[nAges*I + i] = e_pde[i] * y[nAges*E + i] + c_pde[i - 1] * y[nAges*I + (i - 1)]
										- (g_pde[i] + c_pde[i] + mu_pde[i])*y[nAges*I + i];

			ddt_y[nAges*R + 0] = g_pde[0] * y[nAges*I + 0] - (delta_pde[0] + c_pde[0] + mu_pde[0])*y[nAges*R + 0];
			for (int i = 1; i < nAges; ++i)
				ddt_y[nAges*R + i] = g_pde[i] * y[nAges*I + i] + c_pde[i - 1] * y[nAges*R + (i - 1)]
									- (delta_pde[i] + c_pde[i] + mu_pde[i])*y[nAges*R + i];
		}
	private:
		int nAges;
		double q;
		double B_pde;

		const Matrixd & w_pde;
		const Vecd & c_pde;
		const Vecd & mu_pde;
		const Vecd & e_pde;
		const Vecd & g_pde;
		const Vecd & delta_pde;
		const Vecd & cov_pde;
		const Vecd & y_init_pde;
};

#undef S
#undef E
#undef I
#undef R

void SEIRStructuralModel::evalU(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd u) const {
	SEIR ode( nAges, psi[0], w_pde, c_pde, mu_pde, B_pde, e_pde, g_pde, delta_pde, cov_pde, y_init_pde );
	ode::ODEIntegrator integrator(ode, true);

	integrator.setIC();
	integrator.setTolerances(1e-6, 1e-9);
	integrator.setMaxNumSteps(50000);

	for (int i = 0; i < t.size(); ++i) {
		integrator.integrateTo(t[i]);
		u.col(i) = integrator.currentState();
	}
}

void SEIRStructuralModel::transphi(ConstRefVecd & psi, RefVecd phi) const {
	phi = psi.array().log();
}

void SEIRStructuralModel::transpsi(ConstRefVecd & phi, RefVecd psi) const {
	psi = phi.array().exp();
}

void SEIRStructuralModel::dtranspsi(ConstRefVecd & phi, RefVecd dphi) const {
	dphi = phi.array().exp();
}

SEIRStructuralModel::SEIRStructuralModel(const Dict & dict) {
	nAges = dict.getInteger("nAges");

	w_pde = dict.getNumericMatrix("w_pde");
	Require(w_pde.size() >= nAges*nAges, "w_pde must be a matrix containing at least nAges*nAges elements.");

	c_pde = dict.getNumericVector("c_pde");
	Require(c_pde.size() == nAges, "c_pde must be a vector containing nAges elements.");

	mu_pde = dict.getNumericVector("mu_pde");
	Require(mu_pde.size() == nAges, "mu_pde must be a vector containing nAges elements.");

	B_pde = dict.getNumeric("B_pde");

	e_pde = dict.getNumericVector("e_pde");
	Require(e_pde.size() == nAges, "e_pde must be a vector containing nAges elements.");

	g_pde = dict.getNumericVector("g_pde");
	Require(g_pde.size() == nAges, "g_pde must be a vector containing nAges elements.");

	delta_pde = dict.getNumericVector("delta_pde");
	Require(delta_pde.size() == nAges, "delta_pde must be a vector containing nAges elements.");

	cov_pde = dict.getNumericVector("cov_pde");
	Require(cov_pde.size() == nAges, "cov_pde must be a vector containing nAges elements.");

	y_init_pde = dict.getNumericVector("y_init_pde");
	Require(y_init_pde.size() == nAges*4, "y_init_pde must be a vector containing nAges*4 elements.");
}

REGISTER_STRUCTURAL_MODEL(SEIR, SEIRStructuralModel);
