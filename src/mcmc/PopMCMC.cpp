/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * PopMCMC.cpp
 *
 *  Created on: Mar 20, 2015
 *      Author: thaber
 */
#include "mcmc/PopMCMC.hpp"
#include "mcmc/Proposal.hpp"
#include "Distribution.hpp"
#include "Random.hpp"
#include "Statistics.hpp"

#include <iostream>

namespace mcmc {

	PopMCMC::PopMCMC(Distribution & likelihood, PriorDistribution & prior,
			Proposal & proposal, std::vector<double> & temperatures) : Sampler(likelihood, prior) {

		chains.resize( temperatures.size() );
		for(size_t i = 0; i < chains.size(); ++i)
			chains[i] = new Chain(proposal.clone(), temperatures[i]);
	}

	PopMCMC::PopMCMC(Distribution & likelihood, PriorDistribution & prior,
			Proposal & proposal, int N, double temperatures[]) : Sampler(likelihood, prior) {

		chains.resize( N );
		for(size_t i = 0; i < chains.size(); ++i)
			chains[i] = new Chain(proposal.clone(), temperatures[i]);
	}

	PopMCMC::~PopMCMC() {
		for(size_t i = 0; i < chains.size(); ++i)
			delete chains[i];
		chains.clear();
	}

	void PopMCMC::initialize(Random & rng) {
		// Sample initial position from prior
		prior.sample(rng, s);
		initializeChains(s);
	}

	const Vecd & PopMCMC::update(Random & rng) {
		updateChains(rng);
		exchangeChains(rng);
		return chains.back()->position();
	}

	void PopMCMC::initializeChains(const Vecd & x) {
		for(size_t i = 0; i < chains.size(); ++i)
			chains[i]->initialize(x, likelihood, prior);
	}

	void PopMCMC::updateChains(Random & rng) {
		for(size_t i = 0; i < chains.size(); ++i)
			Sampler::update(*chains[i], rng);
	}

	void PopMCMC::exchangeChains(Random & rng) {
		// If there are less than 2 chains then we can't swap so return now
		if( chains.size() < 2 )
			return;

		const size_t num_exchanges = 3;
		for (size_t l = 0; l < num_exchanges; l++) {
			for(size_t i = 0; i < chains.size() - 1; ++i) {
				size_t j = i;

				// Calculate Metropolis-Hastings acceptance ratio
				double alpha = chains[j + 1]->likelihood() * chains[j]->temperature() +
								 chains[j]->likelihood() * chains[j + 1]->temperature() -
								 chains[j]->likelihood() * chains[j]->temperature() -
								 chains[j + 1]->likelihood() * chains[j + 1]->temperature();

				if( alpha > 0.0 || log(rng.uniform()) < alpha) {
					chains[j]->exchange(*chains[j+1]);
				}
			}
		}
	}

}
