/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/RandomInit.hpp"
#include <MatrixVector.hpp>
#include <Error.hpp>
#include <Random.hpp>
#include <Distribution.hpp>
#include <DistributionSampler.hpp>
#include <DictCpp.hpp>
#include <math/number.hpp>

namespace mcmc {
	const Vecd &RandomInit::generate(Vecd &x, double &likelihoodlogpdf, double &priorlogpdf) const {
		x.resize(prior.numberOfDimensions());

		auto sampler = prior.sampler(
			DictCpp{
				"type", "MH",
				"proposal", "amm"
			}
		);

		// Sample initial position from prior
		int i;
		for(i = 0; i < maxtries; ++i) {
			sampler->sample(rng, x);
			priorlogpdf = prior.logpdf(x);
			likelihoodlogpdf = likelihood.logpdf(x);
			const double lp = priorlogpdf + likelihoodlogpdf;
			if(math::isFinite(lp))
				break;
		}

		Require(i < maxtries, "Failed to initialize chain!");
		return x;
	}

	Vecd RandomInit::generate(double &likelihoodlogpdf, double &priorlogpdf) const {
		Vecd x;
		generate(x, likelihoodlogpdf, priorlogpdf);
		return x;
	}

	const Vecd &RandomInit::generate(Vecd &x) const {
		double likelihoodlogpdf, priorlogpdf;
		return generate(x, likelihoodlogpdf, priorlogpdf);
	}

	Vecd RandomInit::generate() const {
		double likelihoodlogpdf, priorlogpdf;
		return generate(likelihoodlogpdf, priorlogpdf);
	}
}
