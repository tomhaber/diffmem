/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/MESampler.hpp"
#include "DictCpp.hpp"
#include "Random.hpp"

namespace mcmc {

	PopulationDistribution::PopulationDistribution(const MEModel & model,
		Distribution & prior, Matrixd & etas) : model(model), prior(prior), etas(etas) {

		const int ncoeffs = Cholesky<Matrixd>::numberOfCoefficients(model.numberOfRandom());
		const int N = model.numberOfBetas() + ncoeffs + model.numberOfTaus();
		Require(N == prior.numberOfDimensions(), "number of parameters of model differs from prior");
	}

	int PopulationDistribution::numberOfDimensions() const {
		return numberOfPopulationDimensions();
	}

	double PopulationDistribution::logpdf(ConstRefVecd & x) const {
		auto beta = x.head( model.numberOfBetas() );
		auto tau = x.tail( model.numberOfTaus() );

		Cholesky<Matrixd> chol_omega;
		const int ncoeffs = Cholesky<Matrixd>::numberOfCoefficients(model.numberOfRandom());
		chol_omega.fromCoefficients( x.segment(model.numberOfBetas(), ncoeffs) );

		double lp = prior.logpdf(x);
		if( math::isNegInf(lp) )
			return lp;

		lp += model.evaluateLikelihood(beta, etas, chol_omega, tau);
		return lp;
	}

	double PopulationDistribution::logpdf(ConstRefVecd & x, RefVecd grad) const {
		notYetImplemented(__PRETTY_FUNCTION__);
	}

	int PopulationDistribution::numberOfPopulationDimensions() const {
		return model.numberOfBetas()
				+ Cholesky<Matrixd>::numberOfCoefficients(model.numberOfRandom())
				+ model.numberOfTaus();
	}

	int PopulationDistribution::numberOfIndividualDimensions() const {
		return model.numberOfIndividuals() * model.numberOfRandom();
	}

	MESampler::MESampler(const MEModel &model, Distribution &prior, size_t thinning)
			: model(model), distribution(model, prior, etas), thinning(thinning) {
		etas.resize(model.numberOfRandom(), model.numberOfIndividuals());
		etas.setZero();

		reset(model.betaMasked(), model.cholOmega(), model.tau());

		s.resize( distribution.numberOfIndividualDimensions() + distribution.numberOfDimensions() );
		pack(etas, beta, chol_omega, tau, s);

		condme = model.createCondDistribution();
		hmc = std::make_unique<MHSampler>(distribution, s.tail(distribution.numberOfDimensions()));

		sampler = std::make_unique<mcmc::ConditionalMESampler>(*condme,
			DictCpp{"type", "MH", "proposal", "MH", "always_adaptive", 1}
		);
	}

	void MESampler::reset(ConstRefVecd & x) {
		unpack(x, beta, chol_omega, tau);
	}

	void MESampler::reset() {
		model.updateCondDistribution(condme.get(), beta, chol_omega, tau);
		hmc->reset();
	}

	void MESampler::warmup(Random & rng, size_t nwarmup) {
		bool old = hmc->setAdaptive(true);

		for(size_t i = 0; i < nwarmup; ++i)
			update(rng);

		hmc->setAdaptive(old);
	}

	int MESampler::dimensions() const {
		const int M = distribution.numberOfIndividualDimensions();
		const int N = distribution.numberOfPopulationDimensions();
		return M+N;
	}

	void MESampler::sample(Random & rng, RefMatrixd samples) {
		const int nsamples = samples.cols();
		samples.resize(dimensions(), nsamples);

		size_t nextTrace = 0;
		for(size_t i = 0; i < nsamples; ++i) {
			if( nextTrace == i && trace ) {
				nextTrace = i + trace(SamplingValues(i, s, hmc->currentPosterior()));
				if( nextTrace == i )
					break;
			}

			update(rng);
			samples.col(i) = s;
		}
	}

	void MESampler::update(Random & rng) {
		MapVecd samples(etas.data(), condme->numberOfDimensions());
		model.updateCondDistribution(condme.get(), beta, chol_omega, tau);
		sampler->reset(samples);
		sampler->sample(rng, samples, thinning);

		hmc->reset(s.tail(distribution.numberOfDimensions()));
		const Vecd & x = hmc->sample(rng);
		auto new_beta = x.head(beta.size());
		tau = x.tail(tau.size() );
		chol_omega.fromCoefficients( x.segment(beta.size(), chol_omega.numberOfCoefficients()) );

		pack(etas, new_beta, chol_omega, tau, s);

		model.updateEta(beta, new_beta, etas);
		beta = new_beta;
	}

}
