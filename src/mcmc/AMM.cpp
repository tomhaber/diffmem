/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/AMM.hpp"
#include "mcmc/Chain.hpp"
#include "Distribution.hpp"
#include "mcmc/ProposalFactory.hpp"
#include "stats/mvnormal.hpp"

namespace mcmc {

	AMM::AMM(const Distribution &target, int adaptive, int periodicity)
			: Proposal(target),
			  scaleF(2.381204 * 2.381204 / target.numberOfDimensions()),
			  adaptive(adaptive),
			  periodicity(periodicity),
			  epsilon(1e-5) {
		Require(adaptive > 1);
		const int N = target.numberOfDimensions();
		mean = Vecd::Zero(N);
		varCov = scaleF * Matrixd::Identity(N, N);
		R.set( std::sqrt(scaleF) * Matrixd::Identity(N, N) );
	}

	AMM::AMM(const Distribution &target, double scaleF, int adaptive, int periodicity)
			: Proposal(target),
			  scaleF(scaleF),
			  adaptive(adaptive),
			  periodicity(periodicity),
			  epsilon(1e-5) {
		Require(adaptive > 1);
		const int N = target.numberOfDimensions();
		mean = Vecd::Zero(N);
		varCov = scaleF * Matrixd::Identity(N, N);
		R.set( std::sqrt(scaleF) * Matrixd::Identity(N, N) );
	}

	AMM::AMM(const Distribution & target, const Dict & dict) : Proposal(target) {
		if( ! dict.exists("scaleF") ) {
			scaleF = (2.381204 * 2.381204 / target.numberOfDimensions());
		} else {
			scaleF = dict.getNumeric("scaleF");
		}

		adaptive = dict.getIntegerDefault("adaptive", 20);
		periodicity = dict.getIntegerDefault("periodicity", 50);
		epsilon = dict.getNumericDefault("epsilon", 1e-5);

		Require(adaptive > 1);
		const int N = target.numberOfDimensions();
		mean = Vecd::Zero(N);
		varCov = scaleF * Matrixd::Identity(N, N);
		R.set( std::sqrt(scaleF) * Matrixd::Identity(N, N) );
	}

	std::string AMM::name() const {
		return "Adaptive Mixture-Metropolis";
	}

	std::unique_ptr<Proposal> AMM::clone(const Distribution & target) const {
		return std::make_unique<AMM>(target, scaleF, adaptive, periodicity);
	}

	void AMM::generate(const ProposalState &current, ProposalState &candidate,
			Random &rng, double *logProposalRatio) {
		if( logProposalRatio != nullptr )
			*logProposalRatio = kSymmetricProposalRatio;

		stats::MVNormal::sample(rng, current.position(), R, candidate.set());
	}

	void AMM::adapt(const Chain & chain) {
		if( chain.nattempts() > adaptive ) {
			const Vecd & X = chain.position();
			const int N = X.size();
			const double t = chain.nattempts();

			Vecd mean_old = mean;
			mean = mean + (X - mean) / t;

			varCov *= ((t - 2) / (t - 1));
			varCov.noalias() += (scaleF / (t - 1)) * ((X - mean_old) * (X - mean).transpose());
			varCov.noalias() += (scaleF / (t - 1)) * epsilon * Matrixd::Identity(N, N);

			if( (chain.nattempts() % periodicity) == 0 ) {
				R.compute(varCov);
				if(R.info() != Eigen::Success)
					R.set( std::sqrt(scaleF) * Matrixd::Identity(N, N) );
			}
		}
	}

}

REGISTER_PROPOSAL(AMM, mcmc::AMM);
