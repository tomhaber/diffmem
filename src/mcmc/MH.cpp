/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/MH.hpp"
#include "mcmc/Chain.hpp"
#include "mcmc/ProposalFactory.hpp"
#include "stats/mvnormal.hpp"

namespace mcmc {

	MH::MH(const Distribution & target, const Dict & dict) : Proposal(target) {
		stepSize = dict.getNumericDefault("stepSize", 0.05);
		lowerRate = dict.getNumericDefault("lowerRate", .2);
		upperRate = dict.getNumericDefault("upperRate", .5);
		adaptRate = dict.getIntegerDefault("adaptRate", 50);
	}

	std::string MH::name() const {
		return "Metropolis-Hastings";
	}

	std::unique_ptr<Proposal> MH::clone(const Distribution & target) const {
		return std::make_unique<MH>(target, stepSize, lowerRate, upperRate, adaptRate);
	}

	void MH::generate(const ProposalState & current, ProposalState & candidate, Random & rng, double *logProposalRatio) {
		if( logProposalRatio != nullptr )
			*logProposalRatio = kSymmetricProposalRatio;

		stats::MVNormal::sample(rng, current.position(), stepSize, candidate.set());
	}

	void MH::adapt(const Chain & chain) {
		if( (adaptRate != 0) && ((chain.nattempts() % adaptRate) == 0) ) {
			const double rate = chain.acceptanceRatio();
			if( rate < lowerRate )
				stepSize *= 0.8;
			else if( rate > upperRate )
				stepSize *= 1.2;
		}
	}

}

REGISTER_PROPOSAL(MH, mcmc::MH);
