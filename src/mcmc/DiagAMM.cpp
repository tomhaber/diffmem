/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/DiagAMM.hpp"
#include "mcmc/Chain.hpp"
#include "Distribution.hpp"
#include "mcmc/ProposalFactory.hpp"
#include "stats/mvnormal.hpp"

namespace mcmc {

	DiagAMM::DiagAMM(const Distribution &target, int adaptive, int periodicity, double epsilon)
			: Proposal(target),
			  scaleF(2.381204 * 2.381204 / target.numberOfDimensions()),
			  adaptive(adaptive),
			  periodicity(periodicity),
			  epsilon(epsilon) {

		const int N = target.numberOfDimensions();
		mean = Vecd::Zero(N);
		varCov.diagonal() = scaleF * Vecd::Ones(N);
		R = varCov;
	}

	DiagAMM::DiagAMM(const Distribution &target, double scaleF, int adaptive, int periodicity, double epsilon)
			: Proposal(target),
			  scaleF(scaleF),
			  adaptive(adaptive),
			  periodicity(periodicity),
			  epsilon(epsilon) {

		const int N = target.numberOfDimensions();
		mean = Vecd::Zero(N);
		varCov.diagonal() = scaleF * Vecd::Ones(N);
		R = varCov;
	}

	DiagAMM::DiagAMM(const Distribution & target, const Dict & dict) : Proposal(target) {
		if( ! dict.exists("scaleF") ) {
			scaleF = (2.381204 * 2.381204 / target.numberOfDimensions());
		} else {
			scaleF = dict.getNumeric("scaleF");
		}

		adaptive = dict.getIntegerDefault("adaptive", 20);
		periodicity = dict.getIntegerDefault("periodicity", 50);
		epsilon = dict.getNumericDefault("epsilon", 1e-5);

		const int N = target.numberOfDimensions();
		mean = Vecd::Zero(N);
		varCov.diagonal() = scaleF * Vecd::Ones(N);
		R = varCov;
	}

	std::string DiagAMM::name() const {
		return "Diagonal Adaptive Mixture-Metropolis";
	}

	std::unique_ptr<Proposal> DiagAMM::clone(const Distribution & target) const {
		return std::make_unique<DiagAMM>(target, scaleF, adaptive, periodicity);
	}

	void DiagAMM::generate(const ProposalState &current, ProposalState &candidate,
			Random &rng, double *logProposalRatio) {
		if( logProposalRatio != nullptr )
			*logProposalRatio = kSymmetricProposalRatio;

		stats::MVNormal::sample(rng, current.position(), R, candidate.set());
	}

	void DiagAMM::adapt(const Chain & chain) {
		if( chain.nattempts() > adaptive ) {
			const Vecd & X = chain.position();
			const double t = chain.nattempts() - adaptive + 2;
			const int N = X.size();

			Vecd mean_old = mean;
			mean = mean + (X - mean) / t;

			varCov.diagonal() *= ((t - 2) / (t - 1));
			varCov.diagonal() += (scaleF / t) * (X - mean_old).cwiseProduct(X - mean);
			varCov.diagonal() += (scaleF / t) * epsilon * Vecd::Ones(N);

			if( (chain.nattempts() % periodicity) == 0 )
				R = varCov;
		}
	}

}

REGISTER_PROPOSAL(DiagAMM, mcmc::DiagAMM);
