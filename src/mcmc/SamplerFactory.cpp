/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/SamplerFactory.hpp"
#include "Distribution.hpp"
#include "PluginManager.hpp"

namespace mcmc {
	SamplerFactory::SamplerFactory() {
		PluginManager::instance();
	}

	SamplerFactory & SamplerFactory::instance() {
		static SamplerFactory inst;
		return inst;
	}

	SamplerFactory::SamplerMap::iterator SamplerFactory::registerSampler(const std::string & name, SamplerConstr constructor) {
		auto r = samplers.insert( {name, constructor} );
		return r.second ? r.first : samplers.end();
	}

	SamplerFactory::SamplerMap::iterator SamplerFactory::registerSampler(std::string && name, SamplerConstr && constructor) {
		auto r = samplers.emplace( std::move(name), std::move(constructor) );
		return r.second ? r.first : samplers.end();
	}

	void SamplerFactory::unregisterSampler(SamplerFactory::SamplerMap::iterator it) {
		if( it != samplers.end() )
			samplers.erase(it);
	}

	std::unique_ptr<Sampler> SamplerFactory::createSampler(const std::string & name,
			const std::shared_ptr<const Distribution> &distr, const Dict & dict) {
		auto it = samplers.find(name);
		if( it == samplers.end() )
			throwException<std::invalid_argument>("Unknown sampler:", name);

		auto constr = it->second;
		return constr.shared(distr, dict);
	}

	std::unique_ptr<Sampler> SamplerFactory::createSampler(const std::string & name,
			const Distribution &distr, const Dict & dict) {
		auto it = samplers.find(name);
		if( it == samplers.end() )
			throwException<std::invalid_argument>("Unknown sampler:", name);

		auto constr = it->second;
		return constr.ref(distr, dict);
	}
}
