/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/ConditionalMESampler.hpp"
#include "ConditionalMEDistribution.hpp"
#include "Dict.hpp"
#include "literals.hpp"
#include "mcmc/SamplerFactory.hpp"
#include "Random.hpp"
#include <tbb/parallel_for.h>

namespace mcmc {

	ConditionalMESampler::ConditionalMESampler(const ConditionalMEDistribution &distribution, const Dict &dict)
			: numDims(distribution.numberOfIndividualDimensions()) {
		std::string type = dict.getStringDefault("type", "MH");

		samplers.resize(distribution.numberOfIndividuals());
		tbb::parallel_for(0, distribution.numberOfIndividuals(),
			[this, &distribution, &dict, &type](int i) {
				samplers[i] = SamplerFactory::instance().createSampler(type, distribution[i], dict);
			}
		);
	}

	void ConditionalMESampler::reset() {
		tbb::parallel_for(0_z, samplers.size(),
			[this](size_t id) {
				samplers[id]->reset();
		});
	}

	void ConditionalMESampler::reset(ConstRefVecd & initial) {
		const auto numberOfIndividuals = samplers.size();
		Require(initial.size() == numDims*numberOfIndividuals);
		ConstMapMatrixd etas(initial.data(), numDims, numberOfIndividuals);

		tbb::parallel_for(0_z, samplers.size(),
			[this, &etas](size_t id) {
				samplers[id]->reset(etas.col(id));
		});
	}

	void ConditionalMESampler::warmup(Random &rng, size_t nwarmup) {
		auto prng = rng.split(samplers.size());
		tbb::parallel_for(0_z, samplers.size(),
			[&](size_t id) {
				Random rng_id = prng[id];
				samplers[id]->warmup(rng_id, nwarmup);
			}
		);
	}

	void ConditionalMESampler::sample(Random &rng, RefMatrixd samples) {
		Require(samples.rows() == numDims*samplers.size());

		auto prng = rng.split(samplers.size());
		tbb::parallel_for(0_z, samplers.size(),
			[&](size_t id) {
				Random rng_id = prng[id];
				samplers[id]->sample(rng_id, samples.block(id*numDims, 0, numDims, samples.cols()));
			}
		);
	}

	void ConditionalMESampler::sample(Random &rng, RefMatrixd samples, size_t nwarmup) {
		Require(samples.rows() == numDims*samplers.size());

		auto prng = rng.split(samplers.size());
		tbb::parallel_for(0_z, samplers.size(),
			[&](size_t id) {
				Random rng_id = prng[id];
				samplers[id]->warmup(rng_id, nwarmup);
				samplers[id]->sample(rng_id, samples.block(id*numDims, 0, numDims, samples.cols()));
			}
		);
	}
}
