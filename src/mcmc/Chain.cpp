/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/Chain.hpp"
#include "mcmc/Proposal.hpp"
#include "Distribution.hpp"
#include "math/number.hpp"
#include "mcmc/RandomInit.hpp"

namespace mcmc {

	Chain::Chain(std::unique_ptr<Proposal> && proposal)
		: proposal(std::move(proposal)), attempts(0), accepted(0) {
	}

	void Chain::initialize(const RandomInit & init) {
		Vecd & s = state.set();
		init.generate( s );
		proposal->evaluate(state, s);
	}

	void Chain::initialize(ConstRefVecd & x) {
		proposal->evaluate(state, x);
	}

	const Vecd & Chain::generateCandidate(Random & rng, double *logProposalRatio) {
		attempts++;
		proposal->generate(state, cand, rng, logProposalRatio);
		return cand.position();
	}

	double Chain::evaluateCandidate() {
		return proposal->evaluate(cand);
	}

	double Chain::logProposalRatio() {
		return proposal->logProposalRatio(state, cand);
	}

	void Chain::accept(const Vecd & x) {
		accepted++;
		attempts++;
		proposal->evaluate(state, x);
	}

	void Chain::adapt() {
		proposal->adapt(*this);
	}

}
