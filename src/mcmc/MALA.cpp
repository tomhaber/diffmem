/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/MALA.hpp"
#include "mcmc/Chain.hpp"
#include "Distribution.hpp"
#include "mcmc/ProposalFactory.hpp"
#include "stats/mvnormal.hpp"

namespace mcmc {

	MALA::MALA(const Distribution & target, const Dict & dict) : Proposal(target) {
		stepSize = dict.getNumericDefault("stepSize", 0.0118);
		truncation = dict.getNumericDefault("truncation", 1e5);
		lowerRate = dict.getNumericDefault("lowerRate", .4);
		upperRate = dict.getNumericDefault("upperRate", .7);
		adaptRate = dict.getIntegerDefault("adaptRate", 50);
	}

	std::string MALA::name() const {
		return "Metropolis Adjusted Langevin Algorithm";
	}

	std::unique_ptr<Proposal> MALA::clone(const Distribution & target) const {
		return std::make_unique<MALA>(target, stepSize, lowerRate, upperRate, adaptRate);
	}

	double MALA::evaluate(ProposalState & state) {
		return evaluateGradient(state);
	}

	void MALA::generate(const ProposalState & current, ProposalState & candidate, Random & rng, double *logProposalRatio) {
		const Vecd & grad = current.gradient();
		const double scale = (truncation / std::max(truncation, grad.norm()));
		Vecd mu = current.position() + 0.5 * scale * (stepSize*stepSize) * grad;
		stats::MVNormal::sample(rng, mu, stepSize, candidate.set());

		if( logProposalRatio != nullptr ) {
			evaluateGradient(candidate);

			// Calculate new given old
			const double prob_NewGivenOld = stats::MVNormal::logpdf<false>(candidate.position(), mu, stepSize);

			// Calculate old given new
			{
				const Vecd & grad = candidate.gradient();
				const double scale = (truncation / std::max(truncation, grad.norm()));
				mu = candidate.position() + 0.5 * scale * (stepSize*stepSize) * grad;
			}
			const double prob_OldGivenNew = stats::MVNormal::logpdf<false>(current.position(), mu, stepSize);

			*logProposalRatio = prob_OldGivenNew - prob_NewGivenOld;
		}
	}

	double MALA::logProposalRatio(const ProposalState & current, const ProposalState & candidate) const {
		Require( candidate.isEvaluated() && current.isEvaluated(), "both current and candidate states must be evaluated");
		Vecd mu;

		// Calculate new given old
		{
			const Vecd & grad = current.gradient();
			const double scale = (truncation / std::max(truncation, grad.norm()));
			mu = current.position() + 0.5 * scale * (stepSize*stepSize) * grad;
		}
		const double prob_NewGivenOld = stats::MVNormal::logpdf<false>(candidate.position(), mu, stepSize);

		// Calculate old given new
		{
			const Vecd & grad = candidate.gradient();
			const double scale = (truncation / std::max(truncation, grad.norm()));
			mu = candidate.position() + 0.5 * scale * (stepSize*stepSize) * grad;
		}
		const double prob_OldGivenNew = stats::MVNormal::logpdf<false>(current.position(), mu, stepSize);

		return prob_OldGivenNew - prob_NewGivenOld;
	}

	void MALA::adapt(const Chain & chain) {
		if( (chain.nattempts() % adaptRate) == 0 ) {
			const double rate = chain.acceptanceRatio();
			if( rate < lowerRate )
				stepSize *= 0.8;
			else if( rate > upperRate )
				stepSize *= 1.2;
		}
	}

}

REGISTER_PROPOSAL(MALA, mcmc::MALA);
