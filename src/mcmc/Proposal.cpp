/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/Proposal.hpp"
#include "Distribution.hpp"
#include "math/number.hpp"

namespace mcmc {

	void ProposalState::swap(ProposalState & other) {
		x.swap( other.x );
		std::swap(log_posterior, other.log_posterior);
		grad.swap( other.grad );
		H.swap( other.H );
	}

	double Proposal::logProposalRatio(const ProposalState & current, const ProposalState & cand) const {
		return kSymmetricProposalRatio;
	}

	double Proposal::evaluate(ProposalState & state, const Vecd & x) {
		state.set(x);
		return evaluate(state);
	}

	double Proposal::evaluate(ProposalState & state) {
		const double log_posterior = target.logpdf(state.x);
		state.log_posterior = log_posterior;
		return log_posterior;
	}

	double Proposal::evaluateGradient(ProposalState & state) {
		const int N = state.x.size();
		state.grad.resize(N);

		const double log_posterior = target.logpdf(state.x, state.grad);
		state.log_posterior = log_posterior;
		return log_posterior;
	}

	double Proposal::evaluateGradientHessian(ProposalState & state) {
		const int N = state.x.size();
		state.grad.resize(N);
		state.H.resize(N, N);

		const double log_posterior = target.logpdf(state.x, state.grad, state.H);
		state.log_posterior = log_posterior;
		return log_posterior;
	}

}
