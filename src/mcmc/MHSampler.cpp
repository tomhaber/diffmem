/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/MHSampler.hpp"
#include "mcmc/SamplerFactory.hpp"
#include "mcmc/SamplableAdaptor.hpp"
#include "mcmc/Proposal.hpp"
#include "mcmc/ProposalFactory.hpp"
#include "Distribution.hpp"
#include "Random.hpp"
#include "stats/mvnormal.hpp"
#include "stats/uniform.hpp"
#include "math/number.hpp"

namespace mcmc {

	static std::unique_ptr<mcmc::Proposal> createProposal(const Distribution &target, const Dict & dict) {
		std::string type = dict.getStringDefault("proposal", "MH");
		return ProposalFactory::instance().createProposal(type, target, dict);
	}

	MHSampler::MHSampler(const Distribution &target, const Dict & dict)
			: chain(createProposal(target, dict)) {
		adaptive = dict.getIntegerDefault("always_adaptive", 0) == 1;

		if( dict.exists("initial") )
			chain.initialize(dict.getNumericVector("initial"));
		else {
			chain.initialize(Vecd::Zero(target.numberOfDimensions()));
		}
	}

	MHSampler::MHSampler(const Distribution &target, const RandomInit & initial)
			: chain(std::make_unique<MH>(target)) {
		chain.initialize(initial);
	}

	MHSampler::MHSampler(std::unique_ptr<Proposal> && proposal, const RandomInit & initial) : chain(std::move(proposal)) {
		chain.initialize(initial);
	}

	const Vecd & MHSampler::sample(Random & rng, size_t nwarmup) {
		warmup(rng, nwarmup);
		update(rng);
		return chain.position();
	}

	void MHSampler::warmup(Random & rng, size_t nwarmup) {
		bool old = setAdaptive(true);

		for(size_t i = 0; i < nwarmup; ++i)
			update(rng);

		setAdaptive(old);
	}

	void MHSampler::reset(ConstRefVecd & initial) {
		chain.initialize(initial);
	}

	void MHSampler::reset() {
		chain.initialize( chain.position() );
	}

	void MHSampler::sample(Random & rng, RefMatrixd samples) {
		const int nsamples = samples.cols();
		const int DIM = chain.dimensions();
		samples.resize(DIM, nsamples);

		size_t nextTrace = 0;
		for(size_t i = 0; i < nsamples; ++i) {
			if( nextTrace == i && trace ) {
				nextTrace = i + trace(SamplingValues(i, chain.position(), chain.posterior()));
				if( nextTrace == i )
					break;
			}

			update(rng);
			samples.col(i) =  chain.position();
		}
	}

	void MHSampler::update(Random & rng) {
		double logProposalRatio = 0.0;
		chain.generateCandidate(rng, &logProposalRatio);

		// Evaluate the model at new parameters
		chain.evaluateCandidate();

		// Accept or reject according to ratio
		const double alpha = (chain.posteriorCandidate() - chain.posteriorCurrent()) + logProposalRatio;

		if( alpha > 0.0 || std::log(stats::Uniform::sample(rng)) < alpha)
			chain.accept();

		if( adaptive )
			chain.adapt();
	}
}

REGISTER_SAMPLER(MH, mcmc::MHSampler);

