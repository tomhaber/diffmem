/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/HamiltonianSampler.hpp"
#include "mcmc/SamplerFactory.hpp"
#include "mcmc/SamplableAdaptor.hpp"
#include "Random.hpp"
#include "math/number.hpp"
#include "Distribution.hpp"
#include "Dict.hpp"
#include "stats/normal.hpp"
#include "stats/uniform.hpp"
#include <iostream>

using N = stats::Normal;
using U = stats::Uniform;

namespace mcmc {

	HamiltonianSampler::HamiltonianSampler(const Distribution &target, const Dict &dict) : target(target) {
		epsilon = dict.getNumeric("epsilon");
		L = dict.getInteger("L");
		if( dict.exists("initial") )
			reset(dict.getNumericVector("initial"));
		else {
			current_q = Vecd::Zero(target.numberOfDimensions());
			reset(current_q);
		}
	}

	const Vecd & HamiltonianSampler::sample(Random & rng, size_t nwarmup) {
		for(size_t j = 0; j < nwarmup + 1; ++j)
			update(rng);

		return current_q;
	}

	void HamiltonianSampler::evaluateGradient(const Vecd & q, Vecd & grad) {
		grad.resize( q.size() );
		target.logpdf(q, grad);
	}

	double HamiltonianSampler::evaluatePosteriorAndGradient(const Vecd & q, Vecd & grad) {
		grad.resize( q.size() );
		return target.logpdf(q, grad);
	}

	void HamiltonianSampler::reset() {
		reset(current_q);
	}

	void HamiltonianSampler::reset(ConstRefVecd & initial) {
		Require( IMPLIES(current_q.size() != 0, initial.size() == current_q.size()), "reset location has different number of parameters" );

		current_q = initial;
		current_U = evaluatePosteriorAndGradient(current_q, current_grad);
		if( ! math::isFinite(current_U) || any(current_grad, [](double x){ return ! math::isFinite(x); }) )
			throw std::invalid_argument("Trying to initialize HamiltonianSampler with bad position");
	}

	void HamiltonianSampler::warmup(Random & rng, size_t nwarmup) {
		for(size_t j = 0; j < nwarmup; ++j)
			update(rng);
	}

	void HamiltonianSampler::sample(Random & rng, RefMatrixd samples) {
		const int nsamples = samples.cols();
		const int DIM = current_q.size();
		samples.resize(DIM, nsamples);

		size_t nextTrace = 0;
		for(size_t i = 0; i < nsamples; ++i) {
			if( nextTrace == i && trace ) {
				nextTrace = i + trace(SamplingValues(i, current_q, current_U));
				if( nextTrace == i )
					break;
			}

			update(rng);
			samples.col(i) = current_q;
		}
	}

	void HamiltonianSampler::update(Random & rng) {
		// initialize momentum with independent normal variates
		p = generate_random<Vecd>(N::generator(rng), current_q.size());
		q = current_q;

		const double current_K = 0.5 * p.squaredNorm();

		// Make a half step for momentum at the beginning
		p += 0.5 * epsilon * current_grad;

		try {
			// Alternate full steps for position and momentum
			for(int i = 0; i < L; ++i) {
				// Make a full step for the position
				q += epsilon * p;

				if( !math::isFinite( evaluatePosteriorAndGradient(q, grad) ) )
					return;

				// Make a full step for the momentum, except at end of trajectory
				if( i != L )
					p += epsilon * grad;
			}

			double proposed_U = evaluatePosteriorAndGradient(q, grad);

			// Make a half step for momentum at the end.
			p += 0.5 * epsilon * grad;

			const double proposed_K = 0.5 * p.squaredNorm();

			// Accept or reject according to ratio
			const double alpha = proposed_U - current_U + current_K - proposed_K;
			if( alpha > 0.0 || std::log(U::sample(rng)) < alpha) {
				current_q = q;
				current_U = proposed_U;
				current_grad = grad;
			}
		} catch( std::domain_error & e ) {
			std::cerr << e.what() << std::endl;
		}
	}

}

REGISTER_SAMPLER(Hamiltonian, mcmc::HamiltonianSampler);
