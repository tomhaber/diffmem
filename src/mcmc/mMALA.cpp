/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcmc/mMALA.hpp"
#include "mcmc/Chain.hpp"
#include "Distribution.hpp"
#include "mcmc/ProposalFactory.hpp"
#include "Random.hpp"
#include "stats/mvnormal.hpp"
#include "stats/normal.hpp"

using N = stats::Normal;

namespace mcmc {

	mMALA::mMALA(const Distribution & target, const Dict & dict) : Proposal(target) {
		stepSize = dict.getNumericDefault("stepSize", 0.0118);
		epsilon = dict.getNumericDefault("epsilon", 1e-5);
		minStepSize = dict.getNumericDefault("minStepSize", 0.001);
		lowerRate = dict.getNumericDefault("lowerRate", .4);
		upperRate = dict.getNumericDefault("upperRate", .7);
		adaptRate = dict.getIntegerDefault("adaptRate", 50);
	}

	std::string mMALA::name() const {
		return "manifold Metropolis Adjusted Langevin Algorithm";
	}

	std::unique_ptr<Proposal> mMALA::clone(const Distribution & target) const {
		return std::make_unique<mMALA>(target, stepSize, minStepSize, epsilon, lowerRate, upperRate, adaptRate);
	}

	double mMALA::evaluate(ProposalState & state) {
		if( ! state.isEvaluated() )
			evaluateGradientHessian(state);
		return state.posterior();
	}

	void mMALA::generate(const ProposalState & current, ProposalState & candidate, Random & rng, double *logProposalRatio) {
		const double minusOneOverStepSize2 = -1.0 / (stepSize*stepSize);

		const int N = current.position().size();
		Cholesky<Matrixd> G(minusOneOverStepSize2 * (current.hessian() + epsilon * Matrixd::Identity(N, N)));
		Require( G.info() == Eigen::Success, "Cholesky decomposition of G matrix failed" );

		Vecd grad = current.gradient();
		G.solveInPlace( grad );
		Vecd mu = current.position() + 0.5 * grad;

		Vecd v = generate_random<Vecd>(N::generator(rng), G.rows());
		G.solveInPlace(v);
		candidate.set() = mu + v;

		if( logProposalRatio != nullptr ) {
			evaluateGradientHessian(candidate);

			// Calculate new given old
			const double prob_NewGivenOld = stats::MVNormal::logpdf_inv<false>(v, G);

			// Calculate old given new
			G.compute(minusOneOverStepSize2 * (candidate.hessian() + epsilon * Matrixd::Identity(N,N)));
			Require( G.info() == Eigen::Success, "Cholesky decomposition of G matrix failed" );
			grad.noalias() = G.solve( candidate.gradient() );
			mu = candidate.position() + 0.5 * grad;
			const double prob_OldGivenNew = stats::MVNormal::logpdf_inv<false>(current.position(), mu, G);

			*logProposalRatio = prob_OldGivenNew - prob_NewGivenOld;
		}
	}

	double mMALA::logProposalRatio(const ProposalState & current, const ProposalState & candidate) const {
		Require( candidate.isEvaluated() && current.isEvaluated(), "both current and candidate states must be evaluated");
		const double minusOneOverStepSize2 = -1.0 / (stepSize*stepSize);

		const int N = current.position().size();
		Vecd mu;
		Cholesky<Matrixd> G;
		Vecd grad;

		// Calculate new given old
		G.compute(minusOneOverStepSize2 * (current.hessian() + epsilon * Matrixd::Identity(N,N)));
		Require( G.info() == Eigen::Success, "Cholesky decomposition of G matrix failed" );
		grad.noalias() = G.solve( current.gradient() );
		mu = current.position() + 0.5 * grad;
		const double prob_NewGivenOld = stats::MVNormal::logpdf_inv<false>(candidate.position(), mu, G);

		// Calculate old given new
		G.compute(minusOneOverStepSize2 * (candidate.hessian() + epsilon * Matrixd::Identity(N,N)));
		Require( G.info() == Eigen::Success, "Cholesky decomposition of G matrix failed" );
		grad.noalias() = G.solve( candidate.gradient() );
		mu = candidate.position() + 0.5 * grad;
		const double prob_OldGivenNew = stats::MVNormal::logpdf_inv<false>(current.position(), mu, G);

		return prob_OldGivenNew - prob_NewGivenOld;
	}

	void mMALA::adapt(const Chain & chain) {
		if( (adaptRate != 0) && ((chain.nattempts() % adaptRate) == 0) ) {
			const double rate = chain.acceptanceRatio();
			if( rate < lowerRate )
				stepSize *= 0.8;
			else if( rate > upperRate )
				stepSize *= 1.2;

			if( stepSize < minStepSize )
				stepSize = minStepSize;
		}
	}

}

REGISTER_PROPOSAL(mMALA, mcmc::mMALA);
