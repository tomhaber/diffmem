/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <DictCpp.hpp>

#include "Dict.hpp"

bool DictCpp::exists(const std::string & name) const {
	return numericVectorMap.find(name) != numericVectorMap.end() ||
		numericMatrixMap.find(name) != numericMatrixMap.end() ||
		integerVectorMap.find(name) != integerVectorMap.end() ||
		integerMatrixMap.find(name) != integerMatrixMap.end() ||
		booleanVectorMap.find(name) != booleanVectorMap.end() ||
		booleanMatrixMap.find(name) != booleanMatrixMap.end() ||
		stringMap.find(name) != stringMap.end() ||
		integerMap.find(name) != integerMap.end() ||
		numericMap.find(name) != numericMap.end() ||
		dictMap.find(name) != dictMap.end();
}

Vecd DictCpp::getNumericVector(const std::string & name) const {
	if (numericVectorMap.find(name) == numericVectorMap.end())
		throw missingkey_error(name);

	return numericVectorMap.find(name)->second;
}

Matrixd DictCpp::getNumericMatrix(const std::string & name) const {
	if (numericMatrixMap.find(name) == numericMatrixMap.end())
		throw missingkey_error(name);

	return numericMatrixMap.find(name)->second;
}

Veci DictCpp::getIntegerVector(const std::string & name) const {
	if (integerVectorMap.find(name) == integerVectorMap.end())
		throw missingkey_error(name);

	return integerVectorMap.find(name)->second;
}

Matrixi DictCpp::getIntegerMatrix(const std::string & name) const {
	if (integerMatrixMap.find(name) == integerMatrixMap.end())
		throw missingkey_error(name);

	return integerMatrixMap.find(name)->second;
}

Vecb DictCpp::getBooleanVector(const std::string & name) const {
	if (booleanVectorMap.find(name) == booleanVectorMap.end())
		throw missingkey_error(name);

	return booleanVectorMap.find(name)->second;
}

Matrixb DictCpp::getBooleanMatrix(const std::string & name) const {
	if (booleanMatrixMap.find(name) == booleanMatrixMap.end())
		throw missingkey_error(name);

	return booleanMatrixMap.find(name)->second;
}

std::string DictCpp::getString(const std::string & name) const {
	if (stringMap.find(name) == stringMap.end())
		throw missingkey_error(name);

	return stringMap.find(name)->second;
}

int DictCpp::getInteger(const std::string & name) const {
	if (integerMap.find(name) == integerMap.end())
		throw missingkey_error(name);

	return integerMap.find(name)->second;
}

double DictCpp::getNumeric(const std::string & name) const {
	if (numericMap.find(name) == numericMap.end())
		throw missingkey_error(name);

	return numericMap.find(name)->second;
}

std::shared_ptr<Dict> DictCpp::getDict(const std::string & name) const {
	if (dictMap.find(name) == dictMap.end())
		throw missingkey_error(name);

	return dictMap.find(name)->second;
}

void DictCpp::setNumericVector(const std::string & name, ConstRefVecd &value) {
	numericVectorMap[name] = value;
}

void DictCpp::setNumericMatrix(const std::string & name, ConstRefMatrixd &value) {
	numericMatrixMap[name] = value;
}

void DictCpp::setIntegerVector(const std::string & name, ConstRefVeci &value) {
	integerVectorMap[name] = value;
}

void DictCpp::setIntegerMatrix(const std::string & name, ConstRefMatrixi &value) {
	integerMatrixMap[name] = value;
}

void DictCpp::setBooleanVector(const std::string & name, ConstRefVecb &value) {
	booleanVectorMap[name] = value;
}

void DictCpp::setBooleanMatrix(const std::string & name, ConstRefMatrixb &value) {
	booleanMatrixMap[name] = value;
}

void DictCpp::setString(const std::string & name, const std::string &value) {
	stringMap[name] = value;
}

void DictCpp::setInteger(const std::string & name, int value) {
	integerMap[name] = value;
}

void DictCpp::setNumeric(const std::string & name, double value) {
	numericMap[name] = value;
}

void DictCpp::setDict(const std::string & name, std::shared_ptr<DictCpp>&& value) {
	dictMap[name] = std::move(value);
}

void DictCpp::set(const std::string & name, Vecd && value) {
	numericVectorMap[name] = std::move(value);
}

void DictCpp::set(const std::string & name, Matrixd && value) {
	numericMatrixMap[name] = std::move(value);
}

void DictCpp::set(const std::string & name, Veci && value) {
	integerVectorMap[name] = std::move(value);
}

void DictCpp::set(const std::string & name, Matrixi && value) {
	integerMatrixMap[name] = std::move(value);
}

void DictCpp::set(const std::string & name, Vecb && value) {
	booleanVectorMap[name] = std::move(value);
}

void DictCpp::set(const std::string & name, Matrixb && value) {
	booleanMatrixMap[name] = std::move(value);
}

void DictCpp::set(const std::string & name, std::string && value) {
	stringMap[name] = std::move(value);
}

void DictCpp::set(const std::string & name, const Vecd & value) {
	numericVectorMap[name] = value;
}

void DictCpp::set(const std::string & name, const Matrixd & value) {
	numericMatrixMap[name] = value;
}

void DictCpp::set(const std::string & name, const Veci & value) {
	integerVectorMap[name] = value;
}

void DictCpp::set(const std::string & name, const Matrixi & value) {
	integerMatrixMap[name] = value;
}

void DictCpp::set(const std::string & name, const Vecb & value) {
	booleanVectorMap[name] = value;
}

void DictCpp::set(const std::string & name, const Matrixb & value) {
	booleanMatrixMap[name] = value;
}

void DictCpp::set(const std::string & name, const std::string & value) {
	stringMap[name] = value;
}

void DictCpp::set(const std::string & name, int value) {
	integerMap[name] = value;
}

void DictCpp::set(const std::string & name, double value) {
	numericMap[name] = value;
}
