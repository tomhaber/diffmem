/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "StructuralModel.hpp"
#include "LikelihoodModel.hpp"
#include "FiniteDifference.hpp"
#include "LocalMatrix.hpp"

void StructuralModel::transphi(ConstRefVecd &psi, RefVecd phi) const {
	phi = psi;
}

void StructuralModel::transpsi(ConstRefVecd &phi, RefVecd psi) const {
	psi = phi;
}

void StructuralModel::dtranspsi(ConstRefVecd &phi, RefVecd dphi) const {
	dphi = Vecd::Ones(phi.size());
}

void StructuralModel::evalSens(ConstRefVecd &psi, ConstRefVecd &t, RefMatrixd s) const {
	const int N = numberOfObservations();
	const int M = t.size();
	const int numberOfSensitivities = N * psi.size();
	Assert(s.rows() == numberOfSensitivities + N && s.cols() == t.size());

	Matrixd u(N, M);
	evalU(psi, t, u);
	s.block(0, 0, N, M) = u;

	Matrixd tmp(N, M);
	Vecd psi_eps = psi;
	for(int i = 0; i < psi.size(); ++i) {
		const double eps = FiniteDifference::epsilon(psi[i]);

		psi_eps[i] += eps;
		evalU(psi_eps, t, tmp);
		psi_eps[i] = psi[i];

		s.block(N * (i + 1), 0, N, M) = (tmp - u) / eps;
	}
}

double StructuralModel::logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
		const LikelihoodModel &llm) const {

	LOCAL_VECTOR(Vecd, psi, phi.size());
	transpsi(phi, psi);

	Matrixd u(numberOfObservations(), t.size());
	evalU(psi, t, u);
	return llm.logpdf(tau, u);
}

double StructuralModel::logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
		const LikelihoodModel &llm, RefVecd grad) const {

	LOCAL_VECTOR(Vecd, psi, phi.size());
	transpsi(phi, psi);

	const int N = numberOfObservations();
	const int numberOfSensitivities = N * psi.size();
	Matrixd s(numberOfSensitivities + N, t.size());
	evalSens(psi, t, s);

	const auto u = s.block(0, 0, N, t.size());
	const auto sens = s.block(N, 0, numberOfSensitivities, t.size());
	llm.logpdfGrad(tau, u, sens, grad);

	LOCAL_VECTOR(Vecd, dpsi, phi.size());
	dtranspsi(phi, dpsi);
	grad = grad.cwiseProduct(dpsi);

	return llm.logpdf(tau, u);
}

double StructuralModel::logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
		const LikelihoodModel &llm, RefVecd grad, RefMatrixd hess) const {

	const int N = phi.size();
	LOCAL_VECTOR(Vecd, psi, N);
	transpsi(phi, psi);

	const int n = numberOfObservations();
	const int numberOfSensitivities = n * psi.size();
	Matrixd s(numberOfSensitivities + n, t.size());
	evalSens(psi, t, s);

	const auto u = s.block(0, 0, n, t.size());
	const auto sens = s.block(N, 0, numberOfSensitivities, t.size());
	llm.logpdfHess(tau, u, sens, grad, hess);

	LOCAL_VECTOR(Vecd, dpsi, N);
	dtranspsi(phi, dpsi);

	grad = grad.cwiseProduct(dpsi);
	for(int j = 0; j < N; ++j) {
		for(int k = j; k < N; ++k) {
			hess(k,j) = hess(j,k) = dpsi[k] * dpsi[j] * hess(k,j);
		}
	}

	return llm.logpdf(tau, u);
}

double StructuralModel::logpdfGradTau(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
		const LikelihoodModel &llm, RefVecd gradTau) const {

	LOCAL_VECTOR(Vecd, psi, phi.size());
	transpsi(phi, psi);

	Matrixd u(numberOfObservations(), t.size());
	evalU(psi, t, u);
	return llm.logpdfGradTau(tau, u, gradTau);
}

double StructuralModel::logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
		const PointwiseLikelihoodModel &ll) const {
	return logpdf(phi, tau, t, reinterpret_cast<const LikelihoodModel &>(ll));
}

double StructuralModel::logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
		const PointwiseLikelihoodModel &ll, RefVecd gradPhi) const {
	return logpdf(phi, tau, t, reinterpret_cast<const LikelihoodModel &>(ll), gradPhi);
}
double StructuralModel::logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
		const PointwiseLikelihoodModel &ll, RefVecd gradPhi, RefMatrixd hessPhi) const {
	return logpdf(phi, tau, t, reinterpret_cast<const LikelihoodModel &>(ll), gradPhi, hessPhi);
}
