/*
* Copyright (c) 2016, Hasselt University
* All rights reserved.

* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "ode/EulerODEIntegrator.hpp"

namespace ode {

	EulerODEIntegrator::EulerODEIntegrator(ODEBase & odeProblem) : odeProblem_(odeProblem), currentTime_(0.0), stepSize_(0.001){
		currentTime_ = 0;
		currentState_.resize(odeProblem.numberOfEquations());
	}

	void EulerODEIntegrator::setIC(const double initialTime) {
		currentTime_ = initialTime;

		MapVecd currentStateM(currentState_.data(), currentState_.size());
		odeProblem_.initial(currentStateM);
	}

	void EulerODEIntegrator::setStepSize(const double stepSize) {
		stepSize_ = stepSize;
	}

	void EulerODEIntegrator::integrateTo(const double tOut) {
		if (tOut == currentTime_) return;

		Vecd ddt(currentState_.size());

		auto nIterations = static_cast<int> (floor((tOut - currentTime_) / stepSize_) );
		for(int iter = 0; iter < nIterations; ++iter) {
			odeProblem_.ddt(currentTime_ + (iter - 1)*stepSize_, currentState_, ddt);
			currentState_ = currentState_ + stepSize_ * ddt;

		}
		currentTime_ = tOut;
	}

}
