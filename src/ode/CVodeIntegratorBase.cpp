/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cvodes/cvodes_dense.h>
#include <stdexcept>
#include "ode/CVodeIntegratorBase.hpp"

namespace ode {

	extern "C"
	int OdeRhs( double t, N_Vector y_, N_Vector ydot_, void * f_data ) {
		ODEBase *explicitOde = static_cast<ODEBase *>(f_data);
		MapVecd y(NV_DATA_S(y_), NV_LENGTH_S(y_));
		MapVecd ydot(NV_DATA_S(ydot_), NV_LENGTH_S(ydot_));

		try {
			explicitOde->ddt(t, y , ydot);
		} catch(...) {
			return 1;
		}

		return 0;
	}

	extern "C"
	int OdeJacobian(long int N, realtype t, N_Vector y_, N_Vector fy_,
			DlsMat Jac, void *jac_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3) {
		ODEBase *explicitOde = static_cast<ODEBase *>(jac_data);

		MapVecd y(NV_DATA_S(y_), NV_LENGTH_S(y_));
		MapVecd ydot(NV_DATA_S(fy_), NV_LENGTH_S(fy_));

		try {
			MapMatrixd J(Jac->data, Jac->M, Jac->N);
			explicitOde->jacobian( t, y, ydot, J);
		} catch(...) {
			return 1;
		}

		return 0;
	}

	extern "C"
	void SilentErrHandler(int error_code, const char *module, const char *function, char *msg, void *eh_data) {
	}

	void throwCVodeError(int flag, const char *msg) {
		switch( flag ) {
			case CV_ILL_INPUT:
			case CV_NO_MALLOC:
			case CV_MEM_NULL:
				throwException<std::logic_error>("CVODE failure {}: {}", flag, msg);

			case CV_MEM_FAIL:
				throw std::bad_alloc();

			default:
				throwException<model_error>("CVODE failure {}: {}", flag, msg);
		}
	}

	CVodeIntegratorBase::CVodeIntegratorBase()
	: cvodeMem_(0),
	  nvCurrentState_(0),
	  maxNumSteps_( 10000 ),
	  relTol_( 1.0e-10 ),
	  absTol_( 1.0e-12 ),
	  currentTime_( 0.0 ) {
	}

	CVodeIntegratorBase::~CVodeIntegratorBase() {
		if( nvCurrentState_ != 0 ) {
			N_VDestroy_Serial(nvCurrentState_);
			nvCurrentState_ = 0;
		}

		if( cvodeMem_ != 0 ) {
			CVodeFree( &cvodeMem_ );
			cvodeMem_ = 0;
		}
	}

	void CVodeIntegratorBase::create(int Neqs, bool stiff, bool functional) {
		const int iter = functional ? CV_FUNCTIONAL : CV_NEWTON;
		const int lmm = stiff ? CV_BDF : CV_ADAMS;
		cvodeMem_ = CVodeCreate( lmm, iter );

		if( cvodeMem_ == 0 )
			throw std::runtime_error( "CVodeCreate failed" );

		currentTime_ = 0;
		currentState_.resize(Neqs);
		nvCurrentState_ = N_VMake_Serial( currentState_.size(), &currentState_[0] );
		currentState_.setZero();
	}

	void CVodeIntegratorBase::initializeSettings() {
#ifdef NDEBUG
		CVodeSetErrHandlerFn(cvodeMem_, SilentErrHandler, 0);
#endif

		setTolerances( relTol_, absTol_ );

		throwIfCVodeError(CVodeSetInitStep( cvodeMem_, 0.0 ), "CVodeSetInitStep");
		throwIfCVodeError(CVodeSetMaxNumSteps( cvodeMem_, maxNumSteps_ ), "CVodeSetMaxNumSteps");
		throwIfCVodeError(CVodeSetMaxErrTestFails( cvodeMem_, 20 ), "CVodeSetMaxErrTestFails");
		throwIfCVodeError(CVodeSetMaxConvFails( cvodeMem_, 50 ), "CVodeSetMaxConvFails");
	}

	void CVodeIntegratorBase::initialize(ODEBase & odeProblem, bool functional) {
		create( odeProblem.numberOfEquations(), odeProblem.isStiff(), functional );

		throwIfCVodeError(CVodeInit( cvodeMem_, OdeRhs, currentTime_, nvCurrentState_ ), "CVodeInit");
		throwIfCVodeError(CVodeSetUserData( cvodeMem_, (void*) &odeProblem ), "CVodeSetUserData");

		initializeSettings();

		if( ! functional ) {
			throwIfCVodeError(CVDense( cvodeMem_, currentState_.size() ), "CVDense");

			if( odeProblem.hasJacobian() ) {
				int flag = CVDlsSetDenseJacFn(cvodeMem_, OdeJacobian);
				if( flag != CVDLS_SUCCESS ) {
					if( flag == CVDLS_MEM_FAIL )
						throw std::bad_alloc();

					throwException<std::logic_error>("CVODE failure ", flag, ": CVDlsSetDenseJacFn");
				}
			}
		}
	}

	void CVodeIntegratorBase::integrateTo( const double tOut ) {
		if( tOut == currentTime_ ) return;

		throwIfCVodeError(CVode( cvodeMem_, tOut, nvCurrentState_, &currentTime_, CV_NORMAL ), "CVode");
	}

	void CVodeIntegratorBase::setTolerances( double relTol, double absTol ) {
		relTol_ = relTol;
		absTol_ = absTol;

		throwIfCVodeError(CVodeSStolerances( cvodeMem_, relTol_, absTol_ ), "CVodeSStolerances");
	}

	void CVodeIntegratorBase::setMaxNumSteps(int maxNumSteps) {
		maxNumSteps_ = maxNumSteps;

		throwIfCVodeError(CVodeSetMaxNumSteps( cvodeMem_, maxNumSteps_ ), "CVodeSetMaxNumSteps");
	}

	int CVodeIntegratorBase::numSteps() const {
		long int nsteps;
		throwIfCVodeError(CVodeGetNumSteps(cvodeMem_, &nsteps), "CVodeGetNumSteps");
		return nsteps;
	}

	int CVodeIntegratorBase::numEvals() const {
		long int nevals;
		throwIfCVodeError(CVodeGetNumRhsEvals(cvodeMem_, &nevals), "CVodeGetNumRhsEvals");
		return nevals;
	}

	void CVodeIntegratorBase::setMaxStepSize(double hmax) {
		throwIfCVodeError(CVodeSetMaxStep(cvodeMem_, hmax), "CVodeSetMaxStep");
	}

	void CVodeIntegratorBase::setMinStepSize(double hmin) {
		throwIfCVodeError(CVodeSetMinStep(cvodeMem_, hmin), "CVodeSetMinStep");
	}

	void CVodeIntegratorBase::setInitStepSize(double hinit) {
		throwIfCVodeError(CVodeSetInitStep(cvodeMem_, hinit), "CVodeSetInitStep");
	}

	double CVodeIntegratorBase::lastStepSize() const {
		double step;
		throwIfCVodeError(CVodeGetLastStep(cvodeMem_, &step), "CVodeGetLastStep");
		return step;
	}

	void CVodeIntegratorBase::cvodeReInit(){
		throwIfCVodeError(CVodeReInit(cvodeMem_, currentTime_, nvCurrentState_), "CVodeReInit");
	}

	void CVodeIntegratorBase::reinit(const double initialTime, const Vecd & initialState) {
		if( currentState_.size() != initialState.size() )
			throw std::runtime_error("reinit vector has different size");

		currentTime_ = initialTime;
		currentState_ = initialState;

		cvodeReInit();
	}

#if 0
	void CheckJacobian() {
		for(int i = 0; i < N; ++i) {
			const double eps = 1e-6;
			N_VScale_Serial(1.0, y, tmp1);
			NV_Ith_S(tmp1, i) += eps;
			OdeRhs(t, tmp1, tmp2, jac_data);
			N_VLinearSum_Serial(-1.0/eps, fy, 1.0/eps, tmp2, tmp1);
			N_VPrint_Serial(tmp1);
		}
	}
#endif

}
