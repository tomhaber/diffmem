/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "ode/RungeKutta45.hpp"
#include <math/eps.hpp>

namespace ode {

	RungeKutta45::RungeKutta45(ODEBase & odeProblem)
		: odeProblem_( odeProblem ),
			relTol_( 1.0e-6 ),
			absTol_( 1.0e-9 ),
			currentTime_( 0.0 ),
			maxNumSteps_( 50000 ),
			stepSize_(0.0),
			hmin_(1e-30),
			hmax_(1e30),
			hinit_(0.0) {

		Require( !odeProblem.isStiff(), "RungeKutta45 cannot deal with stiff systems." );
		currentState_ = Vecd::Zero( odeProblem_.numberOfEquations() );
		tmp.resize(currentState_.size(), 7);
	}

	const double power = 1./5.;
	const double A[6] = { 1. / 5., 3. / 10., 4. / 5., 8. / 9., 1., 1. };
	const double B[42] = {
		   1. / 5., 0., 0., 0., 0., 0., 0.,
		   3. / 40., 9. / 40., 0., 0., 0., 0., 0.,
		   44. / 45., -56. / 15., 32. / 9., 0., 0., 0., 0.,
		   19372. / 6561., -25360. / 2187.0f, 64448. / 6561., -212. / 729., 0., 0., 0.,
		   9017. / 3168., -355. / 33., 46732. / 5247., 49. / 176., -5103. / 18656., 0., 0.,
		   35. / 384., 0., 500. / 1113., 125. / 192., -2187. / 6784., 11. / 84., 0.
	};
	const double E[7] = { 71. / 57600., 0., -71. / 16695., 71. / 1920., -17253. / 339200., 22. / 525., -1. / 40. };

	double RungeKutta45::estimateInitial_dt(double tspan, ConstRefVecd & f0) {
		// Compute an initial step size h using y'(t).
		const double threshold = absTol_ / relTol_;
		double rh = 0;
		for(int j = 0; j < odeProblem_.numberOfEquations(); ++j) {
			const double tmp = std::abs( f0[j] / std::max(std::abs(currentState_[j]), threshold) );
			if( tmp > rh ) rh = tmp;
		}

		rh /= (0.8 * std::pow(relTol_,power));
		return (tspan * rh > 1.0) ? 1.0 / rh : tspan;
	}

	void RungeKutta45::integrateTo( const double tOut ) {
		odeProblem_.ddt(currentTime_, currentState_, tmp.col(0));

		if( stepSize_ == 0.0 ) {
			 stepSize_ = (hinit_ == 0.0) ? estimateInitial_dt( tOut - currentTime_, tmp.col(0) ) : hinit_;
		}

		const double threshold = absTol_ / relTol_;
		const double hmax = std::max(hmax_, tOut - currentTime_);

		double err;
		double dt = stepSize_;
		while( currentTime_ < tOut ) {
			if( dt > hmax ) dt = hmax;

			// Stretch the step if within 10% of tOut - t
			if( 1.1 * dt >= (tOut - currentTime_) )
			  dt = tOut - currentTime_;

			double t = currentTime_;
			const double hmin = std::max(math::eps(t, tOut), hmin_);

			int nfailed = 0;
			while( true ) {
				if( dt < hmin ) dt = hmin;

				for(int i = 0; i < 6; ++i) {
					y = currentState_;
					for(int j = 0; j <= i; ++j)
						y += dt * B[i*7 + j] * tmp.col(j);

					odeProblem_.ddt(t + dt * A[i], y, tmp.col(i+1));
				}
				t = currentTime_ + dt;

				// Compute error
				err = 0;
				for(int j = 0; j < odeProblem_.numberOfEquations(); ++j) {
					double err_sum = 0.0;
					for(int i = 0; i <= 6; ++i)
						err_sum += E[i] * tmp(j, i);
					err_sum = std::abs(err_sum / std::max(std::max(std::abs(currentState_[j]),std::abs(y[j])),threshold));

					if( err_sum > err )
						err = err_sum;
				}
				err *= dt;

				if( err > relTol_ ) {
					nfailed++;
					if( dt <= hmin )
						throwException<model_error>("Stepsize too small: time {} hmin {}", dt, hmin);

					if( nfailed == 1 )
						dt = dt * std::max(0.1, 0.8* std::pow(relTol_/err, power));
					else
						dt = 0.5 * dt;
				} else {
					break;
				}

				if( nfailed > maxNumSteps_ )
					throwException<model_error>("Too many failures in RK45");
			}

			if( nfailed == 0 ) {
			  const double tmp = 1.25 * std::pow( (err/relTol_), power);
			  dt = ( tmp > 0.2 ) ? (dt / tmp) : (5.0 * dt);
			}

			currentTime_ = t;
			currentState_ = y;
			tmp.col(0) = tmp.col(6);
		 }

		stepSize_ = dt;
	}

	void RungeKutta45::reinit(const double initialTime, const Vecd & initialState) {
		currentTime_ = initialTime;
		currentState_ = initialState;
	}

	void RungeKutta45::setInitial(const double initialTime, const Vecd & initialState) {
		currentTime_ = initialTime;
		currentState_ = initialState;
	}

	void RungeKutta45::setIC(const double initialTime) {
		currentTime_ = initialTime;
		odeProblem_.initial( currentState_ );
	}

}
