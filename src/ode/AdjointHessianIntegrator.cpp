/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cvodes/cvodes_dense.h>
#include <stdexcept>
#include "ode/AdjointHessianIntegrator.hpp"

namespace ode {

	extern "C" int SensRhs(int Ns, realtype t, N_Vector y_, N_Vector ydot_,
			N_Vector *yS_, N_Vector *ySdot_, void *f_data, N_Vector tmp1,
			N_Vector tmp2);
	extern "C" int AdjRhs(double t, N_Vector y_, N_Vector yB_, N_Vector yBdot_,
			void * f_data);

	extern "C" void SilentErrHandler(int error_code, const char *module,
			const char *function, char *msg, void *eh_data);

	extern "C" int AdjSensJacobian(long int N, realtype t, N_Vector y_, N_Vector *yS, N_Vector yB_,
			N_Vector yBdot_, DlsMat Jac, void *jac_data, N_Vector tmp1,
			N_Vector tmp2, N_Vector tmp3){
		AdjointSensitivityODEBase* adjSensOde =
				static_cast<AdjointSensitivityODEBase *>(jac_data);

		MapVecd y(NV_DATA_S(y_), NV_LENGTH_S(y_));
		MapVecd yB(NV_DATA_S(yB_), NV_LENGTH_S(yB_));
		MapVecd yBdot(NV_DATA_S(yBdot_), NV_LENGTH_S(yBdot_));

		try {
			MapMatrixd J(Jac->data, Jac->M, Jac->N);
			adjSensOde->jacobian( t, y, yB, yBdot, J );
		} catch(...) {
			return 1;
		}

		return 0;
	};

	//typedef int (*CVQuadRhsFnBS)(realtype t, NVector y, NVector *yS, NVector yB, NVector qBdot, void *userdataB);
	extern "C" int QuadSensRhs(double t, N_Vector y_, N_Vector* yS_, N_Vector yB_,
			N_Vector qQdot_, void * f_data) {
		AdjointSensitivityODEBase* adjSensOde =
				static_cast<AdjointSensitivityODEBase *>(f_data);
		MapVecd y(NV_DATA_S(y_), NV_LENGTH_S(y_));
		MapVecd yB(NV_DATA_S(yB_), NV_LENGTH_S(yB_));
		MapVecd qQdot(NV_DATA_S(qQdot_), NV_LENGTH_S(qQdot_));

		int N = adjSensOde->numberOfEquations();
		int Ns = static_cast<int>( std::sqrt(NV_LENGTH_S(qQdot_)) );

		//TODO: we copy the sensitivities. how to optimize it?
		Matrixd yS(N, Ns);
		MapMatrixd ySM(yS.data(), yS.rows(), yS.cols());

		for(int i = 0; i < Ns; ++i) {
			MapVecd ySv(NV_DATA_S(yS_[i]), N);
			yS.col(i) = ySv;
		}

		try {
			adjSensOde->quadratureSens(t, y, ySM, yB, qQdot);
		} catch( ... ) {
			return 1;
		}

		return 0;
	}

	AdjointHessianIntegrator::AdjointHessianIntegrator(
			SensitivityODEBase & sensProblem, int Nd, Interpolation interpolation) :
					sensProblem_(sensProblem), interpolation(interpolation), Nnc_(1000), numOfCheckpoints_(0) {
		initialize();
	}

	AdjointHessianIntegrator::~AdjointHessianIntegrator() {
		clearAdjoint();
		N_VDestroyVectorArray(nvCurrentSens_, sensProblem_.numberOfParameters());
		CVodeSensFree(cvodeMem_);
	}

	void AdjointHessianIntegrator::initialize() {
		initializeSensitivity();
		initializeAdjoint();
	}

	void AdjointHessianIntegrator::initializeSensitivity() {
		CVodeIntegratorBase::initialize(sensProblem_);

		const int Ns = sensProblem_.numberOfParameters();
		currentSens_.resize(sensProblem_.numberOfEquations(), Ns);

		nvCurrentSens_ = N_VCloneEmptyVectorArray(Ns, nvCurrentState_);
		for(int j = 0; j < Ns; j++)
			N_VSetArrayPointer_Serial(currentSens_.col(j).data(),
					nvCurrentSens_[j]);

		// Activate sensitivity calculations
		throwIfCVodeError(CVodeSensInit(cvodeMem_, Ns, CV_SIMULTANEOUS, SensRhs, nvCurrentSens_), "CVodeSensInit");

		// Set sensitivity tolerances
		throwIfCVodeError(CVodeSensEEtolerances(cvodeMem_), "CVodeSensEEtolerances");
	}

	void AdjointHessianIntegrator::initializeAdjoint() {
		int interp;
		switch( interpolation) {
			case Hermite:
				interp = CV_HERMITE;
				break;
			case Polynomial:
				interp = CV_POLYNOMIAL;
				break;

			default:
				std::ostringstream ss;
				ss << "Unknown interpolation type: " << interpolation;
				throw std::logic_error(ss.str());
		}

		// Allocate space for the adjoint computation
		throwIfCVodeError(CVodeAdjInit(cvodeMem_, Nnc_, interp), "CVodeAdjInit");
	}

	void AdjointHessianIntegrator::clearAdjoint() {
		// Delete all backward problems
		for(size_t i = 0; i < backwardProblems_.size(); ++i)
			delete backwardProblems_[i];
		backwardProblems_.clear();

		// Free space for the adjoint computation
		CVodeAdjFree(cvodeMem_);
	}

	void AdjointHessianIntegrator::integrateTo(const double tOut) {
		if( tOut == currentTime_ )
			return;

		throwIfCVodeError(CVodeF(cvodeMem_, tOut, nvCurrentState_, &currentTime_, CV_NORMAL, &numOfCheckpoints_), "CVodeF");

		double tret = currentTime_;
		int flag = CVodeGetSens(cvodeMem_, &tret, nvCurrentSens_);
		if( flag != CV_SUCCESS || (tret != currentTime_) ) {
			std::ostringstream ss; ss << "Failed to extract ODE sensitivity solution: " << flag;
			throw std::runtime_error(ss.str());
		}
	}

	void AdjointHessianIntegrator::setIC(const double initialTime) {
		currentTime_ = initialTime;

		MapVecd currentStateM(currentState_.data(), currentState_.size());
		sensProblem_.initial(currentStateM);

		MapMatrixd currentSensM(currentSens_.data(), currentSens_.rows(),
				currentSens_.cols());
		sensProblem_.jacobianParametersInitial(currentSensM);

		cvodeReInit();
	}

	void AdjointHessianIntegrator::reinit(const double initialTime,
			const Vecd & initialState, const Matrixd & initialSensivity) {
		if( currentState_.size() != initialState.size() )
			throw std::runtime_error("reinit vector has different size");
		if( (currentState_.rows() != initialSensivity.rows())
				|| (currentState_.cols() != initialSensivity.cols()) )
			throw std::runtime_error("reinit matrix has different size");

		currentTime_ = initialTime;
		currentState_ = initialState;
		currentSens_ = initialSensivity;

		cvodeReInit();
	}

	void AdjointHessianIntegrator::cvodeReInit() {
		CVodeIntegratorBase::cvodeReInit();

		throwIfCVodeError(CVodeSensReInit(cvodeMem_, CV_SIMULTANEOUS, nvCurrentSens_), "CVodeSensReInit");
		throwIfCVodeError(CVodeAdjReInit(cvodeMem_), "CVodeAdjReInit");
	}

	void AdjointHessianIntegrator::integrateBackwardTo(const double tOut) {
		if( tOut == backwardTime_ )
			return;

		throwIfCVodeError(CVodeB(cvodeMem_, tOut, CV_NORMAL), "CVodeB");

		// Update all backward integrators
		for(size_t i = 0; i < backwardProblems_.size(); ++i) {
			double time = backwardProblems_[i]->update();

			if( time != tOut )
				throw std::runtime_error("Failed to backward integrate to the current tOut");
		}

		backwardTime_ = tOut;
	}

	BackwardHessianIntegrator *AdjointHessianIntegrator::createBackward(
			AdjointSensitivityODEBase & adjProblem) {
		int flag, identity;

		if( adjProblem.isStiff() )
			flag = CVodeCreateB(cvodeMem_, CV_BDF, CV_NEWTON, &identity);
		else
			flag = CVodeCreateB(cvodeMem_, CV_ADAMS, CV_NEWTON, &identity);
		throwIfCVodeError(flag, "CVodeCreateB");

		BackwardHessianIntegrator *backInt = new BackwardHessianIntegrator(
				adjProblem, cvodeMem_, identity);
		backwardProblems_.push_back(backInt);
		return backInt;
	}

	void AdjointHessianIntegrator::setupBackward() {
		backwardTime_ = currentTime_;
	}

	void AdjointHessianIntegrator::setupBackward(const double time) {
		backwardTime_ = time;
	}

	BackwardHessianIntegrator::BackwardHessianIntegrator(
			AdjointODEBase & adjProblem, void *cvodeMem, int identity) :
					cvodeMem_(cvodeMem), identity_(identity),
					adjProblem_(adjProblem), nvCurrentState_(0), nvCurrentQ_(0),
					relTol_(1.0e-10), absTol_(1.0e-12), relTolQ_( 1.0e-8), absTolQ_(1.0e-10),
				   	currentTime_(0.0), maxNumSteps_(1000000) {
		initialize();
	}

	BackwardHessianIntegrator::~BackwardHessianIntegrator() {
		if( nvCurrentState_ != 0 ) {
			N_VDestroy_Serial(nvCurrentState_);
			nvCurrentState_ = 0;
		}

		if( nvCurrentQ_ != 0 ) {
			N_VDestroy_Serial(nvCurrentQ_);
			nvCurrentQ_ = 0;
		}
	}

	void BackwardHessianIntegrator::setTolerances(double relTol,
			double absTol) {
		relTol_ = relTol;
		absTol_ = absTol;

		throwIfCVodeError(CVodeSStolerancesB(cvodeMem_, identity_, relTol_, absTol_), "CVodeSStolerancesB");
	}

	void BackwardHessianIntegrator::setTolerancesQuadrature(double relTol,
			double absTol) {
		relTolQ_ = relTol;
		absTolQ_ = absTol;

		throwIfCVodeError(CVodeQuadSStolerancesB(cvodeMem_, identity_, relTolQ_, absTolQ_), "CVodeQuadSStolerancesB");
	}

	void BackwardHessianIntegrator::cvodeReInit() {
		throwIfCVodeError(CVodeReInitB(cvodeMem_, identity_, currentTime_, nvCurrentState_), "CVodeReInitB");

		if( nvCurrentQ_ != 0 ) {
			throwIfCVodeError(CVodeQuadReInitB(cvodeMem_, identity_, nvCurrentQ_), "CVodeQuadReInitB");
		}
	}

	void BackwardHessianIntegrator::reinitQuadrature() {
		if( nvCurrentQ_ != 0 ) {
			currentQ_.setZero();
			throwIfCVodeError(CVodeQuadReInitB(cvodeMem_, identity_, nvCurrentQ_), "CVodeQuadReInitB");
		}
	}

	void BackwardHessianIntegrator::initialize() {
		currentTime_ = 0.0;
		currentState_.resize(adjProblem_.numberOfEquations());
		nvCurrentState_ = N_VMake_Serial(currentState_.size(), &currentState_[0]);
		currentState_.setZero();

		throwIfCVodeError(CVodeInitB(cvodeMem_, identity_, AdjRhs, currentTime_, nvCurrentState_), "CVodeInitB");
		throwIfCVodeError(CVodeSetUserDataB(cvodeMem_, identity_, (void*) &adjProblem_), "CVodeSetUserDataB");

#ifdef NDEBUG
		CVodeSetErrHandlerFn(cvodeMem_, SilentErrHandler, 0);
#endif

		setTolerances(relTol_, absTol_);

		throwIfCVodeError(CVodeSetInitStepB(cvodeMem_, identity_, 0.0), "CVodeSetInitStepB");
		throwIfCVodeError(CVodeSetMaxNumStepsB(cvodeMem_, identity_, maxNumSteps_), "CVodeSetMaxNumStepsB");
		throwIfCVodeError(CVDenseB(cvodeMem_, identity_, currentState_.size()), "CVDenseB");

#ifdef SUNMIN // defined in newer cvodes versions with CVDlsSetDenseJacFnBS
		if( adjProblem_.hasJacobian() ) {
			int flag = CVDlsSetDenseJacFnBS(cvodeMem_, identity_, AdjSensJacobian);
			if( flag != CVDLS_SUCCESS ) {
				if( flag == CVDLS_MEM_FAIL )
					throw std::bad_alloc();

				throwException<std::logic_error>("CVODE failure ", flag, ": CVDlsSetDenseJacFnBS");
			}
		}
#endif

		if( adjProblem_.numberOfQuadratureVars() > 0 ) {
			currentQ_.resize(
					adjProblem_.numberOfQuadratureVars()
					* adjProblem_.numberOfQuadratureVars());
			nvCurrentQ_ = N_VMake_Serial(currentQ_.size(), &currentQ_[0]);
			currentQ_.setZero();

			throwIfCVodeError(CVodeQuadInitBS(cvodeMem_, identity_, QuadSensRhs, nvCurrentQ_), "CVodeQuadInitBS");
			throwIfCVodeError(CVodeSetQuadErrConB(cvodeMem_, identity_, TRUE), "CVodeSetQuadErrConB");
			setTolerancesQuadrature(relTolQ_, absTolQ_);
		}
	}

	void BackwardHessianIntegrator::setIC(const double initialTime) {
		currentTime_ = initialTime;

		MapVecd currentStateM(currentState_.data(), currentState_.size());
		adjProblem_.initial(currentStateM);

		cvodeReInit();
	}

	void BackwardHessianIntegrator::reinit(const double initialTime,
			const Vecd & initialState) {
		if( currentState_.size() != initialState.size() )
			throw std::runtime_error("reinit vector has different size");

		currentTime_ = initialTime;
		currentState_ = initialState;

		cvodeReInit();
	}

	double BackwardHessianIntegrator::update() {
		double tRet;
		throwIfCVodeError(CVodeGetB(cvodeMem_, identity_, &tRet, nvCurrentState_), "CVodeGetB");

		if( nvCurrentQ_ != 0 ) {
			double tQuadRet;
			throwIfCVodeError(CVodeGetQuadB(cvodeMem_, identity_, &tQuadRet, nvCurrentQ_), "CVodeGetQuadB");

			if( tQuadRet != tRet )
				throw std::runtime_error("Failed to backward integrate quadratures");
		}

		return tRet;
	}

}
