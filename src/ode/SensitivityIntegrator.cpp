/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cvodes/cvodes_dense.h>
#include <stdexcept>
#include "ode/SensitivityIntegrator.hpp"
#include "LocalMatrix.hpp"

namespace ode {

	extern "C"
	int SensRhs(int Ns, realtype t, N_Vector y_, N_Vector ydot_, N_Vector *yS_, N_Vector *ySdot_,
				void *f_data, N_Vector tmp1, N_Vector tmp2) {
		SensitivityODEBase *explicitOde = static_cast<SensitivityODEBase *>(f_data);
		MapVecd y(NV_DATA_S(y_), NV_LENGTH_S(y_));
		MapVecd ydot(NV_DATA_S(ydot_), NV_LENGTH_S(ydot_));

		const int N = NV_LENGTH_S(y_);

		try {
			LOCAL_MATRIX(Matrixd, Jp, N, Ns);
			LOCAL_MATRIX(Matrixd, Js, N, N);

			MapMatrixd JpM(Jp.data(), Jp.rows(), Jp.cols());
			explicitOde->jacobianParameters(t, y, ydot, JpM);

			MapMatrixd JsM(Js.data(), Js.rows(), Js.cols());
			explicitOde->jacobianState(t, y, ydot, JsM);

			for(int i = 0; i < Ns; ++i) {
				MapVecd yS(NV_DATA_S(yS_[i]), N);
				MapVecd ySdot(NV_DATA_S(ySdot_[i]), N);
				ySdot.noalias() = JsM * yS;
				ySdot += Jp.col(i);
			}
		} catch(...) {
			return 1;
		}

		return 0;
	}

	SensitivityIntegrator::SensitivityIntegrator(SensitivityODEBase & sensProblem, bool functional)
	: sensProblem_(sensProblem) {
		initialize(functional);
	}

	SensitivityIntegrator::~SensitivityIntegrator() {
		N_VDestroyVectorArray(nvCurrentSens_, sensProblem_.numberOfParameters());
		CVodeSensFree(cvodeMem_);
	}

	void SensitivityIntegrator::initialize(bool functional) {
		CVodeIntegratorBase::initialize( sensProblem_, functional );

		const int Ns = sensProblem_.numberOfParameters();
		currentSens_.resize( sensProblem_.numberOfEquations(), Ns );

		nvCurrentSens_ = N_VCloneEmptyVectorArray(Ns, nvCurrentState_);
		for(int j = 0; j < Ns; j++)
			N_VSetArrayPointer_Serial( currentSens_.col(j).data(), nvCurrentSens_[j] );

		// Activate sensitivity calculations
		throwIfCVodeError(CVodeSensInit(cvodeMem_, Ns, CV_SIMULTANEOUS, SensRhs, nvCurrentSens_), "CVodeSensInit");

		// Set sensitivity tolerances
		throwIfCVodeError(CVodeSensEEtolerances(cvodeMem_), "CVodeSensEEtolerances");
	}

	void SensitivityIntegrator::integrateTo( const double tOut ) {
		if( tOut == currentTime_ ) return;

		throwIfCVodeError(CVode( cvodeMem_, tOut, nvCurrentState_, &currentTime_, CV_NORMAL ), "CVode");

		double tret = currentTime_;
		int flag = CVodeGetSens(cvodeMem_, &tret, nvCurrentSens_);
		if( flag != CV_SUCCESS || (tret != currentTime_) ) {
			std::ostringstream ss; ss << "Failed to extract ODE sensitivity solution: " << flag;
			throw std::runtime_error(ss.str());
		}
	}

	void SensitivityIntegrator::setIC(const double initialTime) {
		currentTime_ = initialTime;

		MapVecd currentStateM(currentState_.data(), currentState_.size());
		sensProblem_.initial(currentStateM);

		MapMatrixd currentSensM(currentSens_.data(), currentSens_.rows(), currentSens_.cols());
		sensProblem_.jacobianParametersInitial(currentSensM);

		cvodeReInit();
	}

	void SensitivityIntegrator::reinit(const double initialTime, const Vecd & initialState, const Matrixd & initialSensivity) {
		if( currentState_.size() != initialState.size() )
			throw std::runtime_error("reinit vector has different size");
		if( (currentState_.rows() != initialSensivity.rows()) || (currentState_.cols() != initialSensivity.cols()))
			throw std::runtime_error("reinit matrix has different size");

		currentTime_ = initialTime;
		currentState_ = initialState;
		currentSens_ = initialSensivity;

		cvodeReInit();
	}

	void SensitivityIntegrator::cvodeReInit(){
		CVodeIntegratorBase::cvodeReInit();
		throwIfCVodeError(CVodeSensReInit(cvodeMem_, CV_SIMULTANEOUS, nvCurrentSens_), "CVodeSensReInit");
	}

}
