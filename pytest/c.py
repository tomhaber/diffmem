import pandas as pd
import numpy as np
import pydiffmem.core as dm

def f(x):
    print(x)

dist = dm.Distribution("MvN", mu=np.array([0,0,0], np.double), omega=np.eye(3))
x = np.array([.0, .0, -0.], np.double)
samples = dm.sample(dist, 10, type_="MH", theta=x, trace=f)
print(samples)
