import numpy as np
import pydiffmem.core as dm
import unittest
from utilities import CustomTestCase

class TestOptim(CustomTestCase):
    def setUp(self):
        self.distribution = dm.Distribution("mvn", mu=np.array([0,0,0], np.double), omega=np.eye(3))

    def test_optim(self):
        phi = np.array([1.2, 1.0, -1.], np.double)

        for algo in ["NelderMead", "BFGS", "PSO", "Pattern"]:
            with self.subTest(algo=algo):
                fit = dm.optim(self.distribution, phi, sigma=1, algo=algo)
                if 'convergence' in fit:
                    self.assertTrue(fit['convergence'])
                self.npAssertAlmostEqual(fit['par'], np.zeros((3,1)), atol=1e-6)


if __name__ == '__main__':
    unittest.main()
