Compilation for Microsoft Visual Studio 14 (2015)

Dependencies
------------------------

1) Eigen3
	Download latest header-only version of Eigen3 (http://eigen.tuxfamily.org/index.php?title=Main_Page)
	Copy folder to Dependencies
	
2) Sundials (cvodes-2.8.2)
	Requires Python (tested with Python 2.7)
	Download latest version of Sundials (http://computation.llnl.gov/projects/sundials-suite-nonlinear-differential-algebraic-equation-solvers/sundials-software)
	Copy cvodes-2.8.2 folder to Dependencies
	Use CMake3.6 to build dependencies (Microsoft Visual Studio 14 x64 compiler)
	Copy sundials_cvodes.lib,sundials_cvodes.dll,sundials_nvecserial.lib and sundials_nvecserial.dll to lib directory
	Create environment variable SUNDIALS_ROOT to the root directory of cvcodes (..\diffmem_edm\Dependencies\cvodes-2.8.2)
	copy generated build/include/sundials/sundials_config.h into the include/sundials directory
	
3) tbb44 (Threading Building Blocks)
	Download latest version of TBB including prebuilt libraries (https://www.threadingbuildingblocks.org/)
	Copy tbb4 folder to Dependencies
	
4) trng-4.19
	Download latest version of trng-4.19 (https://github.com/rabauke/trng4)
	Copy trng-4.19 folder to Dependencies
	Copy CMakeList.txt into the src directory
	Use CMake3.6 to build dependencies (Microsoft Visual Studio 14 x64 compiler)
	Copy trng.lib to lib directory

5) gtest (needed for UnitTester)
	see https://github.com/google/googletest
	Copy gtest folder to Dependencies
	GTEST_INCLUDE_DIR:PATH=.../diffmem_edm/Dependencies/gtest/include
	GTEST_LIBRARY:FILEPATH=.../diffmem_edm/build/unittests/gtest/Release/gtest.lib
	GTEST_LIBRARY_DEBUG:FILEPATH=.../diffmem_edm/build/unittests/gtest/Debug/gtest.lib
	GTEST_MAIN_LIBRARY:FILEPATH=.../diffmem_edm/build/unittests/gtest/Release/gtest_main.lib
	GTEST_MAIN_LIBRARY_DEBUG:FILEPATH=.../diffmem_edm/build/unittests/gtest/Debug/gtest_main.lib

6) pthread (not needed under Windows, only for gtest under linux)
	see https://github.com/BrianGladman/pthreads


Compiling diffmem_edm
------------------------

set DIFFMEM_DISABLE_HDF5 on TRUE in CMakeLists.txt (todo fix HDF5 for Windows)
set TRNG_LIBRARY to the directory of trng.lib (..\diffmem_edm\Dependencies\trng-4.19\lib\trng.lib)
optionally: set the directories for Sundials, tbb44
Use CMake3.6 to build diffmem_edm (Microsoft Visual Studio 14 x64 compiler)
