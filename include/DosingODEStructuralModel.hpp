/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DOSINGODESTRUCTURALMODEL_H
#define DOSINGODESTRUCTURALMODEL_H

#include <ode/ODE.hpp>
#include <ode/ODEIntegrator.hpp>
#include <ode/Sensitivity.hpp>
#include <StructuralModel.hpp>
#include <LikelihoodModel.hpp>
#include <LocalMatrix.hpp>
#include <Dosing.hpp>

namespace details {
	Dosing DIFFMEM_EXPORT constructDosingScheme(const Dict & dictionary);
}

template <class Model>
class DosingODEStructuralModel : public StructuralModel {
	public:
		DosingODEStructuralModel(const Dict & dictionary);

	public:
		int numberOfParameters() const override { return Model::numberOfParameters(); }
		int numberOfObservations() const override { return Model::numberOfObservations(); }

	public:
		void transphi(ConstRefVecd & psi, RefVecd phi) const override {
			phi.resize( psi.size() );
			Model::Transform::transphi(psi, phi);
		}

		void transpsi(ConstRefVecd & phi, RefVecd psi) const override {
			psi.resize( phi.size() );
			Model::Transform::transpsi(phi, psi);
		}

		void dtranspsi(ConstRefVecd & phi, RefVecd dpsi) const override {
			dpsi.resize( phi.size() );
			Model::Transform::dtranspsi(phi, dpsi);
		}

	public:
		void evalU(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd u) const override;
		void evalSens(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd sens) const override;

	public:
		double logpdf(ConstRefVecd & phi, ConstRefVecd & tau,
				ConstRefVecd & t, const PointwiseLikelihoodModel & ll) const override;
		double logpdf(ConstRefVecd & phi, ConstRefVecd & tau,
				ConstRefVecd & t, const PointwiseLikelihoodModel & ll, RefVecd gradPhi) const override;
		double logpdf(ConstRefVecd &phi, ConstRefVecd &tau,
				ConstRefVecd &t, const PointwiseLikelihoodModel &llm,
				RefVecd gradPhi, RefMatrixd hessPhi) const override;

	private:
		Dosing scheme;
};

struct InjectDose {
	InjectDose(double amount, int cmt) : amount(amount), cmt(cmt) {}
	void operator() (Vecd & y) const {
		y[cmt] += amount;
	}
	double amount;
	int cmt;
};

template <class Model>
DosingODEStructuralModel<Model>::DosingODEStructuralModel(const Dict & dictionary) {
	scheme = details::constructDosingScheme(dictionary);
}

template <class Model>
void DosingODEStructuralModel<Model>::evalU(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd u) const {
	ode::ODE<Model> ode(psi);
	ode::ODEIntegrator integrator(ode);
	integrator.setIC();

	u.resize(Model::numberOfObservations(), t.size());

	DosingIterator it(scheme);
	double nextDose_t = it.nextEventTime();

	for(int i = 0; i < t.size(); ++i) {
		const double nextMeasurement_t = t[i];

		while( nextDose_t <= nextMeasurement_t ) {
			integrator.integrateTo( nextDose_t );
			double amount;
			int cmt;
			it.popNextEvent(amount, cmt);
			integrator.apply(InjectDose(amount, cmt));
			nextDose_t = it.nextEventTime();
		}

		integrator.integrateTo( nextMeasurement_t );

		VecNd<Model::numberOfObservations()> obs;
		Model::Projection::project(nextMeasurement_t, integrator.currentState(), psi, obs);

		u.block(0, i, Model::numberOfObservations(), 1) = obs;
	}
}

template <class Model>
void DosingODEStructuralModel<Model>::evalSens(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd sens) const {
	ode::ODE<Model> ode(psi);
	ode::SensitivityIntegrator integrator(ode);
	integrator.setIC();

	const int N = Model::numberOfObservations();
	const int numberOfSensitivities = N * Model::numberOfParameters();
	sens.resize(numberOfSensitivities + N, t.size());

	DosingIterator it(scheme);
	double nextDose_t = it.nextEventTime();

	for(int i = 0; i < t.size(); ++i) {
		const double nextMeasurement_t = t[i];

		while( nextDose_t <= nextMeasurement_t ) {
			integrator.integrateTo( nextDose_t );
			double amount;
			int cmt;
			it.popNextEvent(amount, cmt);
			integrator.apply(InjectDose(amount, cmt));
			nextDose_t = it.nextEventTime();
		}

		integrator.integrateTo( nextMeasurement_t );

		VecNd<Model::numberOfObservations()> obs;
		Model::Projection::project(nextMeasurement_t, integrator.currentState(), psi, obs);

		MatrixMNd<Model::numberOfObservations(), Model::numberOfEquations()> Jp;
		Model::Projection::jacobian(nextMeasurement_t, integrator.currentState(), psi, Jp);

		MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> Jpp;
		Model::Projection::jacobianParameters(nextMeasurement_t, integrator.currentState(), psi, Jpp);

		MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> Jp_s = Jp * integrator.currentSensitivity() + Jpp;

		sens.block(0, i, N, 1) = obs;
		sens.block(N, i, numberOfSensitivities, 1) = MapVecd(Jp_s.data(), numberOfSensitivities);
	}
}

template <class Model>
double DosingODEStructuralModel<Model>::logpdf(ConstRefVecd &phi, ConstRefVecd &tau,
		ConstRefVecd &t, const PointwiseLikelihoodModel &llm) const {

	const int N = phi.size();
	LOCAL_VECTOR(Vecd, psi, N);
	transpsi(phi, psi);

	ode::ODE<Model> ode(psi);
	ode::ODEIntegrator integrator(ode);
	integrator.setIC();

	DosingIterator it(scheme);
	double nextDose_t = it.nextEventTime();

	double ll = llm.logProportions(tau);
	for(int i = 0; i < t.size(); ++i) {
		const double nextMeasurement_t = t[i];

		while( nextDose_t <= nextMeasurement_t ) {
			integrator.integrateTo( nextDose_t );
			double amount;
			int cmt;
			it.popNextEvent(amount, cmt);
			integrator.apply(InjectDose(amount, cmt));
			nextDose_t = it.nextEventTime();
		}

		integrator.integrateTo( nextMeasurement_t );

		VecNd<Model::numberOfObservations()> obs;
		Model::Projection::project(nextMeasurement_t, integrator.currentState(), psi, obs);

		ll += llm.logProportionalPdf(tau, i, obs);
	}

	return ll;
}

template <class Model>
double DosingODEStructuralModel<Model>::logpdf(ConstRefVecd & phi, ConstRefVecd & tau,
		ConstRefVecd & t, const PointwiseLikelihoodModel & llm, RefVecd gradPhi) const {

	const int N = phi.size();
	LOCAL_VECTOR(Vecd, psi, N);
	transpsi(phi, psi);

	ode::ODE<Model> ode(psi);
	ode::SensitivityIntegrator integrator(ode);
	integrator.setIC();

	DosingIterator it(scheme);
	double nextDose_t = it.nextEventTime();

	double ll = llm.logProportions(tau);
	gradPhi = Vecd::Zero( Model::numberOfParameters() );
	for(int i = 0; i < t.size(); ++i) {
		const double nextMeasurement_t = t[i];

		while( nextDose_t <= nextMeasurement_t ) {
			integrator.integrateTo( nextDose_t );
			double amount;
			int cmt;
			it.popNextEvent(amount, cmt);
			integrator.apply(InjectDose(amount, cmt));
			nextDose_t = it.nextEventTime();
		}

		integrator.integrateTo( nextMeasurement_t );

		const Vecd & u = integrator.currentState();
		const Matrixd & s = integrator.currentSensitivity();

		VecNd<Model::numberOfObservations()> yu;
		Model::Projection::project(nextMeasurement_t, u, psi, yu);

		MatrixMNd<Model::numberOfObservations(), Model::numberOfEquations()> Jp;
		Model::Projection::jacobian(nextMeasurement_t, u, psi, Jp);

		MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> Jpp;
		Model::Projection::jacobianParameters(nextMeasurement_t, integrator.currentState(), psi, Jpp);

		VecNd<Model::numberOfObservations()> gradU;
		llm.logpdfGradU(tau, i, yu, gradU);
		ll += llm.logProportionalPdf(tau, i, yu);

		MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> z = (Jp * s) + Jpp;
		gradPhi.noalias() += z.transpose() * gradU;
	}

	LOCAL_VECTOR(Vecd, dpsi, N);
	dtranspsi(phi, dpsi);
	gradPhi = gradPhi.cwiseProduct(dpsi);

	return ll;
}

template <class Model>
double DosingODEStructuralModel<Model>::logpdf(ConstRefVecd & phi, ConstRefVecd & tau,
		ConstRefVecd & t, const PointwiseLikelihoodModel & llm, RefVecd gradPhi, RefMatrixd hessPhi) const {

	const int N = phi.size();
	LOCAL_VECTOR(Vecd, psi, N);
	transpsi(phi, psi);

	ode::ODE<Model> ode(psi);
	ode::SensitivityIntegrator integrator(ode);
	integrator.setIC();

	double ll = llm.logProportions(tau);
	gradPhi = Vecd::Zero(N);
	hessPhi = Matrixd::Zero(N, N);

	DosingIterator it(scheme);
	double nextDose_t = it.nextEventTime();

	for(int i = 0; i < t.size(); ++i) {
		const double nextMeasurement_t = t[i];

		while( nextDose_t <= nextMeasurement_t ) {
			integrator.integrateTo( nextDose_t );
			double amount;
			int cmt;
			it.popNextEvent(amount, cmt);
			integrator.apply(InjectDose(amount, cmt));
			nextDose_t = it.nextEventTime();
		}

		integrator.integrateTo( nextMeasurement_t );
		const Vecd & u = integrator.currentState();
		const Matrixd & s = integrator.currentSensitivity();

		VecNd<Model::numberOfObservations()> yu;
		Model::Projection::project(nextMeasurement_t, u, psi, yu);

		MatrixMNd<Model::numberOfObservations(), Model::numberOfEquations()> Jp;
		Model::Projection::jacobian(nextMeasurement_t, u, psi, Jp);

		VecNd<Model::numberOfObservations()> gradU;
		MatrixMNd<Model::numberOfObservations(), Model::numberOfObservations()> hessU;
		llm.logpdfHessU(tau, i, yu, gradU, hessU);
		ll += llm.logProportionalPdf(tau, i, yu);

		MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> Jpp;
		Model::Projection::jacobianParameters(nextMeasurement_t, integrator.currentState(), psi, Jpp);

		MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> z = (Jp * s) + Jpp;

		gradPhi.noalias() += z.transpose() * gradU;
		hessPhi.noalias() += z.transpose() * hessU * z;
	}

	LOCAL_VECTOR(Vecd, dpsi, N);
	dtranspsi(phi, dpsi);

	gradPhi = gradPhi.cwiseProduct(dpsi);
	for(int j = 0; j < N; ++j) {
		for(int k = j; k < N; ++k) {
			hessPhi(k,j) = hessPhi(j,k) = dpsi[k] * dpsi[j] * hessPhi(k,j);
		}
	}

	return ll;
}
#endif
