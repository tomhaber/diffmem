/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ODESTRUCTURALMODEL_H
#define ODESTRUCTURALMODEL_H

#include <ode/ODE.hpp>
#include <ode/ODEIntegrator.hpp>
#include <ode/Sensitivity.hpp>
#include <ode/Adjoint.hpp>
#include <StructuralModel.hpp>
#include <LikelihoodModel.hpp>
#include <LocalMatrix.hpp>

template <class Model>
class ODEStructuralModel : public StructuralModel {
	public:
		ODEStructuralModel() {} // TODO remove this
		ODEStructuralModel(const Dict & dict) {}

	public:
		int numberOfParameters() const override { return Model::numberOfParameters(); }
		int numberOfObservations() const override { return Model::numberOfObservations(); }

	public:
		void transphi(ConstRefVecd & psi, RefVecd phi) const override {
			phi.resize( psi.size() );
			Model::Transform::transphi(psi, phi);
		}

		void transpsi(ConstRefVecd & phi, RefVecd psi) const override {
			psi.resize( phi.size() );
			Model::Transform::transpsi(phi, psi);
		}

		void dtranspsi(ConstRefVecd & phi, RefVecd dpsi) const override {
			dpsi.resize( phi.size() );
			Model::Transform::dtranspsi(phi, dpsi);
		}

	public:
		void evalU(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd u) const override;
		void evalSens(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd sens) const override;

	public:
		double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
				const PointwiseLikelihoodModel &ll) const override;
		double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
				const PointwiseLikelihoodModel &ll, RefVecd gradPhi) const override;
		double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
				const PointwiseLikelihoodModel &llm, RefVecd gradPhi, RefMatrixd hessPhi) const override;
};

template <class Model>
void ODEStructuralModel<Model>::evalU(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd u) const {
	ode::ODE<Model> ode(psi);
	ode::ODEIntegrator integrator(ode);
	integrator.setIC();

	u.resize(Model::numberOfObservations(), t.size());

	for(int i = 0; i < t.size(); ++i) {
		integrator.integrateTo( t[i] );

		VecNd<Model::numberOfObservations()> obs;
		Model::Projection::project(t[i], integrator.currentState(), psi, obs);

		u.block(0, i, Model::numberOfObservations(), 1) = obs;
	}
}

template <class Model>
void ODEStructuralModel<Model>::evalSens(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd sens) const {
	ode::ODE<Model> ode(psi);
	ode::SensitivityIntegrator integrator(ode);
	integrator.setIC();

	const int N = Model::numberOfObservations();
	const int numberOfSensitivities = N * Model::numberOfParameters();
	sens.resize(numberOfSensitivities + N, t.size());

	for(int i = 0; i < t.size(); ++i) {
		integrator.integrateTo( t[i] );

		VecNd<Model::numberOfObservations()> obs;
		Model::Projection::project(t[i], integrator.currentState(), psi, obs);

		MatrixMNd<Model::numberOfObservations(), Model::numberOfEquations()> Jp;
		Model::Projection::jacobian(t[i], integrator.currentState(), psi, Jp);

		MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> Jpp;
		Model::Projection::jacobianParameters(t[i], integrator.currentState(), psi, Jpp);

		MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> Jp_s = Jp * integrator.currentSensitivity() + Jpp;

		sens.block(0, i, N, 1) = obs;
		sens.block(N, i, numberOfSensitivities, 1) = MapVecd(Jp_s.data(), numberOfSensitivities);
	}
}

template <class Model>
double ODEStructuralModel<Model>::logpdf(ConstRefVecd & phi, ConstRefVecd & tau,
		ConstRefVecd & t, const PointwiseLikelihoodModel & llm) const {

	LOCAL_VECTOR(Vecd, psi, phi.size());
	transpsi(phi, psi);

	ode::ODE<Model> ode(psi);
	ode::ODEIntegrator integrator(ode);
	integrator.setIC();

	double ll = llm.logProportions(tau);
	for(int i = 0; i < t.size(); ++i) {
		const double time = t[i];
		integrator.integrateTo(time);

		VecNd<Model::numberOfObservations()> obs;
		Model::Projection::project(time, integrator.currentState(), psi, obs);

		ll += llm.logProportionalPdf(tau, i, obs);
	}

	return ll;
}

template <class Model>
double ODEStructuralModel<Model>::logpdf(ConstRefVecd & phi, ConstRefVecd & tau,
		ConstRefVecd & t, const PointwiseLikelihoodModel & llm, RefVecd gradPhi) const {

	const int N = phi.size();
	LOCAL_VECTOR(Vecd, psi, N);
	transpsi(phi, psi);

	const double ll = ode::Sensitivity<Model>::computeGradient(t, llm, psi, tau, gradPhi);

	LOCAL_VECTOR(Vecd, dpsi, N);
	dtranspsi(phi, dpsi);
	gradPhi = gradPhi.cwiseProduct(dpsi);

	return ll;
	//ode::Adjoint<Model> adj(measurements, llm, tau);
	//adj.solve(psi);
	//gradPsi = adj.getGrad();
	//return adj.getLikelihood();
}

template <class Model>
double ODEStructuralModel<Model>::logpdf(ConstRefVecd & phi, ConstRefVecd & tau,
		ConstRefVecd & t, const PointwiseLikelihoodModel & llm, RefVecd gradPhi, RefMatrixd hessPhi) const {

	const int N = phi.size();
	LOCAL_VECTOR(Vecd, psi, N);
	transpsi(phi, psi);

	const double ll = ode::Sensitivity<Model>::computeHessian(t, llm, psi, tau, gradPhi, hessPhi);

	LOCAL_VECTOR(Vecd, dpsi, N);
	dtranspsi(phi, dpsi);

	gradPhi = gradPhi.cwiseProduct(dpsi);
	for(int j = 0; j < N; ++j) {
		for(int k = j; k < N; ++k) {
			hessPhi(k,j) = hessPhi(j,k) = dpsi[k] * dpsi[j] * hessPhi(k,j);
		}
	}

	return ll;
}
#endif
