/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MODELFACTORY_H_
#define MODELFACTORY_H_

#include <common.hpp>
#include <map>
#include <vector>
#include <functional>
#include <memory>

class Dict;
class StructuralModel;
class LikelihoodModel;
class Distribution;

class DIFFMEM_EXPORT ModelFactory {
	public:
		ModelFactory();
		static ModelFactory & instance();

	public:
		std::shared_ptr<StructuralModel> createStructuralModel(const std::string & name, const Dict & dict) const;
		std::shared_ptr<LikelihoodModel> createLikelihoodModel(const std::string & name, const Dict & dict) const;
		std::shared_ptr<Distribution> createDistribution(const std::string & name, const Dict & dict) const;

	public:
		std::vector<std::string> listStructuralModels() const;
		std::vector<std::string> listLikelihoodModels() const;
		std::vector<std::string> listDistributions() const;

	private:
		struct ci_less {
			struct nocase_compare {
				bool operator()(const unsigned char &c1, const unsigned char &c2) const {
					return tolower(c1) < tolower(c2);
				}
			};
			bool operator()(const std::string &s1, const std::string &s2) const {
				return std::lexicographical_compare(s1.begin(), s1.end(),
						s2.begin(), s2.end(),
						nocase_compare());
			}
		};

	public:
		typedef std::function<std::shared_ptr<StructuralModel>(const Dict &)> StructuralConstr;
		typedef std::function<std::shared_ptr<LikelihoodModel>(const Dict &)> LikelihoodConstr;
		typedef std::function<std::shared_ptr<Distribution>(const Dict &)> DistributionConstr;
		typedef std::map<std::string, StructuralConstr, ci_less> StructuralMap;
		typedef std::map<std::string, LikelihoodConstr, ci_less> LikelihoodMap;
		typedef std::map<std::string, DistributionConstr, ci_less> DistributionMap;

	public:
		StructuralMap::iterator registerStructuralModel(const std::string & name, StructuralConstr constructor);
		StructuralMap::iterator registerStructuralModel(std::string && name, StructuralConstr constructor);
		void unregisterStructuralModel(StructuralMap::iterator it);

		LikelihoodMap::iterator registerLikelihoodModel(const std::string & name, LikelihoodConstr constructor);
		LikelihoodMap::iterator registerLikelihoodModel(std::string && name, LikelihoodConstr constructor);
		void unregisterLikelihoodModel(LikelihoodMap::iterator it);

		DistributionMap::iterator registerDistribution(const std::string & name, DistributionConstr constructor);
		DistributionMap::iterator registerDistribution(std::string && name, DistributionConstr constructor);
		void unregisterDistribution(DistributionMap::iterator it);

	protected:
		StructuralMap structuralModels;
		LikelihoodMap likelihoodModels;
		DistributionMap distributions;
};

class StructuralModelType {
	public:
		template <class F>
		StructuralModelType(std::string name, F && f) noexcept {
			it = ModelFactory::instance().registerStructuralModel(std::move(name), std::forward<F>(f));
		}
		~StructuralModelType() {
			ModelFactory::instance().unregisterStructuralModel(it);
		}

	private:
		ModelFactory::StructuralMap::iterator it;
};

class LikelihoodModelType {
	public:
		template <class F>
		LikelihoodModelType(std::string name, F && f) noexcept {
			it = ModelFactory::instance().registerLikelihoodModel(std::move(name), std::forward<F>(f));
		}
		~LikelihoodModelType() {
			ModelFactory::instance().unregisterLikelihoodModel(it);
		}

	private:
		ModelFactory::LikelihoodMap::iterator it;
};

class DistributionType {
	public:
		template <class F>
		DistributionType(std::string name, F && f) noexcept {
			it = ModelFactory::instance().registerDistribution(std::move(name), std::forward<F>(f));
		}
		~DistributionType() {
			ModelFactory::instance().unregisterDistribution(it);
		}

	private:
		ModelFactory::DistributionMap::iterator it;
};

#define REGISTER_STRUCTURAL_MODEL(name, type) \
	static StructuralModelType CONCATENATE( name, __regged )( STRINGIZE(name), [](const Dict & d) { return std::make_shared<type>(d); } )
#define REGISTER_LIKELIHOOD_MODEL(name, type) \
	static LikelihoodModelType CONCATENATE( name, __regged )( STRINGIZE(name), [](const Dict & d) { return std::make_shared<type>(d); } )
#define REGISTER_DISTRIBUTION(name, type) \
	static DistributionType CONCATENATE( name, __regged )( STRINGIZE(name), [](const Dict & d) { return std::make_shared<type>(d); } )
#endif
