/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef NORMALDISTRIBUTION_H
#define NORMALDISTRIBUTION_H

#include <Distribution.hpp>

class Random;
class DIFFMEM_EXPORT ZMMvNDistribution : public Distribution {
	public:
		ZMMvNDistribution();
		ZMMvNDistribution(const Dict &d);
		ZMMvNDistribution(const Matrixd & omega);
		ZMMvNDistribution(const Cholesky<Matrixd> & chol_omega);

	public:
		int numberOfDimensions() const override { return chol_omega.cols(); }

	public:
		double logpdf(ConstRefVecd & x) const override;
		double logpdf(ConstRefVecd & x, RefVecd grad) const override;
		double logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const override;
		void sample(Random & rng, RefVecd s) const;
		std::unique_ptr<DistributionSampler> sampler(const Dict &dict) const override;

	public:
		void set(ConstRefMatrixd & omega);
		void set(const Cholesky<Matrixd> & chol_omega);

	private:
		Cholesky<Matrixd> chol_omega;
};

inline ZMMvNDistribution::ZMMvNDistribution() {
}

inline ZMMvNDistribution::ZMMvNDistribution(const Matrixd & omega) {
	set(omega);
}

inline ZMMvNDistribution::ZMMvNDistribution(const Cholesky<Matrixd> & chol_omega) : chol_omega(chol_omega) {
}

inline void ZMMvNDistribution::set(const Cholesky<Matrixd> & chol_omega) {
	this->chol_omega = chol_omega;
}

class DIFFMEM_EXPORT MvNDistribution : public Distribution {
	public:
		MvNDistribution();
		MvNDistribution(const Dict &d);
		MvNDistribution(const Vecd & mu, const Matrixd & omega);
		MvNDistribution(const Vecd & mu, const Cholesky<Matrixd> & chol_omega);

	public:
		int numberOfDimensions() const override { return chol_omega.cols(); }

	public:
		double logpdf(ConstRefVecd & x) const override;
		double logpdf(ConstRefVecd & x, RefVecd grad) const override;
		double logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const override;
		void sample(Random & rng, RefVecd s) const;
		std::unique_ptr<DistributionSampler> sampler(const Dict &dict) const override;

	public:
		void set(const Vecd & mu, const Matrixd & omega);
		const Vecd & mean() const { return mu; }

	private:
		Vecd mu;
		Cholesky<Matrixd> chol_omega;
};

inline MvNDistribution::MvNDistribution() {
}

inline MvNDistribution::MvNDistribution(const Vecd & mu, const Matrixd & omega) {
	set(mu, omega);
}

inline MvNDistribution::MvNDistribution(const Vecd & mu, const Cholesky<Matrixd> & chol_omega) : mu(mu), chol_omega(chol_omega) {
}

#endif /* NORMALDISTRIBUTION_H */
