/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CONDITIONALMEDISTRIBUTION_H_
#define CONDITIONALMEDISTRIBUTION_H_

#include <Individual.hpp>
#include <IndividualLikelihoodEta.hpp>
#include <ProductDistribution.hpp>
#include <NormalDistribution.hpp>
#include <memory>
#include <vector>

class Population;

class DIFFMEM_EXPORT ConditionalIndividualDistribution : public Distribution {
	public:
		ConditionalIndividualDistribution(ZMMvNDistribution & mvn,
				const Individual & subject, const Vecd & beta, const Vecd & tau);

	public:
		int numberOfDimensions() const override {
			return distribution.numberOfDimensions();
		}

	public:
		double logpdf(ConstRefVecd & x) const override {
			return distribution.logpdf(x);
		}

		double logpdf(ConstRefVecd & x, RefVecd grad) const override {
			return distribution.logpdf(x, grad);
		}

		double logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const override {
			return distribution.logpdf(x, grad, H);
		}

	public:
		void update(ConstRefVecd & beta, ConstRefVecd & tau);
		void error(ConstRefVecd & eta, Vecd & err);

	protected:
		IndividualLikelihoodEta ll;
		ProductDistribution<IndividualLikelihoodEta, ZMMvNDistribution> distribution;
};

class DIFFMEM_EXPORT ConditionalMEDistribution : public Distribution {
	public:
		ConditionalMEDistribution(const Population & population,
				const Vecd & beta, const Cholesky<Matrixd> & omega, const Vecd & tau);

		ConditionalMEDistribution(const ConditionalMEDistribution &) = delete;
		ConditionalMEDistribution & operator =(const ConditionalMEDistribution &) = delete;

	public:
		int numberOfDimensions() const override {
			return numberOfIndividuals() * subjects[0]->numberOfDimensions();
		}

	public:
		double logpdf(ConstRefVecd & x) const override;
		double logpdf(ConstRefVecd & x, RefVecd grad) const override;
		double logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const override;

	public:
		std::unique_ptr<DistributionSampler> sampler(const Dict &dict) const override;

	public:
		int numberOfIndividuals() const { return static_cast<int>( subjects.size() ); }
		int numberOfIndividualDimensions() const { return subjects[0]->numberOfDimensions(); }
		std::shared_ptr<ConditionalIndividualDistribution> operator[](int id) const { return subjects[id]; }
		void update(ConstRefVecd & beta, ConstRefMatrixd & omega, ConstRefVecd & tau);
		void update(ConstRefVecd & beta, const Cholesky<Matrixd> & chol_omega, ConstRefVecd & tau);

	public:
		void errorPopulation(ConstRefMatrixd & etas, Vecd & err);
		void errorIndividual(int id, ConstRefVecd & eta, Vecd & err);

	protected:
		ZMMvNDistribution mvn;
		std::vector<std::shared_ptr<ConditionalIndividualDistribution>> subjects;
};
#endif
