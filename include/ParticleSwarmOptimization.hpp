/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PARTICLESWARMOPTIMIZATION_H_
#define PARTICLESWARMOPTIMIZATION_H_

#include <MatrixVector.hpp>
#include <Function.hpp>
#include <Random.hpp>
#include <OptimizationTrace.hpp>

class DIFFMEM_EXPORT ParticleSwarmOptimization {
	public:
		struct Options {
			size_t numParticles{20};
			size_t numEpochs{1000};
			double c1{2.0};
			double c2{2.0};
			double w_start{0.95};
			double w_end{0.4};
			double w_varyfor{1.0};
			double V_max{100.0};
			OptimizationTraceCallback trace{nullptr};

			Options() {}
		};

	public:
		ParticleSwarmOptimization(const Function & problem, Options const & options = Options());
		ParticleSwarmOptimization(const ParticleSwarmOptimization&) = delete;
		ParticleSwarmOptimization& operator=(const ParticleSwarmOptimization&) = delete;

	public:
		void optimize(ConstRefVecd & x, double sigma);
		void optimize(ConstRefVecd & lb, ConstRefVecd & ub);
		const Vecd & optimum() const { return xBest; }
		double value() const { return xFitness; }

	private:
		void initParticles(ConstRefVecd & x, double sigma);
		void initParticles(ConstRefVecd & lb, ConstRefVecd & ub);
		void optimize();
		void step();
		void update(double c1, double c2, double w);

	private:
		const Function & func;
		Vecd xBest;
		double xFitness;
		Options options;
		Random rng;

	private:
		struct Particle {
			Vecd x, xBest, v;
			double fitnessBest;
		};
		std::vector<Particle> particles;
};
#endif
