/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TIME_H
#define TIME_H

#include <common.hpp>
#include <TimePlatform.hpp>
#include <iostream>

class DIFFMEM_EXPORT Timer {
	public:
		void tic();
		double toc(std::ostream & ostr = std::cout);
		double toq();

	public:
		template <typename F>
		static double tictoc(F &&f, std::ostream & ostr = std::cout);

		template <typename F>
		static double tictoq(F &&f);

	public:
		double now();

	public:
		static void tic(Timer & t);
		static double toc(Timer & t, std::ostream & ostr = std::cout);
		static double toq(Timer & t);

	public:
		typedef ticks ticks_t;
		static ticks getTicks();
		static double elapsed(ticks t0, ticks t1);

	private:
		pl_time_t ts;
};

inline void Timer::tic(Timer & t) {
	t.tic();
}

inline double Timer::toc(Timer & t, std::ostream & ostr) {
	return t.toc(ostr);
}

template <typename F>
inline double Timer::tictoc(F &&f, std::ostream &ostr) {
	Timer t;
	t.tic();
	std::forward<F>(f)();
	return t.toc(ostr);
}

template <typename F>
inline double Timer::tictoq(F &&f) {
	Timer t;
	t.tic();
	std::forward<F>(f)();
	return t.toq();
}

inline double Timer::toq(Timer & t) {
	return t.toq();
}

inline ticks Timer::getTicks() {
	return getticks();
}

inline double Timer::elapsed(ticks t0, ticks t1) {
	return ::elapsed(t0, t1);
}
#endif
