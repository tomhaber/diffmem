/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ENUMERATE_H
#define ENUMERATE_H

#include <iterator>

namespace internal {
	using std::begin;
	using std::end;

	template <typename Iterable>
	auto adl_begin(Iterable & iterable) -> decltype(begin(iterable)) {
		return begin(iterable);
	}

	template <typename Iterable>
	auto adl_end(Iterable & iterable) -> decltype(end(iterable)) {
		return end(iterable);
	}

	template <typename Iterable>
	class enumerate_object {
	private:
		Iterable _iter;
		std::size_t _size;
		decltype(adl_begin(_iter)) _begin;
		const decltype(adl_end(_iter)) _end;

	public:
		enumerate_object(Iterable iter)
				: _iter(iter), _size(0), _begin(adl_begin(iter)), _end(adl_end(iter)) {}

		const enumerate_object &begin() const { return *this; }
		const enumerate_object &end() const { return *this; }

		bool operator!=(const enumerate_object &) const {
			return _begin != _end;
		}

		void operator++() {
			++_begin;
			++_size;
		}

		auto operator*() const
				-> std::pair<std::size_t, decltype(*_begin)> {
			return {_size, *_begin};
		}
	};
}

template <typename Iterable>
internal::enumerate_object<Iterable> enumerate(Iterable &&iter) {
	return {std::forward<Iterable>(iter)};
}
#endif