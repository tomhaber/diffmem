/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ROSENBROCK_H
#define ROSENBROCK_H

#include <Distribution.hpp>

template <int dim>
class DIFFMEM_EXPORT RosenbrockDistribution : public Distribution {
public:
	RosenbrockDistribution() : a(1), b(100) {}
	RosenbrockDistribution(const Dict &d);

public:
	int numberOfDimensions() const override {
		return dim;
	}

	double logpdf(ConstRefVecd &x) const override {
		auto s1 = x.segment(0, dim - 1);
		auto s2 = x.segment(1, dim - 1);
		VecNd<dim - 1> e1 = (s2 - s1.cwiseProduct(s1));
		VecNd<dim - 1> e2 = (a - s1.array());

		return -(b * e1.cwiseProduct(e1) + e2.cwiseProduct(e2)).sum();
	}

	double logpdf(ConstRefVecd &x, RefVecd grad) const override {
		notYetImplemented(__PRETTY_FUNCTION__);
		return 0;
	}

	double logpdf(ConstRefVecd &x, RefVecd grad, RefMatrixd H) const override {
		notYetImplemented(__PRETTY_FUNCTION__);
		return 0;
	}

private:
	double a, b;
};

#endif /* ROSENBROCK_H */
