/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

#include <MatrixVector.hpp>
#include <memory>
#include <StructuralModel.hpp>
#include <LikelihoodModel.hpp>

class DIFFMEM_EXPORT Individual {
	public:
		Individual() : id(-1), structuralModel(nullptr), likelihoodModel(nullptr), B_identity(false) {}

		template <typename TT, typename VT>
		Individual(int id,
				std::shared_ptr<StructuralModel> structuralModel,
				std::shared_ptr<LikelihoodModel> likelihoodModel,
				const ConstRefMatrixd & A, const ConstRefMatrixd & B)
				: id(id), structuralModel(std::move(structuralModel)), likelihoodModel(std::move(likelihoodModel)), A(A), B(B) {
			init();
		}

		template <typename TT, typename VT, typename WT>
		Individual(int id,
				std::shared_ptr<StructuralModel> structuralModel,
				std::shared_ptr<LikelihoodModel> likelihoodModel,
				const ConstRefMatrixd & A, const ConstRefMatrixd & B)
				: id(id), structuralModel(std::move(structuralModel)), likelihoodModel(std::move(likelihoodModel)), A(A), B(B) {
			init();
		}

		Individual(int id,
				std::shared_ptr<StructuralModel> structuralModel,
				std::shared_ptr<LikelihoodModel> likelihoodModel,
				const ConstRefMatrixd & A, const ConstRefMatrixd & B)
				: id(id), structuralModel(std::move(structuralModel)), likelihoodModel(std::move(likelihoodModel)), A(A), B(B) {
			init();
		}

	public:
		int numberOfBetas() const { return A.cols(); }
		int numberOfParameters() const { return A.rows(); }
		int numberOfRandom() const { return B.cols(); }
		int numberOfSigmas() const { return likelihoodModel->numberOfSigmas(); }
		int numberOfTaus() const { return likelihoodModel->numberOfTaus(); }
		int numberOfObservations() const { return likelihoodModel->numberOfObservations(); }
		int numberOfMeasurements() const { return likelihoodModel->numberOfMeasurements(); }

	public:
		template <typename BT, typename ET, typename PT>
		void toParameter(const MatrixBase<BT> & beta, const MatrixBase<ET> & eta, const MatrixBase<PT> & phi_) const {
			Require(beta.size() == A.cols(), "beta should equal number of columns of A ({:d} != {:d})", beta.size(), A.cols());
			Require(eta.size() == B.cols(), "eta should equal number of columns of B ({:d} != {:d})", eta.size(), B.cols());
			MatrixBase<PT> & phi = const_cast< MatrixBase<PT>& >(phi_);
			phi.noalias() = A * beta + B * eta;
		}

		template <typename BT,typename PT>
		void toParameter(const MatrixBase<BT> & beta, const MatrixBase<PT> & phi_) const {
			Require(beta.size() == A.cols(), "beta should equal number of columns of A ({:d} != {:d})", beta.size(), A.cols());
			MatrixBase<PT> & phi = const_cast< MatrixBase<PT>& >(phi_);
			phi.noalias() = A * beta;
		}

		template <typename PT,typename ET>
		void toRandom(const MatrixBase<PT> & phi, const MatrixBase<ET> & eta_) const {
			Require(phi.size() == A.cols(), "phi should equal number of parameters ({:d} != {:d})", phi.size(), A.cols());
			MatrixBase<ET> & eta = const_cast< MatrixBase<ET>& >(eta_);
			eta.noalias() = Binv * phi;
		}

		Matrixd toCovariance(const Matrixd & omega) const {
			return B * omega * B.transpose();
		}

		const Matrixd & betaToRandomXf() const { return BinvA; }
		const Matrixd & phiToRandomXf() const { return Binv; }
		const Matrixd & randomToPhiXf() const { return B; }

		void evalU(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd u) const;
		void evalSens(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd s) const;

		void evalU(ConstRefVecd & beta, ConstRefVecd & eta, ConstRefVecd & t, RefMatrixd u) const;
		void evalSens(ConstRefVecd & beta, ConstRefVecd & eta, ConstRefVecd & t, RefMatrixd s) const;

	public:
		double logpdf(ConstRefVecd & phi, ConstRefVecd & tau) const;
		double logpdf(ConstRefVecd & phi, ConstRefVecd & tau, RefVecd grad) const;
		double logpdf(ConstRefVecd & phi, ConstRefVecd & tau, RefVecd grad, RefMatrixd H) const;
		double error(ConstRefVecd & phi, ConstRefVecd & tau, RefVecd err) const;

	public:
		void transsigma(ConstRefVecd & tau, RefVecd sigma) const;
		void transtau(ConstRefVecd & sigma, RefVecd tau) const;
		void updateTau(RefVecd tau, ConstRefVecd & gradTau, int N, double alpha) const;

	private:
		void init();

	private:
		int id;
		std::shared_ptr<StructuralModel> structuralModel;
		std::shared_ptr<LikelihoodModel> likelihoodModel;
		Matrixd A, B;
		Matrixd BinvA, Binv;
		bool B_identity;

	friend class Population;
};

inline void Individual::transsigma(ConstRefVecd & tau, RefVecd sigma) const {
	likelihoodModel->transsigma(tau, sigma);
}

inline void Individual::transtau(ConstRefVecd & sigma, RefVecd tau) const {
	likelihoodModel->transtau(sigma, tau);
}

inline void Individual::updateTau(RefVecd tau, ConstRefVecd & gradTau, int N, double alpha) const {
	likelihoodModel->updateTau(tau, gradTau, N, alpha);
}

inline void Individual::evalU(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd u) const {
	structuralModel->evalU(psi, t, u);
}

inline void Individual::evalSens(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd s) const {
	structuralModel->evalSens(psi, t, s);
}
#endif
