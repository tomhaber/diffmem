/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef FUNCTION_TRAITS_H
#define FUNCTION_TRAITS_H

#include <utility>
#include <type_traits>

template <typename F, typename... Args>
using resultOf = decltype(std::declval<F>()(std::declval<Args>()...));

template <typename F, typename... Args>
using decayedResultOf = typename std::decay<resultOf<F, Args...>>::type;

template <typename F, typename... Args>
struct isCallableWith {
	template <typename T, typename = resultOf<T, Args...>>
	static constexpr std::true_type check(std::nullptr_t) {
		return std::true_type{};
	};

	template <typename>
	static constexpr std::false_type check(...) {
		return std::false_type{};
	};

	typedef decltype(check<F>(nullptr)) type;
	static constexpr bool value = type::value;
};

template <typename R, typename... Args>
struct arg_result {};

template <class F>
struct function_traits;

// function pointer
template <class R, class... Args>
struct function_traits<R (*)(Args...)> : public function_traits<R(Args...)> {};

template <class R, class... Args>
struct function_traits<R(Args...)> {
	using return_type = R;

	static constexpr std::size_t arity = sizeof...(Args);

	typedef R function_type(Args...);
	using argResult = arg_result<R, Args...>;

	template <std::size_t N>
	struct argument {
		static_assert(N < arity, "error: invalid parameter index.");
		using type = typename std::tuple_element<N, std::tuple<Args...>>::type;
	};

	template <std::size_t N>
	using argument_t = typename argument<N>::type;
};

// member function pointer
template <class C, class R, class... Args>
struct function_traits<R (C::*)(Args...)> : public function_traits<R(C &, Args...)> {
};

// const member function pointer
template <class C, class R, class... Args>
struct function_traits<R (C::*)(Args...) const>
		: public function_traits<R(C &, Args...)> {
};

// member object pointer
template <class C, class R>
struct function_traits<R(C::*)> : public function_traits<R(C &)> {
};

namespace detail {
	template <typename T>
	struct strip_first_parameter;

	template <typename R, typename C, typename...Args>
	struct strip_first_parameter<R(C,Args...)> {
		typedef R function_type(Args...);
		using argResult = arg_result<R, Args...>;
	};
}

// functor
template <class F>
struct function_traits {
private:
	using call_type = function_traits<decltype(&F::operator())>;
	using stripped = detail::strip_first_parameter<typename call_type::function_type>;

public:
	using return_type = typename call_type::return_type;

	static constexpr std::size_t arity = call_type::arity;

	using function_type = typename stripped::function_type;
	using argResult = typename stripped::argResult;

	template <std::size_t N>
	struct argument {
		static_assert(N < arity, "error: invalid parameter index.");
		using type = typename call_type::template argument<N + 1>::type;
	};

	template <std::size_t N>
	using argument_t = typename argument<N>::type;
};

template <class F>
struct function_traits<F &> : public function_traits<F> {};
template <typename F>
struct function_traits<const F&> : public function_traits<F> {};
template <typename F>
struct function_traits<volatile F&> : public function_traits<F> {};
template <typename F>
struct function_traits<const volatile F&> : public function_traits<F> {};
template <typename F>
struct function_traits<F&&> : public function_traits<F> {};
template <typename F>
struct function_traits<const F&&> : public function_traits<F> {};
template <typename F>
struct function_traits<volatile F&&> : public function_traits<F> {};
template <typename F>
struct function_traits<const volatile F&&> : public function_traits<F> {};


#endif
