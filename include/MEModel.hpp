/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MEMODEL_H
#define MEMODEL_H

#include <memory>
#include <MatrixVector.hpp>
#include <Population.hpp>
#include <ConditionalMEDistribution.hpp>

class DIFFMEM_EXPORT MEModel {
	public:
		MEModel(const Population & population,
				const Vecd & beta, const Vecb & estimated, const Matrixd & omega,
				const Matrixb & cov_model, const Vecd & sigma, const Vecb & sigma_model);

	public:
		int numberOfIndividuals() const { return population.numberOfIndividuals(); }
		int numberOfBetas() const { return nfixed + nrandom; }
		int numberOfRandom() const { return omega_.rows(); }
		int numberOfSigmas() const { return sigma_.size(); }
		int numberOfTaus() const { return tau_.size(); }

	public:
		void set(const Vecd & beta, const Matrixd & omega, const Vecd & sigma);
		const Vecd & beta() const { return beta_; }
		Vecd betaMasked() const;
		const Cholesky<Matrixd> cholOmega() const { return omega_; }
		Matrixd omega() const { return omega_.reconstructedMatrix(); }
		const Vecd & sigma() const { return sigma_; }
		const Vecd & tau() const { return tau_; }

		void solveBeta(const Matrixd & S, const Matrixd & eta, double stepsize);
		void updateOmega(const Matrixd & S0, const Matrixd & S1, const Matrixd & mu);
		void updateSigma(const Vecd & err);

		void initializeMu(Matrixd & mu);
		void updateMuEta(const Matrixd & BInvPhi, Matrixd & mu, Matrixd & eta);
		void updateEta(ConstRefVecd & old_beta, ConstRefVecd & new_beta, Matrixd & eta) const;

		double evaluateLikelihood(ConstRefVecd & beta, ConstRefMatrixd & etas) const;
		double evaluateLikelihood(ConstRefVecd & beta, ConstRefMatrixd & etas, Cholesky<Matrixd> & chol_omega, ConstRefVecd & tau) const;
		void empiricalBayesEstimate(RefMatrixd etas) const;

	public:
		std::unique_ptr<ConditionalMEDistribution> createCondDistribution() const;
		void updateCondDistribution(ConditionalMEDistribution *distribution) const;
		void updateCondDistribution(ConditionalMEDistribution *distribution, ConstRefVecd &beta,
				Cholesky<Matrixd> &chol_omega, ConstRefVecd &tau) const;

	private:
		void finalize(const Vecb & estimated);
		void computeMasks(const Vecb & estimated);
		void estimateBetaRandom(const Matrixd & S);
		void estimateBetaFixed(const Matrixd & eta, double stepsize);

	protected:
		Vecd beta_;
		Cholesky<Matrixd> omega_;
		Vecd sigma_, tau_;

	protected:
		const Population & population;
		int nfixed, nrandom;
		Vecb fixed, random;
		Matrixb covariance_model;
		Vecb sigma_model;
};
#endif //MEMODEL_H
