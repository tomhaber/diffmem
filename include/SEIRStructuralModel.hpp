/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SEIRSTRUCTURALMODEL_H
#define SEIRSTRUCTURALMODEL_H

#include <common.hpp>
#include <StructuralModel.hpp>

class DIFFMEM_EXPORT SEIRStructuralModel : public StructuralModel {
	public:
		SEIRStructuralModel(const Dict & dict);

	public:
		int numberOfParameters() const override { return 1; }
		int numberOfObservations() const override { return nAges*4; }

	public:
		void evalU(ConstRefVecd & psi, ConstRefVecd & t, RefMatrixd u) const override;

	public:
		void transphi(ConstRefVecd & psi, RefVecd phi) const override;
		void transpsi(ConstRefVecd & phi, RefVecd psi) const override;
		void dtranspsi(ConstRefVecd & phi, RefVecd dphi) const override;

	private:
		int nAges;
		Matrixd w_pde;
		Vecd c_pde;
		Vecd mu_pde;
		double B_pde;
		Vecd e_pde;
		Vecd g_pde;
		Vecd delta_pde;
		Vecd cov_pde;
		Vecd y_init_pde;
};

#endif
