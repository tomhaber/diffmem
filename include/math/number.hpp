/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MATH_NUMBER_H
#define MATH_NUMBER_H

#include <cmath>
#include <limits>
#include <type_traits>
#include <common.hpp>

/*
 * R's NA (Not-Available)
 */
namespace math {

	namespace details {
		typedef union {
		   double value;
		   unsigned int word[2];
		} ieee_double;

#ifdef WORDS_BIGENDIAN
		static const int hw = 0;
		static const int lw = 1;
#else  /* !WORDS_BIGENDIAN */
		static const int hw = 1;
		static const int lw = 0;
#endif /* WORDS_BIGENDIAN */
	}

	inline double valueOfNA() {
		volatile details::ieee_double x;
		x.word[details::hw] = 0x7ff00000;
		x.word[details::lw] = 1954;
		return x.value;
	}

	inline double NA() {
		return valueOfNA();
	}

	inline bool isNA(double x) {
		if( ! std::isnan(x) )
			return false;

		details::ieee_double y;
		y.value = x;
		return (y.word[details::lw] == 1954);
	}

	inline constexpr double NaN() {
		return std::numeric_limits<double>::quiet_NaN();
	}

	inline bool isNaN(double x) {
		if( ! std::isnan(x) )
			return false;

		details::ieee_double y;
		y.value = x;
		return (y.word[details::lw] != 1954);
	}

	inline constexpr double negInf() {
		return -std::numeric_limits<double>::infinity();
	}

	inline constexpr double posInf() {
		return +std::numeric_limits<double>::infinity();
	}

	template <typename T>
	inline bool isNegInf(T x) {
		return (std::isinf(x) && std::signbit(x));
	}

	template <typename T>
	inline bool isPosInf(T x) {
		return (std::isinf(x) && ! std::signbit(x));
	}

	template <typename T>
	inline bool isFinite(T x) {
		return std::isfinite(x);
	}

	inline double epsilon() {
		return std::numeric_limits<double>::epsilon();
	}

	inline double cutoff(double x, double eps=1e-12) {
		return (x < eps) ? eps : x;
	}

	template <typename T, typename U>
	inline bool isLessOrEqual(const T & x, const U & upper) {
		return ( x <= T(upper) );
	}

	template <typename T, typename U>
	inline bool isGreaterOrEqual(const T & x, const U & lower) {
		return ( x >= T(lower) );
	}

	template <typename T, typename U>
	inline bool isLess(const T & x, const U & upper) {
		return ( x < T(upper) );
	}

	template <typename T, typename U>
	inline bool isGreater(const T & x, const U & lower) {
		return ( x > T(lower) );
	}

	template <typename T>
	inline bool isNonNegative(const T & x) {
		return ( std::is_signed<T>() && (x >= T(0)) );
	}

	template <typename T>
	inline bool isNonNegativeFinite(const T & x) {
		return ( std::isfinite(x) && (std::is_signed<T>() && (x >= T(0))) );
	}

	template <typename T>
	inline bool isStrictlyPositive(const T & x) {
		return ( std::is_signed<T>() && (x > T(0)) );
	}

	template <typename T>
	inline bool isStrictlyPositiveFinite(const T & x) {
		return ( std::isfinite(x) && (std::is_signed<T>() && (x > T(0))) );
	}

	template <typename T, typename U, typename V>
	inline bool isBetween(const T & x, const U & lower, const V & upper) {
		return ( x >= T(lower) && x < T(upper) );
	}

	template <typename T, typename U, typename V>
	inline bool isStrictlyBetween(const T & x, const U & lower, const V & upper) {
		return (x > T(lower) && x < T(upper));
	}

	template <typename T, typename U, typename V>
	inline bool isBounded(const T & x, const U & lower, const V & upper) {
		return ( x >= T(lower) && x <= T(upper) );
	}
}
#endif
