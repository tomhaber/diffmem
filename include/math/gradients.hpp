/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MATH_GRADIENTS_H_
#define MATH_GRADIENTS_H_

#include <array>

namespace math {

	template <std::size_t N>
	struct Gradients : std::array<double, N> {
		Gradients(double x) {
			std::array<double, N>::fill(x);
		}

		Gradients(const Gradients<N> & g) = default;
		Gradients(Gradients<N> && g) = default;

		template <std::size_t OtherN>
		Gradients(const Gradients<OtherN> & g) = delete;

		template <std::size_t OtherN>
		Gradients(Gradients<OtherN> && g) = delete;

		template <typename... Args, typename =
			typename std::enable_if<sizeof...(Args) == N && N != 1>::type
		>
		Gradients(Args &&... args) {
			assign<0>( std::forward<Args>(args)... );
		}

		template <std::size_t I, typename... Args>
		void assign(Args &&... args);

		template <std::size_t I, typename Arg>
		void assign(Arg && arg) {
			std::get<I>(*this) = std::forward<Arg>(arg);
		}

		template <std::size_t I, typename Arg, typename... Args>
		void assign(Arg && arg, Args &&... args) {
			std::get<I>(*this) = std::forward<Arg>(arg);
			assign<I+1>( std::forward<Args>(args)... );
		}

		Gradients<N> operator-() {
			Gradients<N> g(*this);
			for(size_t i = 0; i < N; ++i)
				g[i] = -(*this)[i];
			return g;
		}
	};

}
#endif
