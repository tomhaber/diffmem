/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MATH_NTHROOT_H_
#define MATH_NTHROOT_H_

#include <cmath>
#include <valuecheck.hpp>

namespace math {

	namespace internal {
		template <class T>
		typename std::enable_if<std::is_floating_point<T>::value, T>::type nthroot_impl(T x, T n) {
			if( n < 0.0 ) return 1.0 / nthroot_impl(x, -n);

			const T tolerance = 1e-10;
			const int maxIterations = 200000;

			// Find root of function f(y) = y^n - x using halley's method

			T y = 1.0;
			int i;
			for(i = 0; i < maxIterations; ++i) {
				const T yn = std::pow(y, n);
				const T delta = (2*y*(yn - x))/((n+1)*yn + (n-1)*x);
				y = y - delta;

				if( std::abs(delta) <= tolerance * std::abs(y) )
					break;
			}

			if( i == maxIterations )
				DomainError("x", x, "cannot compute", n ,"th root");

			return y;
		}
	}

	template <class T>
	typename std::enable_if<std::is_floating_point<T>::value, T>::type nthroot(T x, T n) {
		if( x >= 0.0 )
			return std::pow(x, 1./n);

		return internal::nthroot_impl(x, n);
	}
}
#endif
