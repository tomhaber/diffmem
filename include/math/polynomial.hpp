/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MATH_POLYNOMIAL_H
#define MATH_POLYNOMIAL_H

#include <common.hpp>
#include <array>
#include <vector>
#include <Error.hpp>

namespace math {

	namespace details {
		template <std::size_t N>
		struct eval_helper {
			constexpr eval_helper() {}
			static constexpr std::size_t value{N};
		};

		template <class T, class U>
		U evaluate_impl(const T* a, const U&, eval_helper<0>) {
		   return static_cast<U>(0);
		}

		template <class T, class U>
		U evaluate_impl(const T* a, const U&, eval_helper<1>) {
		   return static_cast<U>(a[0]);
		}

		template <class T, class U>
		U evaluate_impl(const T* a, const U & x, eval_helper<2>) {
		   return static_cast<U>(a[1] * x + a[0]);
		}

		template <class T, class U>
		U evaluate_impl(const T* a, const U & x, eval_helper<3>) {
		   return static_cast<U>((a[2] * x + a[1]) * x + a[0]);
		}

		template <class T, class U>
		U evaluate_impl(const T* a, const U & x, eval_helper<4>) {
		   return static_cast<U>(((a[3] * x + a[2]) * x + a[1]) * x + a[0]);
		}

		template <class T, class U>
		U evaluate_impl(const T* a, const U & x, eval_helper<5>) {
		   return static_cast<U>((((a[4] * x + a[3]) * x + a[2]) * x + a[1]) * x + a[0]);
		}

		template <class T, class U>
		U evaluate_impl(const T* a, const U & x, eval_helper<6>) {
		   return static_cast<U>(((((a[5] * x + a[4]) * x + a[3]) * x + a[2]) * x + a[1]) * x + a[0]);
		}

		template <class T, class U>
		U evaluate_impl(const T* a, const U & x, eval_helper<7>) {
		   return static_cast<U>((((((a[6] * x + a[5]) * x + a[4]) * x + a[3]) * x + a[2]) * x + a[1]) * x + a[0]);
		}

		template <class T, class U>
		U evaluate_impl(const T* a, const U & x, eval_helper<8>) {
		   return static_cast<U>(((((((a[7] * x + a[6]) * x + a[5]) * x + a[4]) * x + a[3]) * x + a[2]) * x + a[1]) * x + a[0]);
		}

		template <class T, class U>
		U evaluate_impl(const T* a, const U & x, eval_helper<9>) {
		   return static_cast<U>((((((((a[8] * x + a[7]) * x + a[6]) * x + a[5]) * x + a[4]) * x + a[3]) * x + a[2]) * x + a[1]) * x + a[0]);
		}

		template <class T, class U>
		U evaluate_impl(const T* a, const U & x, eval_helper<10>) {
		   return static_cast<U>(((((((((a[9] * x + a[8]) * x + a[7]) * x + a[6]) * x + a[5]) * x + a[4]) * x + a[3]) * x + a[2]) * x + a[1]) * x + a[0]);
		}

		template <class T, class U>
		U evaluate_impl(const T *poly, U const & z, std::size_t count) {
		   Assert(count > 0);
		   U sum = static_cast<U>(poly[count - 1]);
		   for(int i = static_cast<int>(count) - 2; i >= 0; --i) {
			  sum *= z;
			  sum += static_cast<U>(poly[i]);
		   }
		   return sum;
		}

		template <std::size_t N, class T, class U>
		constexpr U evaluate_impl(const T *a, U const & z, eval_helper<N>) {
			return evaluate_impl(a, z, eval_helper<N>::value);
		}
	}

	template <class T, class U>
	DIFFMEM_EXPORT U evaluate_polynomial(const T *poly, U const & z, std::size_t count) {
		return details::evaluate_impl(poly, z, count);
	}

	template <std::size_t N, class T, class U>
	DIFFMEM_EXPORT U evaluate_polynomial(const T(&a)[N], U const & z) {
		return details::evaluate_impl<N>(static_cast<const T*>(a), z, details::eval_helper<N>{});
	}

	template <std::size_t N, class T, class U>
	DIFFMEM_EXPORT U evaluate_polynomial(const std::array<T,N> & a, U const & z) {
		return details::evaluate_impl<N>(static_cast<const T*>(a.data()), z, details::eval_helper<N>{});
	}

	template <class T, class U>
	DIFFMEM_EXPORT U evaluate_polynomial(const std::vector<T> & a, U const & z) {
		return evaluate_polynomial(static_cast<const T*>(a.data()), z, a.size());
	}
};
#endif
