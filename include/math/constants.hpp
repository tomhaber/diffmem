/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MATH_CONSTANTS_H_
#define MATH_CONSTANTS_H_

#include <cmath>

namespace math {

	constexpr double log2() {
#ifndef CONSTEXPR_LOG
		return 0.69314718055994530942;
#else
		return std::log(2.);
#endif
	}

	constexpr double pi() {
#ifdef M_PI
		return M_PI;
#else
		return 3.14159265358979323846;
#endif
	}

	constexpr double e() {
#ifdef M_E
		return M_E;
#else
		return 2.718281828459045235360287471352662497757247093699959574966;
#endif
	}

	constexpr double sqrt2() {
#ifdef M_SQRT2
		return M_SQRT2;
#else
		return 1.414213562373095048801688724209698078569671875376948073176;
#endif
	}

	constexpr double oneOverSqrt2() {
		return 1.0 / sqrt2();
	}

	constexpr double log2Pi() {
#ifndef CONSTEXPR_LOG
		return 1.83787706640934548356;
#else
		return std::log(2*pi());
#endif
	}

	constexpr double logSqrt2Pi() {
		return 0.5 * log2Pi();
	}

	constexpr double sqrtPi() {
		return 1.77245385090551602729816748334;
	}

	constexpr double sqrt2Pi() {
		return sqrt2() * sqrtPi();
	}

	constexpr double logPi() {
#ifndef CONSTEXPR_LOG
		return 1.14472988584940017414;
#else
		return std::log(pi());
#endif
	}

	constexpr double invPi() {
		return 1.0 / pi();
	}

}
#endif
