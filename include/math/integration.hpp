/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MATH_INTEGRATION_H_
#define MATH_INTEGRATION_H_

#include <common.hpp>
#include <MatrixVector.hpp>

class Random;
namespace math {

	namespace details {
		template <typename T, typename F = std::decay_t<T>>
		void f_helper_1d(ConstRefVecd &x, RefVecd ret, void *f_data) {
			F *f = static_cast<F *>(f_data);
			ret[0] = f->operator()(x[0]);
		}

		template <typename T, typename F = std::decay_t<T>>
		void f_helper_nd(ConstRefVecd &x, RefVecd ret, void *f_data) {
			F *f = static_cast<F *>(f_data);
			ret[0] = f->operator()(x);
		}

		template <typename T, typename F = std::decay_t<T>>
		void f_helper_mnd(ConstRefVecd &x, RefVecd ret, void *f_data) {
			F *f = static_cast<F *>(f_data);
			f->operator()(x, ret);
		}

		struct f_helper {
			using caller_type = void (*)(ConstRefVecd &x, RefVecd ret, void *f_data);
			caller_type caller;
			void *object_ptr;

			void operator()(ConstRefVecd &x, RefVecd ret) {
				caller(x, ret, object_ptr);
			}
		};
	}

	DIFFMEM_EXPORT double cubature(details::f_helper f, ConstRefVecd &lb, ConstRefVecd &ub,
			double reltol = 1e-8, double abstol = 0, size_t maxevals = 0);

	DIFFMEM_EXPORT void cubature(details::f_helper f, ConstRefVecd &lb, ConstRefVecd &ub, RefVecd res,
			double reltol = 1e-8, double abstol = 0, size_t maxevals = 0);

	template <typename F>
	double cubature(F &&f, ConstRefVecd &lb, ConstRefVecd &ub,
			double reltol = 1e-8, double abstol = 0, size_t maxevals = 0) {
		details::f_helper helper{details::f_helper_nd<F>, std::addressof(f)};
		return cubature(helper, lb, ub, reltol, abstol, maxevals);
	}

	template <typename F>
	void cubature(F &&f, ConstRefVecd &lb, ConstRefVecd &ub, RefVecd res,
			double reltol = 1e-8, double abstol = 0, size_t maxevals = 0) {
		details::f_helper helper{details::f_helper_mnd<F>, std::addressof(f)};
		cubature(helper, lb, ub, res, reltol, abstol, maxevals);
	}

	template <typename F>
	double cubature(F &&f, double lb_, double ub_,
			double reltol = 1e-8, double abstol = 0, size_t maxevals = 0) {
		details::f_helper helper{details::f_helper_1d<F>, std::addressof(f)};
		ConstMapVecd lb(&lb_, 1);
		ConstMapVecd ub(&ub_, 1);
		return cubature(helper, lb, ub, reltol, abstol, maxevals);
	}

	DIFFMEM_EXPORT double cubature_normal(details::f_helper f, size_t dim, bool log,
			double q = 0.9999, double reltol = 1e-8, double abstol = 0, size_t maxevals = 0);

	DIFFMEM_EXPORT void cubature_normal(details::f_helper f, size_t dim, bool log,
			RefVecd res, double q = 0.9999, double reltol = 1e-8, double abstol = 0, size_t maxevals = 0);

	template <typename F>
	double cubature_normal(F &&f, size_t dim, bool log, double q = 0.9999,
			double reltol = 1e-8, double abstol = 0, size_t maxevals = 0) {
		details::f_helper helper{details::f_helper_nd<F>, std::addressof(f)};
		return cubature_normal(helper, dim, log, q, reltol, abstol, maxevals);
	}

	template <typename F>
	double cubature_normal(F &&f, bool log, double q = 0.9999,
			double reltol = 1e-8, double abstol = 0, size_t maxevals = 0) {
		details::f_helper helper{details::f_helper_1d<F>, std::addressof(f)};
		return cubature_normal(helper, 1, log, q, reltol, abstol, maxevals);
	}

	template <typename F>
	void cubature_normal(F &&f, size_t dim, bool log, RefVecd res, double q = 0.9999,
			double reltol = 1e-8, double abstol = 0, size_t maxevals = 0) {
		details::f_helper helper{details::f_helper_mnd<F>, std::addressof(f)};
		cubature_normal(helper, dim, log, res, q, reltol, abstol, maxevals);
	}

	DIFFMEM_EXPORT double quadrature(details::f_helper f, ConstRefVecd &lb, ConstRefVecd &ub, size_t n);
	DIFFMEM_EXPORT void quadrature(details::f_helper f, ConstRefVecd &lb, ConstRefVecd &ub, size_t n, RefVecd res);

	template <typename F>
	double quadrature(F &&f, ConstRefVecd &lb, ConstRefVecd &ub, size_t n) {
		details::f_helper helper{details::f_helper_nd<F>, std::addressof(f)};
		return quadrature(helper, lb, ub, n);
	}

	template <typename F>
	double quadrature(F &&f, double lb_, double ub_, size_t n) {
		details::f_helper helper{details::f_helper_1d<F>, std::addressof(f)};
		ConstMapVecd lb(&lb_, 1);
		ConstMapVecd ub(&ub_, 1);
		return quadrature(helper, lb, ub, n);
	}

	template <typename F>
	void quadrature(F &&f, ConstRefVecd &lb, ConstRefVecd &ub, size_t n, RefVecd res) {
		details::f_helper helper{details::f_helper_mnd<F>, std::addressof(f)};
		quadrature(helper, lb, ub, n, res);
	}

	DIFFMEM_EXPORT double quadrature_normal(details::f_helper f, size_t dim, bool log, size_t n);
	DIFFMEM_EXPORT void quadrature_normal(details::f_helper f, size_t dim, bool log, size_t n, RefVecd res);

	template <typename F>
	double quadrature_normal(F &&f, size_t dim, bool log, size_t n) {
		details::f_helper helper{details::f_helper_nd<F>, std::addressof(f)};
		return quadrature_normal(helper, dim, log, n);
	}

	template <typename F>
	double quadrature_normal(F &&f, bool log, size_t n) {
		details::f_helper helper{details::f_helper_1d<F>, std::addressof(f)};
		return quadrature_normal(helper, 1, log, n);
	}

	template <typename F>
	void quadrature_normal(F &&f, size_t dim, bool log, size_t n, RefVecd res) {
		details::f_helper helper{details::f_helper_mnd<F>, std::addressof(f)};
		quadrature_normal(helper, dim, log, n, res);
	}

	DIFFMEM_EXPORT double mc_normal(details::f_helper f, size_t dim, bool log, Random &rng, size_t n);
	DIFFMEM_EXPORT void mc_normal(details::f_helper f, size_t dim, bool log, Random &rng, size_t n, RefVecd res);

	template <typename F>
	double mc_normal(F &&f, size_t dim, bool log, Random &rng, size_t n) {
		details::f_helper helper{details::f_helper_nd<F>, std::addressof(f)};
		return mc_normal(helper, dim, log, rng, n);
	}

	template <typename F>
	double mc_normal(F &&f, bool log, Random &rng, size_t n) {
		details::f_helper helper{details::f_helper_1d<F>, std::addressof(f)};
		return mc_normal(helper, 1, log, rng, n);
	}

	template <typename F>
	void mc_normal(F &&f, size_t dim, bool log, Random &rng, size_t n, RefVecd res) {
		details::f_helper helper{details::f_helper_mnd<F>, std::addressof(f)};
		mc_normal(helper, dim, log, rng, n, res);
	}

}
#endif
