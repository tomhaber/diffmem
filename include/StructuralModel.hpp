/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STRUCTURALMODEL_H
#define STRUCTURALMODEL_H

#include <common.hpp>
#include <MatrixVector.hpp>
#include <Dict.hpp>

class LikelihoodModel;
class PointwiseLikelihoodModel;

class DIFFMEM_EXPORT StructuralModel {
public:
	virtual ~StructuralModel() {
	}

public:
	virtual int numberOfParameters() const = 0;
	virtual int numberOfObservations() const = 0;

public:
	virtual void transphi(ConstRefVecd &psi, RefVecd phi) const;
	virtual void transpsi(ConstRefVecd &phi, RefVecd psi) const;
	virtual void dtranspsi(ConstRefVecd &phi, RefVecd dphi) const;

public:
	virtual void evalU(ConstRefVecd &psi, ConstRefVecd &t, RefMatrixd u) const = 0;
	virtual void evalSens(ConstRefVecd &psi, ConstRefVecd &t, RefMatrixd sens) const;

public:
	virtual double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
			const LikelihoodModel &ll) const;
	virtual double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
			const LikelihoodModel &ll, RefVecd gradPhi) const;
	virtual double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
			const LikelihoodModel &ll, RefVecd gradPhi, RefMatrixd hessPhi) const;
	virtual double logpdfGradTau(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
			const LikelihoodModel &ll, RefVecd gradTau) const;

	virtual double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
			const PointwiseLikelihoodModel &ll) const;
	virtual double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
			const PointwiseLikelihoodModel &ll, RefVecd gradPhi) const;
	virtual double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, ConstRefVecd & t,
			const PointwiseLikelihoodModel &ll, RefVecd gradPhi, RefMatrixd hessPhi) const;
};
#endif
