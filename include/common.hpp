/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef COMMON_H
#define COMMON_H

#define UNUSED(arg) (void)arg;    //  trick of Qt
#define IMPLIES(a,b) (!(a) || (b)) // a => b
#define LOGICAL_XOR(a,b) (((a) || (b)) && !((a) && (b)))

// noinline
#ifdef _MSC_VER
# define NOINLINE __declspec(noinline)
#elif defined(__clang__) || defined(__GNUC__)
# define NOINLINE __attribute__((__noinline__))
#else
# define NOINLINE
#endif

// always inline
#ifdef _MSC_VER
# define ALWAYS_INLINE __forceinline
#elif defined(__clang__) || defined(__GNUC__)
# define ALWAYS_INLINE inline __attribute__((__always_inline__))
#else
# define ALWAYS_INLINE inline
#endif

#if defined(__clang__) || defined(__GNUC__)
# define DEPRECATED(msg) __attribute__((__deprecated__(msg)))
#elif defined(_MSC_VER)
# define DEPRECATED(msg) __declspec(deprecated(msg))
#else
# define DEPRECATED(msg)
#endif


#ifndef CONCATENATE
# define CONCATENATE_IMPL(s1, s2) s1##s2
# define CONCATENATE(s1, s2) CONCATENATE_IMPL(s1, s2)
#endif

#ifndef STRINGIZE
# define STRINGIZE(s) #s
#endif

#ifndef ANONYMOUS_VARIABLE
# ifdef __COUNTER__
#  define ANONYMOUS_VARIABLE(str) CONCATENATE(str, __COUNTER__)
# else
#  define ANONYMOUS_VARIABLE(str) CONCATENATE(str, __LINE__)
# endif
#endif

#ifdef _MSC_VER
# define strerror_r(errno,buf,len) strerror_s(buf,len,errno)
# define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

#if _MSC_VER == 1800
#define NO_CONSTEXPR

#ifndef constexpr
#define constexpr const
#endif

#ifndef noexcept
#define noexcept throw()
#endif
#endif

// Helper for template programming :)
namespace internal {
	template <typename T>
	class Id;
}

#include <diffmem_export.h>
#endif
