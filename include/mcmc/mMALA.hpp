/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MCMC_MMALA_H
#define MCMC_MMALA_H

#include <mcmc/Proposal.hpp>

namespace mcmc {

	class DIFFMEM_EXPORT mMALA : public Proposal {
		public:
			mMALA(const Distribution & target, const Dict & dict);
			mMALA(const Distribution & target, double initStepSize = 0.5, double minStepSize=0.001, double epsilon=1e-5, double lowerRate = .4, double upperRate = .7, int adaptRate=50);

		public:
			std::string name() const;
			std::unique_ptr<Proposal> clone(const Distribution & target) const;
			double evaluate(ProposalState & state);
			void generate(const ProposalState & current, ProposalState & candidate, Random & rng, double *logProposalRatio=nullptr);
			double logProposalRatio(const ProposalState & current, const ProposalState & candidate) const;
			void adapt(const Chain & chain);

		private:
			double stepSize;
			double minStepSize;
			double epsilon;
			double lowerRate, upperRate;
			int adaptRate;
	};

	inline mMALA::mMALA(const Distribution & target, double initStepSize, double minStepSize, double epsilon, double lowerRate, double upperRate, int adaptRate)
		: Proposal(target), stepSize(initStepSize), minStepSize(minStepSize), epsilon(epsilon), lowerRate(lowerRate), upperRate(upperRate), adaptRate(adaptRate) {
	}

}
#endif
