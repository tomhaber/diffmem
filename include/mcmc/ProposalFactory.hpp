/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MCMC_PROPOSALFACTORY_H
#define MCMC_PROPOSALFACTORY_H

#include <common.hpp>
#include <unordered_map>
#include <functional>
#include <memory>
#include <Dict.hpp>

class Distribution;

namespace mcmc {

	class Proposal;

	class DIFFMEM_EXPORT ProposalFactory {
		public:
			static ProposalFactory & instance();

		public:
			std::unique_ptr<Proposal> createProposal(const std::string & name, const Distribution & target, const Dict & dict);

		public:
			typedef std::function<std::unique_ptr<Proposal>(const Distribution & target, const Dict &)> ProposalConstr;
			bool registerProposal(const std::string & name, ProposalConstr constructor);
			bool registerProposal(std::string && name, ProposalConstr constructor);

		protected:
			std::unordered_map<std::string, ProposalConstr> proposals;
	};

}

#define REGISTER_PROPOSAL(name, type) static bool CONCATENATE( name, __regged ) = \
		mcmc::ProposalFactory::instance().registerProposal( STRINGIZE(name), [](const Distribution & target, const Dict & d) { return std::make_unique<type>(target, d); } )
#endif
