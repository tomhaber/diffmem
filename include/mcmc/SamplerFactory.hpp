/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SAMPLERFACTORY_H_
#define SAMPLERFACTORY_H_

#include <common.hpp>
#include <map>
#include <functional>
#include <memory>
#include <mcmc/Sampler.hpp>

class Dict;
class Distribution;

namespace mcmc {

	class DIFFMEM_EXPORT SamplerFactory {
		public:
			SamplerFactory();
			static SamplerFactory & instance();

		public:
			std::unique_ptr<Sampler> createSampler(const std::string &name,
				const std::shared_ptr<const Distribution> &distr, const Dict &dict);
			std::unique_ptr<Sampler> createSampler(const std::string &name,
				const Distribution &distr, const Dict &dict);

		private:
			struct ci_less {
				struct nocase_compare {
					bool operator()(const unsigned char &c1, const unsigned char &c2) const {
						return tolower(c1) < tolower(c2);
					}
				};
				bool operator()(const std::string &s1, const std::string &s2) const {
					return std::lexicographical_compare(s1.begin(), s1.end(),
							s2.begin(), s2.end(),
							nocase_compare());
				}
			};

		public:
			struct SamplerConstr {
				std::function<std::unique_ptr<Sampler>(const std::shared_ptr<const Distribution>&, const Dict &)> shared;
				std::function<std::unique_ptr<Sampler>(const Distribution&, const Dict &)> ref;
			};
			typedef std::map<std::string, SamplerConstr, ci_less> SamplerMap;

		public:
			SamplerMap::iterator registerSampler(const std::string & name, SamplerConstr constructor);
			SamplerMap::iterator registerSampler(std::string && name, SamplerConstr && constructor);
			void unregisterSampler(SamplerMap::iterator it);

		protected:
			SamplerMap samplers;
	};

	class SamplerType {
		public:
			template <class F, class G>
			SamplerType(std::string name, F && f, G && g) noexcept {
				it = SamplerFactory::instance().registerSampler(
					std::move(name), SamplerFactory::SamplerConstr{std::forward<F>(f), std::forward<G>(g)});
			}
			~SamplerType() {
				SamplerFactory::instance().unregisterSampler(it);
			}

		private:
			SamplerFactory::SamplerMap::iterator it;
	};
}

#define REGISTER_SAMPLER(name, type) \
	static mcmc::SamplerType CONCATENATE( name, __regged )( STRINGIZE(name), \
		[](const std::shared_ptr<const Distribution> &distr, const Dict & d) { \
			return std::make_unique<mcmc::SamplableAdaptor<type>>(distr, d); \
		}, \
		[](const Distribution &distr, const Dict & d) { \
			return std::make_unique<type>(distr, d); \
		} )
#endif
