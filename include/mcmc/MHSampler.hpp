/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MCMC_MHSAMPLER_H
#define MCMC_MHSAMPLER_H

#include <memory>
#include <mcmc/Sampler.hpp>
#include <mcmc/Proposal.hpp>
#include <mcmc/Chain.hpp>
#include <mcmc/MH.hpp>

class Dict;
class Random;
class Distribution;

namespace mcmc {

	class DIFFMEM_EXPORT MHSampler : public Sampler {
		public:
			MHSampler(const Distribution &target, const Dict & dict);

			template <typename PT>
			MHSampler(const Distribution &target, const MatrixBase<PT> & initial);

			template <typename PT>
			MHSampler(std::unique_ptr<Proposal> && proposal, const MatrixBase<PT> & initial);

			MHSampler(const Distribution &target, const RandomInit & initial);
			MHSampler(std::unique_ptr<Proposal> && proposal, const RandomInit & initial);

		public:
			void reset() override;
			void reset(ConstRefVecd & initial) override;
			void warmup(Random &rng, size_t nwarmup) override;
			void sample(Random & rng, RefMatrixd samples) override;

			const Vecd & sample(Random & rng, size_t nwarmup = 0);
			double currentPosterior() const { return chain.posterior(); }

		public:
			bool isAdaptive() const { return adaptive; }
			bool setAdaptive(bool adapt = true);

		protected:
			void update(Random & rng);

		protected:
			Chain chain;

		protected:
			bool adaptive{false};
	};

	template <typename PT>
	MHSampler::MHSampler(std::unique_ptr<Proposal> && proposal,
			const MatrixBase<PT> & initial) : chain(std::move(proposal)) {
		reset(initial);
	}

	template <typename PT>
	inline MHSampler::MHSampler(const Distribution &target, const MatrixBase<PT> & initial)
			 : chain(std::make_unique<MH>(target)) {
		reset(initial);
	}

	inline bool MHSampler::setAdaptive(bool adapt) {
		bool old = adaptive;
		adaptive = adapt;
		return old;
	}
}
#endif
