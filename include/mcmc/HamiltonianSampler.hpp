/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MCMC_HAMILTONIANSAMPLER_H
#define MCMC_HAMILTONIANSAMPLER_H

#include <memory>
#include <mcmc/Sampler.hpp>
#include <math/number.hpp>

class Dict;
class Random;
class Distribution;

namespace mcmc {

	class DIFFMEM_EXPORT HamiltonianSampler : public Sampler {
		public:
			HamiltonianSampler(const Distribution &target, const Dict &dict);

			template <typename PT>
			HamiltonianSampler(const Distribution &target, double epsilon, int L, const MatrixBase<PT> &initial);

		public:
			void reset() override;
			void reset(ConstRefVecd & initial) override;
			void warmup(Random &rng, size_t nwarmup) override;
			void sample(Random &rng, RefMatrixd samples) override;

			const Vecd & sample(Random & rng, size_t nwarmup = 0);

		private:
			void evaluateGradient(const Vecd &q, Vecd &grad);
			double evaluatePosteriorAndGradient(const Vecd &q, Vecd &grad);
			void update(Random &rng);

		protected:
			const Distribution &target;
			double epsilon;
			int L;

		protected:
			Vecd current_q, current_grad;
			double current_U;
			Vecd p, q, grad;
	};

	template <typename PT>
	HamiltonianSampler::HamiltonianSampler(const Distribution &target,
			double epsilon, int L, const MatrixBase<PT> & initial)
			: target(target), epsilon(epsilon), L(L) {
		reset(initial);
	}

}
#endif
