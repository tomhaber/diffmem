/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MCMC_CHAIN_H
#define MCMC_CHAIN_H

#include <MatrixVector.hpp>
#include <mcmc/Proposal.hpp>
#include <Error.hpp>

class Random;
class Distribution;

namespace mcmc {

	class RandomInit;

	class DIFFMEM_EXPORT Chain {
		public:
			Chain(std::unique_ptr<Proposal> && proposal);

		public:
			double posterior() const;
			const Vecd & position() const;
			double posteriorCurrent() const;
			double posteriorCandidate() const;

			int dimensions() const;

		public:
			void initialize(ConstRefVecd & x);
			void initialize(const RandomInit & init);
			const Vecd & generateCandidate(Random & rng, double *logProposalRatio=nullptr);
			double logProposalRatio();
			double evaluateCandidate();
			void accept();
			void accept(const Vecd & x);
			void exchange(Chain & other);
			void adapt();

		public:
			int nattempts() const { return attempts; }
			double acceptanceRatio() const { return double(accepted) / double(attempts); }

		private:
			std::unique_ptr<Proposal> proposal;
			ProposalState state, cand;

		private:
			int attempts;
			int accepted;
	};

	inline double Chain::posterior() const {
		return state.posterior();
	}

	inline double Chain::posteriorCurrent() const {
		 return posterior();
	}

	inline double Chain::posteriorCandidate() const {
		return cand.posterior();
	}

	inline const Vecd & Chain::position() const {
		return state.position();
	}

	inline int Chain::dimensions() const {
		return state.position().size();
	}

	inline void Chain::accept() {
		accepted++;
		state.swap(cand);
	}

	inline void Chain::exchange(Chain & other) {
		state.swap( other.state );
	}

}
#endif
