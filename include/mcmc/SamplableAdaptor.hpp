/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MCMC_SAMPLABLEADAPTOR_H
#define MCMC_SAMPLABLEADAPTOR_H

#include <memory>
#include <DistributionSampler.hpp>
#include <Dict.hpp>
#include <mcmc/MHSampler.hpp>

class Distribution;

namespace mcmc {

	template <class SamplerType = MHSampler>
	class SamplableAdaptor : public Sampler {
	public:
		static constexpr int kNumWarmup{100};

	public:
		SamplableAdaptor(const std::shared_ptr<const Distribution> &distribution, const Dict &dict)
				: distribution(distribution), sampler(*distribution, dict) {
			nwarmup = dict.getIntegerDefault("warmup", kNumWarmup);
		}

		template <typename... Args>
		SamplableAdaptor(const std::shared_ptr<const Distribution> &distribution, Args &&... args)
				: distribution(distribution), sampler(*distribution, std::forward<Args>(args)...), nwarmup(kNumWarmup) {
		}

	public:
		void reset() override {
			sampler.reset();
		}

		void reset(ConstRefVecd & initial) override {
			sampler.reset(initial);
		}

		void warmup(Random &rng, size_t nwarmup) override {
			sampler.warmup(rng, nwarmup);
			this->nwarmup = 0;
		}

		void sample(Random &rng, RefMatrixd samples) override {
			if( trace ) {
				sampler.setTrace(trace);
				trace = nullptr;
			}

			if(nwarmup != 0) {
				sampler.warmup(rng, nwarmup);
				nwarmup = 0;
			}

			sampler.sample(rng, samples);
		}

	private:
		std::shared_ptr<const Distribution> distribution;
		SamplerType sampler;
		size_t nwarmup;
	};
}
#endif
