/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MCMC_PROPOSAL_H
#define MCMC_PROPOSAL_H

#include <common.hpp>
#include <MatrixVector.hpp>
#include <memory>
#include <math/number.hpp>

class Dict;
class Random;
class Distribution;

namespace mcmc {

	class Chain;

	class DIFFMEM_EXPORT ProposalState {
		public:
			double posterior() const { return log_posterior; }
			const Vecd & position() const { return x; }

			Vecd & set() {
				log_posterior = math::NaN();
				return x;
			}

			bool isEvaluated() const {
				return !math::isNaN( log_posterior );
			}

			void set(const Vecd & xc) {
				set() = xc;
			}

			void swap(ProposalState & other);

		public:
			const Vecd & gradient() const { return grad; }
			const Matrixd & hessian() const { return H; }

		private:
			Vecd x;

		private:
			double log_posterior;
			Vecd grad;
			Matrixd H;

		friend class Proposal;
	};

	class DIFFMEM_EXPORT Proposal {
		public:
			Proposal(const Distribution & target) : target(target) {}
			virtual ~Proposal() {}

		public:
			virtual std::string name() const = 0;
			virtual std::unique_ptr<Proposal> clone(const Distribution & target) const = 0;
			double evaluate(ProposalState & state, const Vecd & x);
			virtual double evaluate(ProposalState & state);
			virtual void generate(const ProposalState & current, ProposalState & cand, Random & rng, double *proposalRatio=nullptr) = 0;
			virtual double logProposalRatio(const ProposalState & current, const ProposalState & cand) const;
			virtual void adapt(const Chain & chain) = 0;

		protected:
			static constexpr double kSymmetricProposalRatio = 0.0;
			double evaluateGradient(ProposalState & state);
			double evaluateGradientHessian(ProposalState & state);

		private:
			const Distribution & target;
	};

}
#endif
