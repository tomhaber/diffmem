/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MCMC_MH_H
#define MCMC_MH_H

#include <mcmc/Proposal.hpp>

namespace mcmc {

	class DIFFMEM_EXPORT MH : public Proposal {
		public:
			MH(const Distribution & target, const Dict & dict);
			MH(const Distribution & target, double initStepSize = 0.05, double lowerRate = .2, double upperRate = .5, int adaptRate=50);

		public:
			std::string name() const;
			std::unique_ptr<Proposal> clone(const Distribution & target) const;
			void generate(const ProposalState & state, ProposalState & candidate, Random & rng, double *logProposalRatio=nullptr);
			void adapt(const Chain & chain);

		private:
			double stepSize;
			double lowerRate, upperRate;
			int adaptRate;
	};

	inline MH::MH(const Distribution & target, double initStepSize, double lowerRate, double upperRate, int adaptRate)
		: Proposal(target), stepSize(initStepSize), lowerRate(lowerRate), upperRate(upperRate), adaptRate(adaptRate) {
	}

}
#endif
