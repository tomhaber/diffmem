/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MCMC_POPMCMC_H
#define MCMC_POPMCMC_H

#include <MatrixVector.hpp>
#include <Error.hpp>
#include <mcmc/Proposal.hpp>
#include <mcmc/Sampler.hpp>
#include <mcmc/Chain.hpp>

#include <vector>

class Random;
class Distribution;
class PriorDistribution;

namespace mcmc {

	class DIFFMEM_EXPORT PopMCMC : protected Sampler {
		public:
			PopMCMC(Distribution & likelihood, PriorDistribution & prior,
					Proposal & proposal, std::vector<double> & temperatures);
			PopMCMC(Distribution & likelihood, PriorDistribution & prior,
					Proposal & proposal, int N, double temperatures[]);
			~PopMCMC();

		public:
			void initialize(Random & rng);

			template <typename PT>
			void initialize(const MatrixBase<PT> & initial);

			template <typename PT, typename ST>
			void sample(Random & rng, const MatrixBase<PT> & initial,
					int nsamples, MatrixBase<ST> & samples, int thinning = 1);

			const Vecd & update(Random & rng);

		private:
			void initializeChains(const Vecd & x);
			void updateChains(Random & rng);
			void exchangeChains(Random & rng);

		private:
			std::vector<Chain*> chains;
	};

	template <typename PT>
	void PopMCMC::initialize(const MatrixBase<PT> & initial) {
		s = initial;
		initializeChains(s);
	}

	template <typename PT, typename ST>
	void PopMCMC::sample(Random & rng, const MatrixBase<PT> & initial,
			int nsamples, MatrixBase<ST> & samples, int thinning) {
		Assert(thinning >= 1, "Thinning option should be >= 1");

		const int DIM = initial.size();
		samples.resize(DIM, nsamples);

		initialize(initial);
		for(int i = 0; i < nsamples*thinning; ++i) {
			const Vecd & p = update(rng);
			if( (i % thinning) == (thinning-1) ) {
				samples.col(i) = p;
			}
		}
	}

}
#endif
