/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MCMC_MESAMPLER_H
#define MCMC_MESAMPLER_H

#include <MEModel.hpp>
#include <ConditionalMEDistribution.hpp>
#include <mcmc/MHSampler.hpp>
#include <mcmc/ConditionalMESampler.hpp>
#include <memory>

class Random;

namespace mcmc {

	class DIFFMEM_EXPORT PopulationDistribution : public Distribution {
		public:
			PopulationDistribution(const MEModel & model, Distribution & prior, Matrixd & etas);

		public:
			int numberOfDimensions() const override;
			int numberOfPopulationDimensions() const;
			int numberOfIndividualDimensions() const;

		public:
			double logpdf(ConstRefVecd & x) const override;
			double logpdf(ConstRefVecd & x, RefVecd grad) const override;

		private:
			const MEModel & model;
			Distribution & prior;
			const Matrixd & etas;
	};

	class DIFFMEM_EXPORT MESampler : public Sampler {
		public:
			static constexpr size_t kThinning = 10;

		public:
			MESampler(const MEModel & model, Distribution & prior, size_t thinning = kThinning);

		public:
			template <typename BT, typename OT, typename TT>
			void reset(const MatrixBase<BT> & beta, const Cholesky<OT> & chol_omega, const MatrixBase<TT> & tau);

			int dimensions() const;
			void warmup(Random & rng, size_t nwarmup) override;
			void reset() override;
			void reset(ConstRefVecd & initial) override;
			void sample(Random & rng, RefMatrixd samples) override;

		private:
			template <typename ET, typename BT, typename OT, typename TT, typename XT>
			void pack(const PlainObjectBase<ET> & etas, const MatrixBase<BT> & beta,
				const MatrixBase<OT> & omega, const MatrixBase<TT> & tau, const MatrixBase<XT> & x);

			template <typename ET, typename BT, typename OT, typename TT, typename XT>
			void pack(const PlainObjectBase<ET> & etas, const MatrixBase<BT> & beta,
				const Cholesky<OT> & chol_omega, const MatrixBase<TT> & tau, const MatrixBase<XT> & x);

			template <typename XT, typename BT, typename OT, typename TT>
			void unpack(const MatrixBase<XT> & x, PlainObjectBase<BT> & beta,
				Cholesky<OT> & chol_omega, PlainObjectBase<TT> & tau);

			template <typename XT, typename ET, typename BT, typename OT, typename TT>
			void unpack(const MatrixBase<XT> & x, PlainObjectBase<ET> & etas, PlainObjectBase<BT> & beta,
				PlainObjectBase<OT> & omega, PlainObjectBase<TT> & tau);

		private:
			void update(Random & rng);

		private:
			const MEModel & model;
			PopulationDistribution distribution;
			std::unique_ptr<MHSampler> hmc;
			std::unique_ptr<ConditionalMEDistribution> condme;
			std::unique_ptr<mcmc::ConditionalMESampler> sampler;
			size_t thinning;
			Vecd beta;
			Cholesky<Matrixd> chol_omega;
			Vecd tau;
			Matrixd etas;
			Vecd s;
	};

	template <typename ET, typename BT, typename OT, typename TT, typename XT>
	void MESampler::pack(const PlainObjectBase<ET> & etas, const MatrixBase<BT> & beta,
			const MatrixBase<OT> & omega, const MatrixBase<TT> & tau, const MatrixBase<XT> & x_) {
		MatrixBase<XT> & x = const_cast<MatrixBase<XT> &>(x_);
		Cholesky<Matrixd> chol_omega( omega );
		x.resize( etas.size() + beta.size() + chol_omega.numberOfCoefficients() + tau.size() );
		x.head( etas.size() ) = ConstMapVecd(etas.data(), etas.size());
		x.segment( etas.size(), beta.size() ) = beta;
		auto coeffs = x.segment( etas.size() + beta.size(), chol_omega.numberOfCoefficients());
		chol_omega.extractCoefficients( coeffs );
		x.tail( tau.size() ) = tau;
	}

	template <typename ET, typename BT, typename OT, typename TT, typename XT>
	void MESampler::pack(const PlainObjectBase<ET> & etas, const MatrixBase<BT> & beta,
			const Cholesky<OT> & chol_omega, const MatrixBase<TT> & tau, const MatrixBase<XT> & x_) {
		MatrixBase<XT> & x = const_cast<MatrixBase<XT> &>(x_);
		x.resize( etas.size() + beta.size() + chol_omega.numberOfCoefficients() + tau.size() );
		x.head( etas.size() ) = ConstMapVecd(etas.data(), etas.size());
		x.segment( etas.size(), beta.size() ) = beta;
		auto coeffs = x.segment( etas.size() + beta.size(), chol_omega.numberOfCoefficients());
		chol_omega.extractCoefficients( coeffs );
		x.tail( tau.size() ) = tau;
	}

	template <typename XT, typename BT, typename OT, typename TT>
	void MESampler::unpack(const MatrixBase<XT> & x, PlainObjectBase<BT> & beta,
			Cholesky<OT> & chol_omega, PlainObjectBase<TT> & tau) {
		const int M = distribution.numberOfIndividualDimensions();
		Require( beta.size() != 0 && chol_omega.totalSize() != 0 && tau.size() != 0, "beta, omega and tau should be initialized");
		Require( M + beta.size() + chol_omega.numberOfCoefficients() + tau.size() == x.size(), "unpack locations have incorrect size" );

		beta = x.segment( M, beta.size() );
		chol_omega.fromCoefficients( x.segment( M + beta.size(), chol_omega.numberOfCoefficients()) );
		tau = x.tail( tau.size() );
	}

	template <typename XT, typename ET, typename BT, typename OT, typename TT>
	void MESampler::unpack(const MatrixBase<XT> & x, PlainObjectBase<ET> & etas, PlainObjectBase<BT> & beta,
			PlainObjectBase<OT> & omega, PlainObjectBase<TT> & tau) {
		Require( etas.size() != 0 && beta.size() != 0 && omega.size() != 0 && tau.size() != 0, "etas, beta, omega and tau should be initialized");
		const int ncoeffs = Cholesky<Matrixd>::numberOfCoefficients( omega.rows() );
		Require( etas.size() + beta.size() + ncoeffs + tau.size() == x.size(), "unpack locations have incorrect size" );

		etas = x.head( etas.size() );
		beta = x.segment( etas.size(), beta.size() );
		tau = x.tail( tau.size() );

		Cholesky<Matrixd> chol_omega;
		chol_omega.fromCoefficients( x.segment( etas.size() + beta.size(), ncoeffs) );
		omega = chol_omega.reconstructedMatrix();
	}

	template <typename BT, typename OT, typename TT>
	void MESampler::reset(const MatrixBase<BT> & beta_, const Cholesky<OT> & chol_omega_, const MatrixBase<TT> & tau_) {
		beta = beta_;
		chol_omega = chol_omega_;
		tau = tau_;
	}

}
#endif
