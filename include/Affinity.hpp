/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef AFFINITY_H
#define AFFINITY_H

#include <common.hpp>
#include <sched.h>
#include <string>

class DIFFMEM_EXPORT Affinity {
	public:
		Affinity();
		Affinity(size_t ncpus);
		Affinity(const std::string & list);
		~Affinity();

		Affinity(const Affinity &) = delete;
		Affinity& operator =(const Affinity &) = delete;
		Affinity(Affinity && other) { *this = std::move(other); }
		Affinity& operator =(Affinity && other) {
			std::swap(this->mask, other.mask);
			std::swap(this->setsize, other.setsize);
			return *this;
		}

	public:
		static Affinity active();
		static size_t findMaxCPUs();
		static constexpr pid_t currentPid{0};

	public:
		size_t numcpus() const;
		size_t size() const { return numcpus(); }
		size_t maxcpus() const;

	public:
		std::string toString() const;
		void fromString(const std::string &s);

	public:
		void clear();
		bool has(size_t id) const;
		void add(size_t id);
		void set(pid_t pid = currentPid);

		Affinity pin(size_t idx, pid_t pid = currentPid);

	private:
		Affinity(cpu_set_t *mask, size_t setsize) : mask(mask), setsize(setsize) {}

	private:
		cpu_set_t *mask{nullptr};
		size_t setsize{0};
};

inline std::string to_string(Affinity & af) {
	return af.toString();
}
#endif