/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef REDUCE_H
#define REDUCE_H

#include <function_traits.hpp>

template <typename Index, typename Value, typename Function,
		typename T = decayedResultOf<Function, Index>>
T reduce_sum(Index first, Index last, const Value &init, const Function &f) {
	T accum = init;

	for(Index i = first; i < last; i++) {
		accum += f(i);
	}

	return accum;
}

template <typename Index, typename Function, typename T = decayedResultOf<Function, Index>>
T reduce_sum(Index first, Index last, const Function &f) {
	return reduce_sum(first, last, T{}, f);
}

template <typename Range, typename Value, typename Function,
		typename T = decayedResultOf<Function, typename Range::const_iterator>>
T reduce_sum(const Range &range, const Value &init, const Function &f) {
	T accum = init;

	for(typename Range::const_iterator i = range.begin(); i != range.end(); ++i) {
		accum += f(i);
	}

	return accum;
}

template <typename Range, typename Function,
		typename T = decayedResultOf<Function, typename Range::const_iterator>>
T reduce_sum(const Range &range, const Function &f) {
	return reduce_sum(range, T{}, f);
}
#endif