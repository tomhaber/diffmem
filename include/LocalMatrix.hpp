/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LOCALMATRIX_H_
#define LOCALMATRIX_H_

#include <common.hpp>
#include <MatrixVector.hpp>
#include <Likely.hpp>

template<typename T>
struct aligned_stack_memory {
	aligned_stack_memory(void* vptr, size_t size) : size(size), deallocate(vptr == nullptr) {
		if( UNLIKELY(vptr == nullptr) )
			vptr = Eigen::internal::aligned_malloc(size);

		ptr = reinterpret_cast<T*>(vptr);
		for (size_t i=0; i < size; ++i) ::new (ptr + i) T;
	}

	~aligned_stack_memory() {
		while(size) ptr[--size].~T();
		if( deallocate )
			Eigen::internal::aligned_free(ptr);
	}

	T* ptr;
	size_t size;
	bool deallocate;
};

#define STACK_ALLOCATION_LIMIT (1024*64)

#if EIGEN_VERSION_AT_LEAST(3,3,0)
	#	if EIGEN_DEFAULT_ALIGN_BYTES>0
	#		define ALIGNED_ALLOCA(SIZE) reinterpret_cast<void*>((Eigen::internal::UIntPtr(EIGEN_ALLOCA(SIZE+EIGEN_DEFAULT_ALIGN_BYTES-1)) + EIGEN_DEFAULT_ALIGN_BYTES-1) & ~(std::size_t(EIGEN_DEFAULT_ALIGN_BYTES-1)))
	#	else
	#		define ALIGNED_ALLOCA(SIZE) EIGEN_ALLOCA(SIZE)
	#	endif
#else
	#	define ALIGNED_ALLOCA(SIZE) EIGEN_ALIGNED_ALLOCA(SIZE)
#endif

#define LOCAL_VECTOR(TYPE, NAME, SIZE) \
	aligned_stack_memory<TYPE::Scalar> CONCATENATE(NAME, _buffer) ( \
		LIKELY(sizeof(TYPE::Scalar)*SIZE<=STACK_ALLOCATION_LIMIT) ? \
			ALIGNED_ALLOCA(sizeof(TYPE::Scalar)*SIZE) : nullptr, SIZE); \
	Eigen::Map<TYPE, Eigen::Aligned> NAME(CONCATENATE(NAME, _buffer).ptr, SIZE)

#define LOCAL_MATRIX(TYPE, NAME, NROW, NCOL) \
	aligned_stack_memory<TYPE::Scalar> CONCATENATE(NAME, _buffer) ( \
		LIKELY(sizeof(TYPE::Scalar)*NCOL*NROW<=STACK_ALLOCATION_LIMIT) ? \
			ALIGNED_ALLOCA(sizeof(TYPE::Scalar)*NCOL*NROW) : nullptr, NCOL*NROW); \
	Eigen::Map<TYPE, Eigen::Aligned> NAME(CONCATENATE(NAME, _buffer).ptr, NROW, NCOL)
#endif
