/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LOCKE_H
#define LOCKE_H

#include <cmath>
#include <MatrixVector.hpp>

#define g1 0
#define g2 1
#define k1 2
#define k2 3
#define k3 4
#define k4 5
#define k5 6
#define k6 7
#define m1 8
#define m2 9
#define m3 10
#define m4 11
#define m5 12
#define m6 13
#define n1 14
#define n2 15
#define p1 16
#define p2 17
#define r1 18
#define r2 19
#define r3 20
#define r4 21
#define LHYm0 22
#define LHYc0 23
#define LHYn0 24
#define TOC1m0 25
#define TOC1c0 26
#define TOC1n0 27

#define LHYm 0
#define LHYc 1
#define LHYn 2
#define TOC1m 3
#define TOC1c 4
#define TOC1n 5

class Locke {
	public:
		static constexpr int numberOfEquations() { return 6; }
		static constexpr int numberOfParameters() { return 28; }
		static constexpr int numberOfObservations() { return 6; }

	class ODE {
		public:
			static bool isStiff() { return true; }

			template <typename ST, typename PT, typename SDT>
			static void ddt(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<SDT> & ddt_y) {
				ddt_y[LHYm] = (p[n1]*y[TOC1n])/(p[g1] + y[TOC1n]) - (p[m1] * y[LHYm])/(p[k1] + y[LHYm]);
				ddt_y[LHYc] = p[p1]*y[LHYm] - p[r1]*y[LHYc] + p[r2]*y[LHYn] - (p[m2] * y[LHYc])/(p[k2] + y[LHYc]);
				ddt_y[LHYn] = p[r1]*y[LHYc] - p[r2]*y[LHYn] - (p[m3] * y[LHYn])/(p[k3] + y[LHYn]);
				ddt_y[TOC1m] = (p[n2]*pow(p[g2], 2.0))/(pow(p[g2], 2.0) + pow(y[LHYn],2.0)) - (p[m4] * y[TOC1m])/(p[k4] + y[TOC1m]);
				ddt_y[TOC1c] = p[p2]*y[TOC1m] - p[r3]*y[TOC1c] + p[r4]*y[TOC1n] - (p[m5] * y[TOC1c])/(p[k5] + y[TOC1c]);
				ddt_y[TOC1n] = p[r3]*y[TOC1c] - p[r4]*y[TOC1n] - (p[m6] * y[TOC1n])/(p[k6] + y[TOC1n]);
			}

			template <typename ST, typename PT>
			static void initial(const MatrixBase<PT> & p, MatrixBase<ST> & y) {
				y[LHYm] = p[LHYm0];
				y[LHYc] = p[LHYc0];
				y[LHYn] = p[LHYn0];
				y[TOC1m] = p[TOC1m0];
				y[TOC1c] = p[TOC1c0];
				y[TOC1n] = p[TOC1n0];
			}

		public:
			template <typename ST, typename PT, typename JT>
			static void jacobianState(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(LHYm, LHYm) = (y[LHYm] * p[m1]) / pow(y[LHYm] + p[k1],2) - p[m1] / (y[LHYm] + p[k1]);
				J(LHYm, LHYc) = 0;
				J(LHYm, LHYn) = 0;
				J(LHYm, TOC1m) = 0;
				J(LHYm, TOC1c) = 0;
				J(LHYm, TOC1n) = p[n1] / (y[TOC1n] + p[g1]) - (y[TOC1n] * p[n1]) / pow(y[TOC1n] + p[g1],2);
				J(LHYc, LHYm) = p[p1];
				J(LHYc, LHYc) = ((y[LHYc] * p[m2]) / pow(y[LHYc] + p[k2],2) - p[m2] / (y[LHYc] + p[k2])) - p[r1];
				J(LHYc, LHYn) = p[r2];
				J(LHYc, TOC1m) = 0;
				J(LHYc, TOC1c) = 0;
				J(LHYc, TOC1n) = 0;
				J(LHYn, LHYm) = 0;
				J(LHYn, LHYc) = p[r1];
				J(LHYn, LHYn) = ((y[LHYn] * p[m3]) / pow(y[LHYn] + p[k3],2) - p[m3] / (y[LHYn] + p[k3])) - p[r2];
				J(LHYn, TOC1m) = 0;
				J(LHYn, TOC1c) = 0;
				J(LHYn, TOC1n) = 0;
				J(TOC1m, LHYm) = 0;
				J(TOC1m, LHYc) = 0;
				J(TOC1m, LHYn) = -(2 * y[LHYn] * pow(p[g2],2) * p[n2]) / pow(pow(y[LHYn],2) + pow(p[g2],2),2);
				J(TOC1m, TOC1m) = (y[TOC1m] * p[m4]) / pow(y[TOC1m] + p[k4],2) - p[m4] / (y[TOC1m] + p[k4]);
				J(TOC1m, TOC1c) = 0;
				J(TOC1m, TOC1n) = 0;
				J(TOC1c, LHYm) = 0;
				J(TOC1c, LHYc) = 0;
				J(TOC1c, LHYn) = 0;
				J(TOC1c, TOC1m) = p[p2];
				J(TOC1c, TOC1c) = ((y[TOC1c] * p[m5]) / pow(y[TOC1c] + p[k5],2) - p[m5] / (y[TOC1c] + p[k5])) - p[r3];
				J(TOC1c, TOC1n) = p[r4];
				J(TOC1n, LHYm) = 0;
				J(TOC1n, LHYc) = 0;
				J(TOC1n, LHYn) = 0;
				J(TOC1n, TOC1m) = 0;
				J(TOC1n, TOC1c) = p[r3];
				J(TOC1n, TOC1n) = ((y[TOC1n] * p[m6]) / pow(y[TOC1n] + p[k6],2) - p[m6] / (y[TOC1n] + p[k6])) - p[r4];
			}

			template <typename ST, typename PT, typename JT>
			static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J = JT::Zero(numberOfEquations(), numberOfParameters());
				J(LHYm, g1) = -(y[TOC1n] * p[n1]) / pow(y[TOC1n] + p[g1],2);
				J(LHYm, k1) = (y[LHYm] * p[m1]) / pow(y[LHYm] + p[k1],2);
				J(LHYm, m1) = -(y[LHYm]) / (y[LHYm] + p[k1]);
				J(LHYm, n1) = y[TOC1n] / (y[TOC1n] + p[g1]);
				J(LHYc, k2) = (y[LHYc] * p[m2]) / pow(y[LHYc] + p[k2],2);
				J(LHYc, m2) = -(y[LHYc]) / (y[LHYc] + p[k2]);
				J(LHYc, p1) = y[LHYm];
				J(LHYc, r1) = -(y[LHYc]);
				J(LHYc, r2) = y[LHYn];
				J(LHYn, k3) = (y[LHYn] * p[m3]) / pow(y[LHYn] + p[k3],2);
				J(LHYn, m3) = -(y[LHYn]) / (y[LHYn] + p[k3]);
				J(LHYn, r1) = y[LHYc];
				J(LHYn, r2) = -(y[LHYn]);
				J(TOC1m, g2) = (2 * p[g2] * p[n2]) / (pow(y[LHYn],2) + pow(p[g2],2)) - (2 * pow(p[g2],3) * p[n2]) / pow(pow(y[LHYn],2) + pow(p[g2],2),2);
				J(TOC1m, k4) = (y[TOC1m] * p[m4]) / pow(y[TOC1m] + p[k4],2);
				J(TOC1m, m4) = -(y[TOC1m]) / (y[TOC1m] + p[k4]);
				J(TOC1m, n2) = pow(p[g2],2) / (pow(y[LHYn],2) + pow(p[g2],2));
				J(TOC1c, k5) = (y[TOC1c] * p[m5]) / pow(y[TOC1c] + p[k5],2);
				J(TOC1c, m5) = -(y[TOC1c]) / (y[TOC1c] + p[k5]);
				J(TOC1c, p2) = y[TOC1m];
				J(TOC1c, r3) = -(y[TOC1c]);
				J(TOC1c, r4) = y[TOC1n];
				J(TOC1n, k6) = (y[TOC1n] * p[m6]) / pow(y[TOC1n] + p[k6],2);
				J(TOC1n, m6) = -(y[TOC1n]) / (y[TOC1n] + p[k6]);
				J(TOC1n, r3) = y[TOC1c];
				J(TOC1n, r4) = -(y[TOC1n]);

			}

			template <typename PT, typename JT>
			static void jacobianParametersInitial(const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J = JT::Zero(numberOfEquations(), numberOfParameters());
				J(LHYm, LHYm0) = 1;
				J(LHYc, LHYc0) = 1;
				J(LHYn, LHYn0) = 1;
				J(TOC1m, TOC1m0) = 1;
				J(TOC1c, TOC1c0) = 1;
				J(TOC1n, TOC1n0) = 1;
			}
	};

	class Projection {
		public:
			template <typename ST, typename PT, typename OT>
			static void project(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<OT> & o) {
				o = y;
			}

			template <typename ST, typename PT, typename JT>
			static void jacobian(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
			  J = JT::Identity(6,6);
			}

			template <typename ST, typename PT, typename JT>
			static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J.setZero();
			}
	};

	class Transform {
		public:
			template <typename IT, typename OT>
			static void transphi(const MatrixBase<IT> & psi, MatrixBase<OT> & phi) {
				phi = psi;
			}

			template <typename IT, typename OT>
			static void transpsi(const MatrixBase<IT> & phi, MatrixBase<OT> & psi) {
				psi = phi;
			}

			template <typename IT, typename OT>
			static void dtranspsi(const MatrixBase<IT> & phi, MatrixBase<OT> & dpsi) {
				dpsi = OT::Ones( numberOfParameters() );
			}

			template <typename IT, typename OT>
			static void ddtranspsi(const MatrixBase<IT> & phi, MatrixBase<OT> & ddpsi) {
				ddpsi = OT::Zero( numberOfParameters() );
			}
	};

};
#undef g1
#undef g2
#undef k1
#undef k2
#undef k3
#undef k4
#undef k5
#undef k6
#undef m1
#undef m2
#undef m3
#undef m4
#undef m5
#undef m6
#undef n1
#undef n2
#undef p1
#undef p2
#undef r1
#undef r2
#undef r3
#undef r4
#undef a
#undef b

#undef LHYm
#undef LHYc
#undef LHYn
#undef TOC1m
#undef TOC1c
#undef TOC1n
#endif
