/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CANA_H
#define CANA_H

#include <cmath>
#include <MatrixVector.hpp>

#define KOHB 0
#define HB0 1
#define EFP 2
#define EC50 3
#define Emax 4
#define KA 5
#define K23 6
#define K32 7
#define K 8
#define V2 9
#define DOSE 10

#define PK 0
#define PD 1

class Cana {
	public:
		static constexpr int numberOfEquations() { return 4; }
		static constexpr int numberOfParameters() { return 11; }
		static constexpr int numberOfObservations() { return 1; }

	class ODE {
		public:
			static bool isStiff() { return true; }

		public:
			template <typename ST, typename PT, typename SDT>
			static void ddt(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<SDT> & ddt_y) {
				ddt_y[0] = -p[KA] * y[0];
				ddt_y[1] = p[KA] * y[0] - p[K] * y[1] - p[K23] * y[1] + p[K32] * y[2];
				ddt_y[2] = p[K23] * y[1] - p[K32] * y[2];

				const double KIHB = p[KOHB] * p[HB0];
				const double Ct = y[1]/p[V2];
				const double EFC = (p[DOSE] > 0) ? (p[Emax] * Ct / (p[EC50] + Ct)) : 0;
				const double EFF = (t > 0) ? (p[KOHB] * (p[EFP] + EFC) * (p[HB0]-5)/(8-5)) : 0;
				ddt_y[3] = EFF + KIHB - p[KOHB] * y[3];
			}

			template <typename ST, typename PT>
			static void initial(const MatrixBase<PT> & p, MatrixBase<ST> & y) {
				y[0] = 0.0;
				y[1] = 0.0;
				y[2] = 0.0;
				y[3] = p[HB0];
			}

		public:
			template <typename ST, typename PT, typename JT>
			static void jacobianState(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(0, 0) = -p[KA];
				J(0, 1) = 0.0;
				J(0, 2) = 0.0;
				J(0, 3) = 0.0;

				J(1, 0) = p[KA];
				J(1, 1) = -p[K] - p[K23];
				J(1, 2) = p[K32];
				J(1, 3) = 0.0;

				J(2, 0) = 0.0;
				J(2, 1) = p[K23];
				J(2, 2) = -p[K32];
				J(2, 3) = 0.0;

				J(3, 0) = 0.0;
				J(3, 1) = 0.0;
				const double dEFC = (p[DOSE] > 0) ? ((p[Emax]*p[EC50]*p[V2]) / ((p[EC50]*p[V2] + y[1])*(p[EC50]*p[V2] + y[1]))) : 0;
				const double dEFF = (t > 0) ? (p[KOHB] * dEFC * (p[HB0]-5)/(8-5)) : 0;
				J(3, 2) = dEFF;
				J(3, 3) = -p[KOHB];
			}

			template <typename ST, typename PT, typename JT>
			static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(0, KOHB) = 0.0;
				J(0, HB0) = 0.0;
				J(0, EFP) = 0.0;
				J(0, EC50) = 0.0;
				J(0, Emax) = 0.0;
				J(0, KA) = -y[0];
				J(0, K23) = 0.0;
				J(0, K32) = 0.0;
				J(0, K) = 0.0;
				J(0, V2) = 0.0;
				J(0, DOSE) = 0.0;

				J(1, KOHB) = 0.0;
				J(1, HB0) = 0.0;
				J(1, EFP) = 0.0;
				J(1, EC50) = 0.0;
				J(1, Emax) = 0.0;
				J(1, KA) = y[0];
				J(1, K23) = -y[1];
				J(1, K32) = y[2];
				J(1, K) = -y[1];
				J(1, V2) = 0.0;
				J(1, DOSE) = 0.0;

				J(2, KOHB) = 0.0;
				J(2, HB0) = 0.0;
				J(2, EFP) = 0.0;
				J(2, EC50) = 0.0;
				J(2, Emax) = 0.0;
				J(2, KA) = 0.0;
				J(2, K23) = y[1];
				J(2, K32) = -y[2];
				J(2, K) = 0.0;
				J(2, V2) = 0.0;
				J(2, DOSE) = 0.0;

				const double Ct = y[1]/p[V2];
				const double EFC = (p[DOSE] > 0) ? (p[Emax] * Ct / (p[EC50] + Ct)) : 0;

				J(3, KOHB) = -y[3] + p[HB0] + ((t > 0) ? ((p[EFP] + EFC) * (p[HB0]-5)/(8-5)) : 0);
				J(3, HB0) = p[KOHB] + ((t > 0) ? (p[KOHB] * (p[EFP] + EFC) / (8-5)) : 0);
				J(3, EFP) = (t > 0) ? (p[KOHB] * (p[HB0]-5)/(8-5)) : 0.0;

				const double dEFC_EC50 = (p[DOSE] > 0) ? (p[Emax] * Ct / ((p[EC50] + Ct) * (p[EC50] + Ct))) : 0;
				J(3, EC50) = (t > 0) ? (p[KOHB] * dEFC_EC50 * (p[HB0]-5)/(8-5)) : 0;

				const double dEFC_Emax = (p[DOSE] > 0) ? (Ct / (p[EC50] + Ct)) : 0;
				J(3, Emax) = (t > 0) ? (p[KOHB] * dEFC_Emax * (p[HB0]-5)/(8-5)) : 0;

				J(3, KA) = 0.0;
				J(3, K23) = 0.0;
				J(3, K32) = 0.0;
				J(3, K) = 0.0;
				const double dEFC_V2 = (p[DOSE] > 0) ? ((p[Emax]*p[EC50]*y[1]) / ((p[EC50]*p[V2] + y[1])*(p[EC50]*p[V2] + y[1]))) : 0;
				J(3, V2) = (t > 0) ? (p[KOHB] * dEFC_V2 * (p[HB0]-5)/(8-5)) : 0;;
				J(3, DOSE) = 0.0;
			}

			template <typename PT, typename JT>
			static void jacobianParametersInitial(const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(0, KOHB) = 0.0;
				J(0, HB0) = 0.0;
				J(0, EFP) = 0.0;
				J(0, EC50) = 0.0;
				J(0, Emax) = 0.0;
				J(0, KA) = 0.0;
				J(0, K23) = 0.0;
				J(0, K32) = 0.0;
				J(0, K) = 0.0;
				J(0, V2) = 0.0;
				J(0, DOSE) = 0.0;

				J(1, KOHB) = 0.0;
				J(1, HB0) = 0.0;
				J(1, EFP) = 0.0;
				J(1, EC50) = 0.0;
				J(1, Emax) = 0.0;
				J(1, KA) = 0.0;
				J(1, K23) = 0.0;
				J(1, K32) = 0.0;
				J(1, K) = 0.0;
				J(1, V2) = 0.0;
				J(1, DOSE) = 0.0;

				J(2, KOHB) = 0.0;
				J(2, HB0) = 0.0;
				J(2, EFP) = 0.0;
				J(2, EC50) = 0.0;
				J(2, Emax) = 0.0;
				J(2, KA) = 0.0;
				J(2, K23) = 0.0;
				J(2, K32) = 0.0;
				J(2, K) = 0.0;
				J(2, V2) = 0.0;
				J(2, DOSE) = 0.0;

				J(3, KOHB) = 0.0;
				J(3, HB0) = 1.0;
				J(3, EFP) = 0.0;
				J(3, EC50) = 0.0;
				J(3, Emax) = 0.0;
				J(3, KA) = 0.0;
				J(3, K23) = 0.0;
				J(3, K32) = 0.0;
				J(3, K) = 0.0;
				J(3, V2) = 0.0;
				J(3, DOSE) = 0.0;
			}
	};

	class Projection {
		public:
			static inline double clamp(double x) {
				return (x <= 0) ? 0.00001 : x;
			}

			template <typename ST, typename PT, typename OT>
			static void project(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<OT> & o) {
				//o[PK] = y[2];
				//o[PD] = y[3];
				o[0] = std::log( clamp(y[3]) );
			}

			template <typename ST, typename PT, typename JT>
			static void jacobian(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				/*
			  J(PK,0) = 0.0;
			  J(PK,1) = 0.0;
			  J(PK,2) = 1.0;
			  J(PK,3) = 0.0;

			  J(PD,0) = 0.0;
			  J(PD,1) = 0.0;
			  J(PD,2) = 0.0;
			  J(PD,3) = 1.0;
				*/
				J(0,0) = 0.0;
				J(0,1) = 0.0;
				J(0,2) = 0.0;
				J(0,3) = (y[3] <= 0.0) ? 0.0 : (1.0 / y[3]);
			}

			template <typename ST, typename PT, typename JT>
			static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J.setZero();
			}
	};

	class Transform {
		public:
			template <typename IT, typename OT>
			static void transphi(const MatrixBase<IT> & psi, MatrixBase<OT> & phi) {
				phi[KOHB] = log(2) / (psi[KOHB] * 24);
				phi[HB0] = log(psi[HB0]);
				phi[EFP] = psi[EFP];
				phi[EC50] = log(psi[EC50]);
				phi[Emax] = psi[Emax];

				phi[KA] = psi[KA];
				phi[K23] = psi[K23];
				phi[K32] = psi[K32];
				phi[K] = psi[K];

				phi[V2] = psi[V2];
				phi[DOSE] = psi[DOSE];
			}

			template <typename IT, typename OT>
			static void transpsi(const MatrixBase<IT> & phi, MatrixBase<OT> & psi) {
				psi[KOHB] = log(2) / (phi[KOHB] * 24);
				psi[HB0] = exp(phi[HB0]);
				psi[EFP] = phi[EFP];
				psi[EC50] = exp(phi[EC50]);
				psi[Emax] = phi[Emax];

				psi[KA] = phi[KA];
				psi[K23] = phi[K23];
				psi[K32] = phi[K32];
				psi[K] = phi[K];

				psi[V2] = phi[V2];
				psi[DOSE] = phi[DOSE];
			}

			template <typename IT, typename OT>
			static void dtranspsi(const MatrixBase<IT> & phi, MatrixBase<OT> & dpsi) {
				dpsi[KOHB] = -log(2) / (phi[KOHB]*phi[KOHB] * 24);
				dpsi[HB0] = exp(phi[HB0]);
				dpsi[EFP] = 1.0;
				dpsi[EC50] = exp(phi[EC50]);
				dpsi[Emax] = 1.0;

				dpsi[KA] = 1.0;
				dpsi[K23] = 1.0;
				dpsi[K32] = 1.0;
				dpsi[K] = 1.0;

				dpsi[V2] = 1.0;
				dpsi[DOSE] = 1.0;
			}
	};

};

#undef ka
#undef ke
#undef Vf
#undef kin
#undef kout
#undef IC50
#undef A0
#undef B0
#undef C0

#undef A
#undef B
#undef C

#undef PK
#undef PD
#endif
