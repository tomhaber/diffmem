/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef FITZHUGH_H
#define FITZHUGH_H

#include <cmath>
#include <MatrixVector.hpp>

#define a 0
#define b 1
#define c 2
#define V0 3
#define R0 4

#define V 0
#define R 1

class Fitzhugh {
	public:
		static constexpr int numberOfEquations() { return 2; }
		static constexpr int numberOfParameters() { return 5; }
		static constexpr int numberOfObservations() { return 2; }

	class ODE {
		public:
			static bool isStiff() { return false; }

			template <typename ST, typename PT, typename SDT>
			static void ddt(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<SDT> & ddt_y) {
				ddt_y[V] = p[c]*(y[V] - (y[V]*y[V]*y[V]) / 3 + y[R]);
				ddt_y[R] = -(y[V] - p[a] + p[b]*y[R]) / p[c];
			}

			template <typename ST, typename PT>
			static void initial(const MatrixBase<PT> & p, MatrixBase<ST> & y) {
				y[V] = p[V0];
				y[R] = p[R0];
			}

		public:
			template <typename ST, typename PT, typename JT>
			static void jacobianState(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(V, V) = -p[c]*(y[V]*y[V] - 1.);
				J(V, R) = p[c];
				J(R, V) = -1.0 / p[c];
				J(R, R) = -p[b]/p[c];
			}

			template <typename ST, typename PT, typename JT>
			static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(V, a) = 0.0;
				J(V, b) = 0.0;
				J(V, c) = -(y[V]*y[V]*y[V])/3 + y[V] + y[R];
				J(V, V0) = 0.0;
				J(V, R0) = 0.0;

				J(R, a) = 1.0/p[c];
				J(R, b) = -y[R]/p[c];
				J(R, c) = (y[V] - p[a] + p[b]*y[R])/(p[c]*p[c]);
				J(R, V0) = 0.0;
				J(R, R0) = 0.0;
			}

			template <typename PT, typename JT>
			static void jacobianParametersInitial(const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(V, a) = 0.0;
				J(V, b) = 0.0;
				J(V, c) = 0.0;
				J(V, V0) = 1.0;
				J(V, R0) = 0.0;

				J(R, a) = 0.0;
				J(R, b) = 0.0;
				J(R, c) = 0.0;
				J(R, V0) = 0.0;
				J(R, R0) = 1.0;
			}
	};

	class Projection {
		public:
			template <typename ST, typename PT, typename OT>
			static void project(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<OT> & o) {
				o[V] = y[V];
				o[R] = y[R];
			}

			template <typename ST, typename PT, typename JT>
			static void jacobian(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
			  J(V,V) = 1.0;
			  J(V,R) = 0.0;
			  J(R,V) = 0.0;
			  J(R,R) = 1.0;
			}

			template <typename ST, typename PT, typename JT>
			static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J.setZero();
			}
	};

	class Transform {
		public:
			template <typename IT, typename OT>
			static void transphi(const MatrixBase<IT> & psi, MatrixBase<OT> & phi) {
				phi[a] = psi[a];
				phi[b] = psi[b];
				phi[c] = psi[c];
				phi[V0] = psi[V0];
				phi[R0] = psi[R0];
			}

			template <typename IT, typename OT>
			static void transpsi(const MatrixBase<IT> & phi, MatrixBase<OT> & psi) {
				psi[a] = phi[a];
				psi[b] = phi[b];
				psi[c] = phi[c];
				psi[V0] = phi[V0];
				psi[R0] = phi[R0];
			}

			template <typename IT, typename OT>
			static void dtranspsi(const MatrixBase<IT> & phi, MatrixBase<OT> & psi) {
				psi[a] = 1.0;
				psi[b] = 1.0;
				psi[c] = 1.0;
				psi[V0] = 1.0;
				psi[R0] = 1.0;
			}
	};

};
#undef a
#undef b
#undef c
#undef V0
#undef R0

#undef V
#undef R
#endif
