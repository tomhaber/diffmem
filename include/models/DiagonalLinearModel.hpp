/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef	DIAGONALLINEARMODEL_H
#define DIAGONALLINEARMODEL_H

#include <cmath>
#include <MatrixVector.hpp>

class DiagonalLinearModel {
	public:
		static constexpr int numberOfEquations() { return 30; }
		static constexpr int numberOfParameters() { return 30; }
		static constexpr int numberOfObservations() { return 30; }

		class ODE {
			public:
				static bool isStiff() { return true; }

			public:
				template <typename ST, typename PT, typename SDT>
				static void ddt(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<SDT> & ddt_y) {
					ddt_y = p.cwiseProduct(y);
				}

				template <typename ST, typename PT>
				static void initial(const MatrixBase<PT> & p, MatrixBase<ST> & y) {
					y.setOnes();
				}

			public:
				template <typename ST, typename PT, typename JT>
				static void jacobianState(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
					J.setZero();
					for(int i = 0; i < numberOfEquations(); i++)
						J(i, i) = p[i];
				}

				template <typename ST, typename PT, typename JT>
				static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
					J.setZero();
					for(int i = 0; i < numberOfEquations(); i++)
						J(i, i) = y[i];
				}

				template <typename PT, typename JT>
				static void jacobianParametersInitial(const MatrixBase<PT> & p, MatrixBase<JT> & J) {
					J.setZero();
				}

				template <typename ST, typename PT, typename DT>
				static void derStateState(const int i, const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<DT> & der) {
					der.setZero();
				}

				template <typename ST, typename PT, typename DT>
				static void derStatePar(const int i, const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<DT> & der) {
					der.setZero();
					der(i,i) = 1.;
				}

				template <typename ST, typename PT, typename DT>
				static void derParPar(const int i, const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<DT> & der) {
					der.setZero();
				}
				template <typename PT, typename DT>
				static void derParParInitial(const int i, const MatrixBase<PT> & p, MatrixBase<DT> & der) {
					der.setZero();

				}
		};

		class Projection {
			public:
				template <typename ST, typename PT, typename OT>
				static void project(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<OT> & o) {
					o = y;
				}

				template <typename ST, typename PT, typename JT>
				static void jacobian(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
					J.setZero();
					for(int i = 0; i < numberOfEquations(); i++)
						J(i,i) = 1.0;
				}

				template <typename ST, typename PT, typename JT>
				static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
					J.setZero();
				}

				template <typename ST, typename PT, typename JT>
				static void derStateState(const int i, const double t, const MatrixBase<ST> & y, MatrixBase<JT> & der) {
					der = der.setZero();
				}
		};

		class Transform {
			public:
				template <typename IT, typename OT>
				static void transphi(const MatrixBase<IT> & psi, MatrixBase<OT> & phi) {
					phi = psi;
				}

				template <typename IT, typename OT>
				static void transpsi(const MatrixBase<IT> & phi, MatrixBase<OT> & psi) {
					psi = phi;
				}

				template <typename IT, typename OT>
				static void dtranspsi(const MatrixBase<IT> & phi, MatrixBase<OT> & dpsi) {
					for(int i = 0; i < numberOfParameters(); i++)
						dpsi[i] = 1.0;
				}


				template <typename IT, typename OT>
				static void ddtranspsi(const MatrixBase<IT> & phi, MatrixBase<OT> & ddpsi) {
					for(int i = 0; i < numberOfParameters(); i++)
						ddpsi[i] = 0.0;
				}
		};

};
#endif // DIAGONALLINEARMODEL_H
