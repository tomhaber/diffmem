/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PK_H
#define PK_H

#include <cmath>
#include <MatrixVector.hpp>

#define ka 0
#define Cl 1
#define V 2

#define A 0
#define B 1

#define C 0

class PK {
	public:
		static constexpr int numberOfEquations() { return 2; }
		static constexpr int numberOfParameters() { return 3; }
		static constexpr int numberOfObservations() { return 1; }

	class ODE {
		public:
			static bool isStiff() { return false; }

		public:
			template <typename ST, typename PT, typename SDT>
			static void ddt(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<SDT> & ddt_y) {
				ddt_y[A] = -p[ka] * y[A];
				ddt_y[B] = p[ka] * y[A] - (p[Cl] / p[V]) * y[B];
			}

			template <typename ST, typename PT>
			static void initial(const MatrixBase<PT> & p, MatrixBase<ST> & y) {
				y[A] = 0.0;
				y[B] = 0.0;
			}

		public:
			template <typename ST, typename PT, typename JT>
			static void jacobianState(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(A, A) = -p[ka];
				J(A, B) = 0.0;

				J(B, A) = p[ka];
				J(B, B) = -(p[Cl] / p[V]);
			}

			template <typename ST, typename PT, typename JT>
			static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(A, ka) = -y[A];
				J(A, Cl) = 0.0;
				J(A, V) = 0.0;

				J(B, ka) = y[A];
				J(B, Cl) = -y[B]/p[V];
				J(B, V) = (y[B]*p[Cl])/(p[V]*p[V]);
			}

			template <typename PT, typename JT>
			static void jacobianParametersInitial(const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(A, ka) = 0.0;
				J(A, Cl) = 0.0;
				J(A, V) = 0.0;

				J(B, ka) = 0.0;
				J(B, Cl) = 0.0;
				J(B, V) = 0.0;
			}
	};

	class Projection {
		public:
			template <typename ST, typename PT, typename OT>
			static void project(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<OT> & o) {
				o[C] = y[B]/ p[V];
			}

			template <typename ST, typename PT, typename JT>
			static void jacobian(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
			  J(C,A) = 0.0;
			  J(C,B) = 1./p[V];
			}

			template <typename ST, typename PT, typename JT>
			static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J.setZero();
				J(C, V) = -y[B]/(p[V]*p[V]);
			}
	};

	class Likelihood {
		public:
			template <typename YUT, typename YT, typename WT, typename ST>
			static double likelihood(const double t, const MatrixBase<YUT> & yu, const MatrixBase<YT> & y,
					const MatrixBase<WT> & w, const MatrixBase<ST> & inv_sigma) {
				auto d = w.cwiseProduct(y - yu);
				return -0.5 * d.transpose() * inv_sigma * d;
			}

			template <typename YUT, typename YT, typename WT, typename ST, typename GT>
			static double gradient(const double t, const MatrixBase<YUT> & yu, const MatrixBase<YT> & y,
					const MatrixBase<WT> & w, const MatrixBase<ST> & inv_sigma, MatrixBase<GT> & grad) {
				auto d = w.cwiseProduct(y - yu);
				grad = inv_sigma * d;
				return -0.5 * d.transpose() * grad;
			}

			template <typename YUT, typename YT, typename WT, typename ST, typename GT, typename HT>
			static double gradient(const double t, const MatrixBase<YUT> & yu, const MatrixBase<YT> & y,
					const MatrixBase<WT> & w, const MatrixBase<ST> & inv_sigma, MatrixBase<GT> & grad, MatrixBase<HT> & H) {
				auto d = w.cwiseProduct(y - yu);
				grad = inv_sigma * (y - yu);
				H = -inv_sigma;
				return -0.5 * (y - yu).transpose() * grad;
			}

			template <typename YUT, typename YT, typename WT, typename ET>
			static void error(const double t, const MatrixBase<YUT> & yu, const MatrixBase<YT> & y,
					const MatrixBase<WT> & w, MatrixBase<ET> & err) {
				auto d = w.cwiseProduct(y - yu);
				err += d * d.transpose();
			}
	};

	class Transform {
		public:
			template <typename IT, typename OT>
			static void transphi(const MatrixBase<IT> & psi, MatrixBase<OT> & phi) {
				phi[ka] = log( psi[ka] );
				phi[Cl] = log( psi[Cl] );
				phi[V] = log( psi[V] );
			}

			template <typename IT, typename OT>
			static void transpsi(const MatrixBase<IT> & phi, MatrixBase<OT> & psi) {
				psi[ka] = exp( phi[ka] );
				psi[Cl] = exp( phi[Cl] );
				psi[V] = exp( phi[V] );
			}

			template <typename IT, typename OT>
			static void dtranspsi(const MatrixBase<IT> & phi, MatrixBase<OT> & dpsi) {
				dpsi[ka] = exp( phi[ka] );
				dpsi[Cl] = exp( phi[Cl] );
				dpsi[V] = exp( phi[V] );
			}

			template <typename IT, typename OT>
			static void ddtranspsi(const MatrixBase<IT> & phi, MatrixBase<OT> & ddpsi) {
				ddpsi[ka] = exp( phi[ka] );
				ddpsi[Cl] = exp( phi[Cl] );
				ddpsi[V] = exp( phi[V] );
			}
	};

};

#undef ka
#undef Cl
#undef V

#undef A
#undef B
#undef C

#undef PK
#endif
