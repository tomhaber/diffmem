/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HIVLATENT_H
#define HIVLATENT_H

#include <cmath>
#include <MatrixVector.hpp>
#include <math/logistic.hpp>
#include <math/logit.hpp>

#define lambda 1
#define gamma 2
#define fo 3
#define a 4
#define p0 5
#define muT 6
#define muL 7
#define muA 8
#define muV 9
#define etaPI 10
#define etaRTI 11

#define TC 1
#define TL 2
#define TA 3
#define VI 4
#define VNI 5

#define LVL 0
#define TTOT 1

class HIVLatent {
	public:
		static constexpr int numberOfEquations() { return 5; }
		static constexpr int numberOfParameters() { return 11; }
		static constexpr int numberOfObservations() { return 2; }

		class ODE {
			public:
				static bool isStiff() { return true; }

			public:
				template <typename ST, typename PT, typename SDT>
				static void ddt(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<SDT> & ddt_y) {
					const double gamma0 = (1-p[etaRTI-1])*p[gamma-1];
					const double p_I = (1-p[etaPI-1])*p[p0-1];
					const double p_NI = p[p0-1] - p_I;

					ddt_y[TC-1] = p[lambda-1]-gamma0*y[TC-1]*y[VI-1]-p[muT-1]*y[TC-1]; //ccode of Matlab is not very flexible
					ddt_y[TL-1] = (1-p[fo-1])*gamma0*y[TC-1]*y[VI-1]-p[a-1]*y[TL-1]-p[muL-1]*y[TL-1];
					ddt_y[TA-1] = p[fo-1]*gamma0*y[TC-1]*y[VI-1]+p[a-1]*y[TL-1]-p[muA-1]*y[TA-1];
					ddt_y[VI-1] = p_I*y[TA-1]-p[muV-1]*y[VI-1];
					ddt_y[VNI-1] = p_NI*y[TA-1]-p[muV-1]*y[VNI-1];
				}

				template <typename ST, typename PT>
				static void initial(const MatrixBase<PT> & p, MatrixBase<ST> & y) {
					y[TC-1] = (p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[muL-1]*p[fo-1]));
					y[VI-1] = (p[lambda-1]-y[TC-1]*p[muT-1])/(p[gamma-1]*y[TC-1]);
					y[VNI-1] = 0.0;
					y[TA-1] = p[muV-1]*y[VI-1]/p[p0-1];
					y[TL-1] = (1-p[fo-1])*p[gamma-1]*y[TC-1]*y[VI-1]/(p[a-1]+p[muL-1]);
				}

			public:
				template <typename ST, typename PT, typename JT>
				static void jacobianState(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
					J.setZero();
					J(TC-1,TC-1) =    -p[muT-1]+p[gamma-1]*y[VI-1]*(p[etaRTI-1]-1.0);
					J(TC-1,VI-1) =    p[gamma-1]*y[TC-1]*(p[etaRTI-1]-1.0);
					J(TL-1,TC-1) =    p[gamma-1]*y[VI-1]*(p[etaRTI-1]-1.0)*(p[fo-1]-1.0);
					J(TL-1,TL-1) =    -p[a-1]-p[muL-1];
					J(TL-1,VI-1) =    p[gamma-1]*y[TC-1]*(p[etaRTI-1]-1.0)*(p[fo-1]-1.0);
					J(TA-1,TC-1) =    -p[gamma-1]*y[VI-1]*p[fo-1]*(p[etaRTI-1]-1.0);
					J(TA-1,TL-1) =    p[a-1];
					J(TA-1,TA-1) =    -p[muA-1];
					J(TA-1,VI-1) =    -p[gamma-1]*y[TC-1]*p[fo-1]*(p[etaRTI-1]-1.0);
					J(VI-1,TA-1) =    -p[p0-1]*(p[etaPI-1]-1.0);
					J(VI-1,VI-1) =    -p[muV-1];
					J(VNI-1,TA-1) =    p[etaPI-1]*p[p0-1];
					J(VNI-1,VNI-1) =    -p[muV-1];
				}

				template <typename ST, typename PT, typename JT>
				static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
					J.setZero();
					J(TC-1,lambda-1) =    1.0;
					J(TC-1,gamma-1) =    y[TC-1]*y[VI-1]*(p[etaRTI-1]-1.0);
					J(TC-1,muT-1) =    -y[TC-1];
					J(TC-1,etaRTI-1) =    p[gamma-1]*y[TC-1]*y[VI-1];
					J(TL-1,gamma-1) =    y[TC-1]*y[VI-1]*(p[etaRTI-1]-1.0)*(p[fo-1]-1.0);
					J(TL-1,fo-1) =    p[gamma-1]*y[TC-1]*y[VI-1]*(p[etaRTI-1]-1.0);
					J(TL-1,a-1) =    -y[TL-1];
					J(TL-1,muL-1) =    -y[TL-1];
					J(TL-1,etaRTI-1) =    p[gamma-1]*y[TC-1]*y[VI-1]*(p[fo-1]-1.0);
					J(TA-1,gamma-1) =    -y[TC-1]*y[VI-1]*p[fo-1]*(p[etaRTI-1]-1.0);
					J(TA-1,fo-1) =    -p[gamma-1]*y[TC-1]*y[VI-1]*(p[etaRTI-1]-1.0);
					J(TA-1,a-1) =    y[TL-1];
					J(TA-1,muA-1) =    -y[TA-1];
					J(TA-1,etaRTI-1) =    -p[gamma-1]*y[TC-1]*y[VI-1]*p[fo-1];
					J(VI-1,p0-1) =    -y[TA-1]*(p[etaPI-1]-1.0);
					J(VI-1,muV-1) =    -y[VI-1];
					J(VI-1,etaPI-1) =    -y[TA-1]*p[p0-1];
					J(VNI-1,p0-1) =    y[TA-1]*p[etaPI-1];
					J(VNI-1,muV-1) =    -y[VNI-1];
					J(VNI-1,etaPI-1) =    y[TA-1]*p[p0-1];
				}

				template <typename PT, typename JT>
				static void jacobianParametersInitial(const MatrixBase<PT> & p, MatrixBase<JT> & J) {
					J.setZero();
					J(TC-1,gamma-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
					J(TC-1,fo-1) =    -(p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
					J(TC-1,a-1) =    (p[muA-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
					J(TC-1,p0-1) =    -(p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
					J(TC-1,muL-1) =    (p[muA-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
					J(TC-1,muA-1) =    (p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
					J(TC-1,muV-1) =    (p[muA-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
					J(TL-1,lambda-1) =    -(p[fo-1]-1.0)/(p[a-1]+p[muL-1]);
					J(TL-1,gamma-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1]*(p[fo-1]-1.0))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
					J(TL-1,fo-1) =    -(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))/(p[a-1]+p[muL-1])-(p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
					J(TL-1,a-1) =    (p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],2.0)+((p[fo-1]-1.0)*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[a-1]+p[muL-1]);
					J(TL-1,p0-1) =    -(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
					J(TL-1,muT-1) =    (p[muA-1]*p[muV-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
					J(TL-1,muL-1) =    (((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[fo-1]-1.0))/(p[a-1]+p[muL-1])+(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],2.0);
					J(TL-1,muA-1) =    (p[muT-1]*p[muV-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
					J(TL-1,muV-1) =    (p[muA-1]*p[muT-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
					J(TA-1,lambda-1) =    (p[a-1]+p[fo-1]*p[muL-1])/(p[muA-1]*(p[a-1]+p[muL-1]));
					J(TA-1,gamma-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[muT-1]*p[muV-1])/p[p0-1];
					J(TA-1,fo-1) =    (p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))))/(p[muA-1]*(p[a-1]+p[muL-1]))+(p[muL-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
					J(TA-1,a-1) =    (p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))-((p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1];
					J(TA-1,p0-1) =    (p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/p[gamma-1];
					J(TA-1,muT-1) =    -p[muV-1]/(p[gamma-1]*p[p0-1]);
					J(TA-1,muL-1) =    -(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]))+(p[fo-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))))/(p[muA-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1];
					J(TA-1,muA-1) =    -(1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[a-1]+p[muL-1])-(p[muT-1]*p[muV-1])/(p[gamma-1]*p[muA-1]*p[p0-1]);
					J(TA-1,muV-1) =    -p[muT-1]/(p[gamma-1]*p[p0-1]);
					J(VI-1,lambda-1) =    (p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
					J(VI-1,gamma-1) =    1.0/(p[gamma-1]*p[gamma-1])*p[muT-1];
					J(VI-1,fo-1) =    (p[muL-1]*p[muT-1])/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
					J(VI-1,a-1) =    ((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1]);
					J(VI-1,p0-1) =    p[muT-1]/(p[gamma-1]*p[p0-1])+((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
					J(VI-1,muT-1) =    -1.0/p[gamma-1];
					J(VI-1,muL-1) =    -(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+(p[fo-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1]);
					J(VI-1,muA-1) =    -p[muT-1]/(p[gamma-1]*p[muA-1])-(1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muV-1]*(p[a-1]+p[muL-1]));
					J(VI-1,muV-1) =    -p[muT-1]/(p[gamma-1]*p[muV-1])-(1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]));
				}

				template <typename ST, typename PT, typename DT>
				static void derStateState(const int i, const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<DT> & der) {
					der.setZero();
					if(i == 0){
						der(TC-1,VI-1) =    p[gamma-1]*(p[etaRTI-1]-1.0);
						der(VI-1,TC-1) =    p[gamma-1]*(p[etaRTI-1]-1.0);
					}
					if(i == 1){
						der(TC-1,VI-1) =    p[gamma-1]*(p[etaRTI-1]-1.0)*(p[fo-1]-1.0);
						der(VI-1,TC-1) =    p[gamma-1]*(p[etaRTI-1]-1.0)*(p[fo-1]-1.0);
					}
					if(i == 2){
						der(TC-1,VI-1) =    -p[gamma-1]*p[fo-1]*(p[etaRTI-1]-1.0);
						der(VI-1,TC-1) =    -p[gamma-1]*p[fo-1]*(p[etaRTI-1]-1.0);
					}
					if(i == 3){
					}
					if(i == 4){
					}
				}

				template <typename ST, typename PT, typename DT>
				static void derStatePar(const int i, const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<DT> & der) {
					der.setZero();
					if(i == 0){
						der(TC-1,gamma-1) =    y[VI-1]*(p[etaRTI-1]-1.0);
						der(TC-1,muT-1) =    -1.0;
						der(TC-1,etaRTI-1) =    p[gamma-1]*y[VI-1];
						der(VI-1,gamma-1) =    y[TC-1]*(p[etaRTI-1]-1.0);
						der(VI-1,etaRTI-1) =    p[gamma-1]*y[TC-1];
					}
					if(i == 1){
						der(TC-1,gamma-1) =    y[VI-1]*(p[etaRTI-1]-1.0)*(p[fo-1]-1.0);
						der(TC-1,fo-1) =    p[gamma-1]*y[VI-1]*(p[etaRTI-1]-1.0);
						der(TC-1,etaRTI-1) =    p[gamma-1]*y[VI-1]*(p[fo-1]-1.0);
						der(TL-1,a-1) =    -1.0;
						der(TL-1,muL-1) =    -1.0;
						der(VI-1,gamma-1) =    y[TC-1]*(p[etaRTI-1]-1.0)*(p[fo-1]-1.0);
						der(VI-1,fo-1) =    p[gamma-1]*y[TC-1]*(p[etaRTI-1]-1.0);
						der(VI-1,etaRTI-1) =    p[gamma-1]*y[TC-1]*(p[fo-1]-1.0);
					}
					if(i == 2){
						der(TC-1,gamma-1) =    -y[VI-1]*p[fo-1]*(p[etaRTI-1]-1.0);
						der(TC-1,fo-1) =    -p[gamma-1]*y[VI-1]*(p[etaRTI-1]-1.0);
						der(TC-1,etaRTI-1) =    -p[gamma-1]*y[VI-1]*p[fo-1];
						der(TL-1,a-1) =    1.0;
						der(TA-1,muA-1) =    -1.0;
						der(VI-1,gamma-1) =    -y[TC-1]*p[fo-1]*(p[etaRTI-1]-1.0);
						der(VI-1,fo-1) =    -p[gamma-1]*y[TC-1]*(p[etaRTI-1]-1.0);
						der(VI-1,etaRTI-1) =    -p[gamma-1]*y[TC-1]*p[fo-1];
					}
					if(i == 3){
						der(TA-1,p0-1) =    -p[etaPI-1]+1.0;
						der(TA-1,etaPI-1) =    -p[p0-1];
						der(VI-1,muV-1) =    -1.0;
					}
					if(i == 4){
						der(TA-1,p0-1) =    p[etaPI-1];
						der(TA-1,etaPI-1) =    p[p0-1];
						der(VNI-1,muV-1) =    -1.0;
					}
				}

				template <typename ST, typename PT, typename DT>
				static void derParPar(const int i, const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<DT> & der) {
					der.setZero();
					if(i == 0){
						der(gamma-1,etaRTI-1) =    y[TC-1]*y[VI-1];
						der(etaRTI-1,gamma-1) =    y[TC-1]*y[VI-1];
					}
					if(i == 1){
						der(gamma-1,fo-1) =    y[TC-1]*y[VI-1]*(p[etaRTI-1]-1.0);
						der(gamma-1,etaRTI-1) =    y[TC-1]*y[VI-1]*(p[fo-1]-1.0);
						der(fo-1,gamma-1) =    y[TC-1]*y[VI-1]*(p[etaRTI-1]-1.0);
						der(fo-1,etaRTI-1) =    p[gamma-1]*y[TC-1]*y[VI-1];
						der(etaRTI-1,gamma-1) =    y[TC-1]*y[VI-1]*(p[fo-1]-1.0);
						der(etaRTI-1,fo-1) =    p[gamma-1]*y[TC-1]*y[VI-1];
					}
					if(i == 2){
						der(gamma-1,fo-1) =    -y[TC-1]*y[VI-1]*(p[etaRTI-1]-1.0);
						der(gamma-1,etaRTI-1) =    -y[TC-1]*y[VI-1]*p[fo-1];
						der(fo-1,gamma-1) =    -y[TC-1]*y[VI-1]*(p[etaRTI-1]-1.0);
						der(fo-1,etaRTI-1) =    -p[gamma-1]*y[TC-1]*y[VI-1];
						der(etaRTI-1,gamma-1) =    -y[TC-1]*y[VI-1]*p[fo-1];
						der(etaRTI-1,fo-1) =    -p[gamma-1]*y[TC-1]*y[VI-1];
					}
					if(i == 3){
						der(p0-1,etaPI-1) =    -y[TA-1];
						der(etaPI-1,p0-1) =    -y[TA-1];
					}
					if(i == 4){
						der(p0-1,etaPI-1) =    y[TA-1];
						der(etaPI-1,p0-1) =    y[TA-1];
					}
				}
				template <typename PT, typename DT>
				static void derParParInitial(const int i, const MatrixBase<PT> & p, MatrixBase<DT> & der) {
					der.setZero();
					if(i == 0){
						der(gamma-1,gamma-1) =    (1.0/(p[gamma-1]*p[gamma-1]*p[gamma-1])*p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1])*2.0)/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(gamma-1,fo-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[p0-1];
						der(gamma-1,a-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[p0-1];
						der(gamma-1,p0-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[a-1]+p[muL-1]))/(p[a-1]+p[fo-1]*p[muL-1]);
						der(gamma-1,muL-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(1.0/(p[gamma-1]*p[gamma-1])*p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[p0-1];
						der(gamma-1,muA-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muV-1]*(p[a-1]+p[muL-1]))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(gamma-1,muV-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*(p[a-1]+p[muL-1]))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(fo-1,gamma-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[p0-1];
						der(fo-1,fo-1) =    (p[muA-1]*pow(p[muL-1],2.0)*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]);
						der(fo-1,a-1) =    -(p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]);
						der(fo-1,p0-1) =    (p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[gamma-1];
						der(fo-1,muL-1) =    -(p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])-(p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[fo-1]*p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]);
						der(fo-1,muA-1) =    -(p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
						der(fo-1,muV-1) =    -(p[muA-1]*p[muL-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
						der(a-1,gamma-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[p0-1];
						der(a-1,fo-1) =    -(p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]);
						der(a-1,a-1) =    (p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*-2.0)/(p[gamma-1]*p[p0-1])+(p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]);
						der(a-1,p0-1) =    -(p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[gamma-1];
						der(a-1,muL-1) =    -(p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])-(p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]);
						der(a-1,muA-1) =    p[muV-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
						der(a-1,muV-1) =    p[muA-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
						der(p0-1,gamma-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[a-1]+p[muL-1]))/(p[a-1]+p[fo-1]*p[muL-1]);
						der(p0-1,fo-1) =    (p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[gamma-1];
						der(p0-1,a-1) =    -(p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[gamma-1];
						der(p0-1,p0-1) =    (p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(p0-1,muL-1) =    -(p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[gamma-1];
						der(p0-1,muA-1) =    -(p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(p0-1,muV-1) =    -(p[muA-1]*1.0/pow(p[p0-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muL-1,gamma-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(1.0/(p[gamma-1]*p[gamma-1])*p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[p0-1];
						der(muL-1,fo-1) =    -(p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])-(p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[fo-1]*p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]);
						der(muL-1,a-1) =    -(p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])-(p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]);
						der(muL-1,p0-1) =    -(p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[gamma-1];
						der(muL-1,muL-1) =    (p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*-2.0)/(p[gamma-1]*p[p0-1])+(pow(p[fo-1],2.0)*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]);
						der(muL-1,muA-1) =    p[muV-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
						der(muL-1,muV-1) =    p[muA-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
						der(muA-1,gamma-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muV-1]*(p[a-1]+p[muL-1]))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muA-1,fo-1) =    -(p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
						der(muA-1,a-1) =    p[muV-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
						der(muA-1,p0-1) =    -(p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muA-1,muL-1) =    p[muV-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
						der(muA-1,muV-1) =    (p[a-1]+p[muL-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muV-1,gamma-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*(p[a-1]+p[muL-1]))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muV-1,fo-1) =    -(p[muA-1]*p[muL-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
						der(muV-1,a-1) =    p[muA-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
						der(muV-1,p0-1) =    -(p[muA-1]*1.0/pow(p[p0-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muV-1,muL-1) =    p[muA-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]);
						der(muV-1,muA-1) =    (p[a-1]+p[muL-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
					}
					if(i == 1){
						der(lambda-1,fo-1) =    -1.0/(p[a-1]+p[muL-1]);
						der(lambda-1,a-1) =    (p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],2.0);
						der(lambda-1,muL-1) =    (p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],2.0);
						der(gamma-1,gamma-1) =    (1.0/(p[gamma-1]*p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1]*(p[fo-1]-1.0)*2.0)/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(gamma-1,fo-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/p[p0-1];
						der(gamma-1,a-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/p[p0-1];
						der(gamma-1,p0-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[fo-1]-1.0))/(p[a-1]+p[fo-1]*p[muL-1]);
						der(gamma-1,muT-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muV-1]*(p[fo-1]-1.0))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(gamma-1,muL-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/p[p0-1];
						der(gamma-1,muA-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muT-1]*p[muV-1]*(p[fo-1]-1.0))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(gamma-1,muV-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*(p[fo-1]-1.0))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(fo-1,lambda-1) =    -1.0/(p[a-1]+p[muL-1]);
						der(fo-1,gamma-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/p[p0-1];
						der(fo-1,fo-1) =    (p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*-2.0)/(p[gamma-1]*p[p0-1])+(p[muA-1]*pow(p[muL-1],2.0)*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[fo-1]-1.0)*2.0)/(p[gamma-1]*p[p0-1]);
						der(fo-1,a-1) =    ((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))/(p[a-1]+p[muL-1])+(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0)+(p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[fo-1]-1.0)*2.0)/(p[gamma-1]*p[p0-1]);
						der(fo-1,p0-1) =    -(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/p[gamma-1];
						der(fo-1,muT-1) =    (p[muA-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
						der(fo-1,muL-1) =    ((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))/(p[a-1]+p[muL-1])+(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0)-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1])+(p[fo-1]*p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[fo-1]-1.0)*2.0)/(p[gamma-1]*p[p0-1]);
						der(fo-1,muA-1) =    (p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
						der(fo-1,muV-1) =    (p[muA-1]*p[muT-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muL-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
						der(a-1,lambda-1) =    (p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],2.0);
						der(a-1,gamma-1) =    -((p[fo-1]-1.0)*((1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[p0-1]))/(p[a-1]+p[muL-1])+(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1]*(p[fo-1]-1.0))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]));
						der(a-1,fo-1) =    ((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))/(p[a-1]+p[muL-1])+(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0)-(((p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])-(p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]))*(p[fo-1]-1.0))/(p[a-1]+p[muL-1])+(p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[muL-1]));
						der(a-1,a-1) =    (p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],3.0)*-2.0-(p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],2.0)*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*2.0-((p[fo-1]-1.0)*((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*2.0)/(p[gamma-1]*p[p0-1])-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[a-1]+p[muL-1]);
						der(a-1,p0-1) =    -((p[fo-1]-1.0)*((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[gamma-1]))/(p[a-1]+p[muL-1])+(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]));
						der(a-1,muT-1) =    (((p[muA-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[fo-1]-1.0))/(p[a-1]+p[muL-1])-(p[muA-1]*p[muV-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]));
						der(a-1,muL-1) =    -((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],2.0)-((p[fo-1]-1.0)*((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[a-1]+p[muL-1])-(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],3.0)*2.0-(p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],2.0)*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]));
						der(a-1,muA-1) =    (((p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[fo-1]-1.0))/(p[a-1]+p[muL-1])-(p[muT-1]*p[muV-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]));
						der(a-1,muV-1) =    (((p[muA-1]*p[muT-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[fo-1]-1.0))/(p[a-1]+p[muL-1])-(p[muA-1]*p[muT-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]));
						der(p0-1,gamma-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[fo-1]-1.0))/(p[a-1]+p[fo-1]*p[muL-1]);
						der(p0-1,fo-1) =    -(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/p[gamma-1];
						der(p0-1,a-1) =    (p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/p[gamma-1];
						der(p0-1,p0-1) =    (p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],3.0)*(p[fo-1]-1.0)*2.0)/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(p0-1,muT-1) =    -(p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(p0-1,muL-1) =    (p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/p[gamma-1];
						der(p0-1,muA-1) =    -(p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(p0-1,muV-1) =    -(p[muA-1]*p[muT-1]*1.0/pow(p[p0-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muT-1,gamma-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muV-1]*(p[fo-1]-1.0))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muT-1,fo-1) =    (p[muA-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muL-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
						der(muT-1,a-1) =    -(p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
						der(muT-1,p0-1) =    -(p[muA-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muT-1,muL-1) =    -(p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
						der(muT-1,muA-1) =    (p[muV-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muT-1,muV-1) =    (p[muA-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muL-1,lambda-1) =    (p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],2.0);
						der(muL-1,gamma-1) =    -(((1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(1.0/(p[gamma-1]*p[gamma-1])*p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[p0-1])*(p[fo-1]-1.0))/(p[a-1]+p[muL-1])+(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1]*(p[fo-1]-1.0))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]));
						der(muL-1,fo-1) =    ((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))/(p[a-1]+p[muL-1])+(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0)-((p[fo-1]-1.0)*((p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])-(p[fo-1]*p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[a-1]+p[muL-1])+(p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[muL-1]));
						der(muL-1,a-1) =    -((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],2.0)-((p[fo-1]-1.0)*((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[a-1]+p[muL-1])-(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],3.0)*2.0-(p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],2.0)*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]));
						der(muL-1,p0-1) =    -(((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[gamma-1])*(p[fo-1]-1.0))/(p[a-1]+p[muL-1])+(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]));
						der(muL-1,muT-1) =    ((p[fo-1]-1.0)*((p[muA-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[a-1]+p[muL-1])-(p[muA-1]*p[muV-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]));
						der(muL-1,muL-1) =    ((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],2.0)*-2.0-(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[fo-1]-1.0)*1.0/pow(p[a-1]+p[muL-1],3.0)*2.0-((p[fo-1]-1.0)*((p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*2.0)/(p[gamma-1]*p[p0-1])-(pow(p[fo-1],2.0)*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[a-1]+p[muL-1]);
						der(muL-1,muA-1) =    ((p[fo-1]-1.0)*((p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[a-1]+p[muL-1])-(p[muT-1]*p[muV-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]));
						der(muL-1,muV-1) =    ((p[fo-1]-1.0)*((p[muA-1]*p[muT-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[a-1]+p[muL-1])-(p[muA-1]*p[muT-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]));
						der(muA-1,gamma-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muT-1]*p[muV-1]*(p[fo-1]-1.0))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muA-1,fo-1) =    (p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
						der(muA-1,a-1) =    -(p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
						der(muA-1,p0-1) =    -(p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muA-1,muT-1) =    (p[muV-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muA-1,muL-1) =    -(p[fo-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
						der(muA-1,muV-1) =    (p[muT-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muV-1,gamma-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*(p[fo-1]-1.0))/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muV-1,fo-1) =    (p[muA-1]*p[muT-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muL-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
						der(muV-1,a-1) =    -(p[muA-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
						der(muV-1,p0-1) =    -(p[muA-1]*p[muT-1]*1.0/pow(p[p0-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muV-1,muT-1) =    (p[muA-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muV-1,muL-1) =    -(p[fo-1]*p[muA-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]);
						der(muV-1,muA-1) =    (p[muT-1]*(p[fo-1]-1.0))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
					}
					if(i == 2){
						der(lambda-1,fo-1) =    p[muL-1]/(p[muA-1]*(p[a-1]+p[muL-1]));
						der(lambda-1,a-1) =    1.0/(p[muA-1]*(p[a-1]+p[muL-1]))-((p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1];
						der(lambda-1,muL-1) =    p[fo-1]/(p[muA-1]*(p[a-1]+p[muL-1]))-((p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1];
						der(lambda-1,muA-1) =    -(1.0/pow(p[muA-1],2.0)*(p[a-1]+p[fo-1]*p[muL-1]))/(p[a-1]+p[muL-1]);
						der(gamma-1,gamma-1) =    (1.0/(p[gamma-1]*p[gamma-1]*p[gamma-1])*p[muT-1]*p[muV-1]*-2.0)/p[p0-1];
						der(gamma-1,p0-1) =    -1.0/(p[gamma-1]*p[gamma-1])*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0);
						der(gamma-1,muT-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[muV-1])/p[p0-1];
						der(gamma-1,muV-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[muT-1])/p[p0-1];
						der(fo-1,lambda-1) =    p[muL-1]/(p[muA-1]*(p[a-1]+p[muL-1]));
						der(fo-1,a-1) =    -(p[muL-1]*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))-(p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1]-(p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1]);
						der(fo-1,muL-1) =    (p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))-(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*p[muL-1])/(p[muA-1]*(p[a-1]+p[muL-1]))-(p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1]+(p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1]);
						der(fo-1,muA-1) =    -(1.0/pow(p[muA-1],2.0)*p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))))/(p[a-1]+p[muL-1])-(p[muL-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[muA-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(a-1,lambda-1) =    1.0/(p[muA-1]*(p[a-1]+p[muL-1]))-((p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1];
						der(a-1,gamma-1) =    ((p[a-1]+p[fo-1]*p[muL-1])*((1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[p0-1]))/(p[muA-1]*(p[a-1]+p[muL-1]))-(1.0/(p[gamma-1]*p[gamma-1])*p[muT-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[muL-1]))+(1.0/(p[gamma-1]*p[gamma-1])*p[muT-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(a-1,fo-1) =    -(p[muL-1]*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))-(p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1]+(((p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])-(p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]))+(p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])-(p[muL-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]));
						der(a-1,a-1) =    ((p[muA-1]*p[muT-1]*p[muV-1]*-2.0)/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]))/(p[muA-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0)*2.0)/p[muA-1]+((p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0)*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*2.0)/p[muA-1]+((p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*2.0)/(p[gamma-1]*p[p0-1])-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))+((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],3.0)*2.0)/p[muA-1];
						der(a-1,p0-1) =    ((p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[gamma-1]))/(p[muA-1]*(p[a-1]+p[muL-1]))-(p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[muL-1]))+(p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(a-1,muT-1) =    -p[muV-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))+p[muV-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[muL-1]))-(((p[muA-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]));
						der(a-1,muL-1) =    -((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))/(p[muA-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1]-(p[fo-1]*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))+((p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0)*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/p[muA-1]+(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1]-(p[fo-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1]+((p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))+((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],3.0)*2.0)/p[muA-1];
						der(a-1,muA-1) =    -(1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))))/(p[a-1]+p[muL-1])+(1.0/pow(p[muA-1],2.0)*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[a-1]+p[muL-1])+1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0)-(((p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]))-(p[muT-1]*p[muV-1])/(p[gamma-1]*p[muA-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(p[muT-1]*p[muV-1])/(p[gamma-1]*p[muA-1]*p[p0-1]*(p[a-1]+p[muL-1]));
						der(a-1,muV-1) =    -p[muT-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))+p[muT-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[muL-1]))-(((p[muA-1]*p[muT-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]));
						der(p0-1,gamma-1) =    -1.0/(p[gamma-1]*p[gamma-1])*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0);
						der(p0-1,p0-1) =    (p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],3.0)*-2.0)/p[gamma-1];
						der(p0-1,muT-1) =    (p[muV-1]*1.0/pow(p[p0-1],2.0))/p[gamma-1];
						der(p0-1,muV-1) =    (p[muT-1]*1.0/pow(p[p0-1],2.0))/p[gamma-1];
						der(muT-1,gamma-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[muV-1])/p[p0-1];
						der(muT-1,p0-1) =    (p[muV-1]*1.0/pow(p[p0-1],2.0))/p[gamma-1];
						der(muT-1,muV-1) =    -1.0/(p[gamma-1]*p[p0-1]);
						der(muL-1,lambda-1) =    p[fo-1]/(p[muA-1]*(p[a-1]+p[muL-1]))-((p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1];
						der(muL-1,gamma-1) =    (((1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(1.0/(p[gamma-1]*p[gamma-1])*p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[p0-1])*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]))-(1.0/(p[gamma-1]*p[gamma-1])*p[muT-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[muL-1]))+(1.0/(p[gamma-1]*p[gamma-1])*p[fo-1]*p[muT-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muL-1,fo-1) =    (p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))-(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*p[muL-1])/(p[muA-1]*(p[a-1]+p[muL-1]))-(p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1]+((p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])-(p[fo-1]*p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))-(p[muL-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]))+(p[fo-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1]);
						der(muL-1,a-1) =    -((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))/(p[muA-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1]-(p[fo-1]*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))+((p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0)*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/p[muA-1]+(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1]-(p[fo-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1]+((p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))+((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],3.0)*2.0)/p[muA-1];
						der(muL-1,p0-1) =    (((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[gamma-1])*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]))-(p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[muL-1]))+(p[fo-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muL-1,muT-1) =    -((p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))+p[muV-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[muL-1]))-(p[fo-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muL-1,muL-1) =    ((p[a-1]+p[fo-1]*p[muL-1])*((p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*2.0)/(p[gamma-1]*p[p0-1])-(pow(p[fo-1],2.0)*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))-(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*p[fo-1]*2.0)/(p[muA-1]*(p[a-1]+p[muL-1]))+(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0)*2.0)/p[muA-1]-(p[fo-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*1.0/pow(p[a-1]+p[muL-1],2.0)*2.0)/p[muA-1]+((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],3.0)*2.0)/p[muA-1];
						der(muL-1,muA-1) =    -((p[a-1]+p[fo-1]*p[muL-1])*((p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))+(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*1.0/pow(p[muA-1],2.0)*(p[a-1]+p[fo-1]*p[muL-1]))/(p[a-1]+p[muL-1])-(p[fo-1]*1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))))/(p[a-1]+p[muL-1])+1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0)+(p[muT-1]*p[muV-1])/(p[gamma-1]*p[muA-1]*p[p0-1]*(p[a-1]+p[muL-1]))-(p[fo-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[muA-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muL-1,muV-1) =    -((p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))+p[muT-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[muL-1]))-(p[fo-1]*p[muT-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muA-1,lambda-1) =    -(1.0/pow(p[muA-1],2.0)*(p[a-1]+p[fo-1]*p[muL-1]))/(p[a-1]+p[muL-1]);
						der(muA-1,fo-1) =    -(1.0/pow(p[muA-1],2.0)*p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))))/(p[a-1]+p[muL-1])-(p[muL-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[muA-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(muA-1,a-1) =    -(1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))))/(p[a-1]+p[muL-1])+(1.0/pow(p[muA-1],2.0)*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[a-1]+p[muL-1])+1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0);
						der(muA-1,muL-1) =    (((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*1.0/pow(p[muA-1],2.0)*(p[a-1]+p[fo-1]*p[muL-1]))/(p[a-1]+p[muL-1])-(p[fo-1]*1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))))/(p[a-1]+p[muL-1])+1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0);
						der(muA-1,muA-1) =    (1.0/pow(p[muA-1],3.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*2.0)/(p[a-1]+p[muL-1])+(1.0/pow(p[muA-1],2.0)*p[muT-1]*p[muV-1]*2.0)/(p[gamma-1]*p[p0-1]);
						der(muV-1,gamma-1) =    (1.0/(p[gamma-1]*p[gamma-1])*p[muT-1])/p[p0-1];
						der(muV-1,p0-1) =    (p[muT-1]*1.0/pow(p[p0-1],2.0))/p[gamma-1];
						der(muV-1,muT-1) =    -1.0/(p[gamma-1]*p[p0-1]);
					}
					if(i == 3){
						der(lambda-1,fo-1) =    (p[muL-1]*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(lambda-1,a-1) =    p[p0-1]/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1]);
						der(lambda-1,p0-1) =    (p[a-1]+p[fo-1]*p[muL-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(lambda-1,muL-1) =    (p[fo-1]*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1]);
						der(lambda-1,muA-1) =    -(1.0/pow(p[muA-1],2.0)*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muV-1]*(p[a-1]+p[muL-1]));
						der(lambda-1,muV-1) =    -(1.0/pow(p[muV-1],2.0)*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]));
						der(gamma-1,gamma-1) =    1.0/(p[gamma-1]*p[gamma-1]*p[gamma-1])*p[muT-1]*-2.0;
						der(gamma-1,muT-1) =    1.0/(p[gamma-1]*p[gamma-1]);
						der(fo-1,lambda-1) =    (p[muL-1]*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(fo-1,a-1) =    -(p[muL-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/p[gamma-1]-(p[muL-1]*p[p0-1]*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1]);
						der(fo-1,p0-1) =    (p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+(p[muL-1]*p[muT-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(fo-1,muL-1) =    p[muT-1]/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muL-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/p[gamma-1]+((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*p[muL-1]*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1]);
						der(fo-1,muA-1) =    -(p[muL-1]*p[muT-1])/(p[gamma-1]*p[muA-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(1.0/pow(p[muA-1],2.0)*p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muV-1]*(p[a-1]+p[muL-1]));
						der(fo-1,muV-1) =    -(p[muL-1]*p[muT-1])/(p[gamma-1]*p[muV-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muL-1]*1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muA-1]*(p[a-1]+p[muL-1]));
						der(a-1,lambda-1) =    p[p0-1]/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1]);
						der(a-1,gamma-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muT-1])/(p[a-1]+p[muL-1])+(1.0/(p[gamma-1]*p[gamma-1])*p[muT-1])/(p[a-1]+p[fo-1]*p[muL-1])+(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[p0-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(a-1,fo-1) =    (p[muL-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/p[gamma-1]-(p[muL-1]*p[muT-1])/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]))-(p[muL-1]*p[p0-1]*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1])+(p[p0-1]*((p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])-(p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(a-1,a-1) =    ((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*1.0/pow(p[a-1]+p[muL-1],2.0)*-2.0)/(p[muA-1]*p[muV-1])-(p[p0-1]*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*2.0)/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0)*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*2.0)/(p[muA-1]*p[muV-1])+(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*2.0)/(p[gamma-1]*p[p0-1])-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],3.0)*2.0)/(p[muA-1]*p[muV-1]);
						der(a-1,p0-1) =    (p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+p[muT-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-p[muT-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[muL-1]))-((p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1])+(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[gamma-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(a-1,muT-1) =    -1.0/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))+1.0/(p[gamma-1]*(p[a-1]+p[muL-1]))-(p[p0-1]*((p[muA-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(a-1,muL-1) =    -(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1])-(p[fo-1]*p[p0-1]*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0)*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1])+(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1])-(p[fo-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1])+(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],3.0)*2.0)/(p[muA-1]*p[muV-1]);
						der(a-1,muA-1) =    -p[muT-1]/(p[gamma-1]*p[muA-1]*(p[a-1]+p[fo-1]*p[muL-1]))+p[muT-1]/(p[gamma-1]*p[muA-1]*(p[a-1]+p[muL-1]))-(1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muV-1]*(p[a-1]+p[muL-1]))+(1.0/pow(p[muA-1],2.0)*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muV-1]*(p[a-1]+p[muL-1]))+(1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muV-1]-(p[p0-1]*((p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(a-1,muV-1) =    -p[muT-1]/(p[gamma-1]*p[muV-1]*(p[a-1]+p[fo-1]*p[muL-1]))+p[muT-1]/(p[gamma-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muA-1]*(p[a-1]+p[muL-1]))+(1.0/pow(p[muV-1],2.0)*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))+(1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1]-(p[p0-1]*((p[muA-1]*p[muT-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(p0-1,lambda-1) =    (p[a-1]+p[fo-1]*p[muL-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(p0-1,fo-1) =    (p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+(p[muL-1]*p[muT-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]));
						der(p0-1,a-1) =    (p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-((p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1]);
						der(p0-1,muL-1) =    -(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+(p[fo-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1]);
						der(p0-1,muA-1) =    -p[muT-1]/(p[gamma-1]*p[muA-1]*p[p0-1])-(1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muV-1]*(p[a-1]+p[muL-1]));
						der(p0-1,muV-1) =    -p[muT-1]/(p[gamma-1]*p[muV-1]*p[p0-1])-(1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]));
						der(muT-1,gamma-1) =    1.0/(p[gamma-1]*p[gamma-1]);
						der(muL-1,lambda-1) =    (p[fo-1]*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1]);
						der(muL-1,gamma-1) =    -(1.0/(p[gamma-1]*p[gamma-1])*p[muT-1])/(p[a-1]+p[muL-1])+(1.0/(p[gamma-1]*p[gamma-1])*p[fo-1]*p[muT-1])/(p[a-1]+p[fo-1]*p[muL-1])+(((1.0/(p[gamma-1]*p[gamma-1])*p[muA-1]*p[muT-1]*p[muV-1])/(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(1.0/(p[gamma-1]*p[gamma-1])*p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[p0-1])*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(muL-1,fo-1) =    -(p[muL-1]*p[muT-1])/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1])*(p[a-1]+p[muL-1]))+(p[fo-1]*p[muL-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/p[gamma-1]+((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*p[muL-1]*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1])+(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])-(p[fo-1]*p[muA-1]*p[muL-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(muL-1,a-1) =    -(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*p[p0-1])/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1])-(p[fo-1]*p[p0-1]*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0)*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1])+(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1])-(p[fo-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1])+(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])+(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0))/(p[gamma-1]*p[p0-1])-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],3.0)*2.0)/(p[muA-1]*p[muV-1]);
						der(muL-1,p0-1) =    -p[muT-1]/(p[gamma-1]*p[p0-1]*(p[a-1]+p[muL-1]))-(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+(p[fo-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/(p[muA-1]*p[muV-1])+(p[fo-1]*p[muT-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(((p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0))/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[p0-1],2.0)*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/p[gamma-1])*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(muL-1,muT-1) =    1.0/(p[gamma-1]*(p[a-1]+p[muL-1]))-p[fo-1]/(p[gamma-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(muL-1,muL-1) =    (p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*2.0)/(p[gamma-1]*p[p0-1])-(pow(p[fo-1],2.0)*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],3.0)*(p[a-1]+p[muL-1])*2.0)/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*p[fo-1]*p[p0-1]*2.0)/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]))+(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0)*2.0)/(p[muA-1]*p[muV-1])-(p[fo-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*1.0/pow(p[a-1]+p[muL-1],2.0)*2.0)/(p[muA-1]*p[muV-1])+((p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],3.0)*2.0)/(p[muA-1]*p[muV-1]);
						der(muL-1,muA-1) =    p[muT-1]/(p[gamma-1]*p[muA-1]*(p[a-1]+p[muL-1]))-(p[fo-1]*p[muT-1])/(p[gamma-1]*p[muA-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*1.0/pow(p[muA-1],2.0)*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muV-1]*(p[a-1]+p[muL-1]))-(p[fo-1]*1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muV-1]*(p[a-1]+p[muL-1]))+(1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muV-1]-(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(muL-1,muV-1) =    p[muT-1]/(p[gamma-1]*p[muV-1]*(p[a-1]+p[muL-1]))-(p[fo-1]*p[muT-1])/(p[gamma-1]*p[muV-1]*(p[a-1]+p[fo-1]*p[muL-1]))+(((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*1.0/pow(p[muV-1],2.0)*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]))-(p[fo-1]*1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muA-1]*(p[a-1]+p[muL-1]))+(1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1]-(p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*p[muV-1]*(p[a-1]+p[muL-1]));
						der(muA-1,lambda-1) =    -(1.0/pow(p[muA-1],2.0)*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muV-1]*(p[a-1]+p[muL-1]));
						der(muA-1,fo-1) =    -(p[muL-1]*p[muT-1])/(p[gamma-1]*p[muA-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(1.0/pow(p[muA-1],2.0)*p[muL-1]*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muV-1]*(p[a-1]+p[muL-1]));
						der(muA-1,a-1) =    -(1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muV-1]*(p[a-1]+p[muL-1]))+(1.0/pow(p[muA-1],2.0)*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muV-1]*(p[a-1]+p[muL-1]))+(1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muV-1];
						der(muA-1,p0-1) =    -p[muT-1]/(p[gamma-1]*p[muA-1]*p[p0-1])-(1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muV-1]*(p[a-1]+p[muL-1]));
						der(muA-1,muL-1) =    (((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*1.0/pow(p[muA-1],2.0)*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muV-1]*(p[a-1]+p[muL-1]))-(p[fo-1]*1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muV-1]*(p[a-1]+p[muL-1]))+(1.0/pow(p[muA-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muV-1];
						der(muA-1,muA-1) =    (1.0/pow(p[muA-1],2.0)*p[muT-1]*2.0)/p[gamma-1]+(1.0/pow(p[muA-1],3.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*2.0)/(p[muV-1]*(p[a-1]+p[muL-1]));
						der(muA-1,muV-1) =    p[muT-1]/(p[gamma-1]*p[muA-1]*p[muV-1])+(1.0/pow(p[muA-1],2.0)*1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[a-1]+p[muL-1]);
						der(muV-1,lambda-1) =    -(1.0/pow(p[muV-1],2.0)*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]));
						der(muV-1,fo-1) =    -(p[muL-1]*p[muT-1])/(p[gamma-1]*p[muV-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muL-1]*1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muA-1]*(p[a-1]+p[muL-1]));
						der(muV-1,a-1) =    -(1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muA-1]*(p[a-1]+p[muL-1]))+(1.0/pow(p[muV-1],2.0)*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1])))/(p[muA-1]*(p[a-1]+p[muL-1]))+(1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1];
						der(muV-1,p0-1) =    -p[muT-1]/(p[gamma-1]*p[muV-1]*p[p0-1])-(1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]));
						der(muV-1,muL-1) =    (((p[muA-1]*p[muT-1]*p[muV-1])/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))-(p[fo-1]*p[muA-1]*p[muT-1]*p[muV-1]*1.0/pow(p[a-1]+p[fo-1]*p[muL-1],2.0)*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]))*1.0/pow(p[muV-1],2.0)*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[muA-1]*(p[a-1]+p[muL-1]))-(p[fo-1]*1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1])/(p[muA-1]*(p[a-1]+p[muL-1]))+(1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*1.0/pow(p[a-1]+p[muL-1],2.0))/p[muA-1];
						der(muV-1,muA-1) =    p[muT-1]/(p[gamma-1]*p[muA-1]*p[muV-1])+(1.0/pow(p[muA-1],2.0)*1.0/pow(p[muV-1],2.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1]))/(p[a-1]+p[muL-1]);
						der(muV-1,muV-1) =    (p[muT-1]*1.0/pow(p[muV-1],2.0)*2.0)/p[gamma-1]+(1.0/pow(p[muV-1],3.0)*(p[lambda-1]-(p[muA-1]*p[muT-1]*p[muV-1]*(p[a-1]+p[muL-1]))/(p[gamma-1]*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])))*p[p0-1]*(p[a-1]+p[fo-1]*p[muL-1])*2.0)/(p[muA-1]*(p[a-1]+p[muL-1]));
					}
					if(i == 4){
					}
				}
		};

		class Projection {
			public:
				template <typename ST, typename PT, typename OT>
				static void project(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<OT> & o) {
					o[LVL] = log10(std::max((y[VI-1]+y[VNI-1])*1000.0, 1.));
					o[TTOT] = (y[TC-1] + y[TL-1] + y[TA-1]);
				}

				template <typename ST, typename PT, typename JT>
				static void jacobian(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
					J(LVL,TC-1) = 0.0;
					J(LVL,TL-1) = 0.0;
					J(LVL,TA-1) = 0.0;
					J(LVL,VI-1) = J(LVL,VNI-1) = ((y[VI-1]+y[VNI-1]) > 1./1000) ? 1./(log(10)*(y[VI-1]+y[VNI-1])) : 0.0;

					J(TTOT,TC-1) = 1.0;
					J(TTOT,TL-1) = 1.0;
					J(TTOT,TA-1) = 1.0;
					J(TTOT,VI-1) = 0.0;
					J(TTOT,VNI-1) = 0.0;
				}

				template <typename ST, typename PT, typename JT>
				static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
					J.setZero();
				}

				template <typename ST, typename PT, typename JT>
				static void derStateState(const int i, const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & der) {
					der = der.setZero();
					if (i == 0){
						der(VI-1, VI-1) = ((y[VI-1]+y[VNI-1]) > 1./1000) ? -1./(log(10)*(y[VI-1]+y[VNI-1])*(y[VI-1]+y[VNI-1])) : 0.0;
						der(VI-1, VNI-1) = ((y[VI-1]+y[VNI-1]) > 1./1000) ? -1./(log(10)*(y[VI-1]+y[VNI-1])*(y[VI-1]+y[VNI-1])) : 0.0;
						der(VNI-1, VI-1) = ((y[VI-1]+y[VNI-1]) > 1./1000) ? -1./(log(10)*(y[VI-1]+y[VNI-1])*(y[VI-1]+y[VNI-1])) : 0.0;
						der(VNI-1, VNI-1) = ((y[VI-1]+y[VNI-1]) > 1./1000) ? -1./(log(10)*(y[VI-1]+y[VNI-1])*(y[VI-1]+y[VNI-1])) : 0.0;
					}
				}

		};

		class Transform {
			public:
				template <typename IT, typename OT>
				static void transphi(const MatrixBase<IT> & psi, MatrixBase<OT> & phi) {
					phi[lambda-1] = log( psi[lambda-1] );
					phi[gamma-1] = log( psi[gamma-1] );
					phi[fo-1] = math::logit( psi[fo-1] );
					phi[a-1] = log( psi[a-1] );
					phi[p0-1] = log( psi[p0-1] );
					phi[muT-1] = log( psi[muT-1] );
					phi[muL-1] = log( psi[muL-1] );
					phi[muA-1] = log( psi[muA-1] );
					phi[muV-1] = log( psi[muV-1] );
					phi[etaPI-1] = math::logit( psi[etaPI-1] );
					phi[etaRTI-1] = math::logit( psi[etaRTI-1] );
				}

				template <typename IT, typename OT>
				static void transpsi(const MatrixBase<IT> & phi, MatrixBase<OT> & psi) {
					psi[lambda-1] = exp( phi[lambda-1] );
					psi[gamma-1] = exp( phi[gamma-1] );
					psi[fo-1] = math::logistic( phi[fo-1] );
					psi[a-1] = exp( phi[a-1] );
					psi[p0-1] = exp( phi[p0-1] );
					psi[muT-1] = exp( phi[muT-1] );
					psi[muL-1] = exp( phi[muL-1] );
					psi[muA-1] = exp( phi[muA-1] );
					psi[muV-1] = exp( phi[muV-1] );
					psi[etaPI-1] = math::logistic( phi[etaPI-1] );
					psi[etaRTI-1] = math::logistic( phi[etaRTI-1] );
				}

				template <typename IT, typename OT>
				static void dtranspsi(const MatrixBase<IT> & phi, MatrixBase<OT> & psi) {
					psi[lambda-1] = exp( phi[lambda-1] );
					psi[gamma-1] = exp( phi[gamma-1] );
					psi[fo-1] = math::dlogistic( phi[fo-1] );
					psi[a-1] = exp( phi[a-1] );
					psi[p0-1] = exp( phi[p0-1] );
					psi[muT-1] = exp( phi[muT-1] );
					psi[muL-1] = exp( phi[muL-1] );
					psi[muA-1] = exp( phi[muA-1] );
					psi[muV-1] = exp( phi[muV-1] );
					psi[etaPI-1] = math::dlogistic( phi[etaPI-1] );
					psi[etaRTI-1] = math::dlogistic( phi[etaRTI-1] );
				}

				template <typename IT, typename OT>
				static void ddtranspsi(const MatrixBase<IT> & phi, MatrixBase<OT> & psi) {
					psi[lambda-1] = exp( phi[lambda-1] );
					psi[gamma-1] = exp( phi[gamma-1] );
					psi[fo-1] = math::ddlogistic( phi[fo-1] );
					psi[a-1] = exp( phi[a-1] );
					psi[p0-1] = exp( phi[p0-1] );
					psi[muT-1] = exp( phi[muT-1] );
					psi[muL-1] = exp( phi[muL-1] );
					psi[muA-1] = exp( phi[muA-1] );
					psi[muV-1] = exp( phi[muV-1] );
					psi[etaPI-1] = math::ddlogistic( phi[etaPI-1] );
					psi[etaRTI-1] = math::ddlogistic( phi[etaRTI-1] );
				}
		};

};
#undef lambda
#undef gamma
#undef fo
#undef a
#undef p0
#undef muT
#undef muL
#undef muA
#undef muV
#undef etaPI
#undef etaRTI

#undef TC
#undef TL
#undef TA
#undef VI
#undef VNI

#undef LVL
#undef TTOT
#endif
