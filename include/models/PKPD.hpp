/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PKPD_H
#define PKPD_H

#include <cmath>
#include <MatrixVector.hpp>

#define ka 0
#define ke 1
#define Vf 2
#define kin 3
#define kout 4
#define IC50 5
#define A0 6
#define B0 7
#define C0 8

#define A 0
#define B 1
#define C 2

#define PK 0
#define PD 1

class PKPD {
	public:
		static constexpr int numberOfEquations() { return 3; }
		static constexpr int numberOfParameters() { return 9; }
		static constexpr int numberOfObservations() { return 2; }

	class ODE {
		public:
			static bool isStiff() { return true; }

		public:
			template <typename ST, typename PT, typename SDT>
			static void ddt(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<SDT> & ddt_y) {
				ddt_y[A] = -p[ka] * y[A];
				ddt_y[B] = (p[ka] * y[A] - p[ke] * y[B]) / p[Vf];
				ddt_y[C] = p[kin] * (1.0 - y[B]/(p[IC50] + y[B])) - p[kout] * y[C];
			}

			template <typename ST, typename PT>
			static void initial(const MatrixBase<PT> & p, MatrixBase<ST> & y) {
				y[A] = p[A0];
				y[B] = p[B0];
				y[C] = p[C0];
			}

		public:
			template <typename ST, typename PT, typename JT>
			static void jacobianState(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(A, A) = -p[ka];
				J(A, B) = 0.0;
				J(A, C) = 0.0;

				J(B, A) = p[ka]/p[Vf];
				J(B, B) = -p[ke]/p[Vf];
				J(B, C) = 0.0;

				J(C, A) = 0.0;
				J(C, B) = -p[kin]*(p[IC50] / ((y[B] + p[IC50])*(y[B] + p[IC50])));
				J(C, C) = -p[kout];
			}

			template <typename ST, typename PT, typename JT>
			static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(A, ka) = -y[A];
				J(A, ke) = 0.0;
				J(A, Vf) = 0.0;
				J(A, kin) = 0.0;
				J(A, kout) = 0.0;
				J(A, IC50) = 0.0;
				J(A, A0) = 0.0;
				J(A, B0) = 0.0;
				J(A, C0) = 0.0;

				J(B, ka) = y[A]/p[Vf];
				J(B, ke) = -y[B]/p[Vf];
				J(B, Vf) = -(y[A]*p[ka] - y[B]*p[ke])/(p[Vf]*p[Vf]);
				J(B, kin) = 0.0;
				J(B, kout) = 0.0;
				J(B, IC50) = 0.0;
				J(B, A0) = 0.0;
				J(B, B0) = 0.0;
				J(B, C0) = 0.0;

				J(C, ka) = 0.0;
				J(C, ke) = 0.0;
				J(C, Vf) = 0.0;
				J(C, kin) = 1.0 - y[B]/(y[B] + p[IC50]);
				J(C, kout) = -y[C];
				J(C, IC50) = (y[B]*p[kin])/((y[B] + p[IC50])*(y[B] + p[IC50]));
				J(C, A0) = 0.0;
				J(C, B0) = 0.0;
				J(C, C0) = 0.0;
			}

			template <typename PT, typename JT>
			static void jacobianParametersInitial(const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J(A, ka) = 0.0;
				J(A, ke) = 0.0;
				J(A, Vf) = 0.0;
				J(A, kin) = 0.0;
				J(A, kout) = 0.0;
				J(A, IC50) = 0.0;
				J(A, A0) = 1.0;
				J(A, B0) = 0.0;
				J(A, C0) = 0.0;

				J(B, ka) = 0.0;
				J(B, ke) = 0.0;
				J(B, Vf) = 0.0;
				J(B, kin) = 0.0;
				J(B, kout) = 0.0;
				J(B, IC50) = 0.0;
				J(B, A0) = 0.0;
				J(B, B0) = 1.0;
				J(B, C0) = 0.0;

				J(C, ka) = 0.0;
				J(C, ke) = 0.0;
				J(C, Vf) = 0.0;
				J(C, kin) = 0.0;
				J(C, kout) = 0.0;
				J(C, IC50) = 0.0;
				J(C, A0) = 0.0;
				J(C, B0) = 0.0;
				J(C, C0) = 1.0;
			}
	};

	class Projection {
		public:
			template <typename ST, typename PT, typename OT>
			static void project(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<OT> & o) {
				o[PK] = y[B];
				o[PD] = y[C];
			}

			template <typename ST, typename PT, typename JT>
			static void jacobian(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
			  J(PK,A) = 0.0;
			  J(PK,B) = 1.0;
			  J(PK,C) = 0.0;

			  J(PD,A) = 0.0;
			  J(PD,B) = 0.0;
			  J(PD,C) = 1.0;
			}

			template <typename ST, typename PT, typename JT>
			static void jacobianParameters(const double t, const MatrixBase<ST> & y, const MatrixBase<PT> & p, MatrixBase<JT> & J) {
				J.setZero();
			}
	};

	class Transform {
		public:
			template <typename IT, typename OT>
			static void transphi(const MatrixBase<IT> & psi, MatrixBase<OT> & phi) {
				phi[ka] = log( psi[ka] );
				phi[ke] = log( psi[ke] );
				phi[Vf] = log( psi[Vf] );
				phi[kin] = log( psi[kin] );
				phi[kout] = log( psi[kout] );
				phi[IC50] = log( psi[IC50] );
				phi[A0] = psi[A0];
				phi[B0] = psi[B0];
				phi[C0] = psi[C0];
			}

			template <typename IT, typename OT>
			static void transpsi(const MatrixBase<IT> & phi, MatrixBase<OT> & psi) {
				psi[ka] = exp( phi[ka] );
				psi[ke] = exp( phi[ke] );
				psi[Vf] = exp( phi[Vf] );
				psi[kin] = exp( phi[kin] );
				psi[kout] = exp( phi[kout] );
				psi[IC50] = exp( phi[IC50] );
				psi[A0] = phi[A0];
				psi[B0] = phi[B0];
				psi[C0] = phi[C0];
			}

			template <typename IT, typename OT>
			static void dtranspsi(const MatrixBase<IT> & phi, MatrixBase<OT> & dpsi) {
				dpsi[ka] = exp( phi[ka] );
				dpsi[ke] = exp( phi[ke] );
				dpsi[Vf] = exp( phi[Vf] );
				dpsi[kin] = exp( phi[kin] );
				dpsi[kout] = exp( phi[kout] );
				dpsi[IC50] = exp( phi[IC50] );
				dpsi[A0] = 1.0;
				dpsi[B0] = 1.0;
				dpsi[C0] = 1.0;
			}

			template <typename IT, typename OT>
			static void ddtranspsi(const MatrixBase<IT> & phi, MatrixBase<OT> & ddpsi) {
				ddpsi[ka] = exp( phi[ka] );
				ddpsi[ke] = exp( phi[ke] );
				ddpsi[Vf] = exp( phi[Vf] );
				ddpsi[kin] = exp( phi[kin] );
				ddpsi[kout] = exp( phi[kout] );
				ddpsi[IC50] = exp( phi[IC50] );
				ddpsi[A0] = 0.0;
				ddpsi[B0] = 0.0;
				ddpsi[C0] = 0.0;
			}
	};

};

#undef ka
#undef ke
#undef Vf
#undef kin
#undef kout
#undef IC50
#undef A0
#undef B0
#undef C0

#undef A
#undef B
#undef C

#undef PK
#undef PD
#endif
