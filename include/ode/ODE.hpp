/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ODE_H
#define ODE_H

#include <MatrixVector.hpp>
#include <stdexcept>

namespace ode {

	class DIFFMEM_EXPORT ODEBase {
		public:
			virtual ~ODEBase() {};

		public:
			virtual bool isStiff() const { return true; }
			virtual int numberOfEquations() const = 0;
			virtual void initial(RefVecd y0) = 0;
			virtual void ddt(const double t, const RefVecd & y, RefVecd ydot) = 0;

			virtual bool hasJacobian() const { return false; }
			virtual void jacobian(const double t, const RefVecd & y, const RefVecd & ydot, RefMatrixd J) {
				throw std::runtime_error("no jacobian defined");
			}
	};

	class DIFFMEM_EXPORT SensitivityODEBase : public ODEBase {
		public:
			virtual ~SensitivityODEBase() {};

		public:
			virtual int numberOfParameters() const = 0;
			virtual void jacobianParametersInitial(RefMatrixd yS0) = 0;
			virtual void jacobianParameters(const double t, const RefVecd & y, const RefVecd & ydot, RefMatrixd J) = 0;
			void jacobianState(const double t, const RefVecd & y, const RefVecd & ydot, RefMatrixd J) {
				jacobian(t, y, ydot, J);
			}

		private:
			bool hasJacobian() const { return true; }
	};

	class DIFFMEM_EXPORT AdjointODEBase {
		public:
			virtual ~AdjointODEBase() {};

		public:
			virtual bool isStiff() const { return true; }
			virtual int numberOfEquations() const = 0;
			virtual void ddt(const double t, const RefVecd & y, const RefVecd & v, RefVecd vdot) = 0;
			virtual void initial(RefVecd v0) { v0.setZero(); };
			virtual bool hasJacobian() const { return false; }
			virtual void jacobian(const double t, const RefVecd & y, const RefVecd & v, const RefVecd & vdot, RefMatrixd J) {
				throw std::runtime_error("no jacobian defined");
			}

			virtual int numberOfQuadratureVars() const { return 0; }
			virtual void quadrature(const double t, const RefVecd & y, const RefVecd & v, RefVecd Qdot) {
				throw std::runtime_error("no quadrature defined");
			}
	};

	//only quadrature differs from AdjointODEBase
	//because only there sensitivity solutions are necessary
	class DIFFMEM_EXPORT AdjointSensitivityODEBase: public AdjointODEBase {
		public:
			virtual ~AdjointSensitivityODEBase() {};

		public:
			//virtual bool isStiff() const { return true; }
			//virtual int numberOfEquations() const = 0;
			//TODO: Tom, why did you went back to this artificial division
			//TODO: of adjoint to ODE(Base) and parameters etc? Why UserData
			//TODO: has to be some ODEBase? Adjoint puts it all together
			//TODO: (forward ODE, backward ODE, likelihood, data). Nothing to do about it.
			//TODO: Dividing unity to pieces makes the code unreadable and
			//TODO: unnecessary expanded. One always runs at a problem when
			//TODO: extending it. In Cvode, they have F, B and S in the names of
			//TODO: the functions just because of this. Having numberOfEquations
			//TODO:  in some ODEbase, not knowing to which number it refers to
			//TODO: does not help. Because of lack of the time and the energy, let
			//TODO: us follow the developments...
			//virtual int numberOfSensitivityEquations() const = 0;
			//virtual void ddt(const double t, const RefVecd & y, const RefVecd & v, RefVecd & vdot) = 0;
			//virtual void initial(RefVecd & v0) { v0.setZero(); };
			//virtual bool hasJacobian() const { return false; }
			//virtual void jacobian(const double t, const RefVecd & y, const RefVecd & v, const RefVecd & vdot, RefMatrixd & J) {
			//	throw std::runtime_error("no jacobian defined");
			//}

			//virtual int numberOfQuadratureVars() const { return 0; }
			virtual void quadratureSens(const double t, const RefVecd & y, const RefMatrixd & s, const RefVecd & v, RefVecd Qdot) {
				throw std::runtime_error("no quadrature defined");
			}
	};

	/*
	 * A specific instance of an ODE Model: Model + Parameters
	 */
	template <class Model>
	class ODE : public SensitivityODEBase {
		public:
			typedef VecNd<Model::numberOfParameters()> Parameters;
			typedef VecNd<Model::numberOfEquations()> States;
			typedef MatrixMNd<Model::numberOfEquations(), Model::numberOfEquations()> Jac;

		public:
			ODE() {} // TODO remove or valid initial value for params
			ODE(ConstRefVecd & psi) : params(psi) {}

		public:
			virtual bool isStiff() const { return Model::ODE::isStiff(); }
			virtual int numberOfEquations() const { return Model::numberOfEquations(); }
			virtual void ddt(const double t, const RefVecd & y, RefVecd ydot) {
				Model::ODE::ddt(t, y, params, ydot);
			}

			virtual bool hasJacobian() const { return true; }
			virtual void jacobian(const double t, const RefVecd & y, const RefVecd & ydot, RefMatrixd J) {
				Model::ODE::jacobianState(t, y, params, J);
			}

			virtual void initial(RefVecd y0) {
				Model::ODE::initial(params, y0);
			}

			virtual int numberOfParameters() const { return Model::numberOfParameters(); }

			virtual void jacobianParametersInitial(RefMatrixd yS0) {
				Model::ODE::jacobianParametersInitial(params, yS0);
			}

			virtual void jacobianParameters(const double t, const RefVecd & y, const RefVecd & ydot, RefMatrixd J) {
				Model::ODE::jacobianParameters(t, y, params, J);
			}

		public:
			void setParameters(ConstRefVecd & psi) { this->params = psi; }
			const Parameters & parameters() const { return params; }

		private:
			Parameters params;
	};

}
#endif
