/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CVODEINTEGRATORBASE_H
#define CVODEINTEGRATORBASE_H

#include <MatrixVector.hpp>
#include <ode/ODE.hpp>

#include <cvodes/cvodes.h>
#include <nvector/nvector_serial.h>

namespace ode {

	class DIFFMEM_EXPORT CVodeIntegratorBase {
		public:
			CVodeIntegratorBase(const CVodeIntegratorBase &) = delete;
			CVodeIntegratorBase & operator =(const CVodeIntegratorBase &) = delete;

		public:
			void integrateForwardDt( const double dt ) {
				integrateTo( dt + currentTime() );
			}

			void integrateTo( const double tOut );

		public:
			void setTolerances( double relTol, double absTol );
			void setRelativeTolerance( double relTol );
			void setAbsoluteTolerance( double absTol );
			double relativeTolerance() const;
			double absoluteTolerance() const;

			void setMaxNumSteps(int steps);
			int maxNumSteps() const;

			void setMaxStepSize(double hmax);
			void setMinStepSize(double hmin);
			void setInitStepSize(double hinit);
			double lastStepSize() const;

		public:
			double currentTime() const;
			const Vecd & currentState() const;
			int numSteps() const;
			int numEvals() const;

		public:
			template <typename F>
			void apply(F && f);
			void reinit(const double initialTime, const Vecd & initialState);

		protected:
			void create(int Neqs, bool stiff, bool functional = false);
			void initializeSettings();
			void initialize(ODEBase & odeProblem, bool functional = false);
			void cvodeReInit();

		protected:
			CVodeIntegratorBase();
			~CVodeIntegratorBase();

		protected:
			void * cvodeMem_;
			N_Vector nvCurrentState_;

			int maxNumSteps_;
			double relTol_;
			double absTol_;
			double currentTime_;
			Vecd currentState_;
	};

	inline double CVodeIntegratorBase::currentTime() const {
		return currentTime_;
	}

	inline const Vecd & CVodeIntegratorBase::currentState() const {
		return currentState_;
	}

	inline void CVodeIntegratorBase::setRelativeTolerance( double relTol ) {
		setTolerances(relTol, absTol_);
	}

	inline void CVodeIntegratorBase::setAbsoluteTolerance( double absTol ) {
		setTolerances(relTol_, absTol);
	}

	inline double CVodeIntegratorBase::relativeTolerance() const {
		return relTol_;
	}

	inline double CVodeIntegratorBase::absoluteTolerance() const {
		return absTol_;
	}

	inline int CVodeIntegratorBase::maxNumSteps() const {
		return maxNumSteps_;
	}

	void throwCVodeError(int err, const char *msg);
	inline void throwIfCVodeError(int flag, const char *msg) {
		if( flag >= CV_SUCCESS )
			return;

		throwCVodeError(flag, msg);
	}

	template <typename F>
	void CVodeIntegratorBase::apply(F &&f) {
		std::forward<F>(f)(currentState_);
		cvodeReInit();
	}
}
#endif
