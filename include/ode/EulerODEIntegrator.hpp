/*
* Copyright (c) 2016, Hasselt University
* All rights reserved.

* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef EULERODEINTEGRATOR_H
#define EULERODEINTEGRATOR_H

#include <ode/ODE.hpp>

namespace ode {

	class DIFFMEM_EXPORT EulerODEIntegrator {
		public:
			EulerODEIntegrator(ODEBase & odeProblem);

		public:
			void integrateTo(const double tOut);

		public:
			void setIC(const double initialTime = 0.0);
			void setStepSize(const double stepSize = 0.001);

		public:
			double currentTime() const;
			const Vecd & currentState() const;
			double stepSize() const;

			template <typename F>
			void apply(F &&f);

		private:
			ODEBase & odeProblem_;
			double currentTime_;
			double stepSize_;

			Vecd currentState_;
	};

	inline double EulerODEIntegrator::currentTime() const {
		return currentTime_;
	}

	inline double EulerODEIntegrator::stepSize() const {
		return stepSize_;
	}

	inline const Vecd & EulerODEIntegrator::currentState() const {
		return currentState_;
	}

	template <typename F>
	void EulerODEIntegrator::apply(F &&f) {
		std::forward<F>(f)(currentState_);
	}

}
#endif
