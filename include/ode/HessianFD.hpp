/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HESSIANFD_H
#define HESSIANFD_H

#include <ode/ODE.hpp>
#include <FiniteDifference.hpp>
#include <Measurements.hpp>
#include <LikelihoodModel.hpp>
#include <StructuralModel.hpp>

namespace ode {

	template <class Model, typename PT>
	class HessianFD {
		public:
			HessianFD(){};
			~HessianFD(){};

			template <typename ST, typename HT>
			static void evaluate(const Measurements & measurements,
					StructuralModel & structuralModel, StructuralData *sd,
					const LikelihoodModel & likelihoodModel,
					const MatrixBase<PT> & phi, const MatrixBase<ST> & tau, MatrixBase<HT> & hessian){

				auto ll = [&](const VecNd<Model::numberOfParameters()> & phi) -> double {
					VecNd<Model::numberOfParameters()> psi;
					structuralModel.transpsi(phi, psi);
					return structuralModel.logpdf(sd, ld, psi, tau, measurements, likelihoodModel);
				};

				static const int order = 2;
				static double resdp1, resdp2, resdp1dp2;
				static double resNow;
				static double dp1, dp2;
				const static double h = 1e-4; //5.95e-5
				static VecNd<Model::numberOfParameters()> grad;
				Vecd phi1(Model::numberOfParameters());
				Vecd phi2(Model::numberOfParameters());

				hessian.setZero();
				if (order == 1){
					phi1 = phi; //WE DIFFERENTIATE IN PHI !!!
					phi2 = phi;
					resNow = ll(phi);
					for(int i = 0; i < Model::numberOfParameters(); i++) {
						dp1 = h*std::abs(phi[i]);
						phi1[i] = phi[i] + dp1;
						resdp1 = ll(phi1);
						for(int j = 0; j < i; j++) {
							dp2 = h*std::abs(phi[j]);
							phi2[i] = phi[i];
							phi2[j] = phi[j] - dp2;
							resdp2 = ll(phi2);

							phi2[i] = phi1[i];
							phi2[j] = phi[j] - dp2;
							resdp1dp2 = ll(phi2);

							phi2[i] = phi[i];
							phi2[j] = phi[j];

							hessian(i,j) = ((resdp1 - resNow)/dp1 - (resdp1dp2 - resdp2)/dp1)/dp2;
							hessian(j,i) = hessian(i,j);
						}

						phi1[i] = phi[i] - dp1;
						resdp2 = ll(phi1);
						hessian(i,i) = ((resdp1 - resNow)/dp1 - (resNow - resdp2)/dp1)/dp1;
						phi1[i] = phi[i];
					}
				}

				if (order == 2) {
					phi1 = phi;
					resNow = ll(phi);
					for(int i = 0; i < Model::numberOfParameters(); i++) {
						dp1 = h*std::abs(phi[i]);
						for(int j = 0; j < i; j++) {
							dp2 = h*std::abs(phi[j]);
							///////////////////////////
							phi1[i] = phi[i] + dp1;
							phi1[j] = phi[j] + dp2;
							auto respp = ll(phi1);
							///////////////////////////
							phi1[i] = phi[i] + dp1;
							phi1[j] = phi[j] - dp2;
							auto respm = ll(phi1);
							///////////////////////////
							phi1[i] = phi[i] - dp1;
							phi1[j] = phi[j] + dp2;
							auto resmp = ll(phi1);
							///////////////////////////
							phi1[i] = phi[i] - dp1;
							phi1[j] = phi[j] - dp2;
							auto resmm = ll(phi1);
							///////////////////////////
							hessian(i,j) = ((respp - resmp)/(2*dp1) - (respm - resmm)/(2*dp1))/(2*dp2);
							hessian(j,i) = hessian (i,j);
							phi1[i] = phi[i];
							phi1[j] = phi[j];
						}

						///////////////////////////
						phi1[i] = phi[i] + 2*dp1;
						auto respp = ll(phi1);
						///////////////////////////
						phi1[i] = phi[i] - 2*dp1;
						auto resmm = ll(phi1);
						///////////////////////////
						hessian(i,i) = ((respp - resNow)/(2*dp1) - (resNow - resmm)/(2*dp1))/(2*dp1);
						phi1[i] = phi[i];
					}
				}
			}

	};

	template <class Model, typename PT, typename ST, typename HT>
	static void evaluateHessianFD(const Measurements & meas,
			StructuralModel & structuralModel, StructuralData *sd, const LikelihoodModel & llm,
			const MatrixBase<PT> & phi, const MatrixBase<ST> & tau, MatrixBase<HT> & hessian){
			HessianFD<Model, PT>::evaluate(meas, structuralModel, sd, llm, ld, phi, tau, hessian);
	}

}
#endif // HESSIANFD_H
