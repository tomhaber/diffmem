/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ADJOINT_H
#define ADJOINT_H

#include <ode/ODE.hpp>
#include <ode/AdjointIntegrator.hpp>
#include <PointwiseLikelihoodModel.hpp>

namespace ode {

	template <class Model>
	class Adjoint : public AdjointODEBase {
		public:
			typedef VecNd<Model::numberOfParameters()> Parameters;
			typedef VecNd<Model::numberOfEquations()> States;
			typedef MatrixMNd<Model::numberOfEquations(), Model::numberOfEquations()> JacSol;
			typedef MatrixMNd<Model::numberOfEquations(), Model::numberOfParameters()> JacPar;
			typedef MatrixMNd<Model::numberOfObservations(), Model::numberOfEquations()> JacProj;

			//initialization
		protected:
			void checkMeasurementTimes (double t0){
				Assert(t.size() > 0, "Adjoint::checkData(...): at least 1 data point, please");

				bool dataOrdered = true;
				int i = 0;
				while(dataOrdered && i < t.size() - 1){
					if (t[i] >= t[i+1]) dataOrdered = false;
					i++;
				}
				Assert(dataOrdered, "Adjoint::checkData(...): data should be chronologically ordered");
			}

			void initialize(const double t0, const int N){
				this->t0 = t0;
				checkMeasurementTimes(t0);
				integrator = new AdjointIntegrator(ode, N);
				backwardIntegrator = integrator->createBackward(*this);
				grad.resize(Model::numberOfParameters());
				u.resize(Model::numberOfEquations(), t.size());
			}

		public:
			Adjoint(const Vecd & psi, ConstRefVecd & t,
					const PointwiseLikelihoodModel & likelihoodModel, double t0 = 0.0, int N = 1000)
				: psi(psi), t(t), likelihoodModel(likelihoodModel) {
				initialize(t0, N);
				setParameters(psi);
			}

			// preferred constructor with parameters psi
			Adjoint(const Vecd & psi, ConstRefVecd & t,
					const PointwiseLikelihoodModel & likelihoodModel, ConstRefVecd & tau, double t0 = 0.0, int N = 1000)
					: psi(psi), t(t), likelihoodModel(likelihoodModel), tau(tau) {
				initialize(t0, N);
				setParameters(psi);
			}

			// preferred constructor without parameters psi
			Adjoint(ConstRefVecd & t, const PointwiseLikelihoodModel & likelihoodModel,
					ConstRefVecd & tau, double t0 = 0.0, int N = 1000)
				: t(t), likelihoodModel(likelihoodModel), tau(tau) {
				initialize(t0, N);
			}

			virtual ~Adjoint(){
				if (integrator != NULL){
					delete integrator;
					integrator = NULL;
				}
			}

		public:
			virtual int numberOfEquations() const { return Model::numberOfEquations(); }
			virtual void ddt(const double s, const RefVecd & u, const RefVecd & v, RefVecd vdot) {
				JacSol Jy;
				Model::ODE::jacobianState(s, u, psi, Jy);

				vdot = -Jy.transpose() * v;
			}

			virtual bool hasJacobian() const { return true; }
			virtual void jacobian(const double s, const RefVecd & u, const RefVecd & v, const RefVecd & vdot, RefMatrixd J) {
				JacSol Jy;
				Model::ODE::jacobianState(s, u, psi, Jy);

				J = -Jy.transpose();
			}

			virtual int numberOfQuadratureVars() const { return Model::numberOfParameters(); }
			virtual void quadrature(const double s, const RefVecd & u, const RefVecd & v, RefVecd Qdot) {
				JacPar Jp;
				Model::ODE::jacobianParameters(s, u, psi, Jp);

				Qdot = -Jp.transpose() * v;
			}

		public:
			void setParameters(ConstRefVecd & psiExt) {
				psi = psiExt;
				ode.setParameters(psi);
				integrator->setIC();
			}

		protected:
			void solveForward() {
				int end = t.size();
				for(int i = 0; i < end; i++) {
					integrator->integrateTo( t[i] );
					u.col(i) = integrator->currentState();
				}
			}

			void solveBackward() {
				VecNd<Model::numberOfEquations()> delta;

				int end = t.size() - 1;
				rhsDataTerm(end, u.col(end), delta);
				backwardIntegrator->reinit(t[end], delta);
				backwardIntegrator->reinitQuadrature();

				integrator->setupBackward( t[end] );
				for(int i = end-1; i >= 0; i--) {
					const double ty = t[i];
					integrator->integrateBackwardTo(ty);
					rhsDataTerm(i, u.col(i), delta);
					backwardIntegrator->reinit(ty, backwardIntegrator->currentState() + delta);
				}

				integrator->integrateBackwardTo(t0);

				JacPar Jp;
				Model::ODE::jacobianParametersInitial(psi, Jp);
				grad = backwardIntegrator->currentQuadrature() + Jp.transpose() * backwardIntegrator->currentState();
			}

		public:
			const Vecd & getGrad() const {
				return grad;
			}

			double getLikelihood() const {
				double ll = 0.0;
				for(int idx = 0; idx < t.size(); idx++) {
					VecNd<Model::numberOfObservations()> yu;
					Model::Projection::project(t[idx], u.col(idx), psi, yu);
					ll += likelihoodModel.logpdf(tau, idx, yu);
				}

				return ll;
			}

		public:
			void solve() {
				solveForward();
				solveBackward();
			}

			void solve(ConstRefVecd & psiExt) {
				setParameters(psiExt);
				solveForward();
				solveBackward();
			}

			void solve(ConstRefVecd & psiExt, Vecd & gradExt) {
				solve(psiExt);
				gradExt = getGrad();
			}

		protected:

			template <typename UT, typename GT>
			void rhsDataTerm(int idx, const MatrixBase<UT> & u, MatrixBase<GT> const & grad_) {
				VecNd<Model::numberOfObservations()> yu;
				VecNd<Model::numberOfObservations()> gradLL;

				Model::Projection::project(t[idx], u, psi, yu);
				likelihoodModel.logpdfGradU(tau, idx, yu, gradLL);

				JacProj Jp;
				Model::Projection::jacobian(t[idx], u, psi, Jp);

				MatrixBase<GT> & grad = const_cast< MatrixBase<GT>& >(grad_);
				grad = Jp.transpose()*gradLL;
			}

		protected:
			double t0;
			Vecd psi; // vector of parameters
			ConstRefVecd t;
			const PointwiseLikelihoodModel & likelihoodModel;
			ConstRefVecd tau;
			Vecd grad;
			Matrixd u; // forward solution at the time points
			ODE<Model> ode;
			AdjointIntegrator* integrator;
			BackwardIntegrator *backwardIntegrator;
	};

}
#endif
