/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SENSITIVITYINTEGRATOR_H
#define SENSITIVITYINTEGRATOR_H

#include <ode/CVodeIntegratorBase.hpp>

namespace ode {

	class DIFFMEM_EXPORT SensitivityIntegrator : public CVodeIntegratorBase {
		public:
			SensitivityIntegrator(SensitivityODEBase & sensProblem, bool functional = false);
			~SensitivityIntegrator();

		public:
			void integrateForwardDt( const double dt ) {
				integrateTo( dt + currentTime() );
			}

			void integrateTo( const double tOut );

		public:
			void setIC(const double initialTime = 0.0);
			void reinit(const double initialTime, const Vecd & initialState, const Matrixd & initialSensivity);

		public:
			const Matrixd & currentSensitivity() const;

		private:
			void initialize(bool functional = false);
			void cvodeReInit();

		private:
			SensitivityODEBase & sensProblem_;
			Matrixd currentSens_;
			N_Vector *nvCurrentSens_;
	};

	inline const Matrixd & SensitivityIntegrator::currentSensitivity() const {
		return currentSens_;
	}

}
#endif
