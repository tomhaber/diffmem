/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef HESSIANADJOINT_H
#define HESSIANADJOINT_H

#include <ode/Adjoint.hpp>
#include <Measurements.hpp>

namespace ode {

	template <class Model, typename PT>
	class HessianAdjoint {
	public:
		HessianAdjoint(){};
		~HessianAdjoint(){};
		template <typename ST, typename HT>
		void evaluate(const MatrixBase<PT> & phi, const Measurements & measurements,
				const LikelihoodModel & llm, const MatrixBase<ST> & tau, MatrixBase<HT> & hessian){
			const double h = 1e-5;
			static int order = 1;

			const int nPar = Model::numberOfParameters();
			//const int nEqs = Model::numberOfEquations();
			//const int nObs = Model::numberOfObservations();

			Vecd psi(nPar);
			double dpsi;
			Vecd psiLoc(nPar);
			Vecd grad(nPar);
			Vecd gradp(nPar);
			Vecd gradm(nPar);

			Model::Transform::transpsi(phi, psi);

			Matrixd hessian_psi(nPar, nPar);
			//hessian_psi.setZero();
			//Adjoint<Model> adjoint(ty, y, sigma_inv); does not work for LinearModel. Acc is very bad. Why?
			//but some values are ok
			//hmm, no idea but CVodes works like a charm and the grid version of SensAdjoint as well
			Adjoint<Model> adjoint(measurements, llm, ld, tau);
			if(order == 1){
				adjoint.solve(psi, grad);
				psiLoc = psi;
				for(int i = 0; i < nPar; i++) {
					dpsi = h*std::abs(psi[i]);
					psiLoc[i] = psi[i] + dpsi;
					adjoint.solve(psiLoc, gradp);
					hessian_psi.col(i) = (gradp-grad)/dpsi;
					psiLoc[i] = psi[i];
				}
			}

			if(order == 2){
				psiLoc = psi;
				for(int i = 0; i < nPar; i++) {
					dpsi = h*std::abs(psi[i]);
					psiLoc[i] = psi[i] + dpsi;
					adjoint.solve(psiLoc, gradp);
					psiLoc[i] = psi[i] - dpsi;
					adjoint.solve(psiLoc, gradm);
					hessian_psi.col(i) = (gradp-gradm)/(2*dpsi);
					psiLoc[i] = psi[i];
				}
			}

			VecNd<nPar> dpsi_phi;
			VecNd<nPar> ddpsi_phi;
			Model::Transform::dtranspsi(phi, dpsi_phi);
			Model::Transform::ddtranspsi(phi, ddpsi_phi);
			//TODO: how to do it eigenwise? cwiseProduct does not work as expected
			for(int i = 0; i < nPar; i++)
				hessian.col(i) = hessian_psi.col(i).cwiseProduct(dpsi_phi);
			for(int i = 0; i < nPar; i++)
				hessian.row(i) = (hessian.row(i).transpose()).cwiseProduct(dpsi_phi);//.transpose();
			hessian.diagonal() += grad.cwiseProduct(ddpsi_phi);
		}
	};

	template <class Model, typename PT, typename ST, typename HT>
	static void evaluateHessianAdjoint(const MatrixBase<PT> & phi, const Measurements & measurements,
			const LikelihoodModel & llm, const MatrixBase<ST> & sigma_inv, MatrixBase<HT> & hessian){
		HessianAdjoint<Model, PT>::evaluate(phi, measurements, llm, sigma_inv, hessian);
	}

}
#endif // HESSIANADJOINT_H
