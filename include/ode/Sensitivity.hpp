/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SENSITIVITY_H
#define SENSITIVITY_H

#include <ode/ODE.hpp>
#include <ode/SensitivityIntegrator.hpp>
#include <PointwiseLikelihoodModel.hpp>

namespace ode {

	template <class Model>
	class Sensitivity {
		public:
			template <typename PT,typename ST, typename GT>
			static double computeGradient(ConstRefVecd & time,
					const PointwiseLikelihoodModel & llm, const MatrixBase<PT> & psi,
					const MatrixBase<ST> & tau, MatrixBase<GT> & gradPsi) {

				ODE<Model> ode(psi);
				SensitivityIntegrator integrator(ode);
				integrator.setIC();

				double ll = llm.logProportions(tau);
				gradPsi = MatrixBase<GT>::Zero( Model::numberOfParameters() );
				for(int i = 0; i < time.size(); ++i) {
					const double t = time[i];
					integrator.integrateTo(t);

					const Vecd & u = integrator.currentState();
					const Matrixd & s = integrator.currentSensitivity();

					VecNd<Model::numberOfObservations()> yu;
					Model::Projection::project(t, u, psi, yu);

					MatrixMNd<Model::numberOfObservations(), Model::numberOfEquations()> Jp;
					Model::Projection::jacobian(t, u, psi, Jp);

					MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> Jpp;
					Model::Projection::jacobianParameters(t, u, psi, Jpp);

					VecNd<Model::numberOfObservations()> gradU;
					llm.logpdfGradU(tau, i, yu, gradU);
					ll += llm.logProportionalPdf(tau, i, yu);

					MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> z = (Jp * s) + Jpp;
					gradPsi.noalias() += z.transpose() * gradU;
				}

				return ll;
			}

			template <typename PT, typename ST, typename GT, typename HT>
			static double computeHessian(ConstRefVecd &time,
					const PointwiseLikelihoodModel &llm,
					const MatrixBase<PT> &psi, const MatrixBase<ST> &tau,
					MatrixBase<GT> &gradPsi, MatrixBase<HT> &hessPsi) {

				ODE<Model> ode(psi);
				SensitivityIntegrator integrator(ode);
				integrator.setIC();

				const int N = Model::numberOfParameters();
				double ll = llm.logProportions(tau);
				gradPsi = MatrixBase<GT>::Zero( N );
				hessPsi = MatrixBase<HT>::Zero( N, N );

				for(int i = 0; i < time.size(); ++i) {
					const double t = time[i];
					integrator.integrateTo(t);

					const Vecd & u = integrator.currentState();
					const Matrixd & s = integrator.currentSensitivity();

					VecNd<Model::numberOfObservations()> yu;
					Model::Projection::project(t, u, psi, yu);

					MatrixMNd<Model::numberOfObservations(), Model::numberOfEquations()> Jp;
					Model::Projection::jacobian(t, u, psi, Jp);

					MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> Jpp;
					Model::Projection::jacobianParameters(t, u, psi, Jpp);

					VecNd<Model::numberOfObservations()> gradU;
					MatrixMNd<Model::numberOfObservations(), Model::numberOfObservations()> hessU;
					llm.logpdfHessU(tau, i, yu, gradU, hessU);
					ll += llm.logProportionalPdf(tau, i, yu);

					MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> z = (Jp * s) + Jpp;
					MatrixMNd<Model::numberOfObservations(), Model::numberOfParameters()> tmp = hessU * z;

					gradPsi.noalias() += z.transpose() * gradU;
					hessPsi.noalias() += z.transpose() * tmp;
				}

				return ll;
			}
	};

}
#endif
