/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RUNGEKUTTA45_H
#define RUNGEKUTTA45_H

#include <MatrixVector.hpp>
#include <ode/ODE.hpp>

namespace ode {

	class DIFFMEM_EXPORT RungeKutta45 {
		public:
			RungeKutta45(ODEBase & odeProblem);

		public:
			void integrateForwardDt( const double dt );
			void integrateTo( const double tOut );

			void setIC(const double initialTime = 0.0);
			void setInitial(const double initialTime, const Vecd & initialState);
			void reinit(const double initialTime, const Vecd & initialState);

		public:
			void setTolerances(double absTol, double relTol);
			void setRelativeTolerance( double relTol );
			void setAbsoluteTolerance( double absTol );
			double relativeTolerance() const;
			double absoluteTolerance() const;

			void setMaxNumSteps(int maxsteps);
			void setMaxStepSize(double hmax);
			void setMinStepSize(double hmin);
			void setInitStepSize(double hinit);
			double lastStepSize() const;

		public:
			double currentTime() const;
			const Vecd & currentState() const;

			template <typename F>
			void apply(F &&f);

		private:
			double estimateInitial_dt(double tspan, ConstRefVecd & f0);

		protected:
			ODEBase & odeProblem_;
			Matrixd tmp;
			Vecd y;
			double relTol_;
			double absTol_;
			double currentTime_;
			Vecd currentState_;

			int maxNumSteps_;
			double stepSize_;
			double hmax_, hmin_, hinit_;

			// no copy constructor
			RungeKutta45( const RungeKutta45 & );
	};

	inline void RungeKutta45::integrateForwardDt( const double dt ) {
		integrateTo( dt + currentTime() );
	}

	inline double RungeKutta45::currentTime() const {
		return currentTime_;
	}

	inline const Vecd & RungeKutta45::currentState() const {
		return currentState_;
	}

	inline void RungeKutta45::setTolerances(double absTol, double relTol) {
		absTol_ = absTol;
		relTol_ = relTol;
	}

	inline void RungeKutta45::setRelativeTolerance( double relTol ) {
		relTol_ = relTol;
	}

	inline void RungeKutta45::setAbsoluteTolerance( double absTol ) {
		absTol_ = absTol;
	}

	inline double RungeKutta45::relativeTolerance() const {
	  return relTol_;
	}

	inline double RungeKutta45::absoluteTolerance() const {
	  return absTol_;
	}

	inline void RungeKutta45::setMaxNumSteps(int maxsteps) {
		maxNumSteps_ = maxsteps;
	}

	inline void RungeKutta45::setMaxStepSize(double hmax) {
		hmax_ = hmax;
	}

	inline void RungeKutta45::setMinStepSize(double hmin) {
		hmin_ = hmin;
	}

	inline void RungeKutta45::setInitStepSize(double hinit) {
		hinit_ = hinit;
	}

	inline double RungeKutta45::lastStepSize() const {
		return stepSize_;
	}

	template <typename F>
	void RungeKutta45::apply(F &&f) {
		std::forward<F>(f)(currentState_);
	}
}
#endif
