/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ADJOINTINTEGRATOR_H
#define ADJOINTINTEGRATOR_H

#include <ode/CVodeIntegratorBase.hpp>
#include <vector>

namespace ode {

	class BackwardIntegrator;
	class DIFFMEM_EXPORT AdjointIntegrator : public CVodeIntegratorBase {
		public:
			enum Interpolation { Hermite, Polynomial };
			AdjointIntegrator(ODEBase & odeProblem, int Nd = 100, Interpolation interpolation = Hermite);
			~AdjointIntegrator();

		public:
			void integrateForwardDt( const double dt ) {
				integrateTo( dt + currentTime() );
			}

			void integrateTo( const double tOut );
			void integrateBackwardTo( const double tOut );

		public:
			BackwardIntegrator *createBackward(AdjointODEBase & adjProblem);
			void setupBackward();
			void setupBackward(const double time);

#ifdef BUGGY_CVODES
		private:
#endif
			void clearBackward();
			void resetBackward();

		public:
			void setIC(const double initialTime = 0.0);
			void reinit(const double initialTime, const Vecd & initialState);

		private:
			void initialize();
			void initializeAdjoint();
			void clearAdjoint();
			void cvodeReInit();

		private:
			ODEBase & odeProblem_;
			enum Interpolation interpolation;
			int Nnc_; // number of integration steps between two consecutive checkpoints
			int numOfCheckpoints_;

			double backwardTime_;
			std::vector<BackwardIntegrator *> backwardProblems_;
	};

	class DIFFMEM_EXPORT BackwardIntegrator {
		public:
			void setTolerances( double relTol, double absTol );
			void setRelativeTolerance( double relTol );
			void setAbsoluteTolerance( double absTol );
			double relativeTolerance() const;
			double absoluteTolerance() const;

			void setTolerancesQuadrature( double relTol, double absTol );
			void setRelativeToleranceQuadrature( double relTol );
			void setAbsoluteToleranceQuadrature( double absTol );
			double relativeToleranceQuadrature() const;
			double absoluteToleranceQuadrature() const;

		public:
			double currentTime() const;
			const Vecd & currentState() const;
			const Vecd & currentQuadrature() const;

		public:
			void setIC(const double initialTime = 0.0);
			void reinit(const double initialTime, const Vecd & initialState);
			void reinitQuadrature();

		private:
			void initialize();
			void cvodeReInit();
			double update();

		private:
			void *cvodeMem_;
			int identity_;

			AdjointODEBase & adjProblem_;
			N_Vector nvCurrentState_;
			N_Vector nvCurrentQ_;

			double relTol_, absTol_;
			double relTolQ_, absTolQ_;
			double currentTime_;
			Vecd currentState_;
			Vecd currentQ_;

			int maxNumSteps_;

		private:
			BackwardIntegrator(AdjointODEBase & adjProblem, void *cvodeMem, int identity);
			~BackwardIntegrator();

		friend class AdjointIntegrator;
	};

	inline double BackwardIntegrator::currentTime() const {
		return currentTime_;
	}

	inline const Vecd & BackwardIntegrator::currentState() const {
		return currentState_;
	}

	inline const Vecd & BackwardIntegrator::currentQuadrature() const {
		return currentQ_;
	}

	inline void BackwardIntegrator::setRelativeTolerance( double relTol ) {
		setTolerances(relTol, absTol_);
	}

	inline void BackwardIntegrator::setAbsoluteTolerance( double absTol ) {
		setTolerances(relTol_, absTol);
	}

	inline double BackwardIntegrator::relativeTolerance() const {
		return relTol_;
	}

	inline double BackwardIntegrator::absoluteTolerance() const {
		return absTol_;
	}

	inline void BackwardIntegrator::setRelativeToleranceQuadrature( double relTol ) {
		setTolerancesQuadrature(relTol, absTolQ_);
	}

	inline void BackwardIntegrator::setAbsoluteToleranceQuadrature( double absTol ) {
		setTolerancesQuadrature(relTolQ_, absTol);
	}

	inline double BackwardIntegrator::relativeToleranceQuadrature() const {
		return relTolQ_;
	}

	inline double BackwardIntegrator::absoluteToleranceQuadrature() const {
		return absTolQ_;
	}


	inline void AdjointIntegrator::clearBackward() {
		clearAdjoint();
	}

	inline void AdjointIntegrator::resetBackward() {
		clearAdjoint();
		initializeAdjoint();
	}

}
#endif

