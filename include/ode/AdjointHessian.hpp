/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ADJOINTHESSIAN_H
#define ADJOINTHESSIAN_H

#include <LikelihoodModel.hpp>
#include <ode/ODE.hpp>
#include <ode/AdjointHessianIntegrator.hpp>

namespace ode {

	template<class Model>
	class AdjointHessian: public AdjointSensitivityODEBase {
		public:
			typedef VecNd<Model::numberOfParameters()> Parameters;
			typedef VecNd<Model::numberOfEquations()> States;
			typedef MatrixMNd<Model::numberOfEquations(), Model::numberOfEquations()> JacSol;
			typedef MatrixMNd<Model::numberOfEquations(),
					Model::numberOfParameters()> JacPar;
			typedef MatrixMNd<Model::numberOfObservations(),
					Model::numberOfEquations()> JacProj;

			//initialization
		protected:
			void checkMeasurementTimes(double t0) {
				Assert(measurements.size() > 0,
						"Adjoint::checkData(...): at least 1 data point, please");

				bool dataOrdered = true;
				int i = 0;
				while( dataOrdered && i < measurements.size() - 1 ) {
					if( measurements.time(i) >= measurements.time(i + 1) )
						dataOrdered = false;
					i++;
				}
				Assert(dataOrdered,
						"Adjoint::checkData(...): data should be chronologically ordered");
			}

			void initialize(const double t0, const int N) {
				const int nPar = Model::numberOfParameters();
				this->t0 = t0;
				checkMeasurementTimes(t0);
				integrator = new AdjointHessianIntegrator(ode, N);
				backwardIntegrator = integrator->createBackward(*this);
				grad.resize(nPar);
				u.resize(Model::numberOfEquations(), measurements.size());
				hesI = Matrixd::Zero(nPar, nPar);
				hesFO =  Matrixd::Zero(nPar, nPar);
				hesSO =  Matrixd::Zero(nPar, nPar);
				hessian =  Matrixd::Zero(nPar, nPar);
			}

		public:
			//Adjoint() {}

			AdjointHessian(const Vecd & psi, const Measurements & measurements,
					const LikelihoodModel & likelihoodModel, double t0 = 0.0, int N = 1000) :
					psi(psi), measurements(measurements), likelihoodModel(likelihoodModel) {
				initialize(t0, N);
				setParameters(psi);
			}

			// preferred constructor with parameters psi
			AdjointHessian(const Vecd & psi, const Measurements & measurements,
					const LikelihoodModel & likelihoodModel,
					ConstRefVecd & tau, double t0 = 0.0, int N = 1000) :
					psi(psi), measurements(measurements), likelihoodModel(likelihoodModel), tau(tau) {
				initialize(t0, N);
				setParameters(psi);
			}

			// preferred constructor without parameters psi
			AdjointHessian(const Measurements & measurements,
					const LikelihoodModel & likelihoodModel,
					ConstRefVecd & tau, double t0 = 0.0, int N = 1000) :
					measurements(measurements), likelihoodModel(likelihoodModel), tau(tau) {
				initialize(t0, N);
			}

			virtual ~AdjointHessian() {
				if (integrator != nullptr) {
					delete integrator;
					integrator = nullptr;
				}
			}

		public:
			virtual int numberOfEquations() const {
				return Model::numberOfEquations();
			}
			virtual void ddt(const double s, const RefVecd & u, const RefVecd & v, RefVecd vdot) {
				JacSol Jy;
				Model::ODE::jacobianState(s, u, psi, Jy);

				vdot = -Jy.transpose() * v;
			}

			virtual bool hasJacobian() const {
				return true;
			}
			virtual void jacobian(const double s, const RefVecd & u,
					const RefVecd & v, const RefVecd & vdot, RefMatrixd J) {
				JacSol Jy;
				Model::ODE::jacobianState(s, u, psi, Jy);

				J = -Jy.transpose();
			}

			virtual int numberOfQuadratureVars() const {
				return Model::numberOfParameters();
			}

			void updateFirstOrderHessian(int idx, const double t, const Vecd & u,
					const Matrixd & s, Vecd & grad, Matrixd & foHessian) {
				double ll = 0;
				const int nPar = Model::numberOfParameters();
				const int nEqs = Model::numberOfEquations();
				const int nObs = Model::numberOfObservations();
				Matrixd hes =  Matrixd::Zero(nPar, nPar);

				VecNd<nObs> uProj;
				MatrixMNd<nObs, nEqs> Jp;
				MatrixMNd<nEqs, nEqs> derP;
				MatrixMNd<nObs, nObs> Hproj;
				VecNd<nObs> gProj;
				VecNd<nEqs> g;
				MatrixMNd<nEqs, nEqs> H;
				VecNd<nEqs> tempState;

				Model::Projection::project(t, u, uProj);
				Model::Projection::jacobian(t, u, Jp);
				ll = likelihoodModel.logpdfHessU(tau, measurements, idx, uProj, gProj, Hproj);

				//<s_1, \delta J_u^*(P) \Sigma^{-1} J_u(P) s_2>
				//basically FI
				g = Jp.transpose() * gProj;
				H = Jp.transpose() * Hproj * Jp;
				for(int k = 0; k < nPar; k++) {
					grad[k] += g.dot(s.col(k)); // the gradient is relatively almost for free...
					for(int l = 0; l < k + 1; l++) {
						hes(k, l) += s.col(k).transpose() * H * s.col(l);
					}
				}

				//now the second order term of the projection
				//<\partial_{u,u}(P)(s_2,s_1), \delta\Sigma^{-1} (P(u)-y)>
				for(int j = 0; j < nObs; j++) { //runs through the observations
					Model::Projection::derStateState(j, t, u, derP);
					for(int k = 0; k < nPar; k++) {
						tempState = derP * s.col(k);
						for(int l = 0; l < k + 1; l++) {
							hes(k, l) += tempState.dot(s.col(l)) * gProj[j];
						}
					}
				}

				for(int k = 0; k < nPar; k++)
					for(int l = k + 1; l < nPar; l++)
						hes(k, l) = hes(l, k);

				foHessian = foHessian + hes;
			}

			void initialTimeTermsHessian(const Vecd & v, Matrixd & hesB) {
				const int nPar = Model::numberOfParameters();
				const int nEqs = Model::numberOfEquations();

				Matrixd derParParIC = Matrixd::Zero(nPar, nPar);
				hesB.setZero();
				for(int j = 0; j < nEqs; j++) { //runs through the r.h.s
					Model::ODE::derParParInitial(j, psi, derParParIC);
					for(int k = 0; k < nPar; k++) { //runs through the parameter dimension
						for(int l = 0; l < k + 1; l++) { //runs through the parameter dimension
							hesB(k, l) = hesB(k, l) + derParParIC(k, l) * v[j];
						}
					}
				}

				for(int k = 0; k < nPar; k++)
					for(int l = k + 1; l < nPar; l++)
						hesB(k, l) = hesB(l, k);
			}

			virtual void quadratureSens(const double t, const RefVecd & u,
					const RefMatrixd & s, const RefVecd & v, RefVecd Qdot) {

				const int nPar = Model::numberOfParameters();
				const int nEqs = Model::numberOfEquations();

				VecNd<nEqs> tempState;
				VecNd<nPar> tempPar;

				Matrixd hessian_psi = Matrixd::Zero(nPar, nPar);
				Matrixd derSolPar = Matrixd::Zero(nEqs, nPar);

				for(int j = 0; j < nEqs; j++) { //runs through the r.h.s
					Model::ODE::derStatePar(j, t, u, psi, derSolPar);
					for(int k = 0; k < nPar; k++) { //runs through the parameter dimension
						tempPar = derSolPar.transpose() * s.col(k);
						hessian_psi.col(k) = hessian_psi.col(k) + tempPar * v[j];
						hessian_psi.row(k) = hessian_psi.row(k)
								+ tempPar.transpose() * v[j];
					}
				}

				Matrixd derParPar = Matrixd::Zero(nPar, nPar);
				for(int j = 0; j < nEqs; j++) { //runs through the r.h.s
					Model::ODE::derParPar(j, t, u, psi, derParPar);
					for(int k = 0; k < nPar; k++) { //runs through the parameter dimension
						for(int l = 0; l < k + 1; l++) { //runs through the parameter dimension
							hessian_psi(k, l) = hessian_psi(k, l)
									+ derParPar(k, l) * v[j];
						}
					}
				}

				Matrixd derSolSol = Matrixd::Zero(nEqs, nEqs);
				double mult;
				for(int j = 0; j < nEqs; j++) { //runs through the r.h.s
					Model::ODE::derStateState(j, t, u, psi, derSolSol);
					for(int k = 0; k < nPar; k++) { //runs through the parameter dimension
						tempState = derSolSol * s.col(k);
						for(int l = 0; l < k + 1; l++) { //runs through the parameter dimension
							mult = s.col(l).dot(tempState);
							hessian_psi(k, l) = hessian_psi(k, l) + mult * v[j];
						}
					}
				}

				for(int k = 0; k < nPar; k++)
					for(int l = k + 1; l < nPar; l++)
						hessian_psi(k, l) = hessian_psi(l, k);

				//return the Hessian matrix as a vector
				for(int i = 0; i < nPar; i++)
					Qdot.segment(i * nPar, nPar) = hessian_psi.col(i);

			}

		public:
			void setParameters(const Vecd & psiExt) {
				psi = psiExt;
				ode.setParameters(psi);
				integrator->setIC();
			}

		protected:
			void solveForward() {
				const int nPar = Model::numberOfParameters();
				hesFO = Matrixd::Zero(nPar, nPar);
				grad = Vecd::Zero(nPar);

				int end = measurements.size();
				for(int i = 0; i < end; i++) {
					const double ty = measurements.time(i);
					integrator->integrateTo(ty);
					u.col(i) = integrator->currentState();
					sens = integrator->currentSensitivity();
					//terms for Hessian computation independent of backward problem
					// = first order part
					//it computes the gradient as well
					updateFirstOrderHessian(i, ty, u.col(i), sens, grad, hesFO);
				}
			}

			void solveBackward() {
				VecNd<Model::numberOfEquations()> delta;
				const int nPar = Model::numberOfParameters();

				int end = measurements.size() - 1;
				rhsDataTerm(end, u.col(end), delta);
				backwardIntegrator->reinit(measurements.time(end), delta);
				backwardIntegrator->reinitQuadrature();

				integrator->setupBackward(measurements.time(end));
				for(int i = end - 1; i >= 0; i--) {
					const double ty = measurements.time(i);
					integrator->integrateBackwardTo(ty);
					rhsDataTerm(i, u.col(i), delta);
					backwardIntegrator->reinit(ty,
							backwardIntegrator->currentState() + delta);
				}

				integrator->integrateBackwardTo(t0);

				// recover integrated terms and copy them to a matrix
				Vecd hesSOvec = backwardIntegrator->currentQuadrature(); //TODO: it copies
				for(int i = 0; i < nPar; i++){
					hesSO.col(i) = hesSOvec.segment(i * nPar, nPar);
				}

				initialTimeTermsHessian(backwardIntegrator->currentState(), hesI);

				Matrixd hessian_psi = Matrixd::Zero(nPar, nPar);
				hessian_psi = hesFO - hesSO + hesI;

				VecNd<nPar> dpsi_phi;
				VecNd<nPar> ddpsi_phi;
				VecNd<Model::numberOfParameters()> phi;
				Model::Transform::transphi(psi, phi);
				Model::Transform::dtranspsi(phi, dpsi_phi);
				Model::Transform::ddtranspsi(phi, ddpsi_phi);
				//TODO: how to do it eigenwise? cwiseProduct does not work as expected
				for(int i = 0; i < nPar; i++)
					hessian.col(i) = hessian_psi.col(i).cwiseProduct(dpsi_phi);
				for(int i = 0; i < nPar; i++)
					hessian.row(i) = (hessian.row(i).transpose()).cwiseProduct(
							dpsi_phi); //.transpose();
				hessian.diagonal() += grad.cwiseProduct(ddpsi_phi);
				//hessian = FI;
			}

		public:
			void getGrad(Vecd & gradExt) {
				gradExt = grad;
			}

		public:
			void getHessian(Matrixd & hessianExt) {
				hessianExt = hessian;
			}

		public:
			void solve() {
				solveForward();
				solveBackward();
			}

			void solve(const Vecd & psiExt) {
				setParameters(psiExt);
				solveForward();
				solveBackward();
			}

			void solve(const Vecd & psiExt, Vecd & gradExt) {
				solve(psiExt);
				getGrad(gradExt);
			}

		protected:

			template<typename UT, typename GT>
			void rhsDataTerm(int idx, const MatrixBase<UT> & u,
					MatrixBase<GT> const & grad_) {
				VecNd<Model::numberOfObservations()> yu;
				VecNd<Model::numberOfObservations()> gradLL;

				const double t = measurements.time(idx);
				Model::Projection::project(t, u, yu);
				likelihoodModel.logpdfGradU(tau, measurements, idx, yu, gradLL);

				JacProj Jp;
				Model::Projection::jacobian(t, u, Jp);

				MatrixBase < GT > &grad = const_cast<MatrixBase<GT>&>(grad_);
				grad = Jp.transpose() * gradLL;
			}

		protected:
			double t0;
			Vecd psi; // vector of parameters
			const Measurements & measurements;
			const LikelihoodModel & likelihoodModel;
			ConstRefVecd tau;
			Vecd grad;
			Matrixd u; // forward solution at the time points
			Matrixd sens; // sensitivity solution at one time point
			ODE<Model> ode;
			AdjointHessianIntegrator* integrator;
			BackwardHessianIntegrator *backwardIntegrator;
			Matrixd hesI; // initial terms of the likelihood Hessian
			Matrixd hesFO; // first order terms of the likelihood Hessian
			Matrixd hesSO; // second order terms of the likelihood Hessian
			Matrixd hessian; // the full Hessian
	};

}
#endif
