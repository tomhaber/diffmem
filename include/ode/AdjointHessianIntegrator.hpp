/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ADJOINTHESSIANINTEGRATOR_H
#define ADJOINTHESSIANINTEGRATOR_H

#include <ode/CVodeIntegratorBase.hpp>
#include <vector>

namespace ode {

	class BackwardHessianIntegrator;
	class DIFFMEM_EXPORT AdjointHessianIntegrator : public CVodeIntegratorBase {
		public:
			enum Interpolation { Hermite, Polynomial };
			AdjointHessianIntegrator(SensitivityODEBase & sensProblem, int Nd = 100, Interpolation interpolation = Hermite);
			~AdjointHessianIntegrator();

		public:
			void integrateForwardDt( const double dt ) {
				integrateTo( dt + currentTime() );
			}

			void integrateTo( const double tOut );
			void integrateBackwardTo( const double tOut );

		public:
			BackwardHessianIntegrator *createBackward(AdjointSensitivityODEBase & adjProblem);
			void clearBackward();
			void resetBackward();

			void setupBackward();
			void setupBackward(const double time);

		public:
			void setIC(const double initialTime = 0.0);
			void reinit(const double initialTime, const Vecd & initialState, const Matrixd & initialSensivity);

		public:
			const Matrixd & currentSensitivity() const;

		private:
			void initialize();
			void initializeSensitivity();
			void initializeAdjoint();
			void clearAdjoint();
			void cvodeReInit();

		private:
			SensitivityODEBase & sensProblem_; // contains forward problem as well
			Matrixd currentSens_;
			N_Vector *nvCurrentSens_;

			enum Interpolation interpolation;
			int Nnc_; // number of integration steps between two consecutive checkpoints
			int numOfCheckpoints_;

			double backwardTime_;
			std::vector<BackwardHessianIntegrator *> backwardProblems_;
	};

	class DIFFMEM_EXPORT BackwardHessianIntegrator {
		public:
			void setTolerances( double relTol, double absTol );
			void setRelativeTolerance( double relTol );
			void setAbsoluteTolerance( double absTol );
			double relativeTolerance() const;
			double absoluteTolerance() const;

			void setTolerancesQuadrature( double relTol, double absTol );
			void setRelativeToleranceQuadrature( double relTol );
			void setAbsoluteToleranceQuadrature( double absTol );
			double relativeToleranceQuadrature() const;
			double absoluteToleranceQuadrature() const;

		public:
			double currentTime() const;
			const Vecd & currentState() const;
			const Vecd & currentQuadrature() const;

		public:
			void setIC(const double initialTime = 0.0);
			void reinit(const double initialTime, const Vecd & initialState);
			void reinitQuadrature();

		private:
			void initialize();
			void cvodeReInit();
			double update();

		private:
			void *cvodeMem_;
			int identity_;

			AdjointODEBase & adjProblem_;
			N_Vector nvCurrentState_;
			N_Vector nvCurrentQ_;

			double relTol_, absTol_;
			double relTolQ_, absTolQ_;
			double currentTime_;
			Vecd currentState_;
			Vecd currentQ_;

			int maxNumSteps_;

		private:
			BackwardHessianIntegrator(AdjointODEBase & adjProblem, void *cvodeMem, int identity);
			~BackwardHessianIntegrator();

		friend class AdjointHessianIntegrator;
	};

	inline double BackwardHessianIntegrator::currentTime() const {
		return currentTime_;
	}

	inline const Vecd & BackwardHessianIntegrator::currentState() const {
		return currentState_;
	}

	inline const Vecd & BackwardHessianIntegrator::currentQuadrature() const {
		return currentQ_;
	}

	inline const Matrixd & AdjointHessianIntegrator::currentSensitivity() const {
		return currentSens_;
	}

	inline void BackwardHessianIntegrator::setRelativeTolerance( double relTol ) {
		setTolerances(relTol, absTol_);
	}

	inline void BackwardHessianIntegrator::setAbsoluteTolerance( double absTol ) {
		setTolerances(relTol_, absTol);
	}

	inline double BackwardHessianIntegrator::relativeTolerance() const {
		return relTol_;
	}

	inline double BackwardHessianIntegrator::absoluteTolerance() const {
		return absTol_;
	}

	inline void BackwardHessianIntegrator::setRelativeToleranceQuadrature( double relTol ) {
		setTolerancesQuadrature(relTol, absTolQ_);
	}

	inline void BackwardHessianIntegrator::setAbsoluteToleranceQuadrature( double absTol ) {
		setTolerancesQuadrature(relTolQ_, absTol);
	}

	inline double BackwardHessianIntegrator::relativeToleranceQuadrature() const {
		return relTolQ_;
	}

	inline double BackwardHessianIntegrator::absoluteToleranceQuadrature() const {
		return absTolQ_;
	}


	inline void AdjointHessianIntegrator::clearBackward() {
		clearAdjoint();
	}

	inline void AdjointHessianIntegrator::resetBackward() {
		clearAdjoint();
		initializeAdjoint();
	}

}
#endif

