/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SERIALIZATION_H
#define SERIALIZATION_H

#include <cereal/cereal.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/binary.hpp>
#include <MatrixVector.hpp>
#include <apply.hpp>
#include <function_traits.hpp>
#include <tuple>

namespace serialization {

	class SerializationException : public cereal::Exception {
	public:
		SerializationException(const std::string &msg) : cereal::Exception(msg) {}
		SerializationException(const char *msg) : cereal::Exception(msg) {}
	};

	typedef std::vector<uint8_t> Buffer;

	class BufferOutputArchive : public cereal::OutputArchive<BufferOutputArchive> {
	public:
		BufferOutputArchive(Buffer &buffer)
				: cereal::OutputArchive<BufferOutputArchive>(this),
				position(0),
				buffer(buffer) {}

		BufferOutputArchive(const BufferOutputArchive &other)
			: cereal::OutputArchive<BufferOutputArchive>(this)
			, position(other.position)
			, buffer(other.buffer)
			{ }

		~BufferOutputArchive() noexcept = default;

		void saveBinary(const void *data, std::size_t size) {
			if(position + size > buffer.size())
				buffer.resize(position + size);

			memcpy(&buffer[position], data, size);
			position += size;
		}

	private:
		size_t position;
		Buffer &buffer;
	};

	class BufferInputArchive : public cereal::InputArchive<BufferInputArchive> {
	public:
		BufferInputArchive(const Buffer &buffer)
				: cereal::InputArchive<BufferInputArchive>(this),
				position(0),
				buffer(buffer) {}

		BufferInputArchive(const BufferInputArchive &other)
			: cereal::InputArchive<BufferInputArchive>(this)
			, position(other.position)
			, buffer(other.buffer)
			{ }

		~BufferInputArchive() noexcept = default;

		void loadBinary(void *const data, std::size_t size) {
			if(position + size > buffer.size())
				throw SerializationException("Failed to read " + std::to_string(size) +
					" bytes from input buffer of size " + std::to_string(buffer.size()) +
					" at " + std::to_string(position));

			memcpy(data, &buffer[position], size);
			position += size;
		}

		bool isDone() const { return position == buffer.size(); }

	private:
		size_t position;
		const Buffer &buffer;
	};

	//! Saving for POD types to buffer
	template<class T>
	inline typename std::enable_if<std::is_arithmetic<T>::value, void>::type
	CEREAL_SAVE_FUNCTION_NAME(BufferOutputArchive & ar, T const & t) {
		ar.saveBinary(std::addressof(t), sizeof(t));
	}

	//! Loading for POD types from buffer
	template<class T>
	inline typename std::enable_if<std::is_arithmetic<T>::value, void>::type
	CEREAL_LOAD_FUNCTION_NAME(BufferInputArchive & ar, T & t) {
		ar.loadBinary(std::addressof(t), sizeof(t));
	}

	//! Serializing NVP types to buffer
	template <class Archive, class T>
	inline CEREAL_ARCHIVE_RESTRICT(BufferInputArchive, BufferOutputArchive)
	CEREAL_SERIALIZE_FUNCTION_NAME( Archive & ar, cereal::NameValuePair<T> & t ) {
		ar(t.value);
	}

	//! Serializing SizeTags to buffer
	template <class Archive, class T>
	inline CEREAL_ARCHIVE_RESTRICT(BufferInputArchive, BufferOutputArchive)
	CEREAL_SERIALIZE_FUNCTION_NAME( Archive & ar, cereal::SizeTag<T> & t ) {
		ar(t.size);
	}

	//! Saving binary data
	template <class T>
	inline void CEREAL_SAVE_FUNCTION_NAME(BufferOutputArchive & ar, cereal::BinaryData<T> const & bd) {
		ar.saveBinary(bd.data, static_cast<std::size_t>(bd.size));
	}

	//! Loading binary data
	template <class T>
	inline void CEREAL_LOAD_FUNCTION_NAME(BufferInputArchive & ar, cereal::BinaryData<T> & bd) {
		ar.loadBinary(bd.data, static_cast<std::size_t>(bd.size));
	}

	namespace detail {
		template<typename T, typename F>
		void for_each_element(T &&t, F &&f, std::index_sequence<>) {
		}

		template<typename T, typename F, size_t... Is>
		void for_each_element(T &&t, F &&f, std::index_sequence<Is...>) {
			auto l = { (f(std::get<Is>(t)), 0)... };
			UNUSED(l);
		}

		template <typename Archive, typename Tuple>
		void unpack(Archive &ar, Tuple &t) {
			for_each_element(t, ar, std::make_index_sequence<std::tuple_size<Tuple>{}>{});
		}

		template <typename F, typename Archive, typename R, typename... Args>
		auto call(F &&f, Archive &ar, arg_result<R, Args...>) {
			std::tuple<std::remove_cv_t<std::remove_reference_t<Args>>...> args;
			detail::unpack(ar, args);
			return apply(std::forward<F>(f), args);
		}
	}

	template <typename F, typename Archive>
	auto call(F &&f, Archive &ar) {
		return detail::call(std::forward<F>(f), ar, typename function_traits<F>::argResult{});
	}

	template <typename Archive, typename Arg>
	void pack(Archive &ar, Arg &&arg) {
		ar(std::forward<Arg>(arg));
	}

	template <typename Archive, typename Arg, typename... Args>
	void pack(Archive &ar, Arg &&arg, Args &&... args) {
		ar(std::forward<Arg>(arg));
		pack(ar, std::forward<Args>(args)...);
	}

	template <typename Archive>
	void unpack(Archive &ar) {
	}

	template <typename Archive, typename Arg, typename... Args>
	void unpack(Archive &ar, Arg &&arg, Args &&... args) {
		ar(std::forward<Arg>(arg));
		unpack(ar, std::forward<Args>(args)...);
	}
}

// tie input and output archives together
CEREAL_SETUP_ARCHIVE_TRAITS(serialization::BufferInputArchive, serialization::BufferOutputArchive);

namespace Eigen {
	template<typename Archive, typename Scalar, int Rows, int Cols, int Options, int MaxRows, int MaxCols>
	typename std::enable_if<cereal::traits::is_output_serializable<cereal::BinaryData<Scalar>, Archive>::value, void>::type
	CEREAL_SAVE_FUNCTION_NAME(Archive &ar, const Matrix<Scalar, Rows, Cols, Options, MaxRows, MaxCols> & m) {
		ar(cereal::make_size_tag(static_cast<cereal::size_type>(m.rows())));
		ar(cereal::make_size_tag(static_cast<cereal::size_type>(m.cols())));
		ar(cereal::binary_data(m.data(), m.size() * sizeof(Scalar)));
	}

	template<typename Archive, typename Scalar, int Rows, int Cols, int Options, int MaxRows, int MaxCols>
	typename std::enable_if<cereal::traits::is_output_serializable<cereal::BinaryData<Scalar>, Archive>::value, void>::type
	CEREAL_SAVE_FUNCTION_NAME(Archive &ar, const Ref<const Matrix<Scalar, Rows, Cols, Options, MaxRows, MaxCols>> & m) {
		ar(cereal::make_size_tag(static_cast<cereal::size_type>(m.rows())));
		ar(cereal::make_size_tag(static_cast<cereal::size_type>(m.cols())));
		ar(cereal::binary_data(m.data(), m.size() * sizeof(Scalar)));
	}

	template<typename Archive, typename Scalar, int Rows, int Cols, int Options, int MaxRows, int MaxCols>
	typename std::enable_if<cereal::traits::is_output_serializable<cereal::BinaryData<Scalar>, Archive>::value, void>::type
	CEREAL_SAVE_FUNCTION_NAME(Archive &ar, const Map<const Matrix<Scalar, Rows, Cols, Options, MaxRows, MaxCols>> & m) {
		ar(cereal::make_size_tag(static_cast<cereal::size_type>(m.rows())));
		ar(cereal::make_size_tag(static_cast<cereal::size_type>(m.cols())));
		ar(cereal::binary_data(m.data(), m.size() * sizeof(Scalar)));
	}

	template<typename Archive, typename Scalar, int Rows, int Cols, int Options, int MaxRows, int MaxCols>
	typename std::enable_if<cereal::traits::is_input_serializable<cereal::BinaryData<Scalar>, Archive>::value, void>::type
	CEREAL_LOAD_FUNCTION_NAME(Archive &ar, Matrix<Scalar, Rows, Cols, Options, MaxRows, MaxCols> & m) {
		cereal::size_type rows, cols;
		ar(cereal::make_size_tag(rows));
		ar(cereal::make_size_tag(cols));
		m.resize(rows, cols);
		ar(cereal::binary_data(m.data(), static_cast<std::size_t>(m.size()) * sizeof(Scalar)));
	}

	template<typename Archive, typename Scalar, int Rows, int Cols, int Options, int MaxRows, int MaxCols>
	typename std::enable_if<cereal::traits::is_input_serializable<cereal::BinaryData<Scalar>, Archive>::value, void>::type
	CEREAL_LOAD_FUNCTION_NAME(Archive &ar, Ref<Matrix<Scalar, Rows, Cols, Options, MaxRows, MaxCols>> & m) {
		cereal::size_type rows, cols;
		ar(cereal::make_size_tag(rows));
		ar(cereal::make_size_tag(cols));
		m.resize(rows, cols);
		ar(cereal::binary_data(m.data(), static_cast<std::size_t>(m.size()) * sizeof(Scalar)));
	}
}
#endif
