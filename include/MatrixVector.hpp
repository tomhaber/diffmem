/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MATRIXVECTOR_H
#define MATRIXVECTOR_H

#include <common.hpp>
#include <Error.hpp>
#include <cmath>

#define EIGEN_DEFAULT_DENSE_INDEX_TYPE int
#define EIGEN_DONT_PARALLELIZE
#include <Eigen/Dense>

using Eigen::MatrixBase;
using Eigen::ArrayBase;
using Eigen::PlainObjectBase;

template <typename T, int N> using VecN = Eigen::Matrix<T, N, 1>;
template <int N> using VecNd = Eigen::Matrix<double, N, 1>;
template <int N> using VecNb = Eigen::Array<bool, N, 1>;

template <typename T, int M, int N> using MatrixMN = Eigen::Matrix<T, M, N>;
template <int M, int N> using MatrixMNd = Eigen::Matrix<double, M, N>;
template <int M, int N> using MatrixMNb = Eigen::Array<bool, M, N>;

template <typename T> using Vec = Eigen::Matrix<T, Eigen::Dynamic, 1>;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> Vecd;
typedef Eigen::Array<bool, Eigen::Dynamic, 1> Vecb;
typedef Eigen::Matrix<int, Eigen::Dynamic, 1> Veci;

template <typename T> using Matrix = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;
typedef Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> Matrixd;
typedef Eigen::Array<bool, Eigen::Dynamic, Eigen::Dynamic> Matrixb;
typedef Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic> Matrixi;

typedef Eigen::DiagonalMatrix<double, Eigen::Dynamic> DiagonalMatrixd;
template <int N> using DiagonalMatrixNd = Eigen::DiagonalMatrix<double, N>;

template <typename T> using MapVec = Eigen::Map< Eigen::Matrix<T, Eigen::Dynamic, 1> >;
typedef Eigen::Map< Eigen::Matrix<double, Eigen::Dynamic, 1> > MapVecd;
typedef Eigen::Map< Eigen::Matrix<int, Eigen::Dynamic, 1> > MapVeci;
typedef Eigen::Map< const Eigen::Matrix<double, Eigen::Dynamic, 1> > ConstMapVecd;
typedef Eigen::Map< const Eigen::Matrix<int, Eigen::Dynamic, 1> > ConstMapVeci;

template <typename T> using MapMatrix = Eigen::Map< Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> >;
typedef Eigen::Map< Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> > MapMatrixd;
typedef Eigen::Map< Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic> > MapMatrixi;
typedef Eigen::Map< const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> > ConstMapMatrixd;
typedef Eigen::Map< const Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic> > ConstMapMatrixi;

template <typename T, int N> using MapVecN = Eigen::Map< Eigen::Matrix<T, N, 1> >;
template <int N> using MapVecNd = Eigen::Map< Eigen::Matrix<double, N, 1> >;

template <typename T, int M, int N> using MapMatrixMN = Eigen::Map< Eigen::Matrix<T, M, N> >;
template <int M, int N> using MapMatrixMNd = Eigen::Map< Eigen::Matrix<double, M, N> >;

template <typename T> using RefVec = Eigen::Ref< Eigen::Matrix<T, Eigen::Dynamic, 1> >;
typedef Eigen::Ref< Eigen::Matrix<double, Eigen::Dynamic, 1> > RefVecd;
typedef const Eigen::Ref< const Eigen::Matrix<double, Eigen::Dynamic, 1> > ConstRefVecd;
typedef const Eigen::Ref< const Eigen::Matrix<int, Eigen::Dynamic, 1> > ConstRefVeci;
typedef const Eigen::Ref< const Eigen::Matrix<bool, Eigen::Dynamic, 1> > ConstRefVecb;

template <typename T> using RefMatrix = Eigen::Ref< Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> >;
typedef Eigen::Ref< Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> > RefMatrixd;
typedef const Eigen::Ref< const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> > ConstRefMatrixd;
typedef const Eigen::Ref< const Eigen::Matrix<int, Eigen::Dynamic, Eigen::Dynamic> > ConstRefMatrixi;
typedef const Eigen::Ref< const Eigen::Matrix<bool, Eigen::Dynamic, Eigen::Dynamic> > ConstRefMatrixb;

typedef Eigen::LLT<Matrixd> LLTd;

namespace Eigen {
	template <typename Derived>
	const typename PlainObjectBase<Derived>::Scalar *begin(const PlainObjectBase<Derived> & m) {
		return m.data();
	}

	template <typename Derived>
	const typename PlainObjectBase<Derived>::Scalar *end(const PlainObjectBase<Derived> & m) {
		return m.data() + m.size();
	}

	template <typename Derived>
	typename PlainObjectBase<Derived>::Scalar *begin(PlainObjectBase<Derived> & m) {
		return m.data();
	}

	template <typename Derived>
	typename PlainObjectBase<Derived>::Scalar *end(PlainObjectBase<Derived> & m) {
		return m.data() + m.size();
	}
}

template <typename T1, typename T2>
double dot(const MatrixBase<T1> & x, const MatrixBase<T2> & y) {
	return x.dot(y);
}

template <typename T>
void chol_inv(const LLTd & omega, MatrixBase<T> & omega_inv) {
	omega_inv = T::Identity(omega.rows(), omega.cols());
	omega.solveInPlace(omega_inv);
}

template <typename T>
double determinant(const MatrixBase<T> & omega) {
	return omega.determinant();
}

template <typename T>
double logDeterminant(const MatrixBase<T> & omega) {
	return std::log(omega.determinant());
}

inline double determinant(const LLTd & omega) {
	const double det = omega.matrixL().determinant();
	return det*det;
}

inline double determinant(const Vecd & omega) {
	return omega.prod();
}

inline double logDeterminant(const LLTd & omega) {
	return 2.0*std::log( omega.matrixL().determinant() );
}

inline double logDeterminant(const DiagonalMatrixd & omega) {
	return std::log(omega.diagonal().prod());
}

template <typename T, typename F>
bool any(const MatrixBase<T> & x, F && f) {
	return x.unaryExpr(std::forward<F>(f)).any();
}

inline int triangularElementsFromDim(int n) {
	return n + (n*(n-1))/2;
}

inline int dimFromTriangularElements(int nelems) {
  return static_cast<int>(std::floor((std::sqrt(8 * nelems + 1) - 1) / 2));
}

namespace internal {
	template <typename MT, typename VT, int UpLo>
	struct tri2vecImpl;

	template <typename MT, typename VT>
	struct tri2vecImpl<MT, VT, Eigen::Lower> {
		static void run(const MatrixBase<MT> & mat, MatrixBase<VT> & v) {
			const int n = mat.rows();

			size_t idx = 0;
			for(int j = 0; j < n; j++) {
				for(int i = j; i < n; i++)
					v[idx++] = mat(i, j);
			}
		}
	};

	template <typename MT, typename VT>
	struct tri2vecImpl<MT, VT, Eigen::Upper> {
		static void run(const MatrixBase<MT> & mat, MatrixBase<VT> & v) {
			const int n = mat.rows();

			size_t idx = 0;
			for(int j = 0; j < n; j++) {
				for(int i = 0; i <= j; i++)
					v[idx++] = mat(i, j);
			}
		}
	};

	template <typename VT, typename MT, int UpLo>
	struct vec2triImpl;

	template <typename VT, typename MT>
	struct vec2triImpl<VT, MT, Eigen::Lower> {
		static void run(const MatrixBase<VT> & v, MatrixBase<MT> & mat) {
			const int n = mat.rows();

			size_t idx = 0;
			for(int j = 0; j < n; j++) {
				for(int i = j; i < n; i++)
					mat(i, j) = v[idx++];
			}
		}
	};

	template <typename VT, typename MT>
	struct vec2triImpl<VT, MT, Eigen::Upper> {
		static void run(const MatrixBase<VT> & v, MatrixBase<MT> & mat) {
			const int n = mat.rows();

			size_t idx = 0;
			for(int j = 0; j < n; j++) {
				for(int i = 0; i <= j; i++)
					mat(i, j) = v[idx++];
			}
		}
	};
}

template <int UpLo, typename MT, typename VT>
void tri2vec(const MatrixBase<MT> & mat, MatrixBase<VT> & v) {
	const int nelems = triangularElementsFromDim(mat.rows());
	v.resize( nelems );
	internal::tri2vecImpl<MT, VT, UpLo>::run(mat, v);
}

template <int UpLo, typename VT, typename MT>
void vec2tri(const MatrixBase<VT> & v, MatrixBase<MT> & mat) {
	const int n = dimFromTriangularElements(v.size());
	mat.resize(n,n);
	internal::vec2triImpl<VT, MT, UpLo>::run(v, mat);
}

template <int UpLo, typename MT, typename VT>
void tri2vec(const MatrixBase<MT> & mat, PlainObjectBase<VT> & v) {
	const int nelems = triangularElementsFromDim(mat.rows());
	v.resize( nelems );
	internal::tri2vecImpl<MT, VT, UpLo>::run(mat, v);
}

template <int UpLo, typename VT, typename MT>
void vec2tri(const MatrixBase<VT> & v, PlainObjectBase<MT> & mat) {
	const int n = dimFromTriangularElements(v.size());
	mat.resize(n,n);
	internal::vec2triImpl<VT, MT, UpLo>::run(v, mat);
}

// Y = X(mask, :)
template <typename T, typename MaskT, typename YT>
void sliceRows(const MatrixBase<T> & X, const ArrayBase<MaskT> & mask, PlainObjectBase<YT> & Y) {
	Assert( X.rows() == mask.size(), "Incorrect row dimensions" );

	const int DIM = mask.count();
	Y.resize( DIM, X.cols() );

	for(int j = 0; j < X.cols(); j++) {
		int yi = 0;
		for(int i = 0; i < X.rows(); i++) {
			if( mask[i] ) {
				Y(yi, j) = X(i, j);
				yi++;
			}
		}
	}
}

// Y(mask, :) = X
template <typename T, typename MaskT, typename YT>
void sliceIntoRows(const MatrixBase<T> & X, const ArrayBase<MaskT> & mask, const MatrixBase<YT> & Y_) {
	MatrixBase<YT> & Y = const_cast< MatrixBase<YT>& >(Y_);
	Assert( X.rows() == mask.count(), "Incorrect X row dimensions" );
	Assert( Y.rows() == mask.size(), "Incorrect Y row dimensions" );
	Assert( Y.cols() == X.cols(), "Incorrect Y column dimensions" );

	for(int j = 0; j < Y.cols(); j++) {
		int yi = 0;
		for(int i = 0; i < Y.rows(); i++) {
			if( mask[i] ) {
				Y(i, j) = X(yi, j);
				yi++;
			}
		}
	}
}

// Y = X(:, mask)
template <typename T, typename MaskT, typename YT>
void sliceCols(const MatrixBase<T> & X, const ArrayBase<MaskT> & mask, PlainObjectBase<YT> & Y) {
	Assert( X.cols() == mask.size(), "Incorrect row dimensions" );

	const int DIM = mask.count();
	Y.resize( X.rows(), DIM );

	int yj = 0;
	for(int j = 0; j < X.cols(); j++) {
		if( mask[j] ) {
			for(int i = 0; i < X.rows(); i++)
				Y(i, yj) = X(i, j);
			yj++;
		}
	}
}

// Y(:, mask) = X
template <typename T, typename MaskT, typename YT>
void sliceIntoCols(const MatrixBase<T> & X, const ArrayBase<MaskT> & mask, const MatrixBase<YT> & Y_) {
	MatrixBase<YT> & Y = const_cast< MatrixBase<YT>& >(Y_);
	Assert( X.cols() == mask.count(), "Incorrect row dimensions" );
	Assert( Y.rows() == X.rows(), "Incorrect Y row dimensions" );
	Assert( Y.cols() == mask.size(), "Incorrect Y column dimensions" );

	int yj = 0;
	for(int j = 0; j < Y.cols(); j++) {
		if( mask[j] ) {
			for(int i = 0; i < Y.rows(); i++)
				Y(i, j) = X(i, yj);
			yj++;
		}
	}
}

// Y = X(mask, mask)
template <typename T, typename MaskT, typename YT>
void slice(const MatrixBase<T> & X, const ArrayBase<MaskT> & mask, PlainObjectBase<YT> & Y) {
	Assert( IMPLIES(X.cols() != 1, X.cols() == mask.size()), "Incorrect column dimensions" );
	Assert( IMPLIES(X.rows() != 1, X.rows() == mask.size()), "Incorrect row dimensions" );

	if( X.cols() == 1 ) {
		sliceRows(X, mask, Y);
		return;
	}

	if( X.rows() == 1 ) {
		sliceCols(X, mask, Y);
		return;
	}

	const int DIM = mask.count();
	Y.resize(DIM, DIM);

	int yj = 0;
	for(int j = 0; j < X.cols(); j++) {
		if( mask[j] ) {
			int yi = 0;
			for(int i = 0; i < X.rows(); i++) {
				if( mask[i] ) {
					Y(yi, yj) = X(i, j);
					yi++;
				}
			}
			yj++;
		}
	}
}

// Y(mask, mask) = X
template <typename T, typename MaskT, typename YT>
void sliceInto(const MatrixBase<T> & X, const ArrayBase<MaskT> & mask, const MatrixBase<YT> & Y_) {
	MatrixBase<YT> & Y = const_cast< MatrixBase<YT>& >(Y_);
	Assert( IMPLIES(X.cols() != 1, X.cols() == mask.count()), "Incorrect column dimensions" );
	Assert( IMPLIES(X.rows() != 1, X.rows() == mask.count()), "Incorrect row dimensions" );
	Assert( IMPLIES(X.cols() != 1, Y.cols() == mask.size()) &&
			IMPLIES(X.cols() == 1, Y.cols() == 1), "Incorrect Y column dimensions" );
	Assert( IMPLIES(X.rows() != 1, Y.rows() == mask.size()) &&
			IMPLIES(X.rows() == 1, Y.rows() == 1), "Incorrect Y row dimensions" );

	if( X.cols() == 1 ) {
		sliceIntoRows(X, mask, Y);
		return;
	}
	if( X.rows() == 1 ) {
		sliceIntoCols(X, mask, Y);
		return;
	}

	const int DIM = mask.size();
	Y.resize(DIM, DIM);

	int yj = 0;
	for(int j = 0; j < Y.cols(); j++) {
		if( mask[j] ) {
			int yi = 0;
			for(int i = 0; i < Y.rows(); i++) {
				if( mask[i] ) {
					Y(i, j) = X(yi, yj);
					yi++;
				}
			}
			yj++;
		}
	}
}

template <typename T, int Rows=Eigen::Dynamic>
Eigen::Matrix<T, Rows, 1> make_vector(
		std::initializer_list<T> initlist) {
	Eigen::Matrix<T, Rows, 1> v(initlist.size());

	int i = 0;
	for(auto x : initlist) {
		v(i) = T(x);
		i++;
	}
	return v;
}

template <typename T, int Rows=Eigen::Dynamic, int Cols=Eigen::Dynamic>
Eigen::Matrix<T, Rows, Cols> make_matrix(
		std::initializer_list<std::initializer_list<T>> initlist) {
	auto rows = initlist.size();
	auto cols = initlist.begin()->size();
	Eigen::Matrix<T, Rows, Cols> m(rows, cols);

	int i = 0;
	for(auto x : initlist) {
		eigen_assert(x.size() == cols);

		int j = 0;
		for(auto y : x) {
			m(i, j) = T(y);
			j++;
		}
		i++;
	}
	return m;
}

#include <Cholesky.hpp>
#endif
