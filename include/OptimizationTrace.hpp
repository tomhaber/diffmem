/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef OPTIMIZATIONTRACE_H_
#define OPTIMIZATIONTRACE_H_

#include <functional>
#include <math/number.hpp>

enum class OptimizationState {
	Init, //  The algorithm is in the initial state before the first iteration.
	Iter, // The algorithm is at the end of an iteration.
	Interrupt, // The algorithm is performing an iteration. In this state, the output function can interrupt the current iteration of the optimization.
	Done // The algorithm is in the final state after the last iteration.
};

struct OptimizationValues {
	OptimizationValues(size_t iteration, ConstRefVecd & x, double value)
		: iteration(iteration), x(x), value(value), gradNorm(math::NaN()) {}
	OptimizationValues(size_t iteration, ConstRefVecd & x, double value, double gradNorm)
		: iteration(iteration), x(x), value(value), gradNorm(gradNorm) {}

	size_t iteration;
	ConstRefVecd x;
	double value;
	double gradNorm;
};

typedef std::function<bool(OptimizationState, OptimizationValues)> OptimizationTraceCallback;

class MEModel;
typedef std::function<bool(OptimizationState, size_t, const MEModel &)> MEOptimizationTraceCallback;
#endif
