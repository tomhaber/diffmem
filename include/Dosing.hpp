/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DOSING_H
#define DOSING_H

#include <vector>
#include <math/number.hpp>
#include <Error.hpp>

class DIFFMEM_EXPORT Dosing {
	struct Event {
		Event(double time, double amount, int compartment)
			: time(time), amount(amount), compartment(compartment) {}
		bool operator <(const Event & e) const { return (time <= e.time); }

		double time;
		double amount;
		int compartment;
	};

	public:
		void addEvent(double time, double amount, int compartment = 0);
		void addEvent(double start, double amount, int additional, double rate, int compartment = 0);

	private:
		std::vector<Event> events;
		friend class DosingIterator;
};

class DIFFMEM_EXPORT DosingIterator {
	public:
		DosingIterator(const Dosing & scheme) : events(scheme.events) {
			it = events.begin();
		}

	public:
		double nextEventTime();
		void popNextEvent(double & amount, int & compartment);
		double amount() const;
		int compartment() const;
		void proceedToNext();

	private:
		const std::vector<Dosing::Event> & events;
		std::vector<Dosing::Event>::const_iterator it;
};

inline double DosingIterator::amount() const {
	const Dosing::Event & event = *it;
	return event.amount;
}

inline int DosingIterator::compartment() const {
	const Dosing::Event & event = *it;
	return event.compartment;
}

inline void DosingIterator::proceedToNext() {
	if( it == events.end() )
		throw std::runtime_error("no dosing events left");

	it++;
}

inline void DosingIterator::popNextEvent(double & amount, int & compartment) {
	if( it == events.end() )
		throw std::runtime_error("no dosing events left");

	const Dosing::Event & event = *it;
	amount = event.amount;
	compartment = event.compartment;

	it++;
}

inline double DosingIterator::nextEventTime() {
	if( it == events.end() )
		return math::posInf();

	const Dosing::Event & event = *it;
	return event.time;
}
#endif
