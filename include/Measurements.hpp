/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MEASUREMENTS_H
#define MEASUREMENTS_H

#include <MatrixVector.hpp>
#include <math/number.hpp>

class DIFFMEM_EXPORT Measurements {
	public:
		Measurements() {}

		template <typename TT, typename VT>
		Measurements(const MatrixBase<TT> & time, const MatrixBase<VT> & values) : t(time), y(values) {
			Require( time.size() == values.cols(), "number of time points should match values" );
			initialize();
		}

		template <typename TT, typename VT, typename WT>
		Measurements(const MatrixBase<TT> & time, const MatrixBase<VT> & values, const MatrixBase<WT> & weights)
			: t(time), y(values), w(weights) {
			Require( time.size() == values.cols(), "number of time points should match values" );
		}

	public:
		template <typename TT, typename VT>
		void init(const MatrixBase<TT> & time, const MatrixBase<VT> & values) {
			Require( time.size() == values.cols(), "number of time points should match values" );
			t = time;
			y = values;
			initialize();
		}

	public:
		const Vecd & time() const { return t; }
		const Matrixd & values() const { return y; }
		double norm() const { return (w*y).norm(); }

		int size() const { return y.cols(); }
		int dim() const { return y.rows(); }

		double time(int i) const { return t[i]; }
		Matrixd::ConstColXpr value(int i) const { return y.col(i); }
		Matrixd::ConstColXpr weight(int i) const { return w.col(i); }

	private:
		void initialize() {
			w.setOnes( y.rows(), y.cols() );
			for(int i = 0; i < y.size(); ++i) {
				if( math::isNA( y(i) ) ) {
					y(i) = 0.0;
					w(i) = 0.0;
				}
			}
		}

	private:
		Vecd t;
		Matrixd y, w;
};
#endif
