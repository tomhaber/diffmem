/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef INDIVIDUALLIKELIHOODETA_H_
#define INDIVIDUALLIKELIHOODETA_H_

#include <Individual.hpp>
#include <LocalMatrix.hpp>
#include <Distribution.hpp>

class DIFFMEM_EXPORT IndividualLikelihoodEta : public Distribution {
	public:
		template <typename BT, typename ST>
		IndividualLikelihoodEta(const Individual & subject, const MatrixBase<BT> & beta, const MatrixBase<ST> & tau)
			: subject(subject), tau(tau) {

			setBeta(beta);
		}

	public:
		template <typename BT>
		void setBeta(const MatrixBase<BT> & beta) {
			subject.toParameter(beta, mu);
		}

		template <typename ST>
		void setTau(const MatrixBase<ST> & tau) {
			this->tau = tau;
		}

	public:
		int numberOfDimensions() const {
			return subject.numberOfRandom();
		}

	public:
		double logpdf(ConstRefVecd & eta) const {
			LOCAL_VECTOR(Vecd, phi, mu.size());
			phi = mu + subject.randomToPhiXf() * eta;
			return subject.logpdf(phi, tau);
		}

		double logpdf(ConstRefVecd & eta, RefVecd grad) const {
			LOCAL_VECTOR(Vecd, phi, mu.size());
			phi = mu + subject.randomToPhiXf() * eta;

			LOCAL_VECTOR(Vecd, grad_phi, mu.size());

			double ll = subject.logpdf(phi, tau, grad_phi);
			grad = grad_phi.transpose() * subject.randomToPhiXf();
			return ll;
		}

		double logpdf(ConstRefVecd & eta, RefVecd grad, RefMatrixd H) const {
			LOCAL_VECTOR(Vecd, phi, mu.size());
			phi = mu + subject.randomToPhiXf() * eta;

			LOCAL_VECTOR(Vecd, grad_phi, mu.size());
			LOCAL_MATRIX(Matrixd, hess_phi, mu.size(), mu.size());

			double ll = subject.logpdf(phi, tau, grad_phi, hess_phi);
			grad = grad_phi.transpose() * subject.randomToPhiXf();
			H = subject.randomToPhiXf().transpose() * hess_phi * subject.randomToPhiXf();
			return ll;
		}

		double error(ConstRefVecd & eta, RefVecd err) {
			LOCAL_VECTOR(Vecd, phi, mu.size());
			phi = mu + subject.randomToPhiXf() * eta;
			return subject.error(phi, tau, err);
		}

	private:
		const Individual subject;
		Vecd mu;
		Vecd tau;
};
#endif
