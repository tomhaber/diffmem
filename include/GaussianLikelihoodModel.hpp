/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef GAUSSIANLIKELIHOODMODEL_H
#define GAUSSIANLIKELIHOODMODEL_H

#include <Measurements.hpp>
#include <PointwiseLikelihoodModel.hpp>

class DIFFMEM_EXPORT GaussianLikelihoodModel final : public PointwiseLikelihoodModel {
public:
	GaussianLikelihoodModel(const Measurements & measurements) : measurements(measurements) {}
	GaussianLikelihoodModel(const Dict &dict);

public:
	int numberOfObservations() const override { return measurements.dim(); }
	int numberOfMeasurements() const override { return measurements.size(); }
	int numberOfSigmas() const override { return measurements.dim() * measurements.dim(); }
	int numberOfTaus() const override {
		const int dim = measurements.dim();
		return Cholesky<Matrixd>::numberOfCoefficients(dim);
	}

public:
	void transsigma(ConstRefVecd &tau, RefVecd sigma) const override;
	void transtau(ConstRefVecd &sigma, RefVecd tau) const override;

public:
	double logpdf(ConstRefVecd &tau, ConstRefMatrixd &u) const override;
	double logpdfGradTau(ConstRefVecd &tau, ConstRefMatrixd &u, RefVecd gradTau) const override;

	double logProportions(ConstRefVecd &tau) const override;
	double logProportionalPdf(ConstRefVecd &tau, int i, ConstRefVecd &u) const override;
	double logpdfGradTau(ConstRefVecd &tau, int i, ConstRefVecd &u, RefVecd gradTau) const override;

	void logpdfGradU(ConstRefVecd &tau, int i, ConstRefVecd &u, RefVecd gradU) const override;
	void logpdfHessU(ConstRefVecd &tau, int i, ConstRefVecd &u, RefVecd gradU, RefMatrixd hessU) const override;

	void updateTau(RefVecd tau, ConstRefVecd &gradTau, int N, double alpha) const override;


	LIKELIHOODMODEL_DISPATCH

private:
	Measurements measurements;
};
#endif
