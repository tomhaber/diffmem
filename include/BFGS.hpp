/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BFGS_H
#define BFGS_H

#include <MatrixVector.hpp>
#include <Function.hpp>
#include <OptimizationTrace.hpp>

class DIFFMEM_EXPORT BFGSstate {
	public:
		BFGSstate(int dim, int m=10);

	public:
		void multH(const Vecd & grad, Vecd & q);
		void push(const Vecd & x, const Vecd & grad, const Vecd & xOld, const Vecd & gradOld);
		void estimateH(Matrixd & H) const;

	private:
		Matrixd dx, dgrad;
		Vecd alpha, rho;
		int numOfIter;
};

class DIFFMEM_EXPORT BFGS {
	public:
		struct Options {
			double xTol{1e-32};
			double fTol{1e-8};
			double gradTol{1e-8};
			int m{10};
			int maxiters{1000};
			OptimizationTraceCallback trace{nullptr};

			Options() {}
			Options(double xTol, double fTol, double gradTol) : xTol(xTol), fTol(fTol), gradTol(gradTol) {}
		};

	public:
		BFGS(const Function & d, ConstRefVecd & initial_x, const Options & options=Options());

	public:
		int optimize();
		int optimize(const Options & options);

		bool step();

	public:
		bool isConverged() const;
		const Vecd & optimum() const;
		double value() const;
		void estimateH(Matrixd & H) const;

	private:
		double linesearch(const Vecd & s, double alpha_init);

	private:
		const Function & func;
		Options options;

	private:
		BFGSstate state;
		Vecd x, xOld, grad, gradOld, descDir;
		double fVal, fValOld;

	private:
		Vecd xLoc, gradLoc; //local
};

inline void BFGSstate::push(const Vecd & x, const Vecd & grad, const Vecd & xOld, const Vecd & gradOld) {
	const int m = dx.cols();
	const int index = numOfIter%m;

	dx.col(index) = x - xOld;
	dgrad.col(index) = grad - gradOld;
	rho[index] = 1.0 / dot(dx.col(index), dgrad.col(index));
	numOfIter++;
}

inline const Vecd & BFGS::optimum() const {
	return x;
}

inline double BFGS::value() const {
	return fVal;
}

inline void BFGS::estimateH(Matrixd &H) const {
	return state.estimateH(H);
}

inline int BFGS::optimize(const Options & options) {
	this->options = options;
	return optimize();
}
#endif
