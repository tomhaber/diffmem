/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STRING_UTIL_H_
#define STRING_UTIL_H_

#include <string>

static constexpr auto kSpaces = " \t\r\n";

inline const std::string & trim_right_ins(std::string & s,
											const std::string & t = kSpaces) {
	std::string::size_type i( s.find_last_not_of(t) );
	if( i != std::string::npos )
		s.erase( s.find_last_not_of(t) + 1 );

	return s;
}

inline const std::string & trim_left_ins(std::string & s,
										 const std::string & t = kSpaces) {
	return s.erase(0, s.find_first_not_of(t) );
}

inline const std::string & trim_ins(std::string & s,
									const std::string & t = kSpaces) {
	trim_left_ins(s, t);
	trim_right_ins(s, t);
	return s;
}

inline std::string trim_right(const std::string & s,
								const std::string & t = kSpaces) {
	std::string d(s);

	std::string::size_type i( d.find_last_not_of(t) );
	if( i == std::string::npos )
		return "";
	else
		return d.erase( d.find_last_not_of(t) + 1 );
}

inline std::string trim_left(const std::string & s,
							 const std::string & t = kSpaces) {
	std::string d(s);
	return d.erase(0, s.find_first_not_of(t) );
}

inline std::string trim(const std::string & s, const std::string & t = kSpaces) {
	std::string d(s);
	trim_left_ins(d, t);
	trim_right_ins(d, t);

	return d;
}

inline bool endsWith(const std::string & str, const std::string & postfix) {
	size_t n = postfix.length();
	return str.find(postfix,str.length()-n-1) != std::string::npos;
}
#endif
