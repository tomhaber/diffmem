/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SMLMDISTRIBUTION_H
#define SMLMDISTRIBUTION_H

#include <memory>
#include <StructuralModel.hpp>
#include <LikelihoodModel.hpp>
#include <math/number.hpp>

class DIFFMEM_EXPORT SMLMDistribution : public Distribution {
public:
	SMLMDistribution(const std::shared_ptr<StructuralModel> &sm, const std::shared_ptr<LikelihoodModel> &lm)
			: sm(sm), lm(lm) {}

	int numberOfDimensions() const override {
		return sm->numberOfParameters() + lm->numberOfTaus();
	}

	double logpdf(ConstRefVecd &x) const override {
		auto phi = x.head(sm->numberOfParameters());
		auto tau = x.tail(lm->numberOfTaus());

		Vecd psi(sm->numberOfParameters());

		try {
			return lm->logpdf(phi, tau, *sm);
		} catch (model_error &e) {
			return math::negInf();
		}
	}

private:
	std::shared_ptr<StructuralModel> sm;
	std::shared_ptr<LikelihoodModel> lm;
};

#endif /* SMLMDISTRIBUTION_H */
