#ifndef FAKEDISTRIBUTION_H
#define FAKEDISTRIBUTION_H

#include <NormalDistribution.hpp>
#include <stats/uniform_discrete.hpp>
#include <Random.hpp>

class DIFFMEM_EXPORT FakeDistribution : public ZMMvNDistribution {
public:
	using WeightList = std::vector<double>;

	FakeDistribution(const Dict &d);

	FakeDistribution(Matrixd omega, WeightList weights, size_t multiplier=1)
		: ZMMvNDistribution(omega), weights_(std::move(weights)), multiplier(multiplier) {}

	FakeDistribution(Cholesky<Matrixd> chol_omega, WeightList weights, size_t multiplier=1)
		: ZMMvNDistribution(chol_omega), weights_(std::move(weights)), multiplier(multiplier) {}

public:
	double logpdf(ConstRefVecd & x) const override {
		waitRandomly(x);
		return ZMMvNDistribution::logpdf(x);
	}

	double logpdf(ConstRefVecd & x, RefVecd grad) const override {
		waitRandomly(x);
		return ZMMvNDistribution::logpdf(x, grad);
	}

	double logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const override {
		waitRandomly(x);
		return ZMMvNDistribution::logpdf(x, grad, H);
	}

private:
	void waitRandomly(ConstRefVecd &x) const;

private:
	WeightList weights_;
	size_t multiplier;
};

#endif /* FAKEDISTRIBUTION_H */
