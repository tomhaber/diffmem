/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef EXPECTED_H_
#define EXPECTED_H_

#include <type_traits>
#include <exception>
#include <algorithm>
#include <stdexcept>

/*
 * Expected<T> is a wrapper that contains either an instance of T, an exception, or nothing.
 */
template <typename T>
class Expected;

template <typename T>
struct isExpected : std::false_type {
	typedef T type;
};

template <typename T>
struct isExpected<Expected<T>> : std::true_type {
	typedef T type;
};

namespace details {

	template<typename F, typename... Args>
	using resultOf = decltype(std::declval<F>()(std::declval<Args>()...));

	template<typename F, typename... Args>
	struct isCallableWith {
		template <typename T, typename = details::resultOf<T, Args...>>
		static constexpr std::true_type
		check(std::nullptr_t) { return std::true_type{}; };

		template <typename>
		static constexpr std::false_type
		check(...) { return std::false_type{}; };

		typedef decltype(check<F>(nullptr)) type;
		static constexpr bool value = type::value;
	};

	template <bool isExpected, typename F, typename... Args>
	struct FunctionTraits {
		using result_type = resultOf<F, Args...>;
	};

	template<typename T, typename F>
	struct ExpectedCallable {
		typedef typename std::conditional<isCallableWith<F>::value,
				details::FunctionTraits<false, F>,
				typename std::conditional<isCallableWith<F, T &&>::value,
						details::FunctionTraits<false, F, T &&>,
						typename std::conditional<isCallableWith<F, T &>::value,
								details::FunctionTraits<false, F, T &>,
								typename std::conditional<isCallableWith<F, Expected<T> &&>::value,
										details::FunctionTraits<true, F, Expected<T> &&>,
										details::FunctionTraits<true, F, Expected<T> &>>::type>::type>::type>::type Traits;
		typedef isExpected<typename Traits::result_type> ReturnsExpected;
		typedef Expected<typename ReturnsExpected::type> result_type;
		typedef typename ReturnsExpected::type type;
	};

	template<typename F>
	struct ExpectedCallable<void, F> {
		typedef details::FunctionTraits<false, F> Traits;
		typedef isExpected<typename Traits::result_type> ReturnsExpected;
		typedef Expected<typename ReturnsExpected::type> result_type;
		typedef typename ReturnsExpected::type type;
	};

}

template<class T>
class Expected {
	static_assert(!std::is_reference<T>::value, "Expected may not be used with reference types");

	enum class Contains {
		VALUE, EXCEPTION, NOTHING,
	};

	public:
		typedef T element_type;

		Expected() : contains_(Contains::NOTHING) {}
		explicit Expected(std::exception_ptr ep) : contains_(Contains::EXCEPTION), e_(ep) {
		}

		template <typename... U>
		explicit Expected(const std::piecewise_construct_t&, U&&... args) :
			contains_(Contains::VALUE), value_(std::forward<U>(args)...) {
		}

		template <typename U>
		explicit Expected(U && v,
				typename std::enable_if<std::is_convertible<U, T>::value>::type* = nullptr) :
			contains_(Contains::VALUE), value_(std::forward<U>(v)) {
		}

		// Move constructor
		Expected(Expected<T> && t) noexcept;
		// Move assigner
		Expected& operator=(Expected<T> && t) noexcept;

		// Copy constructor
		Expected(const Expected & t) = delete;
		// Copy assigner
		Expected& operator=(const Expected & t) = delete;

		~Expected();

	public:
		T & value()&;
		T && value()&&;
		const T & value() const &;
		void throwIfFailed() const;

		std::exception_ptr & exception() &;
		std::exception_ptr && exception() &&;
		const std::exception_ptr & exception() const &;

		T & operator*() { return value(); }
		const T  operator->() const { return &value(); }
		T *operator->() { return &value(); }

		operator bool() const { return contains_ != Contains::NOTHING; }
		bool hasValue() const { return contains_ == Contains::VALUE; }
		bool hasException() const { return contains_ == Contains::EXCEPTION; }

		static Expected success(T && t) {
			return Expected(std::move(t));
		}

		template <typename... U>
		static Expected success(U &&... args) {
			return Expected(std::piecewise_construct, std::forward<U>(args)...);
		}

		static Expected failure(std::exception_ptr ep) {
			if( ep == nullptr )
				throw std::invalid_argument("exception_ptr must not be null");

			return Expected(ep);
		}

	public:
		template <typename F, typename Callable = details::ExpectedCallable<T, F>>
		typename Callable::result_type map(F &&f);

		template <typename F>
		auto recover(F &&f) const & -> Expected<typename std::common_type<T, decltype(f(std::exception_ptr()))>::type>;

		template <typename F>
		auto recover(F &&f) && -> Expected<typename std::common_type<T, decltype(f(std::exception_ptr()))>::type>;

		template <bool isExpected, typename R>
		typename std::enable_if<isExpected, R>::type get() {
			return std::forward<R>(*this);
		}

		template <bool isExpected, typename R>
		typename std::enable_if<!isExpected, R>::type get() {
			return std::forward<R>(value());
		}

	private:
		template <typename F, typename Callable, bool isExpected, typename... Args>
		typename std::enable_if<!Callable::ReturnsExpected::value, typename Callable::result_type>::type
		mapImplementation(F &&f, details::FunctionTraits<isExpected, F, Args...>);

		template <typename F, typename Callable, bool isExpected, typename... Args>
		typename std::enable_if<Callable::ReturnsExpected::value, typename Callable::result_type>::type
		mapImplementation(F &&f, details::FunctionTraits<isExpected, F, Args...>);

	private:
		Contains contains_;
		union {
			T value_;
			std::exception_ptr e_;
		};
};

template <>
class Expected<void> {
	public:
		typedef void element_type;

		Expected() : hasValue_(true) {}
		explicit Expected(std::exception_ptr ep) : hasValue_(false), e_(ep) {}

	public:
		void value() const { throwIfFailed(); }
		void operator*() const { return value(); }

		inline void throwIfFailed() const;

		std::exception_ptr & exception() &;
		std::exception_ptr && exception() &&;
		const std::exception_ptr & exception() const &;

		operator bool() const { return hasValue_ || (e_ != nullptr); }
		bool hasValue() const { return hasValue_; }
		bool hasException() const { return !hasValue_ && (e_ != nullptr); }

		static Expected success() {
			return Expected();
		}

		static Expected failure(std::exception_ptr ep) {
			if( ep == nullptr )
				throw std::invalid_argument("exception_ptr must not be null");

			return Expected(ep);
		}

	public:
		template <typename F, typename Callable = details::ExpectedCallable<void, F>>
		typename Callable::result_type map(F &&f);

		template <typename F>
		auto recover(F &&f) const -> Expected<void>;

	private:
		bool hasValue_;
		std::exception_ptr e_ { nullptr };
};

template <class T>
Expected<T>::Expected(Expected<T> && t) noexcept : contains_(t.contains_) {
	if( contains_ == Contains::VALUE ) {
		new (&value_) T(std::move(t.value_));
	} else if( contains_ == Contains::EXCEPTION ) {
		new (&e_)std::exception_ptr(std::move(t.e_));
	}
}

template <class T>
Expected<T>& Expected<T>::operator=(Expected<T> && t) noexcept {
	if( this == &t ) return *this;

	this->~Expected();
	contains_ = t.contains_;
	if( contains_ == Contains::VALUE ) {
		new (&value_) T(std::move(t.value_));
	} else if( contains_ == Contains::EXCEPTION ) {
		new (&e_) std::exception_ptr(std::move(t.e_));
	}
	return *this;
}

template <class T>
Expected<T>::~Expected() {
	if( contains_ == Contains::VALUE ) {
		value_.~T();
	} else if( contains_ == Contains::EXCEPTION ) {
		using std::exception_ptr;
		e_.~exception_ptr();
	}
}

template <class T>
T & Expected<T>::value() & {
	throwIfFailed();
	return value_;
}

template <class T>
T && Expected<T>::value() && {
	throwIfFailed();
	return std::move(value_);
}

template <class T>
const T & Expected<T>::value() const & {
	throwIfFailed();
	return value_;
}

template <class T>
std::exception_ptr & Expected<T>::exception() & {
	if( contains_ != Contains::EXCEPTION )
		throw std::runtime_error("Expected does not contain an exception");
	return e_;
}

template <class T>
std::exception_ptr && Expected<T>::exception() && {
	if( contains_ != Contains::EXCEPTION )
		throw std::runtime_error("Expected does not contain an exception");
	return std::move(e_);
}

template <class T>
const std::exception_ptr & Expected<T>::exception() const & {
	if( contains_ != Contains::EXCEPTION )
		throw std::runtime_error("Expected does not contain an exception");
	return e_;
}

template <class T>
void Expected<T>::throwIfFailed() const {
	if( contains_ != Contains::VALUE ) {
		if( contains_ == Contains::EXCEPTION ) {
			std::rethrow_exception(e_);
		} else {
			throw std::runtime_error("UsingUninitializedExpected");
		}
	}
}

void Expected<void>::throwIfFailed() const {
	if( ! hasValue_ ) {
		if( e_ != nullptr )
			std::rethrow_exception(e_);
		else
			throw std::runtime_error("UsingUninitializedExpected");
	}
}

inline std::exception_ptr & Expected<void>::exception() & {
	if( hasValue_ || e_ == nullptr )
		throw std::runtime_error("Expected does not contain an exception");
	return e_;
}

inline std::exception_ptr && Expected<void>::exception() && {
	if( hasValue_ || e_ == nullptr )
		throw std::runtime_error("Expected does not contain an exception");
	return std::move(e_);
}

inline const std::exception_ptr & Expected<void>::exception() const & {
	if( hasValue_ || e_ == nullptr )
		throw std::runtime_error("Expected does not contain an exception");
	return e_;
}

namespace internal {
	template <typename T, typename F, typename... Args>
	typename std::enable_if< isExpected<T>::value, T>::type tryTo(F && f, Args && ...args) {
		try {
			return f(std::forward<Args>(args)...);
		} catch( ... ) {
			return T::failure(std::current_exception());
		}
	}

	template <typename T, typename F, typename... Args>
	typename std::enable_if<!isExpected<T>::value && !std::is_same<T, void>::value, Expected<T>>::type tryTo(F && f, Args && ...args) {
		try {
			return Expected<T>::success(f(std::forward<Args>(args)...));
		} catch( ... ) {
			return Expected<T>::failure(std::current_exception());
		}
	}

	template<typename T, typename F, typename... Args>
	typename std::enable_if<!isExpected<T>::value && std::is_same<T, void>::value, Expected<void>>::type tryTo(F&& f, Args && ...args) {
		try {
			f(std::forward<Args>(args)...);
			return Expected<void>::success();
		} catch( ... ) {
			return Expected<void>::failure(std::current_exception());
		}
	}
}

template <typename F, typename... Args>
auto tryTo(F && f, Args && ...args) -> Expected<decltype(f(std::forward<Args>(args)...))> {
	return internal::tryTo<decltype(f(std::forward<Args>(args)...))>(std::forward<F>(f), std::forward<Args>(args)...);
}

template <typename T>
Expected<T> makeExpected(T && x) {
	return Expected<T>(std::forward<T>(x));
}

template<class T>
template <typename F, typename Callable>
typename Callable::result_type Expected<T>::map(F && f) {
	typename Callable::Traits traits;
	return mapImplementation<F,Callable>(std::forward<F>(f), traits);
}

template<class T>
template <typename F, typename Callable, bool isExpected, typename... Args>
typename std::enable_if<!Callable::ReturnsExpected::value, typename Callable::result_type>::type
Expected<T>::mapImplementation(F && f, details::FunctionTraits<isExpected,F,Args...>) {
	if( contains_ != Contains::VALUE ) {
		typedef typename Callable::result_type R;
		if( contains_ == Contains::EXCEPTION )
			return R::failure(std::move(exception()));
		else
			throw std::runtime_error("UsingUninitializedExpected");
	}

	typedef typename Callable::type R;
	return internal::tryTo<R>(std::forward<F>(f), get<isExpected, Args>()...);
}

template<class T>
template <typename F, typename Callable, bool isExpected, typename... Args>
typename std::enable_if<Callable::ReturnsExpected::value, typename Callable::result_type>::type
Expected<T>::mapImplementation(F && f, details::FunctionTraits<isExpected,F,Args...>) {
	typedef typename Callable::result_type R;
	if( contains_ != Contains::VALUE ) {
		if( contains_ == Contains::EXCEPTION )
			return R::failure(std::move(exception()));
		else
			throw std::runtime_error("UsingUninitializedExpected");
	}

	return internal::tryTo<R>(std::forward<F>(f), get<isExpected, Args>()...);
}

template <typename F, typename Callable>
typename Callable::result_type Expected<void>::map(F && f) {
	typedef typename Callable::result_type R;
	if( ! hasValue_ ) {
		if( e_ != nullptr )
			return R::failure(std::move(exception()));
		else
			throw std::runtime_error("UsingUninitializedExpected");
	}

	return tryTo(std::forward<F>(f));
}

template<class T>
template <typename F>
auto Expected<T>::recover(F && f) const &
	-> Expected<typename std::common_type<T, decltype(f(std::exception_ptr()))>::type> {

	using U = typename std::common_type<T, decltype(f(std::exception_ptr()))>::type;
	if( contains_ == Contains::VALUE )
		return Expected<U>(value_);

	if( contains_ != Contains::EXCEPTION )
		throw std::runtime_error("UsingUninitializedExpected");

	try {
		return Expected<U>::success(f(e_));
	} catch( ... ) {
		return Expected<U>::failure(std::current_exception());
	}
}

template<class T>
template <typename F>
auto Expected<T>::recover(F && f) &&
	-> Expected<typename std::common_type<T, decltype(f(std::exception_ptr()))>::type> {

	using U = typename std::common_type<T, decltype(f(std::exception_ptr()))>::type;
	if( contains_ == Contains::VALUE )
		return std::move(*this);

	if( contains_ != Contains::EXCEPTION )
		throw std::runtime_error("UsingUninitializedExpected");

	try {
		return Expected<U>::success(f(e_));
	} catch( ... ) {
		return Expected<U>::failure(std::current_exception());
	}
}

template <typename F>
auto Expected<void>::recover(F && f) const -> Expected<void> {
	if( hasValue_ )
		return *this;

	if( e_ == nullptr )
		throw std::runtime_error("UsingUninitializedExpected");

	return tryTo(std::forward<F>(f), e_);
}
#endif
