/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef VALUECHECK_H_
#define VALUECHECK_H_

#include <Error.hpp>
#include <math/number.hpp>

#ifndef NDEBUG
	#define DomainError(fmt, ...) \
		throwDomainError(__PRETTY_FUNCTION__, fmt, __VA_ARGS__)
#else
	#define DomainError(fmt, ...) \
		return math::NaN()
#endif

#define check_strictly_positive(x) \
	if( UNLIKELY(! math::isStrictlyPositive(x)) ) DomainError(STRINGIZE(x) " (= {}) must be strictly positive", x)

#define check_strictly_positive_finite(x) \
	if( UNLIKELY(! math::isStrictlyPositiveFinite(x)) ) DomainError(STRINGIZE(x) " (= {}) must be strictly positive and finite", x)

#define check_nonnegative(x) \
	if( UNLIKELY(! math::isNonNegative(x)) ) DomainError(STRINGIZE(x) " (= {}) must be non-negative", x)

#define check_nonnegative_finite(x) \
	if( UNLIKELY(! math::isNonNegativeFinite(x)) ) DomainError(STRINGIZE(x) " (= {}) must be non-negative and finite", x)

#define check_finite(x) \
	if( UNLIKELY(! math::isFinite(x)) ) DomainError(STRINGIZE(x) " (= {}) must be finite")

#define check_less(x, upper) \
	if( UNLIKELY(! math::isLess(x, upper)) ) DomainError(STRINGIZE(x) " (= {}) must be less than {}", x, upper)

#define check_less_or_equal(x, upper) \
	if( UNLIKELY(! math::isLessOrEqual(x, upper)) ) DomainError(STRINGIZE(x) " (= {}) must be less or equal than {}", x, upper)

#define check_greater(x, lower) \
	if( UNLIKELY(! math::isGreater(x, lower)) ) DomainError(STRINGIZE(x) " (= {}) must be greater than {}", x, lower)

#define check_greater_or_equal(x, lower) \
	if( UNLIKELY(! math::isGreaterOrEqual(x, lower)) ) DomainError(STRINGIZE(x) " (= {}) must be greater or equal than {}", x, lower)

#define check_between(x, lower, upper) \
	if( UNLIKELY(! math::isBetween(x, lower, upper)) ) DomainError(STRINGIZE(x) " (= {}) must be in [{}, {}[", x, lower, upper)

#define check_strictly_between(x, lower, upper) \
	if( UNLIKELY(! math::isStrictlyBetween(x, lower, upper)) ) DomainError(STRINGIZE(x) " (= {}) must be in ]{}, {}[", x, lower, upper)

#define check_bounded(x, lower, upper) \
	if( UNLIKELY(! math::isBounded(x, lower, upper)) ) DomainError(STRINGIZE(x) " (= {}) must be in [{}, {}]", x, lower, upper)

#define check_bounded01(x) \
	if( UNLIKELY(! math::isBounded(x, 0.0, 1.0)) ) DomainError(STRINGIZE(x) " (= {}) must be in [0, 1]", x)
#endif
