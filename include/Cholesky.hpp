/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CHOLESKY_H_
#define CHOLESKY_H_

#include <common.hpp>
#include <MatrixVector.hpp>

template <class MatrixType, int UpLo = Eigen::Lower>
class Cholesky: public Eigen::LLT<MatrixType, UpLo> {
	public:
		enum { Lower = Eigen::Lower, Upper = Eigen::Upper };
		typedef typename MatrixType::Index Index;
		typedef typename Eigen::LLT<MatrixType, UpLo>::Traits Traits;

		Cholesky() : Eigen::LLT<MatrixType, UpLo>() {}
		explicit Cholesky(Index size) : Eigen::LLT<MatrixType, UpLo>(size) {}

		explicit Cholesky(const MatrixType & matrix) : Eigen::LLT<MatrixType, UpLo>(matrix) {
		}

	public:
		static int numberOfCoefficients(int n) {
			return triangularElementsFromDim(n);
		}

		int numberOfCoefficients() const {
			return triangularElementsFromDim( this->rows() );
		}

		template <typename Derived>
		void extractCoefficients(Eigen::PlainObjectBase<Derived> & coeffs) const {
			tri2vec<UpLo>(this->m_matrix, coeffs);
		}

		template <typename Derived>
		void extractCoefficients(Eigen::MatrixBase<Derived> & coeffs) const {
			tri2vec<UpLo>(this->m_matrix, coeffs);
		}

		static int coefficientsToDim(int ncoeffs) {
			return dimFromTriangularElements(ncoeffs);
		}

		template <typename Derived>
		void fromCoefficients(const Eigen::MatrixBase<Derived> & coeffs) {
			vec2tri<UpLo>(coeffs, this->m_matrix);
			this->m_isInitialized = true;
			this->m_info = Eigen::Success;
		}

	public:
		template <typename Derived>
		void set(const Eigen::MatrixBase<Derived> & m) {
			this->m_matrix = m;
			this->m_isInitialized = true;
			this->m_info = Eigen::Success;
		}

		Cholesky & compute(const MatrixType & matrix) {
			Eigen::LLT<MatrixType>::compute(matrix);
			return *this;
		}

		size_t totalSize() const {
			return this->m_matrix.size();
		}

	public:
		double determinant() const {
			const double det = this->matrixL().determinant();
			return det*det;
		}

		double logDeterminant() const {
			return 2.0*std::log(this->matrixL().determinant());
		}

		template <typename Derived>
		void inverse(Eigen::MatrixBase<Derived> & omega_inv) const {
			omega_inv = Derived::Identity(this->rows(), this->cols());
			this->solveInPlace(omega_inv);
		}

		Cholesky<MatrixType> & operator =(const MatrixType & mat) {
			return compute(mat);
		}

	public:
		template <typename Derived>
		Eigen::MatrixBase<Derived> & multLowerInPlace(Eigen::MatrixBase<Derived> & bAndX) const;
};

template <typename Derived, typename OtherDerived>
void chol_inv(const Eigen::MatrixBase<Derived> & omega, Eigen::MatrixBase<OtherDerived> & omega_inv) {
	typedef typename Eigen::MatrixBase<Derived>::PlainObject MatrixType;
	Cholesky<MatrixType> chol_omega(omega);
	omega_inv = OtherDerived::Identity(chol_omega.rows(), chol_omega.cols());
	chol_omega.solveInPlace(omega_inv);
}

template <typename MatrixType, typename Derived>
void chol_inv(const Cholesky<MatrixType> & chol_omega, Eigen::MatrixBase<Derived> & omega_inv) {
	omega_inv = Derived::Identity(chol_omega.rows(), chol_omega.cols());
	chol_omega.solveInPlace(omega_inv);
}

template <typename MatrixType>
inline double determinant(const Cholesky<MatrixType> & omega) {
	return omega.determinant();
}

template <typename MatrixType>
inline double logDeterminant(const Cholesky<MatrixType> & omega) {
	return omega.logDeterminant();
}

namespace internal {
	template <typename Index, typename LhsScalar, typename RhsScalar, int StorageOrder>
	struct triangular_matrix_vector_inplace;

	template <typename Index, typename LhsScalar, typename RhsScalar>
	struct triangular_matrix_vector_inplace<Index, LhsScalar, RhsScalar, Eigen::ColMajor> {
		static void run(Index rows, Index cols, const LhsScalar *A, Index AStride, RhsScalar *x, Index xIncr) {
			for(Index i = cols - 1; i >= 0; --i) {
				RhsScalar s = A[i + i * AStride] * x[i * xIncr];
				for(Index j = 0; j < i; ++j)
					s += A[i + j * AStride] * x[j * xIncr];

				x[i * xIncr] = s;
			}
		}
	};

	template <typename Index, typename LhsScalar, typename RhsScalar>
	struct triangular_matrix_vector_inplace<Index, LhsScalar, RhsScalar, Eigen::RowMajor> {
		static void run(Index rows, Index cols, const LhsScalar *A, Index AStride, RhsScalar *x, Index xIncr) {
			for(Index i = cols - 1; i >= 0; --i) {
				RhsScalar s = A[i * AStride + i] * x[i * xIncr];
				for(Index j = 0; j < i; ++j)
					s += A[i * AStride + j] * x[j * xIncr];

				x[i * xIncr] = s;
			}
		}
	};

	template <typename MatrixType, int UpLo>
	struct traits;

	template <typename MatrixType>
	struct traits<MatrixType, Eigen::Lower> {
		typedef MatrixType MatrixL;
	};

	template <typename MatrixType>
	struct traits<MatrixType, Eigen::Upper> {
		typedef typename MatrixType::AdjointReturnType MatrixL;
	};
}

template <typename MatrixType, int UpLo>
template <typename Derived>
Eigen::MatrixBase<Derived> & Cholesky<MatrixType, UpLo>::multLowerInPlace(Eigen::MatrixBase<Derived> & bAndX) const {
	Assert(this->rows()==bAndX.rows() && this->cols()==bAndX.rows() && bAndX.cols()==1);

	typedef typename internal::traits<MatrixType, UpLo>::MatrixL MatrixL;
    typedef typename MatrixL::Index Index;
    typedef typename MatrixL::Scalar LhsScalar;
    typedef typename Derived::Scalar RhsScalar;

	constexpr int Flags = ((UpLo == Eigen::Lower) ?
					int(Eigen::internal::traits<MatrixL>::Flags) :
					int(Eigen::internal::traits<MatrixL>::Flags ^ Eigen::RowMajorBit));
	internal::triangular_matrix_vector_inplace<Index, LhsScalar, RhsScalar,
		(int(Flags&Eigen::RowMajorBit) ? Eigen::RowMajor : Eigen::ColMajor)>::run(
				this->rows(), this->cols(), this->m_matrix.data(), this->m_matrix.outerStride(),
				bAndX.derived().data(), bAndX.innerStride());

    return bAndX;
}
#endif
