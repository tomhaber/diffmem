/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef GAUSSIANQUADRATURE_H_
#define GAUSSIANQUADRATURE_H_

#include <common.hpp>
#include <functional>
#include <MatrixVector.hpp>
#include <reduce_sum.hpp>

class DIFFMEM_EXPORT GaussianQuadrature {
	public:
		struct GQ {
			size_t n;
			size_t m() const { return (n + 1) >> 1; }
			const double *x;
			const double *w;
		};

		static const struct GQ & findGaussLegendreTable(int n);
		static const struct GQ & findGaussHermiteTable(int n);

		struct NDGaussQuad {
			NDGaussQuad(int dim, const struct GQ & gq) : dim(dim), gq(gq) {}

			double x(size_t i, RefVecd x) const {
				double w = 1.0;
				for(int j = dim - 1; j >= 0; --j) {
					size_t idx = i % gq.n;
					i /= gq.n;

					if((gq.n & 1) != 0) {
						x[j] = (idx < gq.m()) ? -gq.x[gq.m() - idx - 1] : +gq.x[idx - gq.m() + 1];
						w *= (idx < gq.m()) ? gq.w[gq.m() - idx - 1] : gq.w[idx - gq.m() + 1];
					} else {
						x[j] = (idx < gq.m()) ? -gq.x[gq.m() - idx - 1] : +gq.x[idx - gq.m()];
						w *= (idx < gq.m()) ? gq.w[gq.m() - idx - 1] : gq.w[idx - gq.m()];
					}
				}

				return w;
			}

			size_t count() const {
				size_t nd = 1;
				for(int j = 0; j < dim; ++j)
					nd *= gq.n;
				return nd;
			}

			const int dim;
			const struct GQ &gq;
		};

	public:
		template <typename F>
		static double int1d(int n, F &&f, double a = -1.0, double b = 1.0) {
			const double A = 0.5 * (b - a);
			const double B = 0.5 * (b + a);

			const struct GQ &gq = findGaussLegendreTable(n);

			auto g = [A, B, &f](double x) { return std::forward<F>(f)(B + A * x); };
			double s = ((n & 1) != 0) ? int1d_odd(g, gq) : int1d_even(g, gq);
			return A * s;
		}

	public:
		template <typename F>
		static double int1d_hermite(int n, F &&f) {
			const struct GQ &gq = findGaussHermiteTable(n);
			return ((n & 1) != 0) ? int1d_odd(std::forward<F>(f), gq) :
									int1d_even(std::forward<F>(f), gq);
		}

		template <size_t dims, typename F>
		static double intnd_hermite(int n, F && f) {
			VecNd<dims> x;
			NDGaussQuad ndgq(dims, findGaussHermiteTable(n));
			return reduce_sum<size_t>(0, ndgq.count(), [&](size_t i) {
				double w = ndgq.x(i, x);
				return w * f(x);
			});
		}

		template <typename F>
		static double intnd_hermite(int dims, int n, F && f) {
			Vecd x(dims);
			NDGaussQuad ndgq(dims, findGaussHermiteTable(n));
			return reduce_sum<size_t>(0, ndgq.count(), [&](size_t i) {
				double w = ndgq.x(i, x);
				return w * f(x);
			});
		}

	private:
		template <typename F>
		static double int1d_odd(F && f, const struct GQ & gq) {
			double s = gq.w[0] * f(0.0);
			for(size_t i = 1; i < gq.m(); i++) {
				s += gq.w[i]*(f(-gq.x[i])+f(gq.x[i]));
			}
			return s;
		}

		template <typename F>
		static double int1d_even(F && f, const struct GQ & gq) {
			double s = 0.0;
			for(size_t i = 0; i < gq.m(); i++) {
				s += gq.w[i]*(f(-gq.x[i])+f(gq.x[i]));
			}
			return s;
		}
};
#endif
