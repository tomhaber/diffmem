/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef TIMEPLATFORM_H
#define TIMEPLATFORM_H

#define INLINE_ELAPSED(INL) static INL double elapsed(ticks t0, ticks t1)  { \
     return (double)t1 - (double)t0;					  \
}

/*
 * Pentium cycle counter
 */
#if (defined(__GNUC__) || defined(__ICC)) && defined(__i386__)  && !defined(HAVE_TICK_COUNTER)
typedef unsigned long long ticks;

static __inline__ ticks getticks(void) {
     ticks ret;

     __asm__ __volatile__("rdtscp": "=A" (ret) :: "ecx");
     /* no input, nothing else clobbered */
     return ret;
}

INLINE_ELAPSED(__inline__)

#define HAVE_TICK_COUNTER
#define TIME_MIN 5000.0   /* unreliable pentium IV cycle counter */
#endif

/* Visual C++ -- thanks to Morten Nissov for his help with this */
#if _MSC_VER >= 1200 && !defined(HAVE_TICK_COUNTER)
#include <windows.h>
typedef LARGE_INTEGER ticks;

static __inline ticks getticks(void) {
     ticks retval;

#if _M_IX86 >= 500 
	 __asm __emit 0fh __asm __emit 031h /* hack for VC++ 5.0 */
     __asm {
	  mov retval.HighPart, edx
	  mov retval.LowPart, eax
     }
#else
	 retval.QuadPart = __rdtsc();
#endif
     return retval;
}


static __inline double elapsed(ticks t0, ticks t1) {
     return (double)t1.QuadPart - (double)t0.QuadPart;
}

#define HAVE_TICK_COUNTER
#define TIME_MIN 5000.0   /* unreliable pentium IV cycle counter */
#endif


/*----------------------------------------------------------------*/
/*
 * X86-64 cycle counter
 */
#if (defined(__GNUC__) || defined(__ICC) || defined(__SUNPRO_C)) && defined(__x86_64__)  && !defined(HAVE_TICK_COUNTER)
typedef unsigned long long ticks;

static __inline__ ticks getticks(void) {
     unsigned a, d;
     asm volatile("rdtscp" : "=a" (a), "=d" (d) :: "rcx");
     return ((ticks)a) | (((ticks)d) << 32);
}

INLINE_ELAPSED(__inline__)

#define HAVE_TICK_COUNTER
#endif

#ifdef CHRONO_CPP11
#	include <chrono>
	typedef std::chrono::high_resolution_clock::time_point pl_time_t;
#elif defined(WIN32) || defined(WIN64)
	typedef LARGE_INTEGER pl_time_t;
#elif defined(POSIX_TIMERS)
#	include <time.h>
	typedef struct timespec pl_time_t;
#else
#	include <sys/time.h>
	typedef struct timeval pl_time_t;
#endif

#endif
