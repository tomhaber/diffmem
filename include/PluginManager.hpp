/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include <common.hpp>
#include <unordered_map>
#include <list>
#include <string>
#include <DynamicLibrary.hpp>

/*!
  Throws when something went wrong with a plugin.
*/
class PluginException : public std::runtime_error {
	public:
		PluginException(const std::string & name) : std::runtime_error("Plugin Exception"), name(name) {}
		PluginException(const std::string & name, const char *s) : std::runtime_error(s), name(name) {}
		PluginException(const std::string & name, const std::string & s) : std::runtime_error(s), name(name) {}
		PluginException(const PluginException & ex) : std::runtime_error(ex), name(ex.name) {}
		void operator =(const PluginException & ex) { name = ex.name; }

	public:
		//! Returns the name of the plugin.
		const std::string & pluginName() { return name; }

	private:
		std::string name;
};

/*!
  Thrown when a trying to load a plugin that was previously loaded.
*/
class PluginAlreadyLoadedException : public PluginException {
	public:
		PluginAlreadyLoadedException(const std::string & name)
								: PluginException(name, "Plugin is already loaded") {}
		PluginAlreadyLoadedException(const std::string & name, const char *s)
														: PluginException(name, s) {}
		PluginAlreadyLoadedException(const std::string & name, const std::string & s)
														: PluginException(name, s) {}
};

/*!
  Thrown when trying to remove or use a plugin that is not loaded.
*/
class PluginNotLoadedException : public PluginException {
	public:
		PluginNotLoadedException(const std::string & name)
									: PluginException(name, "Plugin is not loaded") {}
		PluginNotLoadedException(const std::string & name, const char *s)
															: PluginException(name, s) {}
		PluginNotLoadedException(const std::string & name, const std::string & s)
															: PluginException(name, s) {}
};

/*!
  The plugin in question was not found.
*/
class PluginNotFoundException : public PluginException {
	public:
		PluginNotFoundException(const std::string & name)
									: PluginException(name, "Plugin could not be found") {}
		PluginNotFoundException(const std::string & name, const char *s)
															: PluginException(name, s) {}
		PluginNotFoundException(const std::string & name, const std::string & s)
															: PluginException(name, s) {}
};

/*!
  Something went wrong while loading the plugin.
*/
class PluginLoadException : public PluginException {
	public:
		PluginLoadException(const std::string & name)
									: PluginException(name, "Plugin could not be loaded") {}
		PluginLoadException(const std::string & name, const char *s)
															: PluginException(name, s) {}
		PluginLoadException(const std::string & name, const std::string & s)
															: PluginException(name, s) {}
};

/*
 * This class keeps track of open plugins and handles loading and unloading.
 */
class DIFFMEM_EXPORT PluginManager {
	public:
		static PluginManager & instance();

	public:
		void loadPlugin(const std::string & name);
		void unloadPlugin(const std::string & name);
		bool isPluginLoaded(const std::string & name);
		std::list<std::string> listPlugins() const;

	protected:
		std::unordered_map<std::string, DynamicLibrary> plugins;
};

#endif
