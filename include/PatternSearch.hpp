/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PATTERNSEARCH_H
#define PATTERNSEARCH_H

#include <MatrixVector.hpp>
#include <Function.hpp>
#include <math/number.hpp>
#include <OptimizationTrace.hpp>

class DIFFMEM_EXPORT PatternSearch {
	public:
		enum PollMethod {
			Positive2N,
			PositiveNp1,
			Cross4N
		};

		struct Options {
			double fTol{1e-6};
			double xTol{1e-6};
			double meshTol{1e-6};
			double initialMeshSize{1.0};
			double expansion{2.0};
			double contraction{0.5};
			double maxMeshSize{ math::posInf() };
			int maxEvals{2000};
			int maxIters{200};
			PollMethod method{Positive2N};
			OptimizationTraceCallback trace{nullptr};

			Options() {}
		};

	public:
		PatternSearch(const Function & d, ConstRefVecd & initial_x, const Options & options = Options());

	public:
		int optimize();
		int optimize(const Options & opts);
		bool step();

	public:
		bool isConverged() const;
		const Vecd & optimum() const;
		double value() const;
		int numberOfEvals() const;
		double meshSize() const;

	private:
		bool isConverged(bool refined) const;

	private:
		const Function & func;
		Options options;

	private:
		Vecd x, xOld;
		double fVal, fValOld;
		int nevals;
		Matrixd directions;
		double meshsize;
};

inline const Vecd & PatternSearch::optimum() const {
	return x;
}

inline double PatternSearch::value() const {
	return fVal;
}

inline bool PatternSearch::isConverged() const {
	return isConverged(false);
}

inline int PatternSearch::numberOfEvals() const {
	return nevals;
}

inline double PatternSearch::meshSize() const {
	return meshsize;
}
#endif
