/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PRODUCTDISTRIBUTION_H_
#define PRODUCTDISTRIBUTION_H_

#include <Distribution.hpp>
#include <LocalMatrix.hpp>

template <typename DistrA=std::shared_ptr<Distribution>, typename DistrB=std::shared_ptr<Distribution>>
class DIFFMEM_EXPORT ProductDistribution final : public Distribution {
	public:
		ProductDistribution(const DistrA &a, const DistrB &b)
				: a(a), b(b) {
			Require(a.numberOfDimensions() == b.numberOfDimensions(), "both distributions require the same number of dimensions");
		}

	public:
		int numberOfDimensions() const override {
			return a.numberOfDimensions();
		}

	public:
		double logpdf(ConstRefVecd & x) const override {
			return a.logpdf(x) + b.logpdf(x);
		}

		double logpdf(ConstRefVecd & x, RefVecd grad) const override {
			double ll = 0.0;

			LOCAL_VECTOR(Vecd, grad_tmp, x.size());
			ll += a.logpdf(x, grad_tmp);

			ll += b.logpdf(x, grad);
			grad += grad_tmp;
			return ll;
		}

		double logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const override {
			double ll = 0.0;

			LOCAL_VECTOR(Vecd, grad_tmp, x.size());
			LOCAL_MATRIX(Matrixd, H_tmp, x.size(), x.size());
			ll += a.logpdf(x, grad_tmp, H_tmp);

			ll += b.logpdf(x, grad, H);

			grad += grad_tmp;
			H += H_tmp;
			return ll;
		}

	private:
		const DistrA & a;
		const DistrB & b;
};

template <>
class DIFFMEM_EXPORT ProductDistribution<std::shared_ptr<Distribution>, std::shared_ptr<Distribution>> final : public Distribution {
	public:
		ProductDistribution(const std::shared_ptr<Distribution> &a, const std::shared_ptr<Distribution> &b)
				: a(a), b(b) {
			Require(a->numberOfDimensions() == b->numberOfDimensions(), "both distributions require the same number of dimensions");
		}

	public:
		int numberOfDimensions() const override {
			return a->numberOfDimensions();
		}

	public:
		double logpdf(ConstRefVecd & x) const override {
			return a->logpdf(x) + b->logpdf(x);
		}

		double logpdf(ConstRefVecd & x, RefVecd grad) const override {
			double ll = 0.0;

			LOCAL_VECTOR(Vecd, grad_tmp, x.size());
			ll += a->logpdf(x, grad_tmp);

			ll += b->logpdf(x, grad);
			grad += grad_tmp;
			return ll;
		}

		double logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const override {
			double ll = 0.0;

			LOCAL_VECTOR(Vecd, grad_tmp, x.size());
			LOCAL_MATRIX(Matrixd, H_tmp, x.size(), x.size());
			ll += a->logpdf(x, grad_tmp, H_tmp);

			ll += b->logpdf(x, grad, H);

			grad += grad_tmp;
			H += H_tmp;
			return ll;
		}

	private:
		const std::shared_ptr<Distribution> a;
		const std::shared_ptr<Distribution> b;
};

#endif
