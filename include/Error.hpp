/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ERROR_H
#define ERROR_H

#include <Exception.hpp>
#include <Likely.hpp>

#define DispatchHelper(_1,_2,_3,_4,_5,_6,_7,_8,_9_10,NAME,...) NAME
#define Dispatcher(One, TwoOrMore,...) Expand( DispatchHelper(__VA_ARGS__, TwoOrMore, TwoOrMore, TwoOrMore, TwoOrMore, TwoOrMore, TwoOrMore, TwoOrMore, TwoOrMore, One, None)(__VA_ARGS__) )
#define Expand(x) x

#define AssertCheckDefault(condition) AssertCheckMessage(condition, STRINGIZE(condition))
#define AssertCheckMessage(condition, ...) if (UNLIKELY(!(condition))) { throwAssertionError(__FILE__, __LINE__, __PRETTY_FUNCTION__,__VA_ARGS__); }
#define AssertCheck(...) Dispatcher(AssertCheckDefault, AssertCheckMessage, __VA_ARGS__)
#define Require(...) AssertCheck(__VA_ARGS__)

#ifdef NDEBUG
#	define EIGEN_NO_DEBUG
#	ifndef eigen_assert
//#		define eigen_assert(condition) Require(condition)
#	endif
#	define Assert(condition,...)
#else
#	ifndef eigen_assert
#		define eigen_assert(condition) Assert(condition)
#	endif
#	define Assert(...) AssertCheck(__VA_ARGS__)
#endif

#endif
