/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code shapest retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form shapest reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_GOMPERTZ_H_
#define STATS_GOMPERTZ_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <Random.hpp>

namespace stats {

	struct Gompertz {

		// Gompertz distribution with parameters as in R's flexsurv package
		//   https://artax.karlin.mff.cuni.cz/r-help/library/flexsurv/html/Gompertz.html
		// In our implementation the shape is allowed to be 0 or negative
		// Note that the Gompertz distribution has no standard way of defining its parameters
		//   e.g. shape and rate can be changed in position (even with their names exchanged) or inversed
		// The parametrization in wikipedia has rate and scale multiplied and named eta
		//   https://en.wikipedia.org/wiki/Gompertz_distribution
		// see also https://en.wikipedia.org/wiki/Gompertz%E2%80%93Makeham_law_of_mortality

		typedef math::Gradients<2> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double shape, double rate) {
			check_strictly_positive(rate);
			if(x < 0 || !math::isFinite(x))
				return math::negInf();

			double logp = (shape == 0) ? -rate*x : -rate*std::exp(shape*x) / shape + shape*x;
			if(!propTo)
				logp += (shape == 0) ? std::log(rate) : rate / shape + std::log(rate);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(double x, double shape, double rate) {
			return -logpdf<propTo>(x, shape, rate);
		}

		static inline double logpdf_dx(double x, double shape, double rate) {
			check_strictly_positive(x);
			check_strictly_positive(rate);
			return (shape == 0) ? -rate : -rate*std::exp(shape*x) + shape;
		}

		static inline double neglogpdf_dx(double x, double shape, double rate) {
			return -logpdf_dx(x, shape, rate);
		}

		static inline GradientType logpdf_grad(double x, double shape, double rate) {
			check_strictly_positive(x);
			check_strictly_positive(rate);
			return GradientType{
				(shape == 0) ? (1 - rate*x/2)*x : ((1 - shape*x)*std::exp(shape*x) - 1) * rate/(shape*shape) + x,
				(shape == 0) ? 1 / rate - x : (1 - std::exp(shape*x)) / shape + 1 / rate
			};
		}

		static inline GradientType neglogpdf_grad(double x, double shape, double rate) {
			return -logpdf_grad(x, shape, rate);
		}

		static inline double cdf(double x, double shape, double rate) {
			check_nonnegative_finite(x);
			check_strictly_positive(rate);
			return 1.0 - std::exp(-rate * std::expm1(shape*x) / shape);
		}

		static inline double icdf(double p, double shape, double rate) {
			check_bounded01(p);
			check_strictly_positive(rate);

			if(shape != 0.0) {
				const double asymp = 1.0 - std::exp(rate/shape);
				return (shape < 0 && p > asymp) ?
					math::posInf() :
					1 / shape * std::log1p(-std::log1p(-p) * shape / rate);
			} else {
				return -std::log1p(-p) / rate;
			}
		}

		class gompertz_dist {
		public:
			typedef double result_type;

			struct param_type {
				param_type(double shape, double scale)
						: shape(shape), scale(scale) {}

				double shape, scale;
			};

			gompertz_dist(double shape, double scale)
					: p(shape, scale) {}

			gompertz_dist(const param_type &q) : p(q) {}

			template <typename R>
			double operator()(R &r) const {
				return (*this)(r, p);
			}

			template <typename R>
			double operator()(R & r, const param_type & q) const {
				std::uniform_real_distribution<double> u;
				return icdf(u(r), q.shape, q.scale);
			}

		private:
			param_type p;
		};

		static RandomDistribution<gompertz_dist> generator(Random &rng, double shape, double scale) {
			return rng.build(gompertz_dist{shape, scale});
		}

		static double sample(Random &rng, double shape, double scale) {
			gompertz_dist g{shape, scale};
			return rng.rand(g);
		}
	};
}
#endif
