/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_BETA_H_
#define STATS_BETA_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/log1m.hpp>
#include <math/digamma.hpp>
#include <math/ibeta.hpp>
#include <Random.hpp>

namespace stats {

	struct Beta {

		typedef math::Gradients<2> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double alpha, double beta) {
			check_strictly_positive(alpha);
			check_strictly_positive(beta);

			if( x < 0.0 || x > 1.0 )
				return math::negInf();

			if( x == 0.0 ) {
				if( alpha > 1 ) return math::negInf();
				if( alpha < 1 ) return math::posInf();
				return propTo ? 0.0 : std::log(beta);
			}

			if( x == 1.0 ) {
				if( beta > 1 ) return math::negInf();
				if( beta < 1 ) return math::posInf();
				return propTo ? 0.0 : std::log(alpha);
			}

			double logp = (alpha - 1.0) * std::log(x) + (beta - 1.0) * math::log1m(x);
			if( ! propTo )
				logp += std::lgamma(alpha + beta) - std::lgamma(alpha) - std::lgamma(beta);
			return logp;
		}

		static inline double logpdf_dx(double x, double alpha, double beta) {
			check_strictly_positive(alpha);
			check_strictly_positive(beta);

			if( x < 0.0 || x > 1.0 )
				return math::NaN();

			if( x == 0.0 && alpha == 1.0 )
				return (1.0 - beta);

			if( x == 1.0 && beta == 1.0 )
				return alpha - 1.0;

			return (alpha - 1.0) / x + (beta - 1.0) / (x - 1.0);
		}

		static inline GradientType logpdf_grad(double x, double alpha, double beta) {
			check_strictly_positive(alpha);
			check_strictly_positive(beta);

			if( x < 0.0 || x > 1.0 )
				return math::NaN();

			if( x == 0.0 )
				return (alpha == 1.0) ? math::Gradients<2>{math::negInf(), 1 / beta} : math::NaN();

			if( x == 1.0 )
				return (beta == 1.0) ? math::Gradients<2>{1 / alpha, math::negInf()} : math::NaN();

			const double digamma_alpha_beta = math::digamma(alpha + beta);
			return GradientType{
				std::log(x) + digamma_alpha_beta - math::digamma(alpha),
				math::log1m(x) + digamma_alpha_beta - math::digamma(beta)
			};
		}

		template <bool propTo>
		static double neglogpdf(double x, double alpha, double beta) {
			return -logpdf<propTo>(x, alpha, beta);
		}

		static inline double neglogpdf_dx(double x, double alpha, double beta) {
			return -logpdf_dx(x, alpha, beta);
		}

		static inline GradientType neglogpdf_grad(double x, double alpha, double beta) {
			return -logpdf_grad(x, alpha, beta);
		}

		static inline double cdf(double x, double alpha, double beta) {
			check_strictly_positive(alpha);
			check_strictly_positive(beta);

			if(x <= 0.0)
				return 0.0;

			if(x >= 1.0)
				return 1.0;

			return math::ibeta(alpha, beta, x);
		}

		template <typename float_t = double>
		struct beta_dist {
			typedef float_t result_type;

			struct param_type {
				param_type(result_type alpha, result_type beta)
						: alpha(alpha), beta(beta) {}
				result_type alpha, beta;
			};

			beta_dist(result_type alpha, result_type beta)
					: p(alpha, beta), a_d(alpha, 1.0), b_d(beta, 1.0) {
			}

			template <typename R>
			result_type operator()(R &r) {
				result_type x = a_d(r);
				result_type y = b_d(r);
				return x / (x + y);
			}

			template <typename R>
			result_type operator()(R &r, const param_type &p) {
				std::gamma_distribution<double> a_d(p.alpha, 1.0), b_d(p.beta, 1.0);
				result_type x = a_d(r);
				result_type y = b_d(r);
				return x / (x + y);
			}

			param_type p;
			std::gamma_distribution<double> a_d, b_d;
		};

		static RandomDistribution<beta_dist<double>> generator(Random &rng, double alpha, double beta) {
			//check_strictly_positive(alpha, "alpha");
			//check_strictly_positive(beta, "beta");
			return rng.build(beta_dist<double>{alpha, beta});
		}

		static double sample(Random &rng, double alpha, double beta) {
			//check_strictly_positive(alpha, "alpha");
			//check_strictly_positive(beta, "beta");
			beta_dist<double> u{alpha, beta};
			return rng.rand(u);
		}
	};
}
#endif
