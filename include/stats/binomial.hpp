/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_BINOMIAL_H_
#define STATS_BINOMIAL_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/log1m.hpp>
#include <math/ibeta.hpp>
#include <Random.hpp>

namespace stats {

	struct Binomial {

		typedef math::Gradients<1> GradientType;
		typedef int result_type;

		template <bool propTo>
		static double logpdf(int k, int size, double prob) {
			check_nonnegative(size);
			check_bounded01(prob);
			if(k < 0 || k > size)
				return math::negInf();

			if(prob == 0)
				return k == 0 ? 0 : math::negInf();
			if(prob == 1)
				return k == size ? 0 : math::negInf();

			double logp = -std::lgamma(k + 1) - std::lgamma(size - k + 1)
					+ k * std::log(prob) + (size - k) * math::log1m(prob);
			if(!propTo)
				logp += std::lgamma(size + 1);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(int x, int size, double prob) {
			return -logpdf<propTo>(x, size, prob);
		}

		static inline double logpdf_dx(int x, int size, double prob) {
			check_strictly_positive(size);
			check_bounded01(prob);
			return math::NaN();
		}

		static inline double neglogpdf_dx(int x, int size, double prob) {
			return -logpdf_dx(x, size, prob);
		}

		static inline GradientType logpdf_grad(int x, int size, double prob) {
			check_strictly_positive(size);
			check_bounded01(prob);
			return GradientType{(x/prob - size) / (1 - prob)};
		}

		static inline GradientType neglogpdf_grad(int x, int size, double prob) {
			return -logpdf_grad(x, size, prob);
		}

		static inline double cdf(int x, int size, double prob) {
			check_strictly_positive(size);
			check_bounded01(prob);
			return math::ibeta(size - x, x + 1, 1 - prob);
		}

		static RandomDistribution<std::binomial_distribution<int>> generator(Random &rng, int size, double prob) {
			//check_strictly_positive(size, "size");
			//check_bounded(prob, 0.0, 1.0, "binomial probability");
			return rng.build(std::binomial_distribution<int>{size, prob});
		}

		static int sample(Random &rng, int size, double prob) {
			//check_strictly_positive(size, "size");
			//check_bounded(prob, 0.0, 1.0, "binomial probability");
			std::binomial_distribution<int> c{size, prob};
			return rng.rand(c);
		}
	};
}
#endif
