/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_GEOMETRIC_H_
#define STATS_GEOMETRIC_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/log1m.hpp>

namespace stats {

	struct Geometric {

		typedef  math::Gradients<1> GradientType;

		template <bool propTo>
		static double logpdf(int k, double prob) {
			check_between(-prob, -1.0, 0.0); // 0 < prob <= 1

			if(k < 0)
				return math::negInf();

			if(prob == 1)
				return k == 0 ? 0 : math::negInf();

			double logp = k * math::log1m(prob);
			if(!propTo)
				logp += std::log(prob);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(int k, double prob) {
			return -logpdf<propTo>(k, prob);
		}

		static inline double logpdf_dx(int k, double prob) {
			check_between(-prob, -1.0, 0.0);  // 0 < prob <= 1
			return math::NaN();
		}

		static inline double neglogpdf_dx(int k, double prob) {
			return -logpdf_dx(k, prob);
		}

		static inline GradientType logpdf_grad(int k, double prob) {
			check_between(prob, 0.0, 1.0);

			if(k < 0)
				return GradientType{ math::NaN() };
			return GradientType{ (k*prob + prob - 1) / (prob*(prob - 1)) };
			//	return GradientType{ k/(prob - 1) + 1/prob };
		}

		static inline GradientType neglogpdf_grad(int k, double prob) {
			return -logpdf_grad(k, prob);
		}

	};
}
#endif
