/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_MVUNIFORM_H_
#define STATS_MVUNIFORM_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <stats/uniform.hpp>
#include <MatrixVector.hpp>

namespace stats {

	struct MVUniform {

		template <bool propTo, typename... Args>
		static double logpdf(Args && ...args) {
			return -neglogpdf<propTo>(std::forward<Args>(args)...);
		}

		template <bool propTo>
		static double neglogpdf(ConstRefVecd &x, ConstRefVecd &lb, ConstRefVecd &ub) {
			double logp = 0.0;
			for(int i = 0; i < x.size(); ++i) {
				if(x[i] >= lb[i] && x[i] <= ub[i])
					logp += propTo ? 0.0 : std::log(ub[i] - lb[i]);
				else
					return math::negInf();
			}
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(ConstRefVecd &x, double lb = 0.0, double ub = 1.0) {
			for(int i = 0; i < x.size(); ++i) {
				if(x[i] <= lb || x[i] >= ub)
					return math::negInf();
			}

			return propTo ? 0.0 : x.size() * std::log(ub - lb);
		}

		static inline void neglogpdf_dx(ConstRefVecd &x, ConstRefVecd &lb, ConstRefVecd &ub, RefVecd grad) {
			for(int i = 0; i < x.size(); ++i) {
				grad[i] = (x[i] <= lb[i] || x[i] >= ub[i]) ? math::NaN() : 0.0;
			}
		}

		static inline void logpdf_dx(ConstRefVecd &x, ConstRefVecd &lb, ConstRefVecd &ub, RefVecd grad) {
			neglogpdf_dx(x, lb, ub, grad);
		}

		static inline void neglogpdf_dx(ConstRefVecd &x, double lb, double ub, RefVecd grad) {
			for(int i = 0; i < x.size(); ++i) {
				grad[i] = (x[i] <= lb || x[i] >= ub) ? math::NaN() : 0.0;
			}
		}

		static inline void logpdf_dx(ConstRefVecd &x, double lb, double ub, RefVecd grad) {
			neglogpdf_dx(x, lb, ub, grad);
		}

		static inline void neglogpdf_dx(ConstRefVecd &x, RefVecd grad) {
			for(int i = 0; i < x.size(); ++i) {
				grad[i] = (x[i] <= 0.0 || x[i] >= 1.0) ? math::NaN() : 0.0;
			}
		}

		static inline void logpdf_dx(ConstRefVecd &x, RefVecd grad) {
			neglogpdf_dx(x, grad);
		}

		template <typename Derived>
		class mvuniform_dist {
		private:
			template <typename R>
			struct uniform_op {
				R &r;
				mutable std::uniform_real_distribution<typename Derived::Scalar> u{0.0, 1.0};

				uniform_op(R &r) : r(r) {}

				template <typename Index>
				inline double operator()(Index, Index = 0) const {
					return u(r);
				}
			};

		public:
			typedef Derived result_type;
			struct param_type {
				param_type(ConstRefVecd &lb, ConstRefVecd &ub) : lb(lb), ub(ub) {}
				Vecd lb, ub;
			};

			mvuniform_dist(ConstRefVecd &lb, ConstRefVecd &ub) : p(lb, ub) {
			}

			template<typename R>
			result_type operator()(R &r) {
				return operator()(r, p);
			}

			template <typename R>
			result_type operator()(R &r, const param_type &q) {
				Vecd v = Eigen::CwiseNullaryOp<uniform_op<R>, Derived>(q.lb.size(), 1, uniform_op<R>(r));
				v.noalias() = q.lb + v * (q.ub - q.lb);
				return v;
			}

		private:
			param_type p;
		};

		template <typename Derived>
		static RandomDistribution<mvuniform_dist<Derived>> generator(Random &rng,
				ConstRefVecd &lb, ConstRefVecd &ub) {
			return rng.build(mvuniform_dist<Derived>{lb, ub});
		}

		static inline void sample(Random &rng, ConstRefVecd &lb, ConstRefVecd &ub, RefVecd v) {
			v = generate_random<Vecd>(Uniform::generator(rng), lb.size());
			v.noalias() = lb + v.cwiseProduct(ub - lb);
		}

		static inline void sample(Random &rng, ConstRefVecd &lb, ConstRefVecd &ub, Vecd &v) {
			v.resize(lb.size());
			sample(rng, lb, ub, RefVecd(v));
		}

		static inline Vecd sample(Random &rng, ConstRefVecd &lb, ConstRefVecd &ub) {
			Vecd v(lb.size());
			sample(rng, lb, ub, v);
			return v;
		}

		static inline void sample(Random &rng, double lb, double ub, RefVecd v) {
			v = generate_random<Vecd>(Uniform::generator(rng, lb, ub), v.size());
		}

		static inline void sample(Random &rng, RefVecd v) {
			v = generate_random<Vecd>(Uniform::generator(rng, 0.0, 1.0), v.size());
		}

		static inline Vecd sample(Random &rng, int d, double lb=0.0, double ub=1.0) {
			Vecd v(d);
			sample(rng, lb, ub, v);
			return v;
		}
	};
}
#endif
