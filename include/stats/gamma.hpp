/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_GAMMA_H_
#define STATS_GAMMA_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/digamma.hpp>
#include <math/pqgamma.hpp>
#include <Random.hpp>

namespace stats {

	struct Gamma {

		typedef math::Gradients<2> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double shape, double rate) {
			check_nonnegative(shape);  // dirac delta distribution when shape==0
			check_nonnegative(rate);  // dirac delta distribution when rate==0

			if( x < 0.0 || !math::isFinite(x) )
				return math::negInf();

			if( shape == 0  ||  rate == 0 )
				return (x == 0) ? math::posInf() : math::negInf();

			if( x == 0 ) {
				if( shape < 1 )
					return math::posInf();
				if( shape > 1 )
					return math::negInf();

				return propTo ? 0.0 : std::log(rate);
			}

			double logp = -x * rate + (shape - 1.0) * std::log(x);
			if( ! propTo )
				logp +=	shape * std::log(rate) - std::lgamma(shape);
			return  logp;
		}

		static inline double logpdf_dx(double x, double shape, double rate) {
			check_strictly_positive(shape);
			check_strictly_positive(rate);
			check_nonnegative(x);

			if( x == 0 )
				return (shape == 1) ? -rate : math::NaN();

			return (shape-1)/x - rate;
		}

		static inline GradientType logpdf_grad(double x, double shape, double rate) {
			check_strictly_positive(shape);
			check_strictly_positive(rate);
			check_nonnegative(x);

			if( x == 0 )
				return GradientType{
					math::NaN(),
					shape == 1 ? 1 / rate : math::NaN()
				};

			return GradientType{
				-math::digamma(shape) + std::log(rate) + std::log(x),
				shape / rate - x
			};
		}

		template <bool propTo>
		static double neglogpdf(double x, double shape, double rate) {
			return -logpdf<propTo>(x, shape, rate);
		}

		static inline double neglogpdf_dx(double x, double shape, double rate) {
			return -logpdf_dx(x, shape, rate);
		}

		static inline GradientType neglogpdf_grad(double x, double shape, double rate) {
			return -logpdf_grad(x, shape, rate);
		}

		static inline double cdf(double x, double shape, double rate) {
			check_nonnegative(shape);
			check_nonnegative(rate);
			check_nonnegative(x);
			return math::pgamma(x*rate, shape);
		}

		static RandomDistribution<std::gamma_distribution<double>> generator(Random &rng, double shape, double rate) {
			//check_less_or_equal(lb, ub, "lb <= ub");
			return rng.build(std::gamma_distribution<double>{shape, 1.0 / rate});
		}

		static double sample(Random &rng, double shape, double rate) {
			//check_less_or_equal(lb, ub, "lb <= ub");
			std::gamma_distribution<double> g{shape, 1.0 / rate};
			return rng.rand(g);
		}
	};
}
#endif
