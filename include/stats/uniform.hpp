/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_UNIFORM_H_
#define STATS_UNIFORM_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <Random.hpp>
#include <MatrixVector.hpp>

namespace stats {

	struct Uniform {

		typedef math::Gradients<2> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double lb, double ub) {
			check_less_or_equal(lb, ub);

			if( x < lb || x > ub )
				return math::negInf();

			if(!math::isFinite(lb) || !math::isFinite(ub))
				return 0.0;

			return propTo ? 0.0 : -std::log(ub - lb);
		}

		static inline double logpdf_dx(double x, double lb, double ub) {
			check_less_or_equal(lb, ub);

			if( x <= lb || x >= ub )
				return math::NaN();

			return 0.0;
		}

		static inline GradientType logpdf_grad(double x, double lb, double ub) {
			check_less_or_equal(lb, ub);

			if( x <= lb || x >= ub )
				return math::NaN();

			const double one_over_ub_min_lb = 1.0 / (ub - lb);
			return GradientType{ one_over_ub_min_lb, -one_over_ub_min_lb };
		}

		template <bool propTo>
		static double neglogpdf(double x, double lb, double ub) {
			return -logpdf<propTo>(x, lb, ub);
		}

		static inline double neglogpdf_dx(double x, double lb, double ub) {
			return -logpdf_dx(x, lb, ub);
		}

		static inline GradientType neglogpdf_grad(double x, double lb, double ub) {
			return -logpdf_grad(x, lb, ub);
		}

		static inline double cdf(double x, double lb=0.0, double ub=1.0) {
			check_less_or_equal(lb, ub);

			if(x >= ub)
				return 1.0;

			if(x <= lb)
				return 0.0;

			return (x - lb) / (ub - lb);
		}

		static inline double icdf(double p, double lb=0.0, double ub=1.0) {
			check_less_or_equal(lb, ub);
			check_bounded01(p);

			return lb + p * (ub - lb);
		}

		static RandomDistribution<std::uniform_real_distribution<double>> generator(Random &rng) {
			return rng.build(std::uniform_real_distribution<double>{});
		}

		static RandomDistribution<std::uniform_real_distribution<double>> generator(Random &rng, double lb, double ub) {
			//check_less_or_equal(lb, ub, "lb <= ub");
			return rng.build(std::uniform_real_distribution<double>{lb, ub});
		}

		static double sample(Random &rng) {
			std::uniform_real_distribution<double> d;
			return rng.rand(d);
		}

		static double sample(Random &rng, double lb, double ub) {
			//check_less_or_equal(lb, ub, "lb <= ub");
			std::uniform_real_distribution<double> u{lb, ub};
			return rng.rand(u);
		}

		struct uniform_vec_bound_op {
			Random &rng;
			const Vecd &lb;
			const Vecd &ub;

			uniform_vec_bound_op(Random &rng, const Vecd &lb, const Vecd &ub)
					: rng(rng), lb(lb), ub(ub) {}

			template <typename Index>
			inline double operator()(Index i, Index = 0) const {
				return sample(rng, lb[i], ub[i]);
			}
		};

		template <typename Derived>
		static Eigen::CwiseNullaryOp<uniform_vec_bound_op, Derived> sample(Random &rng, const Vecd &lb, const Vecd &ub) {
			Require((lb.rows() == ub.rows()) && (lb.rows() == ub.rows()), "lower and upper bound should be same size");
			return Eigen::CwiseNullaryOp<uniform_vec_bound_op, Derived>(lb.rows(), lb.cols(), uniform_vec_bound_op(rng, lb, ub));
		}
	};
}
#endif
