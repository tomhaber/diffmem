/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_TRUNC_NORMAL_H_
#define STATS_TRUNC_NORMAL_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <stats/normal.hpp>
#include <math/inv_phi.hpp>
#include <Random.hpp>

namespace stats {

	struct Truncnormal {

		typedef math::Gradients<4> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double mu=0.0, double sigma=1.0, double lb=math::negInf(), double ub=math::posInf()) {
			check_less_or_equal(lb, ub);
			check_nonnegative(sigma);

			if( sigma == 0.0 )
				return math::negInf();

			if(lb >= ub || x < lb || x > ub) {
				if(lb == ub && x == lb)
					return 0;
				return math::negInf();
			}

			double dst_lpnorm = (lb == math::negInf()) ? 0.0f : Normal::cdf(lb, mu, sigma);
			double dst_rpnorm = (ub == math::posInf()) ? 1.0f : Normal::cdf(ub, mu, sigma);
			double logp = Normal::logpdf<propTo>(x, mu, sigma);
			if(!propTo)
				logp += -std::log(dst_rpnorm - dst_lpnorm);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(double x, double mu=0.0, double sigma=1.0, double lb=math::negInf(), double ub=math::posInf()) {
			return -logpdf<propTo>(x, mu, sigma, lb, ub);
		}

		static inline double logpdf_dx(double x, double mu=0.0, double sigma=1.0, double lb=math::negInf(), double ub=math::posInf()) {
			check_strictly_between(x, lb, ub);
			check_strictly_positive(sigma);
			return (mu - x) / (sigma*sigma);
		}

		static inline double neglogpdf_dx(double x, double mu=0.0, double sigma=1.0, double lb=math::negInf(), double ub=math::posInf()) {
			return -logpdf_dx(x, mu, sigma);
		}

		static inline GradientType logpdf_grad(double x, double mu = 0.0, double sigma = 1.0, double lb = math::negInf(), double ub = math::posInf()) {
			check_strictly_between(x, lb, ub);
			check_strictly_positive(sigma);
			check_less(lb, ub);
			const double y = (x - mu) / sigma;
			const double dst_lpnorm = (math::isNegInf(lb)) ? 0.0 : Normal::cdf(lb, mu, sigma);
			const double dst_rpnorm = (math::isPosInf(ub)) ? 1.0 : Normal::cdf(ub, mu, sigma);
			const double nds2p = (dst_rpnorm - dst_lpnorm) * math::sqrt2Pi();
			const double yl = (lb - mu) / sigma;
			const double yu = (ub - mu) / sigma;
			const double expyl = (math::isNegInf(lb)) ? 0 : std::exp(-0.5*yl*yl) / nds2p;
			const double expyu = (math::isPosInf(ub)) ? 0 : std::exp(-0.5*yu*yu) / nds2p;
			const double ylexpyl = (math::isNegInf(lb)) ? 0 : yl*expyl;
			const double yuexpyu = (math::isPosInf(ub)) ? 0 : yu*expyu;

			return GradientType{
				//diff mu
				(expyu - expyl + y) / sigma,
				//diff sigma: (-pi*nd*sigma*exp(((lb - mu)**2 + (mu - ub)**2) / (2 * sigma**2)) - pi*nd*(mu - x)*y(x, mu, sigma)*exp(((lb - mu)**2 + (mu - ub)**2) / (2 * sigma**2)) + 0.5*sqrt(2)*sqrt(pi)*((lb - mu)*exp((mu - ub)**2 / (2 * sigma**2)) + (mu - ub)*exp((lb - mu)**2 / (2 * sigma**2))))*exp((-(lb - mu)**2 - (mu - ub)**2) / (2 * sigma**2)) / (pi*nd*sigma**2)
				(yuexpyu - ylexpyl + y*y - 1) / sigma,
				//diff lb
				math::isNegInf(lb) ? math::NaN() : expyl / sigma,
				//diff ub
				math::isPosInf(ub) ? math::NaN() : -expyu / sigma
			};
		}

		static inline GradientType neglogpdf_grad(double x, double mu=0.0, double sigma=1.0, double lb=math::negInf(), double ub=math::posInf()) {
			return -logpdf_grad(x, mu, sigma, lb, ub);
		}

		static inline double cdf(double x, double mu=0.0, double sigma=1.0, double lb=math::negInf(), double ub=math::posInf()) {
			check_less_or_equal(lb, ub);
			check_nonnegative(sigma);

			const double dst_lpnorm = (lb == math::negInf()) ? 0.0f : Normal::cdf(lb, mu, sigma);
			const double dst_rpnorm = (ub == math::posInf()) ? 1.0f : Normal::cdf(ub, mu, sigma);
			return (Normal::cdf(x, mu, sigma) - dst_lpnorm) / (dst_rpnorm - dst_lpnorm);
		}

		static inline double icdf(double p, double mu=0.0, double sigma=1.0, double lb=math::negInf(), double ub=math::posInf()) {
			check_less_or_equal(lb, ub);
			check_nonnegative(sigma);
			check_bounded01(p);

			const double dst_lpnorm = (lb == math::negInf()) ? 0.0f : Normal::cdf(lb, mu, sigma);
			const double dst_rpnorm = (ub == math::posInf()) ? 1.0f : Normal::cdf(ub, mu, sigma);
			const double x = p * (dst_rpnorm - dst_lpnorm) + dst_lpnorm;
			return math::inv_Phi(x) * sigma + mu;
		}

		class truncated_normal_dist {
		public:
			typedef double result_type;

			struct param_type {
				param_type(double mu = 0.0, double sigma = 1.0,
						double lb = math::negInf(), double ub = math::posInf())
						: mu(mu), sigma(sigma) {
					phi_lb = (lb == math::negInf()) ? 0.0 : Normal::cdf(lb, mu, sigma);
					phi_ub = (ub == math::posInf()) ? 1.0 : Normal::cdf(ub, mu, sigma);
				}

				double mu, sigma;
				double phi_lb, phi_ub;
			};

			truncated_normal_dist(double mu = 0.0, double sigma = 1.0,
					double lb = math::negInf(), double ub = math::posInf())
					: p(mu, sigma, lb, ub) {}

			truncated_normal_dist(const param_type &q) : p(q) {}

			template <typename R>
			double operator()(R &r) const {
				return (*this)(r, p);
			}

			template <typename R>
			double operator()(R & r, const param_type & q) const {
				std::uniform_real_distribution<double> u(q.phi_lb, q.phi_ub);
				return math::inv_Phi(u(r)) * q.sigma + q.mu;
			}

		private:
			param_type p;
		};

		static RandomDistribution<truncated_normal_dist> generator(Random &rng,
				double mu = 0.0, double sigma = 1.0, double lb = math::negInf(), double ub = math::posInf()) {
			return rng.build(truncated_normal_dist{mu, sigma, lb, ub});
		}

		static double sample(Random &rng, double mu = 0.0, double sigma = 1.0,
				double lb = math::negInf(), double ub = math::posInf()) {
			truncated_normal_dist n{mu, sigma, lb, ub};
			return rng.rand(n);
		}
	};
}
#endif
