/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_NEGBINOMIAL_H_
#define STATS_NEGBINOMIAL_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/log1m.hpp>
#include <math/digamma.hpp>
#include <math/ibeta.hpp>
#include <Random.hpp>

namespace stats {

	struct Negbinomial {

		typedef math::Gradients<2> GradientType;
		typedef int result_type;

		template <bool propTo>
		static double logpdf(int k, double size, double prob) {
			check_nonnegative(size);
			check_between(-prob, -1.0, 0.0); // 0 < prob <= 1

			if(k < 0)
				return math::negInf();

			if(size == 0)
				return k == 0 ? 0 : math::negInf();

			if(prob == 1)
				return k == 0 ? 0 : math::negInf();

			double logp = k * math::log1m(prob) - std::lgamma(k + 1) + std::lgamma(size + k);
			if(!propTo)
				logp += size * std::log(prob) - std::lgamma(size);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(int k, double size, double prob) {
			return -logpdf<propTo>(k, size, prob);
		}

		static inline double logpdf_dx(int k, double size, double prob) {
			check_nonnegative(k);
			check_strictly_positive(size);
			check_between(-prob, -1.0, 0.0);
			return math::NaN();
		}

		static inline double neglogpdf_dx(int k, double size, double prob) {
			return -logpdf_dx(k, size, prob);
		}

		static inline GradientType logpdf_grad(int k, double size, double prob) {
			check_nonnegative(k);
			check_strictly_positive(size);
			check_strictly_between(prob, 0.0, 1.0);
			return GradientType{
				std::log(prob) - math::digamma(size) + math::digamma(size + k),
				k / (prob - 1) + size / prob
			};
		}

		static inline GradientType neglogpdf_grad(int k, double size, double prob) {
			return -logpdf_grad(k, size, prob);
		}

		static inline double cdf(int k, double size, double prob) {
			check_nonnegative(k);
			check_nonnegative(size);
			check_between(-prob, -1.0, 0.0); // 0 < prob <= 1

			if(size == 0.0)
				return 1.0;

			return math::ibeta(size, k + 1, prob);
		}

		static RandomDistribution<std::negative_binomial_distribution<int>> generator(Random &rng, double size, double prob) {
			if(floor(size) != size)
				notYetImplemented("negbinom rng only implemented for natural size");

			return rng.build(std::negative_binomial_distribution<int>{int(size), prob});
		}

		static int sample(Random &rng, double size, double prob) {
			if(floor(size) != size)
				notYetImplemented("negbinom rng only implemented for natural size");

			std::negative_binomial_distribution<int> c{int(size), prob};
			return rng.rand(c);
		}
	};
}
#endif
