/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_LOGNORMAL_H_
#define STATS_LOGNORMAL_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <Random.hpp>

namespace stats {

	struct Lognormal {

		typedef math::Gradients<2> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double neglogpdf(double x, double mu, double sigma) {
			check_nonnegative(sigma);

			if( x <= 0.0 || !math::isFinite(x) )
				return math::posInf();

			if( sigma == 0.0 )
				return (mu == std::log(x)) ? math::negInf() : math::posInf();

			double logp = pow((mu - std::log(x)) / sigma, 2) / 2.0 + std::log(x);
			if( !propTo )
				logp += math::log2Pi() / 2.0 + std::log(sigma);

			return logp;
		}

		static inline double logpdf_dx(double x, double mu, double sigma) {
			check_strictly_positive(sigma);
			check_nonnegative_finite(x);

			return (mu - sigma * sigma - std::log(x)) / (sigma * sigma * x);
		}

		static inline GradientType logpdf_grad(double x, double mu, double sigma) {
			check_strictly_positive(sigma);
			check_strictly_positive_finite(x);

			const double logx_min_mu_div_sigma2 = (std::log(x) - mu) / (sigma*sigma);
			return GradientType{
				logx_min_mu_div_sigma2, (logx_min_mu_div_sigma2 * (std::log(x) - mu) - 1) / sigma
			};
		}

		template <bool propTo>
		static double logpdf(double x, double alpha, double beta) {
			return -neglogpdf<propTo>(x, alpha, beta);
		}

		static inline double neglogpdf_dx(double x, double alpha, double beta) {
			return -logpdf_dx(x, alpha, beta);
		}

		static inline GradientType neglogpdf_grad(double x, double alpha, double beta) {
			return -logpdf_grad(x, alpha, beta);
		}

		static inline double cdf(double x, double mu=0.0, double sigma=1.0) {
			check_nonnegative(sigma);
			return 0.5 * (1.0 + std::erf((std::log(x) - mu) / (math::sqrt2()*sigma)) );
		}

		static RandomDistribution<std::lognormal_distribution<double>> generator(Random &rng, double mu=0.0, double sigma=1.0) {
			//check_less_or_equal(lb, ub, "lb <= ub");
			return rng.build(std::lognormal_distribution<double>{mu, sigma});
		}

		static double sample(Random &rng, double mu=0.0, double sigma=1.0) {
			std::lognormal_distribution<double> n{mu, sigma};
			return rng.rand(n);
		}
	};
}
#endif
