/*
* Copyright (c) 2016, Hasselt University
* All rights reserved.

* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STATS_PARETO_H_
#define STATS_PARETO_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <Random.hpp>

namespace stats {

	struct Pareto {

		typedef math::Gradients<2> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double scale, double shape) {
			check_strictly_positive_finite(scale);
			check_strictly_positive(shape);

			if(x < scale || !math::isFinite(x))
				return math::negInf();
			if(math::isPosInf(shape))
				return (x == scale) ? math::posInf() : math::negInf();

			double logp = -(shape + 1)*std::log(x);
			if(!propTo)
				logp += shape*std::log(scale) + std::log(shape);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(double x, double scale, double shape) {
			return -logpdf<propTo>(x, scale, shape);
		}

		static inline double logpdf_dx(double x, double scale, double shape) {
			check_nonnegative_finite(x - scale);
			check_strictly_positive_finite(scale);
			check_strictly_positive_finite(shape);
			return -(shape + 1)/x;
		}

		static inline double neglogpdf_dx(double x, double scale, double shape) {
			return -logpdf_dx(x, scale, shape);
		}

		static inline GradientType logpdf_grad(double x, double scale, double shape) {
			check_nonnegative_finite(x - scale);
			check_strictly_positive_finite(scale);
			check_strictly_positive_finite(shape);
			return GradientType{ shape / scale, std::log(scale) - std::log(x) + 1.0 / shape };
		}

		static inline GradientType neglogpdf_grad(double x, double scale, double shape) {
			return -logpdf_grad(x, scale, shape);
		}

		static inline double cdf(double x, double scale, double shape) {
			check_strictly_positive_finite(scale);
			check_strictly_positive(shape);

			if(x <= scale)
				return 0.0;

			return 1.0 - std::pow((scale / x), shape);
		}

		struct pareto_distribution {
			typedef double result_type;

			struct param_type {
				param_type(double scale, double shape) : scale(scale), shape(shape) {}
				double scale, shape;
			};

			pareto_distribution(double scale, double shape) : p(scale, shape) {}

			template <typename R>
			double operator()(R &r) const {
				return (*this)(r, p);
			}

			template <typename R>
			double operator()(R &r, const param_type &q) const {
				std::exponential_distribution<double> e{q.shape};
				return q.scale * std::exp(e(r));
			}

			param_type p;
		};

		static RandomDistribution<pareto_distribution> generator(Random &rng, double scale, double shape) {
			return rng.build(pareto_distribution{scale, shape});
		}

		static double sample(Random &rng, double scale, double shape) {
			pareto_distribution p(scale, shape);
			return rng.rand(p);
		}
	};
}
#endif
