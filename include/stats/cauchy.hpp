/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_CAUCHY_H_
#define STATS_CAUCHY_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/atanpi.hpp>
#include <Random.hpp>

namespace stats {

	struct Cauchy {

		typedef math::Gradients<2> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double loc, double scale) {
			check_strictly_positive_finite(scale);

			const double y = (x - loc) / scale;
			double logp = -std::log1p(y * y);
			if( ! propTo )
				logp += -math::logPi() - std::log(scale);
			return logp;
		}

		static inline double logpdf_dx(double x, double loc, double scale) {
			check_strictly_positive_finite(scale);

			const double y = (x - loc);
			return -2.0 * y / (y*y + scale*scale);
		}

		static inline GradientType logpdf_grad(double x, double loc, double scale) {
			check_strictly_positive_finite(scale);

			const double x_min_loc_squared = (x - loc)*(x - loc);
			const double scale_squared = scale * scale;
			return GradientType{
				2 * (x - loc) / (scale_squared + x_min_loc_squared),
				(x_min_loc_squared - scale_squared) / scale / (scale_squared + x_min_loc_squared)
			};
		}

		template <bool propTo>
		static double neglogpdf(double x, double loc, double scale) {
			return -logpdf<propTo>(x, loc, scale);
		}

		static inline double neglogpdf_dx(double x, double loc, double scale) {
			return -logpdf_dx(x, loc, scale);
		}

		static inline GradientType neglogpdf_grad(double x, double loc, double scale) {
			return -logpdf_grad(x, loc, scale);
		}

		static inline double cdf(double x, double loc, double scale) {
			check_strictly_positive_finite(scale);
			x = (x - loc) / scale;
			return math::atanpi(x) + 0.5;
		}

		static RandomDistribution<std::cauchy_distribution<double>> generator(Random &rng, double loc, double scale) {
			//check_strictly_positive_finite(scale, "scale");
			return rng.build(std::cauchy_distribution<double>{loc, scale});
		}

		static double sample(Random &rng, double loc, double scale) {
			//check_strictly_positive_finite(scale, "scale");
			std::cauchy_distribution<double> c{loc, scale};
			return rng.rand(c);
		}
	};
}
#endif
