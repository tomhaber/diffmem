/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_WEIBULL_H_
#define STATS_WEIBULL_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <Random.hpp>

namespace stats {

	struct Weibull {

		typedef math::Gradients<2> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double shape, double scale) {
			check_nonnegative(shape);
			check_nonnegative(scale);

			if( shape == 0 || scale == 0 )
				return x == 0 ? math::posInf() : math::negInf();

			if( x < 0.0 || ! math::isFinite(x) )
				return math::negInf();

			if( x == 0.0 && shape == 1.0 )
				return propTo ? 0.0 : -std::log(scale);

			double logp = -std::pow(x / scale, shape) + (shape - 1) * std::log(x);
			if( ! propTo )
				logp += std::log(shape) - shape * std::log(scale);
			return logp;
		}

		static inline double logpdf_dx(double x, double shape, double scale) {
			check_strictly_positive(shape);
			check_strictly_positive(scale);
			check_nonnegative_finite(x);

			if( x == 0.0 && shape == 1.0 )
				return -1.0/scale;

			return (shape * (1.0 - std::pow(x / scale, shape)) - 1.0) / x;
		}

		static inline GradientType logpdf_grad(double x, double shape, double scale) {
			check_strictly_positive(shape);
			check_strictly_positive(scale);
			check_nonnegative_finite(x);

			const double tmp = std::pow(x / scale, shape);
			return GradientType{
				1.0/shape + (1.0 - tmp) * (std::log(x) - std::log(scale)),
				(shape / scale) * ( tmp - 1.0 )
			};
		}

		template <bool propTo>
		static double neglogpdf(double x, double shape, double scale) {
			return -logpdf<propTo>(x, shape, scale);
		}

		static inline double neglogpdf_dx(double x, double shape, double scale) {
			return -logpdf_dx(x, shape, scale);
		}

		static inline GradientType neglogpdf_grad(double x, double shape, double scale) {
			return -logpdf_grad(x, shape, scale);
		}

		static inline double cdf(double x, double shape, double scale) {
			check_nonnegative(shape);
			check_nonnegative(scale);

			return 1.0 - std::exp(-std::pow(x / scale, shape));
		}

		static RandomDistribution<std::weibull_distribution<double>> generator(Random &rng, double shape, double scale) {
			return rng.build(std::weibull_distribution<double>{shape, scale});
		}

		static double sample(Random &rng, double shape, double scale) {
			std::weibull_distribution<double> w{shape, scale};
			return rng.rand(w);
		}
	};
}
#endif
