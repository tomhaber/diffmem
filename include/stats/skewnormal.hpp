/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_SKEWNORMAL_H_
#define STATS_SKEWNORMAL_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/log1p.hpp>

namespace stats {

	struct Skewnormal {

		typedef math::Gradients<3> GradientType;

		template <bool propTo>
		static double logpdf(double x, double mu=0.0, double sigma=1.0, double shape=0.0) {
			check_nonnegative(sigma);

			if( sigma == 0.0 )
				return x == mu ? math::posInf() : math::negInf();

			const double y = (x - mu) / sigma;
			double logp = -0.5 * y * y + std::log(std::erfc(-shape*y / math::sqrt2()));
			if( ! propTo )
				logp += -math::logSqrt2Pi() - std::log(sigma);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(double x, double mu=0.0, double sigma=1.0, double shape=0.0) {
			return -logpdf<propTo>(x, mu, sigma, shape);
		}

		static inline double logpdf_dx(double x, double mu=0.0, double sigma=1.0, double shape=0.0) {
			check_strictly_positive(sigma);
			const double y = (x - mu) / sigma;
			const double sy = shape * y;
			const double cdfy = std::erf(sy * math::oneOverSqrt2()) + 1;
			const double expss = std::exp(sy*sy / 2);
			return (-y + std::sqrt(2 / math::pi())*shape / (cdfy*expss)) / sigma;
		}

		static inline double neglogpdf_dx(double x, double mu=0.0, double sigma=1.0, double shape=0.0) {
			return -logpdf_dx(x, mu, sigma, shape);
		}

		static inline GradientType logpdf_grad(double x, double mu=0.0, double sigma=1.0, double shape=0.0) {
			check_strictly_positive(sigma);
			const double y = (x - mu) / sigma;
			const double sy = shape * y;
			const double cdfy = std::erf(sy * math::oneOverSqrt2()) + 1;
			const double expss = std::exp(sy*sy / 2);
			const double aux = std::sqrt(2 / math::pi()) / (cdfy*expss);
			return GradientType{
				(y - aux*shape) / sigma,
				((y - aux*shape) * y - 1.0) / sigma,
				aux*y
			};
		}

		static inline GradientType neglogpdf_grad(double x, double mu=0.0, double sigma=1.0, double shape=0.0) {
			return -logpdf_grad(x, mu, sigma, shape);
		}

	};
}
#endif
