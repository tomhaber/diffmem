/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_STUDENTT_H_
#define STATS_STUDENTT_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/digamma.hpp>
#include <math/ibeta.hpp>
#include <Random.hpp>

namespace stats {

	struct Studentt {

		typedef math::Gradients<1> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double nu) {
			check_strictly_positive(nu);
			if (!math::isFinite(x))
				return math::negInf();

			if (math::isPosInf(nu))
				return -0.5*x*x + (propTo ? 0 : 0.5*(-math::logPi() - math::log2()));

			double logp = -0.5*(nu + 1)*std::log(nu + x*x);
			if( ! propTo )
				logp += std::lgamma(0.5*(nu + 1)) - std::lgamma(0.5*nu) + 0.5*(nu*std::log(nu) - math::logPi());
			return logp;
		}

		static inline double logpdf_dx(double x, double nu) {
			check_strictly_positive(nu);
			if (math::isPosInf(nu))
				return -x;
			return -x*(nu + 1) / (nu + x*x);
		}

		static inline GradientType logpdf_grad(double x, double nu) {
			check_strictly_positive_finite(nu);

			const double nuxx = nu + x*x;
			return GradientType{
				0.5*(math::digamma(0.5*(nu + 1)) - math::digamma(0.5*nu) + std::log(nu) - std::log(nuxx) + 1 - (nu + 1) / nuxx)
			};
		}

		template <bool propTo>
		static double neglogpdf(double x, double nu) {
			return -logpdf<propTo>(x, nu);
		}

		static inline double neglogpdf_dx(double x, double nu) {
			return -logpdf_dx(x, nu);
		}

		static inline GradientType neglogpdf_grad(double x, double nu) {
			return -logpdf_grad(x, nu);
		}

		static inline double cdf(double x, double nu) {
			check_strictly_positive(nu);

			auto q = nu / (x * x);
			auto r = 1.0 / (1.0 + q);
			auto z = (q < 2) ?
					math::ibeta(0.5 * nu, 0.5, 1.0 - r) :
					1.0 - math::ibeta(0.5 * nu, 0.5, 1.0 - r);
			return x > 0 ? 1.0 - 0.5 * z : 0.5 * z;
		}

		static RandomDistribution<std::student_t_distribution<double>> generator(Random &rng, double nu) {
			return rng.build(std::student_t_distribution<double>{nu});
		}

		static double sample(Random &rng, double nu) {
			std::student_t_distribution<double> t{nu};
			return rng.rand(t);
		}
	};
}
#endif
