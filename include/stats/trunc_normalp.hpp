/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_TRUNC_NORMALP_H_
#define STATS_TRUNC_NORMALP_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <stats/normalp.hpp>

namespace stats {

	struct Truncnormalp {

		typedef  math::Gradients<4> GradientType;

		template <bool propTo>
		static double logpdf(double x, double mu, double precision, double lb=math::negInf(), double ub=math::posInf()) {
			check_less_or_equal(lb, ub);
			check_nonnegative(precision);

			if(lb >= ub || x < lb || x > ub) {
				if(lb == ub && x == lb)
					return 0;
				return math::negInf();
			}

			double dst_lpnorm = (lb == math::negInf()) ? 0.0f : Normalp::cdf(lb, mu, precision);
			double dst_rpnorm = (ub == math::posInf()) ? 1.0f : Normalp::cdf(ub, mu, precision);
			double logp = Normalp::logpdf<propTo>(x, mu, precision);
			if(!propTo)
				logp += -std::log(dst_rpnorm - dst_lpnorm);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(double x, double mu=0.0, double precision=1.0, double lb=math::negInf(), double ub=math::posInf()) {
			return -logpdf<propTo>(x, mu, precision, lb, ub);
		}

		static inline double logpdf_dx(double x, double mu=0.0, double precision=1.0, double lb=math::negInf(), double ub=math::posInf()) {
			check_strictly_between(x, lb, ub);
			check_nonnegative(precision);
			return (mu - x) * precision;
		}

		static inline double neglogpdf_dx(double x, double mu=0.0, double precision=1.0, double lb=math::negInf(), double ub=math::posInf()) {
			return -logpdf_dx(x, mu, precision, lb, ub);
		}

		static inline GradientType logpdf_grad(double x, double mu = 0.0, double precision = 1.0, double lb = math::negInf(), double ub = math::posInf()) {
			check_strictly_between(x, lb, ub);
			check_strictly_positive(precision);
			check_less(lb, ub);

			const double sqrtprec = std::sqrt(precision);
			const double y = (x - mu) * sqrtprec;
			const double dst_lpnorm = (math::isNegInf(lb)) ? 0.0 : Normalp::cdf(lb, mu, precision);
			const double dst_rpnorm = (math::isPosInf(ub)) ? 1.0 : Normalp::cdf(ub, mu, precision);
			const double nds2p = (dst_rpnorm - dst_lpnorm) * math::sqrt2Pi();
			const double yl = (lb - mu) * sqrtprec;
			const double yu = (ub - mu) * sqrtprec;
			const double expyl = (math::isNegInf(lb)) ? 0 : std::exp(-0.5*yl*yl) / nds2p;
			const double expyu = (math::isPosInf(ub)) ? 0 : std::exp(-0.5*yu*yu) / nds2p;
			const double ylexpyl = (math::isNegInf(lb)) ? 0 : yl*expyl;
			const double yuexpyu = (math::isPosInf(ub)) ? 0 : yu*expyu;

			return GradientType{
				//diff mu
				(expyu - expyl + y) * sqrtprec,
				//diff precision
				//(0.5*math::sqrt2()*std::sqrt(math::pi())*(-yl*exp((0.5)*(yu * yu)) + yu*exp((0.5)*(yl * yl))) + 0.5*math::pi()*(-(y * y) + 1)*(erf((0.5)*math::sqrt2()*yl) - erf((0.5)*math::sqrt2()*yu))*exp((0.5)*(yl * yl) + (0.5)*(yu * yu)))*exp(-0.5*(yl * yl) - 0.5*(yu * yu)) / (math::pi()*precision*(erf((0.5)*math::sqrt2()*yl) - erf((0.5)*math::sqrt2()*yu))),
				0.5*(ylexpyl - yuexpyu - y*y + 1) / precision,
				//diff lb
				math::isNegInf(lb) ? math::NaN() : expyl * sqrtprec,
				//diff ub
				math::isPosInf(ub) ? math::NaN() : -expyu * sqrtprec
			};
		}

		static inline GradientType neglogpdf_grad(double x, double mu=0.0, double precision=1.0, double lb=math::negInf(), double ub=math::posInf()) {
			return -logpdf_grad(x, mu, precision, lb, ub);
		}
	};
}
#endif
