/*
* Copyright (c) 2016, Hasselt University
* All rights reserved.

* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STATS_EXPONENTIAL_H_
#define STATS_EXPONENTIAL_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <Random.hpp>

namespace stats {

	struct Exponential {

		typedef math::Gradients<1> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double rate) {
			check_nonnegative_finite(rate);
			if (rate == 0)
				return x == 0 ? math::posInf() : math::negInf();
			if ( x < 0 || !math::isFinite(x) )
				return math::negInf();
			double logp = -rate*x;
			if ( ! propTo )
				logp += std::log(rate);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(double x, double rate) {
			return -logpdf<propTo>(x, rate);
		}

		static inline double logpdf_dx(double x, double rate) {
			check_strictly_positive_finite(x);
			check_strictly_positive_finite(rate);
			return -rate;
		}

		static inline GradientType logpdf_grad(double x, double rate) {
			check_strictly_positive_finite(x);
			check_strictly_positive_finite(rate);
			return GradientType{ 1.0 / rate - x };
		}

		static inline GradientType neglogpdf_grad(double x, double rate) {
			return -logpdf_grad(x, rate);
		}

		static inline double cdf(double x, double rate) {
			check_nonnegative_finite(rate);
			return 1.0 - std::exp(-rate * x);
		}

		static inline double icdf(double p, double rate) {
			check_nonnegative_finite(rate);
			check_bounded01(p);
			return -std::log1p(-p) / rate;
		}

		static RandomDistribution<std::exponential_distribution<double>> generator(Random &rng, double rate) {
			return rng.build(std::exponential_distribution<double>{rate});
		}

		static double sample(Random &rng, double rate) {
			std::exponential_distribution<double> e{rate};
			return rng.rand(e);
		}
	};
}
#endif
