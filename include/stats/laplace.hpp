/*
* Copyright (c) 2016, Hasselt University
* All rights reserved.

* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the <organization> nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
* ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef STATS_LAPLACE_H_
#define STATS_LAPLACE_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/sgn.hpp>
#include <Random.hpp>

namespace stats {

	struct Laplace {

		typedef math::Gradients<2> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double loc, double scale) {
			check_strictly_positive_finite(scale);
			if ( !math::isFinite(x) )
				return math::negInf();
			double logp = -std::abs(loc - x)/scale;
			if ( ! propTo )
				logp += -std::log(scale) - math::log2();
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(double x, double loc, double scale) {
			return -logpdf<propTo>(x, loc, scale);
		}

		static inline double logpdf_dx(double x, double loc, double scale) {
			check_strictly_positive_finite(scale);
			return (((loc - x) > 0) - ((loc - x) < 0)) / scale;
		}

		static inline double neglogpdf_dx(double x, double loc, double scale) {
			return -logpdf_dx(x, loc, scale);
		}

		static inline GradientType logpdf_grad(double x, double loc, double scale) {
			check_strictly_positive_finite(scale);
			return GradientType{
				(((loc - x) < 0) - ((loc - x) > 0)) / scale,
				(std::abs(loc - x) - scale) / (scale*scale) };
		}

		static inline GradientType neglogpdf_grad(double x, double loc, double scale) {
			return -logpdf_grad(x, loc, scale);
		}

		static inline double cdf(double x, double loc, double scale) {
			return (x < loc) ?
				(0.5 * std::exp((x - loc) / scale)) :
				1.0 - (0.5 * std::exp((loc - x) / scale));
		}

		struct laplace_distribution {
			typedef double result_type;

			struct param_type {
				param_type(double loc, double scale) : loc(loc), scale(scale) {}
				double loc, scale;
			};

			laplace_distribution(double loc, double scale) : p(loc, scale) {}

			template <typename R>
			double operator()(R &r) const {
				return (*this)(r, p);
			}

			template <typename R>
			double operator()(R &r, const param_type &q) const {
				std::uniform_real_distribution<double> ud{-0.5, 0.5};
				const double u = ud(r);
				return q.loc - q.scale * math::sgn(u) * std::log(1.0 - std::abs(u));
			}

			param_type p;
		};

		static RandomDistribution<laplace_distribution> generator(Random &rng, double scale, double shape) {
			return rng.build(laplace_distribution{scale, shape});
		}

		static double sample(Random &rng, double scale, double shape) {
			laplace_distribution p(scale, shape);
			return rng.rand(p);
		}
	};
}
#endif
