/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_BERNOULLI_H_
#define STATS_BERNOULLI_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/log1m.hpp>
#include <Random.hpp>

namespace stats {

	struct Bernoulli {

		typedef math::Gradients<1> GradientType;
		typedef int result_type;

		template <bool propTo>
		static double logpdf(int k, double prob) {
			check_bounded01(prob);

			if(k < 0 || k > 1)
				return math::negInf();

			if(prob == 0 || prob == 1)
				return k == prob ? 0 : math::negInf();

			return k*std::log(prob) + (1-k)*math::log1m(prob);
		}

		template <bool propTo>
		static double neglogpdf(int k, double prob) {
			return -logpdf<propTo>(k, prob);
		}

		static inline double logpdf_dx(int k, double prob) {
			check_bounded01(prob);
			return math::NaN();
		}

		static inline double neglogpdf_dx(int k, double prob) {
			return -logpdf_dx(k, prob);
		}

		static inline GradientType logpdf_grad(int k, double prob) {
			check_bounded01(prob);

			if(k < 0 || k > 1)
				return GradientType{ math::NaN() };
			return GradientType{ (k - prob) / (prob * (1 - prob)) };
		}

		static inline GradientType neglogpdf_grad(int k, double prob) {
			return -logpdf_grad(k, prob);
		}

		static inline double cdf(int k, double prob) {
			check_bounded01(prob);
			if(k < 0)
				return 0.0;
			else if(k >= 1)
				return 1.0;
			else
				return 1 - prob;
		}

		struct bernoulli_dist {
			public:
				typedef int result_type;
				typedef typename std::bernoulli_distribution::param_type param_type;
				bernoulli_dist(double prob) : d(prob) {}

				template <typename R>
				int operator () (R & r) {
					return d(r) ? 1 : 0;
				}

				template <typename R>
				int operator () (R & r, param_type & p) {
					return d(r, p) ? 1 : 0;
				}

			private:
				std::bernoulli_distribution d;
		};

		static RandomDistribution<bernoulli_dist> generator(Random &rng, double prob) {
			//check_bounded(prob, 0.0, 1.0, "binomial probability");
			return rng.build(bernoulli_dist{prob});
		}

		static int sample(Random &rng, double prob) {
			//check_bounded(prob, 0.0, 1.0, "binomial probability");
			bernoulli_dist b{prob};
			return rng.rand(b);
		}

	};
}
#endif
