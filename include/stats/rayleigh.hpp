/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_RAYLEIGH_H_
#define STATS_RAYLEIGH_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/log.hpp>
#include <math/sqr.hpp>
#include <Random.hpp>

namespace stats {

	struct Rayleigh {

		typedef math::Gradients<1> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double sigma) {
			check_strictly_positive(sigma);
			if(x <= 0 || !math::isFinite(x))
				return math::negInf();

			const double y = x / sigma;
			double logp = log(x) - 0.5*y*y;
			if(!propTo)
				logp += -2 * math::log(sigma);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(double x, double sigma) {
			return -logpdf<propTo>(x, sigma);
		}

		static inline double logpdf_dx(double x, double sigma) {
			check_strictly_positive(x);
			check_strictly_positive(sigma);
			return 1 / x - x / (sigma * sigma);
		}

		static inline double neglogpdf_dx(double x, double sigma) {
			return -logpdf_dx(x, sigma);
		}

		static inline GradientType logpdf_grad(double x, double sigma) {
			check_strictly_positive(x);
			check_strictly_positive(sigma);
			const double y = x / sigma;
			return GradientType{
				(y*y - 2) / sigma
			};
		}

		static inline GradientType neglogpdf_grad(double x, double sigma) {
			return -logpdf_grad(x, sigma);
		}

		static inline double cdf(double x, double sigma) {
			check_strictly_positive(sigma);
			if(x <= 0 || !math::isFinite(x))
				return math::NaN();

			return 1.0 - std::exp(-0.5 * math::sqr(x) * math::sqr(1.0 / sigma));
		}

		class rayleigh_dist {
		public:
			typedef double result_type;

			struct param_type {
				param_type(double sigma = 1.0) : sigma(sigma) {}
				double sigma;
			};

			rayleigh_dist(double sigma) : p(sigma) {}
			rayleigh_dist(const param_type &q) : p(q) {}

			template <typename R>
			double operator()(R &r) const {
				return (*this)(r, p);
			}

			template <typename R>
			double operator()(R & r, const param_type & q) const {
				std::uniform_real_distribution<double> u;
				return q.sigma * std::sqrt(-2.0 * std::log(u(r)));
			}

		private:
			param_type p;
		};

		static RandomDistribution<rayleigh_dist> generator(Random &rng, double sigma) {
			return rng.build(rayleigh_dist{sigma});
		}

		static double sample(Random &rng, double sigma) {
			rayleigh_dist r{sigma};
			return rng.rand(r);
		}
	};
}
#endif
