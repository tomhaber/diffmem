/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_BETAPRIME_H_
#define STATS_BETAPRIME_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/log1p.hpp>
#include <math/digamma.hpp>

namespace stats {

	struct Betaprime {

		typedef math::Gradients<2> GradientType;

		template <bool propTo>
		static double logpdf(double x, double alpha, double beta) {
			check_strictly_positive(alpha);
			check_strictly_positive(beta);

			if(x < 0.0)
				return math::negInf();
			if(x == 0.0) {
				if(alpha > 1)
					return math::negInf();
				if(alpha < 1)
					return math::posInf();
				return propTo ? 0.0 : std::log(beta);
			}

			double logp = -(alpha + beta)*math::log1p(x) + (alpha - 1)*std::log(x);
			if(!propTo)
				logp += std::lgamma(alpha + beta) - std::lgamma(alpha) - std::lgamma(beta);

			return logp;
		}

		static inline double logpdf_dx(double x, double alpha, double beta) {
			check_strictly_positive(alpha);
			check_strictly_positive(beta);
			check_strictly_positive(x);

			return (alpha - beta*x - x - 1) / (x*(x + 1));
		}

		static inline GradientType logpdf_grad(double x, double alpha, double beta) {
			check_strictly_positive(alpha);
			check_strictly_positive(beta);
			check_strictly_positive(x);

			const double digamma_alpha_beta = math::digamma(alpha + beta);
			return GradientType{
				std::log(x / (x + 1)) + digamma_alpha_beta - math::digamma(alpha),
				-math::log1p(x) + digamma_alpha_beta - math::digamma(beta)
			};
		}

		template <bool propTo>
		static double neglogpdf(double x, double alpha, double beta) {
			return -logpdf<propTo>(x, alpha, beta);
		}

		static inline double neglogpdf_dx(double x, double alpha, double beta) {
			return -logpdf_dx(x, alpha, beta);
		}

		static inline GradientType neglogpdf_grad(double x, double alpha, double beta) {
			return -logpdf_grad(x, alpha, beta);
		}

	};
}
#endif
