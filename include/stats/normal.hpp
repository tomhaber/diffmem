/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_NORMAL_H_
#define STATS_NORMAL_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/inv_phi.hpp>
#include <Random.hpp>

namespace stats {

	struct Normal {

		typedef math::Gradients<2> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double neglogpdf(double x, double mu=0.0, double sigma=1.0) {
			check_nonnegative(sigma);

			if( sigma == 0.0 )
				return x == mu ? math::negInf() : math::posInf();

			const double y = (x - mu) / sigma;
			double logp = 0.5 * y * y;
			if( ! propTo )
				logp += math::logSqrt2Pi() + std::log(sigma);
			return logp;
		}

		template <bool propTo>
		static double logpdf(double x, double mu=0.0, double sigma=1.0) {
			return -neglogpdf<propTo>(x, mu, sigma);
		}

		static inline double logpdf_dx(double x, double mu=0.0, double sigma=1.0) {
			check_strictly_positive(sigma);
			return (mu - x) / (sigma*sigma);
		}

		static inline double neglogpdf_dx(double x, double mu=0.0, double sigma=1.0) {
			return -logpdf_dx(x, mu, sigma);
		}

		static inline GradientType logpdf_grad(double x, double mu=0.0, double sigma=1.0) {
			check_strictly_positive(sigma);
			const double y = (x - mu) / sigma;
			return GradientType{y / sigma, (y * y - 1) / sigma};
		}

		static inline GradientType neglogpdf_grad(double x, double mu=0.0, double sigma=1.0) {
			return -logpdf_grad(x, mu, sigma);
		}

		static inline double cdf(double x, double mu=0.0, double sigma=1.0) {
			check_nonnegative(sigma);
			return 0.5 * (1.0 + std::erf((x - mu) / (math::sqrt2()*sigma)) );
		}

		static inline GradientType cdf_grad(double x, double mu=0.0, double sigma=1.0) {
			check_strictly_positive(sigma);
			const double half_sqrt_two_over_pi = 0.5 * std::sqrt(2.0 / math::pi());
			const double d_mu = -(half_sqrt_two_over_pi * std::exp(-(x - mu)*(x - mu)/(2*sigma*sigma)))/sigma;
			const double d_sigma = d_mu * (x - mu) / sigma;
			return GradientType{d_mu, d_sigma};
		}

		static inline double icdf(double p, double mu=0.0, double sigma=1.0) {
			check_nonnegative(sigma);
			check_bounded01(p);

			if( sigma == 0.0 )
				return mu;

			return math::inv_Phi(p) * sigma + mu;
		}

		static RandomDistribution<std::normal_distribution<double>> generator(Random &rng, double mu=0.0, double sigma=1.0) {
			//check_less_or_equal(lb, ub, "lb <= ub");
			return rng.build(std::normal_distribution<double>{mu, sigma});
		}

		static double sample(Random &rng, double mu=0.0, double sigma=1.0) {
			std::normal_distribution<double> n{mu, sigma};
			return rng.rand(n);
		}
	};
}
#endif
