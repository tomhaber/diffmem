/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_CHISQUARED_H_
#define STATS_CHISQUARED_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/digamma.hpp>
#include <math/pqgamma.hpp>
#include <Random.hpp>

namespace stats {

	struct Chisquared {

		typedef math::Gradients<1> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double logpdf(double x, double nu) {
			check_strictly_positive_finite(nu);

			if( x < 0.0 || !math::isFinite(x) )
				return math::negInf();

			if( x == 0.0 && nu == 2.0 )
				return propTo ? 0.0 : -math::log2();

			const double half_nu = 0.5*nu;
			double logp = (half_nu - 1.0) * std::log(x) - 0.5*x;
			if( !propTo )
				logp += -half_nu * math::log2() - std::lgamma(half_nu);

			return logp;
		}

		static inline double logpdf_dx(double x, double nu) {
			check_strictly_positive_finite(nu);
			check_nonnegative_finite(x);

			if( nu == 2.0 )
				return -0.5;

			return (0.5*nu - 1.0) / x - 0.5;
		}

		static inline GradientType logpdf_grad(double x, double nu) {
			check_strictly_positive_finite(nu);
			check_nonnegative_finite(x);

			return GradientType{
				0.5 * (-math::log2() - math::digamma(0.5 * nu ) + std::log(x))
			};
		}

		template <bool propTo>
		static double neglogpdf(double x, double nu) {
			return -logpdf<propTo>(x, nu);
		}

		static inline double neglogpdf_dx(double x, double nu) {
			return -logpdf_dx(x, nu);
		}

		static inline GradientType neglogpdf_grad(double x, double nu) {
			return -logpdf_grad(x, nu);
		}

		static inline double cdf(double x, double nu) {
			check_strictly_positive_finite(nu);
			check_nonnegative(x);
			return math::pgamma(x*0.5, nu*0.5);
		}

		static RandomDistribution<std::chi_squared_distribution<double>> generator(Random &rng, double nu) {
			//check_strictly_positive_finite(nu, "nu");
			return rng.build(std::chi_squared_distribution<double>{nu});
		}

		static double sample(Random &rng, double nu) {
			//check_strictly_positive_finite(nu, "nu");
			std::chi_squared_distribution<double> chisq{nu};
			return rng.rand(chisq);
		}
	};
}
#endif
