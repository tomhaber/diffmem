/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_MVNORMAL_H_
#define STATS_MVNORMAL_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <stats/normal.hpp>
#include <MatrixVector.hpp>

namespace stats {

	struct MVNormal {

		static double neglogproportions(ConstRefMatrixd & omega) {
			return 0.5 * logDeterminant(omega) + 0.5 * math::log2Pi() * omega.rows();
		}

		static double neglogproportions(const DiagonalMatrixd & omega) {
			return 0.5 * logDeterminant(omega) + 0.5 * math::log2Pi() * omega.rows();
		}

		static double neglogproportions(const Cholesky<Matrixd> & chol_omega) {
			return 0.5 * logDeterminant(chol_omega) + 0.5 * math::log2Pi() * chol_omega.rows();
		}

		static double neglogproportions(ConstRefVecd & mu, ConstRefMatrixd & omega) {
			UNUSED(mu);
			return 0.5 * logDeterminant(omega) + 0.5 * math::log2Pi() * omega.rows();
		}

		static double neglogproportions(ConstRefVecd & mu, const DiagonalMatrixd & omega) {
			UNUSED(mu);
			return 0.5 * logDeterminant(omega) + 0.5 * math::log2Pi() * omega.rows();
		}

		static double neglogproportions(ConstRefVecd & mu, const Cholesky<Matrixd> & chol_omega) {
			UNUSED(mu);
			return 0.5 * logDeterminant(chol_omega) + 0.5 * math::log2Pi() * chol_omega.rows();
		}

		template <bool propTo, typename... Args>
		static double logpdf(Args && ...args) {
			return -neglogpdf<propTo>(std::forward<Args>(args)...);
		}

		template <typename... Args>
		static double logproportions(Args && ...args) {
			return -neglogproportions(std::forward<Args>(args)...);
		}

		template <bool propTo>
		static double neglogpdf(ConstRefVecd & x, ConstRefMatrixd & omega) {
			Cholesky<Matrixd> chol_omega( omega );
			Assert(chol_omega.info() == Eigen::Success, "MVN covariance should be positive-definite");

			double logp = 0.5 * chol_omega.matrixL().solve(x).squaredNorm();
			if( ! propTo )
				logp += 0.5 * logDeterminant(chol_omega) + 0.5 * math::log2Pi() * x.size();
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(ConstRefVecd & x, const DiagonalMatrixd & omega) {
			double logp = 0.0;
			for(int i = 0; i < x.size(); ++i) {
				Assert(omega.diagonal()[i] > 0.0, "MVN covariance should be positive-definite");
				logp += 0.5 * (x[i]*x[i]) / omega.diagonal()[i];
			}

			if( ! propTo )
				logp += 0.5 * logDeterminant(omega) + 0.5 * math::log2Pi() * x.size();
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(ConstRefVecd & x, const Cholesky<Matrixd> & chol_omega) {
			double logp = 0.5 * chol_omega.matrixL().solve(x).squaredNorm();
			if( ! propTo )
				logp += 0.5 * logDeterminant(chol_omega) + 0.5 * math::log2Pi() * x.size();
			return logp;
		}

		template <bool propTo, typename... Args>
		static double logpdf_inv(Args && ...args) {
			return -neglogpdf_inv<propTo>(std::forward<Args>(args)...);
		}

		template <bool propTo>
		static double neglogpdf_inv(ConstRefVecd & x, ConstRefMatrixd & inv_omega) {
			Assert(propTo, "dmvnormi should not be used without proportional")
			return 0.5 * (inv_omega * x).squaredNorm();
		}

		template <bool propTo>
		static double neglogpdf_inv(ConstRefVecd & x, const Cholesky<Matrixd> & chol_inv_omega) {
			double logp = 0.5 * (chol_inv_omega.matrixU() * x).squaredNorm();
			if( ! propTo )
				logp += 0.5 * -logDeterminant(chol_inv_omega) + 0.5 * math::log2Pi() * chol_inv_omega.rows();
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(ConstRefVecd & x, ConstRefVecd & mu, ConstRefMatrixd & omega) {
			Cholesky<Matrixd> chol_omega( omega );
			Assert(chol_omega.info() == Eigen::Success, "MVN covariance should be positive-definite");

			double logp = 0.5 * chol_omega.matrixL().solve(x - mu).squaredNorm();
			if( ! propTo )
				logp += 0.5 * logDeterminant(chol_omega) + 0.5 * math::log2Pi() * mu.size();
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(ConstRefVecd & x, ConstRefVecd & mu, const DiagonalMatrixd & omega) {
			double logp = 0.0;
			for(int i = 0; i < x.size(); ++i) {
				Assert(omega.diagonal()[i] > 0.0, "MVN covariance should be positive-definite");
				logp += 0.5 * (x[i] - mu[i])*(x[i] - mu[i]) / omega.diagonal()[i];
			}

			if( ! propTo )
				logp += 0.5 * logDeterminant(omega) + 0.5 * math::log2Pi() * omega.rows();
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(ConstRefVecd & x, ConstRefVecd & mu, double sigma) {
			const double sigma2 = sigma*sigma;

			double logp = 0.0;
			for(int i = 0; i < x.size(); ++i)
				logp += 0.5 * (x[i] - mu[i])*(x[i] - mu[i]) / sigma2;

			if( ! propTo )
				logp += 0.5 * (std::log(sigma2) + math::log2Pi()) * mu.size();
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(ConstRefVecd & x, ConstRefVecd & mu, const Cholesky<Matrixd> & chol_omega) {
			double logp = 0.5 * chol_omega.matrixL().solve(x - mu).squaredNorm();
			if( ! propTo )
				logp += 0.5 * logDeterminant(chol_omega) + 0.5 * math::log2Pi() * mu.size();
			return logp;
		}

		template <bool propTo>
		static double neglogpdf_inv(ConstRefVecd & x, ConstRefVecd & mu, ConstRefMatrixd & inv_omega) {
			Assert(propTo, "mvnormal_inv should not be used without proportional")
			return 0.5 * (inv_omega * (x - mu)).squaredNorm();
		}

		template <bool propTo>
		static double neglogpdf_inv(ConstRefVecd & x, ConstRefVecd & mu, const Cholesky<Matrixd> & chol_inv_omega) {
			double logp = 0.5 * (chol_inv_omega.matrixU() * (x - mu)).squaredNorm();
			if( ! propTo )
				logp += 0.5 * -logDeterminant(chol_inv_omega) + 0.5 * math::log2Pi() * mu.size();
			return logp;
		}

		static inline void neglogpdf_dx(ConstRefVecd & x, ConstRefMatrixd & omega, RefVecd grad) {
			Cholesky<Matrixd> chol_omega( omega );
			Assert(chol_omega.info() == Eigen::Success, "MVN covariance should be positive-definite");
			grad = chol_omega.solve( x );
		}

		static inline void logpdf_dx(ConstRefVecd & x, ConstRefMatrixd & omega, RefVecd grad) {
			neglogpdf_dx(x, omega, grad);
			grad = -grad;
		}

		static inline void neglogpdf_dx(ConstRefVecd & x, const DiagonalMatrixd & omega, RefVecd grad) {
			grad = x.cwiseQuotient(omega.diagonal());
		}

		static inline void logpdf_dx(ConstRefVecd & x, const DiagonalMatrixd & omega, RefVecd grad) {
			neglogpdf_dx(x, omega, grad);
			grad = -grad;
		}

		static inline void neglogpdf_dx(ConstRefVecd & x, const Cholesky<Matrixd> & chol_omega, RefVecd grad) {
			grad = chol_omega.solve( x );
		}

		static inline void logpdf_dx(ConstRefVecd & x, const Cholesky<Matrixd> & chol_omega, RefVecd grad) {
			neglogpdf_dx(x, chol_omega, grad);
			grad = -grad;
		}

		static inline void neglogpdf_inv_dx(ConstRefVecd & x, ConstRefMatrixd & inv_omega, RefVecd grad) {
			grad = inv_omega * x;
		}

		static inline void logpdf_inv_dx(ConstRefVecd & x, ConstRefMatrixd & inv_omega, RefVecd grad) {
			neglogpdf_dx(x, inv_omega, grad);
			grad = -grad;
		}

		static inline void neglogpdf_dx(ConstRefVecd & x, ConstRefVecd & mu, ConstRefMatrixd & omega, RefVecd grad) {
			Cholesky<Matrixd> chol_omega( omega );
			Assert(chol_omega.info() == Eigen::Success, "MVN covariance should be positive-definite");

			grad = chol_omega.solve( x - mu );
		}

		static inline void logpdf_dx(ConstRefVecd & x, ConstRefVecd & mu, ConstRefMatrixd & omega, RefVecd grad) {
			neglogpdf_dx(x, mu, omega, grad);
			grad = -grad;
		}

		static inline void neglogpdf_dx(ConstRefVecd & x, ConstRefVecd & mu, const DiagonalMatrixd & omega, RefVecd grad) {
			grad = ( x - mu ).cwiseQuotient(omega.diagonal());
		}

		static inline void logpdf_dx(ConstRefVecd & x, ConstRefVecd & mu, const DiagonalMatrixd & omega, RefVecd grad) {
			neglogpdf_dx(x, mu, omega, grad);
			grad = -grad;
		}

		static inline void neglogpdf_dx(ConstRefVecd & x, ConstRefVecd & mu, const Cholesky<Matrixd> & chol_omega, RefVecd grad) {
			grad = chol_omega.solve( x - mu );
		}

		static inline void logpdf_dx(ConstRefVecd & x, ConstRefVecd & mu, const Cholesky<Matrixd> & chol_omega, RefVecd grad) {
			neglogpdf_dx(x, mu, chol_omega, grad);
			grad = -grad;
		}

		static inline void neglogpdf_inv_dx(ConstRefVecd & x, ConstRefVecd & mu, ConstRefMatrixd & inv_omega, RefVecd grad) {
			grad = inv_omega * (x - mu);
		}

		static inline void logpdf_inv_dx(ConstRefVecd & x, ConstRefVecd & mu, ConstRefMatrixd & inv_omega, RefVecd grad) {
			neglogpdf_dx(x, mu, inv_omega, grad);
			grad = -grad;
		}

		template <typename Derived>
		class mvnormal_dist {
		private:
			template <typename R>
			struct normal_op {
				R &r;
				mutable std::normal_distribution<typename Derived::Scalar> n{0.0, 1.0};

				normal_op(R &r) : r(r) {}

				template <typename Index>
				inline double operator()(Index, Index = 0) const {
					return n(r);
				}
			};

		public:
			typedef Derived result_type;
			struct param_type {
				param_type(ConstRefVecd &mu, Cholesky<Matrixd> &chol_omega)
						: mu(mu), chol_omega(chol_omega) {}
				Vecd mu;
				Cholesky<Matrixd> chol_omega;
			};

			mvnormal_dist(ConstRefVecd &mu, Cholesky<Matrixd> &chol_omega)
					: p(mu, chol_omega) {
			}

			template<typename R>
			result_type operator()(R &r) {
				return operator()(r, p);
			}

			template <typename R>
			result_type operator()(R &r, const param_type &q) {
				Vecd v = Eigen::CwiseNullaryOp<normal_op<R>, Derived>(q.mu.size(), 1, normal_op<R>(r));
				v = q.mu + q.chol_omega.multLowerInPlace(v);
				return v;
			}

		private:
			param_type p;
		};

		template <typename Derived>
		static RandomDistribution<mvnormal_dist<Derived>> generator(Random &rng,
				ConstRefVecd &mu, const ConstRefMatrixd &omega) {
			Cholesky<Matrixd> chol_omega(omega);
			Require( chol_omega.info() == Eigen::Success, "Cholesky decomposition failed" );

			return rng.build(mvnormal_dist<Derived>{mu, chol_omega});
		}

		template <typename Derived>
		static RandomDistribution<mvnormal_dist<Derived>> generator(Random &rng,
				ConstRefVecd &mu, const Cholesky<Matrixd> &chol_omega) {
			return rng.build(mvnormal_dist<Derived>{mu, chol_omega});
		}

		static inline void sample(Random &rng, ConstRefVecd &mu, double sigma, RefVecd v) {
			v = generate_random<Vecd>(Normal::generator(rng), mu.size());
			v.noalias() = mu + sigma * v;
		}

		static inline void sample(Random &rng, ConstRefVecd &mu, double sigma, Vecd &v) {
			v.resize(mu.size());
			sample(rng, mu, sigma, RefVecd(v));
		}

		static inline Vecd sample(Random &rng, ConstRefVecd &mu, double sigma = 1.0) {
			Vecd v(mu.size());
			sample(rng, mu, sigma, v);
			return v;
		}

		static inline void sample(Random &rng, ConstRefVecd &mu, const DiagonalMatrixd &omega, RefVecd v) {
			v = generate_random<Vecd>(Normal::generator(rng), mu.size());
			for(int i = 0; i < mu.size(); ++i)
				v[i] = mu[i] + std::sqrt(omega.diagonal()[i]) * v[i];
		}

		static inline void sample(Random &rng, ConstRefVecd &mu, const DiagonalMatrixd &omega, Vecd &v) {
			v.resize(mu.size());
			sample(rng, mu, omega, RefVecd(v));
		}

		static inline Vecd sample(Random &rng, ConstRefVecd &mu, const DiagonalMatrixd &omega) {
			Vecd v(mu.size());
			sample(rng, mu, omega, v);
			return v;
		}

		static inline void sample(Random &rng, ConstRefVecd &mu, const Cholesky<Matrixd> &chol_omega, RefVecd v) {
			v = generate_random<Vecd>(Normal::generator(rng), mu.size());
			v = mu + chol_omega.multLowerInPlace(v);
		}

		static inline void sample(Random &rng, ConstRefVecd &mu, const Cholesky<Matrixd> &chol_omega, Vecd &v) {
			v.resize(mu.size());
			sample(rng, mu, chol_omega, RefVecd(v));
		}

		static inline Vecd sample(Random &rng, ConstRefVecd &mu, const Cholesky<Matrixd> &chol_omega) {
			Vecd v(mu.size());
			sample(rng, mu, chol_omega, v);
			return v;
		}

		static void sample(Random &rng, ConstRefVecd &mu, const ConstRefMatrixd &omega, RefVecd v) {
			Cholesky<Matrixd> chol_omega(omega);
			Require( chol_omega.info() == Eigen::Success, "Cholesky decomposition failed" );
			sample(rng, mu, chol_omega, v);
		}

		static Vecd sample(Random &rng, ConstRefVecd &mu, const ConstRefMatrixd &omega) {
			Vecd v(mu.size());
			sample(rng, mu, omega, v);
			return v;
		}
	};
}
#endif
