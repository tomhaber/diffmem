/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_NEGBINOMIAL_MU_H_
#define STATS_NEGBINOMIAL_MU_H_

#include <valuecheck.hpp>
#include <stats/poisson.hpp>
#include <stats/negbinomial.hpp>
#include <Random.hpp>

namespace stats {

	struct NegbinomialMu {

		typedef math::Gradients<2> GradientType;
		typedef int result_type;

		template <bool propTo>
		static double logpdf(int k, double mu, double phi) {
			check_nonnegative(mu);
			check_nonnegative(phi);

			if( phi == 0.0 )
				return (k == 0) ? 0.0 : math::negInf();

			if( !math::isFinite(phi) )
				return Poisson::logpdf<propTo>(k, mu);
			else
				return Negbinomial::logpdf<propTo>(k, phi, phi / (phi + mu));

			//double logp = -std::lgamma(k + 1.0) + k * std::log(mu + phi) + k * std::log(mu) + std::lgamma(k + phi);
			//if(!propTo)
			//logp += -std::lgamma(phi) + phi * std::log(phi) - phi * std::log(mu + phi);
			//return logp;
		}

		template <bool propTo>
		static double neglogpdf(int k, double mu, double phi) {
			return -logpdf<propTo>(k, mu, phi);
		}

		static inline double logpdf_dx(int k, double mu, double phi) {
			check_nonnegative(k);
			check_strictly_positive(mu);
			check_strictly_positive(phi);

			if( !math::isFinite(phi) )
				return Poisson::logpdf_dx(k, mu);
			else
				return Negbinomial::logpdf_dx(k, phi, phi / (phi + mu));
		}

		static inline double neglogpdf_dx(int k, double mu, double phi) {
			return -logpdf_dx(k, mu, phi);
		}

		static inline GradientType logpdf_grad(int k, double mu, double phi) {
			check_nonnegative(k);
			check_nonnegative(mu);
			check_nonnegative(phi);

			if( math::isFinite(phi) )
				return Negbinomial::logpdf_grad(k, phi, phi / (phi + mu));

			auto g = Poisson::logpdf_grad(k, mu);
			return GradientType{g[0], 0.0};
		}

		static inline GradientType neglogpdf_grad(int k, double mu, double phi) {
			return -logpdf_grad(k, mu, phi);
		}

		static inline double cdf(int k, double mu, double phi) {
			check_nonnegative(k);
			check_nonnegative(mu);
			check_nonnegative(phi);

			if( phi == 0.0 )
				return 1.0;

			if( !math::isFinite(phi) )
				return Poisson::cdf(k, mu);
			else
				return Negbinomial::cdf(k, phi, phi / (phi + mu));
		}

		static RandomDistribution<std::negative_binomial_distribution<int>> generator(Random &rng, double mu, double phi) {
			if(floor(phi) != phi)
				notYetImplemented("negbinom rng only implemented for natural phi");

			return rng.build(std::negative_binomial_distribution<int>{int(phi), phi / (phi + mu)});
		}

		static int sample(Random &rng, double mu, double phi) {
			if(floor(phi) != phi)
				notYetImplemented("negbinom rng only implemented for natural phi");

			std::negative_binomial_distribution<int> c{int(phi), phi / (phi + mu)};
			return rng.rand(c);
		}
	};
}
#endif
