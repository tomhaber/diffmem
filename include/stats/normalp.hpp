/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_NORMALP_H_
#define STATS_NORMALP_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <Random.hpp>

namespace stats {

	struct Normalp {

		typedef math::Gradients<2> GradientType;
		typedef double result_type;

		template <bool propTo>
		static double neglogpdf(double x, double mu, double precision) {
			check_nonnegative(precision);

			if( precision == 0 )
				return x == mu ? math::negInf() : math::posInf();

			double logp = 0.5 * precision * (x - mu)*(x - mu);
			if( ! propTo )
				logp += math::logSqrt2Pi() - 0.5 * std::log(precision);
			return logp;
		}

		template <bool propTo>
		static double logpdf(double x, double mu, double precision) {
			return -neglogpdf<propTo>(x, mu, precision);
		}

		static inline double logpdf_dx(double x, double mu, double precision) {
			check_strictly_positive(precision);
			return (mu - x) * precision;
		}

		static inline double neglogpdf_dx(double x, double mu, double precision) {
			return -logpdf_dx(x, mu, precision);
		}

		static inline GradientType logpdf_grad(double x, double mu, double precision) {
			check_strictly_positive(precision);
			return GradientType{(x - mu) * precision, 0.5 / precision - 0.5 * (x - mu)*(x - mu)};
		}

		static inline GradientType neglogpdf_grad(double x, double mu, double precision) {
			return -logpdf_grad(x, mu, precision);
		}

		static inline double cdf(double x, double mu, double precision) {
			check_nonnegative(precision);
			return 0.5 * (1.0 + std::erf((x - mu) * std::sqrt(precision/2.0)));
		}

		static RandomDistribution<std::normal_distribution<double>> generator(Random &rng, double mu, double precision) {
			return rng.build(std::normal_distribution<double>{mu, std::pow(precision, -0.5)});
		}

		static double sample(Random &rng, double mu, double precision) {
			std::normal_distribution<double> n{mu, std::pow(precision, -0.5)};
			return rng.rand(n);
		}
	};
}
#endif
