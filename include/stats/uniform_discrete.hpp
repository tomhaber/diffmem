/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_DISCRETEUNIFORM_H_
#define STATS_DISCRETEUNIFORM_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <Random.hpp>
#include <MatrixVector.hpp>

namespace stats {

	struct UniformDiscrete {

		typedef math::Gradients<2> GradientType;
		typedef int result_type;

		template <bool propTo>
		static double logpdf(int x, int lb, int ub) {
			check_less_or_equal(lb, ub);

			if(x < lb || x > ub)
				return math::negInf();

			return propTo ? 0.0 : -std::log(ub - lb + 1.);
		}

		template <bool propTo>
		static double neglogpdf(double x, int lb, int ub) {
			return -logpdf<propTo>(x, lb, ub);
		}

		static inline double cdf(int x, int lb, int ub) {
			check_less_or_equal(lb, ub);

			if(x > ub)
				return 1.0;

			if(x < lb)
				return 0.0;

			return (x - lb + 1.) / (ub - lb + 1.);
		}

		static inline double icdf(double p, int lb, int ub) {
			check_less_or_equal(lb, ub);
			check_bounded01(p);

			return std::floor(p * (ub - lb));
		}

		static RandomDistribution<std::uniform_int_distribution<>> generator(Random &rng) {
			return rng.build(std::uniform_int_distribution<>{});
		}

		static RandomDistribution<std::uniform_int_distribution<>> generator(Random &rng, int lb, int ub) {
			//check_less_or_equal(lb, ub, "lb <= ub");
			return rng.build(std::uniform_int_distribution<>{lb, ub});
		}

		static int sample(Random &rng) {
			std::uniform_int_distribution<> d;
			return rng.rand(d);
		}

		static int sample(Random &rng, int lb, int ub) {
			//check_less_or_equal(lb, ub, "lb <= ub");
			std::uniform_int_distribution<> u{lb, ub};
			return rng.rand(u);
		}

	};
}
#endif
