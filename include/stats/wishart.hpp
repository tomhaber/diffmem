/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_WISHART_H_
#define STATS_WISHART_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/lpgamma.hpp>
#include <MatrixVector.hpp>
#include <Random.hpp>

namespace stats {

	struct Wishart {

		template <bool propTo>
		static double logpdf(const Matrixd & X, double df, const Matrixd & S) {
			const int p = S.cols();
			Assert( S.rows() == S.cols(), "S should be square");
			Assert( X.rows() == S.rows() && X.cols() == S.cols(), "X and S should be equal in size");
			Assert(df > p - 1, "df should be greater than dim - 1.");

			double logp = 0.5 * (((df - (p + 1)) * logDeterminant(X)) - (S.inverse() * X).trace());
			if( ! propTo )
				logp -= 0.5 * df * ((logDeterminant(S) + p * math::log2())) + math::lpgamma(p, df);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(const Matrixd & X, double df, const Matrixd & S) {
			return -logpdf<propTo>(X, df, S);
		}

		template <bool propTo>
		static double logpdf(const Matrixd & X, double df, const Cholesky<Matrixd> & chol_S) {
			const int p = chol_S.cols();
			Assert( chol_S.rows() == chol_S.cols(), "S should be square");
			Assert( X.rows() == chol_S.rows() && X.cols() == chol_S.cols(), "X and S should be equal in size");
			Assert(df > p - 1, "df should be greater than dim - 1.");

			double logp = 0.5 * (((df - (p + 1)) * logDeterminant(X)) - (chol_S.solve(X)).trace());
			if( ! propTo )
				logp -= 0.5 * df * ((logDeterminant(chol_S) + p * math::log2())) + math::lpgamma(p, df);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(const Matrixd & X, double df, const Cholesky<Matrixd> & chol_S) {
			return -logpdf<propTo>(X, df, chol_S);
		}

		template <bool propTo>
		static double logpdf(const Cholesky<Matrixd> & chol_X, double df, const Cholesky<Matrixd> & chol_S) {
			// XXX optimize this!
			return logpdf<propTo>(chol_X.reconstructedMatrix(), df, chol_S);
		}

		template <bool propTo>
		static double neglogpdf(const Cholesky<Matrixd> & chol_X, double df, const Cholesky<Matrixd> & chol_S) {
			return -logpdf<propTo>(chol_X, df, chol_S);
		}

		template <bool propTo>
		static double logpdf(const Matrixd & X, double df, const DiagonalMatrixd & S) {
			const int p = S.rows();
			Assert( X.rows() == S.rows() && X.cols() == S.cols(), "X and S should be equal in size");
			Assert(df > p - 1, "df should be greater than dim - 1.");

			double logp = 0.5 * (((df - (p + 1)) * logDeterminant(X)) - X.diagonal().cwiseQuotient(S.diagonal()).sum());
			if( ! propTo )
				logp -= 0.5 * df * ((logDeterminant(S) + p * math::log2())) + math::lpgamma(p, df);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(const Matrixd & X, double df, const DiagonalMatrixd & S) {
			return -logpdf<propTo>(X, df, S);
		}

		template <bool propTo>
		static double logpdf(const Cholesky<Matrixd> & chol_X, double df, const DiagonalMatrixd & S) {
			const int p = S.rows();
			Assert( chol_X.rows() == S.rows() && chol_X.cols() == S.cols(), "X and S should be equal in size");
			Assert(df > p - 1, "df should be greater than dim - 1.");

			double logp = 0.5 * (((df - (p + 1)) * logDeterminant(chol_X)) - (chol_X.reconstructedMatrix().diagonal().cwiseQuotient(S.diagonal())).sum());
			if( ! propTo )
				logp -= 0.5 * df * ((logDeterminant(S) + p * math::log2())) + math::lpgamma(p, df);
			return logp;
		}

		template <bool propTo>
		static double neglogpdf(const Cholesky<Matrixd> & chol_X, double df, const DiagonalMatrixd & S) {
			return -logpdf<propTo>(chol_X, df, S);
		}

		static inline void wishartUnit(Random &rng, double df, int N, Matrixd &omega_U) {
			std::normal_distribution<double> n(0.0, 1.0);
			omega_U = Matrixd::Zero(N, N);

			for(int i = 0; i < N; ++i) {
				std::gamma_distribution<double> g(0.5 * (df - i), 1.0);
				omega_U(i, i) = sqrt(2.0 * rng.rand(g));

				for(int j = i + 1; j < N; ++j)
					omega_U(i, j) = rng.rand(n);
			}
		}

		static inline void sample(Random &rng, double df, const DiagonalMatrixd &S, Matrixd &omega) {
			const int N = S.rows();

			Matrixd x;
			wishartUnit(rng, df, N, x);

			omega.noalias() = x.transpose() * S * x;
		}

		static inline void sample(Random &rng, double df, const Matrixd &S, Matrixd &omega) {
			Cholesky<Matrixd> chol_S(S);
			Require(chol_S.info() == Eigen::Success, "Cholesky decomposition of scale matrix failed");
			sample(rng, df, chol_S, omega);
		}

		static inline void sample(Random &rng, double df, const Cholesky<Matrixd> &chol_S, Matrixd &omega) {
			const int N = chol_S.rows();
			wishartUnit(rng, df, N, omega);

			Matrixd x = omega * chol_S.matrixU();
			omega.noalias() = x.transpose() * x;
		}
	};
}
#endif
