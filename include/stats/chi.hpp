/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_CHI_H_
#define STATS_CHI_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/digamma.hpp>

namespace stats {

	struct Chi {

		typedef math::Gradients<1> GradientType;

		template <bool propTo>
		static double logpdf(double x, double nu) {
			check_strictly_positive(nu);

			if( x < 0.0 || !math::isFinite(x) )
				return math::negInf();

			double logp = (nu==1 ? 0 : (nu - 1) * std::log(x)) - x * x / 2.0;
			if( !propTo )
				logp += (1 - nu/2.0) * math::log2() - std::lgamma(nu / 2.0);
				// when k==1, this is (log(2) - log(pi)) / 2

			return logp;
		}

		static inline double logpdf_dx(double x, double nu) {
			check_strictly_positive(nu);
			check_nonnegative_finite(x);

			if( nu == 1 )
				return -x;

			return (nu - 1) / x - x;
		}

		static inline GradientType logpdf_grad(double x, double nu) {
			check_strictly_positive(nu);
			check_nonnegative_finite(x);

			return GradientType{ std::log(x) - 0.5*(math::digamma(0.5*nu) + math::log2()) };
		}

		template <bool propTo>
		static double neglogpdf(double x, double nu) {
			return -logpdf<propTo>(x, nu);
		}

		static inline double neglogpdf_dx(double x, double nu) {
			return -logpdf_dx(x, nu);
		}

		static inline GradientType neglogpdf_grad(double x, double nu) {
			return -logpdf_grad(x, nu);
		}

	};
}
#endif
