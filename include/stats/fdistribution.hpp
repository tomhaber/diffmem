/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_FDISTRIBUTION_H_
#define STATS_FDISTRIBUTION_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>
#include <math/digamma.hpp>

namespace stats {

	struct Fdistribution {

		// F-distribution
		//  https://en.wikipedia.org/wiki/F-distribution
		//  http://www.math.uah.edu/stat/special/Fisher.html
		// parameters n1 and n2: degrees of freedom; often integers, but defined for any positive real values
		// also known as Snedecor's F distribution or the Fisher�Snedecor distribution
		// related to many other distributions (Fisher-Z, beta, chisquared, ...)
		// standard function in R: df(x, n1, n2) and related functions

		typedef  math::Gradients<2> GradientType;

		template <bool propTo>
		static double logpdf(double x, double n1, double n2) {
			check_strictly_positive(n1);
			check_strictly_positive(n2);

			if(x < 0)
				return math::negInf();

			if(math::isPosInf(n1)) {
				if(math::isPosInf(n2))
					return math::NaN();
				if(x == 0)
					return math::negInf();
				double logp = -0.5*n2 / x - (0.5*n2 + 1)*std::log(x);
				if(!propTo)
					logp += 0.5*n2*(std::log(n2) - math::log2()) - std::lgamma(0.5*n2);
				return logp;
			}
			if(math::isPosInf(n2)) {
				if(x == 0)
					return n1 == 2 ? 0 : math::negInf()*((n1 > 2) - (n1 < 2));
				double logp = -0.5*n1*x + (0.5*n1 - 1)*std::log(x);
				if(!propTo)
					logp += 0.5*n1*(std::log(n1) - math::log2()) - std::lgamma(0.5*n1);
				return logp;
			}
			if(n1 == 2)
				return (0.5*n2 + 1)*(- std::log(1 + 2*x/n2));

			double logp = (0.5*n1 - 1)*std::log(x) - 0.5*(n1 + n2)*std::log(n1*x + n2);
			if(!propTo)
				logp += 0.5*(n1*std::log(n1)+n2*std::log(n2)) - std::lgamma(0.5*n1) - std::lgamma(0.5*n2) + std::lgamma(0.5*(n1 +n2));

			return logp;
		}

		template <bool propTo>
		static double neglogpdf(double x, double n1, double n2) {
			return -logpdf<propTo>(x, n1, n2);
		}

		static inline double logpdf_dx(double x, double n1, double n2) {
			check_strictly_positive(x);
			check_strictly_positive(n1);
			check_strictly_positive(n2);

			if(math::isPosInf(n1))
				return -(n2 * x - n2 + 2 * x) / (2 * x * x);
			if(math::isPosInf(n2))
				return -(n1 * x - n1 + 2) / (2 * x);
			return 0.5*(-n1*(n1 + n2)/(n1*x + n2) + (n1 - 2)/x);
		}

		static inline double neglogpdf_dx(double x, double n1, double n2) {
			return -logpdf_dx(x, n1, n2);
		}

		static inline GradientType logpdf_grad(double x, double n1, double n2) {
			check_strictly_positive(x);
			check_strictly_positive(n1);
			check_strictly_positive(n2);
			if (math::isPosInf(n1) || math::isPosInf(n2))
				return GradientType{
					(math::isPosInf(n1)) ? math::NaN() : 0.5*(std::log(0.5*n1*x) - math::digamma(0.5*n1) + 1 - x),
					(math::isPosInf(n2)) ? math::NaN() : 0.5*(std::log(0.5*n2/x) - math::digamma(0.5*n2) + 1 - 1/x)
				};
			return GradientType{
				0.5*(-n2*(x - 1) / (n1*x + n2) + std::log(n1*x/(n1*x+n2)) - math::digamma(0.5*n1) + math::digamma(0.5*(n1 + n2))),
				0.5*(n1*(x - 1) / (n1*x + n2) + std::log(n2/(n1*x+n2)) - math::digamma(0.5*n2) + math::digamma(0.5*(n1 + n2)))
			};
		}

		static inline GradientType neglogpdf_grad(double x, double n1, double n2) {
			return -logpdf_grad(x, n1, n2);
		}

	};
}
#endif
