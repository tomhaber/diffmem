/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef STATS_LEVY_H_
#define STATS_LEVY_H_

#include <valuecheck.hpp>
#include <math/constants.hpp>
#include <math/gradients.hpp>

namespace stats {

	struct Levy {

		// Levy distribution, as in R's rmutil package
		//   http://ugrad.stat.ubc.ca/R/library/rmutil/html/Levy.html
		// Wikipedia:
		//   https://en.wikipedia.org/wiki/L%C3%A9vy_distribution

		typedef  math::Gradients<2> GradientType;

		template <bool propTo>
		static double logpdf(double x, double mu, double scale) {
			check_strictly_positive(scale);
			if(x <= mu || !math::isFinite(x))
				return math::negInf();

			double logp = 0.5*scale/(mu - x) - 1.5 * std::log(x - mu);
			if(!propTo)
				logp += 0.5 * (std::log(scale) - math::logPi() - math::log2());

			return logp;
		}

		template <bool propTo>
		static double neglogpdf(double x, double mu, double scale) {
			return -logpdf<propTo>(x, mu, scale);
		}

		static inline double logpdf_dx(double x, double mu, double scale) {
			check_strictly_positive(x-mu);
			check_strictly_positive(scale);
			const double y = mu - x;
			return 0.5 * (3 * y + scale) / (y * y);
		}

		static inline double neglogpdf_dx(double x, double mu, double scale) {
			return -logpdf_dx(x, mu, scale);
		}

		static inline GradientType logpdf_grad(double x, double mu, double scale) {
			check_strictly_positive(x - mu);
			check_strictly_positive(scale);
			const double y = mu - x;
			return GradientType{
				-0.5*(3 * y + scale) / (y*y),
				0.5*(1/y + 1/scale)
			};
		}

		static inline GradientType neglogpdf_grad(double x, double mu, double scale) {
			return -logpdf_grad(x, mu, scale);
		}

	};
}
#endif
