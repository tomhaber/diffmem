/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef BIGINT_H
#define BIGINT_H

#include <cstdint>
#include <stdexcept>
#include <type_traits>
#include <limits>

template <size_t N, typename T=uint64_t>
class bigint {
	static_assert(std::is_integral<T>::value && std::is_unsigned<T>::value,
			"parameter type must be an unsigned integer.");

public:
	constexpr bigint() {}
	template <typename U>
	constexpr bigint(U val);

	template <typename It>
	constexpr bigint(It first, It last) {
		assign(first, last);
	}

	template <typename U>
	constexpr bigint(std::initializer_list<U> l) {
		assign(l.begin(), l.end());
	}

public:
	template <typename U>
	constexpr bigint &operator=(U x);

public:
	constexpr bigint &operator++();
	constexpr bigint operator++(int);
	constexpr bigint &operator+=(const bigint &other);
	constexpr bigint operator+(const bigint &other) const;

	template <typename U>
	constexpr bigint &operator+=(U incr);
	template <typename U>
	constexpr bigint operator+(U incr) const;

	template <typename result_type>
	constexpr result_type at(size_t n) const;

	constexpr T operator [](size_t i) const {
		return vals[i];
	}

	constexpr T &operator[](size_t i) {
		return vals[i];
	}

	T *data() { return vals; }
	const T *data() const { return vals; }

public:
	constexpr static size_t bits() {
		return std::numeric_limits<T>::digits * N;
	}

	template <typename U>
	constexpr static size_t count() {
		constexpr unsigned result_bits = std::numeric_limits<U>::digits;
		constexpr unsigned value_bits = std::numeric_limits<T>::digits;

		if(result_bits <= value_bits) {
			return N * (value_bits / result_bits);
		} else {
			return N / ((result_bits + value_bits - 1) / value_bits);
		}
	}

private:
	template <typename It>
	constexpr void assign(It first, It last) {
		typedef typename std::iterator_traits<It>::value_type it_type;
		static_assert(std::is_integral<it_type>::value, "parameter type must be an integral.");

		constexpr unsigned value_bits = std::numeric_limits<T>::digits;
		constexpr unsigned it_bits = std::numeric_limits<it_type>::digits + std::numeric_limits<it_type>::is_signed;

		if(it_bits == value_bits) {
			for(size_t j = 0; j < N; j++) {
				const it_type itv = (first != last) ? *first++ : 0;
				vals[j] = static_cast<T>(itv);
			}
		} else if( it_bits < value_bits ) {
			for(size_t j = 0; j < N; j++) {
				T val = 0;
				unsigned lshift = 0;
				for(size_t k = 0; k < value_bits / it_bits; ++k) {
					const it_type itv = (first != last) ? *first++ : 0;
					val |= static_cast<T>(itv) << lshift;
					lshift += it_bits;
				}
				vals[j] = val;
			}
		} else {
			const unsigned its_per_value = it_bits / value_bits;
			it_type itv = (first != last) ? *first++ : 0;
			unsigned jj = 0;
			for(size_t j = 0; j < N; ++j) {
				if(jj++ == its_per_value) {
					itv = (first != last) ? *first++ : 0;
					jj = 0;
				}

				vals[j] = static_cast<T>(itv & ~((~(it_type)0)<< value_bits));
				itv >>= value_bits;
			}
		}

		while(first != last) {
			if( *first++ )
				throw std::runtime_error("non-zero values ignored!");
		}
	}

private:
	T vals[N] = {0, };
};

template <size_t N, typename T>
template <typename U>
constexpr bigint<N, T>::bigint(U val) {
	*this = std::move(val);
}

template <size_t N, typename T>
template <typename U>
constexpr bigint<N,T> & bigint<N, T>::operator =(U val) {
	static_assert(std::is_integral<T>::value && std::is_unsigned<T>::value,
			"argument type must be an unsigned integer.");

	for(auto & v : vals)
		v = T(0);
	vals[0] = val;
	return *this;
}

template <size_t N, typename T>
constexpr bigint<N,T> bigint<N,T>::operator++(int) {
	bigint<N,T> res = *this;
	++(*this);
	return res;
}

template <size_t N, typename T>
constexpr bigint<N,T> &bigint<N,T>::operator++() {
	bool carry = true;
	for(size_t i = 0; i < N /*&& carry*/; ++i) {
		T s = vals[i] + (carry ? 1 : 0);
		carry = s < vals[i];
		vals[i] = s;
	}
	return *this;
}

template <size_t N, typename T>
constexpr bigint<N,T> &bigint<N,T>::operator+=(const bigint &other) {
	bool carry = false;
	for(size_t i = 0; i < N; ++i) {
		T s = vals[i] + other.vals[i] + (carry ? 1 : 0);
		carry = s < vals[i];
		vals[i] = s;
	}
	return *this;
}

template <size_t N, typename T>
template <typename U>
constexpr bigint<N,T> &bigint<N,T>::operator+=(U incr) {
	static_assert(std::is_integral<T>::value && std::is_unsigned<T>::value,
			"argument type must be an unsigned integer.");

	constexpr unsigned value_bits = std::numeric_limits<T>::digits;
	constexpr unsigned incr_bits = std::numeric_limits<U>::digits;

	for(size_t i = 0; i < N /*&& (incr != 0)*/; ++i) {
		T s = vals[i] + T(incr);
		incr >>= ((value_bits <= incr_bits) ? value_bits : incr_bits) - 1; incr >>= 1;
		incr += (s < vals[i]) ? 1 : 0;
		vals[i] = s;
	}
	return *this;
}

template <size_t N, typename T>
constexpr bigint<N,T> bigint<N,T>::operator+(const bigint &other) const {
	bigint res = *this;
	res += other;
	return res;
}

template <size_t N, typename T>
template <typename U>
constexpr bigint<N,T> bigint<N,T>::operator+(U incr) const {
	bigint res = *this;
	res += incr;
	return res;
}

template <size_t N, typename T>
template <typename result_type>
constexpr result_type bigint<N,T>::at(size_t n) const {
	const unsigned value_bits = std::numeric_limits<T>::digits;
	const unsigned result_bits = std::numeric_limits<result_type>::digits;

	if( result_bits == value_bits ) {
		if(n >= N) return result_type(0);
		return static_cast<result_type>(vals[n]);
	} else if( result_bits < value_bits ) {
		const unsigned results_per_value = value_bits / result_bits;
		const unsigned idx = n / results_per_value;
		const unsigned shift = (n % results_per_value) * result_bits;
		const T mask = ~(~(T(0)) << result_bits);
		if(idx >= N) return result_type(0);
		return static_cast<result_type>((vals[idx] >> shift) & mask);
	} else {
		const unsigned idx = (n*result_bits) / value_bits;
		const unsigned imax = std::min<size_t>(result_bits / value_bits, N - idx);
		if(idx >= N) return result_type(0);

		result_type res = 0;
		for(unsigned i = 0; i < imax; ++i)
			res |= result_type(vals[idx + i]) << (i*value_bits);
		return res;
	}
}
#endif