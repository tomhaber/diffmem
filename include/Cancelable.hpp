/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef CANCELABLE_H
#define CANCELABLE_H

#include <type_traits>

template <typename T>
class Cancelable;

template <typename T>
struct isCancelable : std::false_type {
	typedef T type;
};

template <typename T>
struct isCancelable<Cancelable<T>> : std::true_type {
	typedef T type;
};

constexpr struct in_place_t{} in_place{};
constexpr struct cancelled_t{} cancelled{};

template <typename T>
class Cancelable {
	static_assert(!std::is_reference<T>::value, "Cancelable may not be used with reference types");

public:
	typedef T element_type;

	Cancelable() : cancelled_(false), value_() {}
	template <typename... U>
	explicit Cancelable(const in_place_t &, U &&... args)
			: cancelled_(false), value_(std::forward<U>(args)...) {}

	template <typename U,
			typename std::enable_if<std::is_convertible<U, T>::value>::type * = nullptr>
	Cancelable(U &&v) : cancelled_(false), value_(std::forward<U>(v)) {}

	Cancelable(cancelled_t) : cancelled_(true) {}

	Cancelable(Cancelable<T> && t) noexcept;
	Cancelable& operator=(Cancelable<T> && t) noexcept;

	Cancelable(const Cancelable<T> &t);
	Cancelable &operator=(const Cancelable<T> &t);

	Cancelable &operator=(cancelled_t);
	template <typename U,
			typename std::enable_if<std::is_convertible<U, T>::value>::type * = nullptr>
	Cancelable &operator=(U &&v);

	~Cancelable();

public:
	T &value() &;
	T &&value() &&;
	const T &value() const &;

	template <typename U>
	T value_or(U && u) &;
	template <typename U>
	T value_or(U &&u) &&;
	template <typename U>
	T value_or(U const &u) const;

	T &operator*() { return value(); }
	const T operator->() const { return &value(); }
	T *operator->() { return &value(); }

	bool isCancelled() const { return cancelled_; }
	operator bool() const { return !isCancelled(); }
	void throwIfCancelled() const;

	Cancelable<T> &operator+=(const Cancelable<T> &v);
	Cancelable<T> operator+(const Cancelable<T> &v) const {
		Cancelable<T> res = *this;
		res += v;
		return res;
	}

private:
	bool cancelled_;
	union {
		unsigned char dummy;
		T value_;
	};
};

template <typename T>
Cancelable<T>::Cancelable(Cancelable<T> && t) noexcept : cancelled_(t.cancelled_) {
	if(!cancelled_)
		new (&value_) T(std::move(t.value_));
}

template <typename T>
Cancelable<T>& Cancelable<T>::operator=(Cancelable<T> && t) noexcept {
	if( this == &t ) return *this;

	if(!cancelled_)
		value_.~T();

	cancelled_ = t.cancelled_;
	if(!cancelled_)
		new (&value_) T(std::move(t.value_));
	return *this;
}

template <typename T>
Cancelable<T>::Cancelable(const Cancelable<T> & t) : cancelled_(t.cancelled_) {
	if(!cancelled_)
		new (&value_) T(t.value_);
}

template <typename T>
Cancelable<T> &Cancelable<T>::operator=(const Cancelable<T> &t) {
	if( this == &t ) return *this;

	if(!cancelled_)
		value_.~T();

	cancelled_ = t.cancelled_;
	if(!cancelled_)
		new (&value_) T(t.value_);
	return *this;
}

template <typename T>
Cancelable<T> &Cancelable<T>::operator=(cancelled_t) {
	if(!cancelled_)
		value_.~T();

	cancelled_ = true;
	return *this;
}

template <typename T>
template<typename U,
		typename std::enable_if<std::is_convertible<U, T>::value>::type *>
Cancelable<T> &Cancelable<T>::operator=(U &&v) {
	if(!cancelled_)
		value_.~T();

	cancelled_ = false;
	new (&value_) T(std::forward<U>(v));
	return *this;
}

template <typename T>
Cancelable<T>::~Cancelable() {
	if(!cancelled_)
		value_.~T();
}

template <typename T>
T & Cancelable<T>::value() & {
	throwIfCancelled();
	return value_;
}

template <typename T>
T && Cancelable<T>::value() && {
	throwIfCancelled();
	return std::move(value_);
}

template <typename T>
const T & Cancelable<T>::value() const & {
	throwIfCancelled();
	return value_;
}

template <typename T>
void Cancelable<T>::throwIfCancelled() const {
	if(cancelled_)
		throw std::runtime_error("Using cancelled result");
}

template <typename T>
Cancelable<T> &Cancelable<T>::operator+=(const Cancelable<T> &other) {
	if(!cancelled_) {
		if(other.cancelled_) {
			value_.~T();
			cancelled_ = true;
		} else {
			value_ += other.value_;
		}
	}

	return *this;
}

template <typename T>
template <class U>
inline T Cancelable<T>::value_or(U &&v) & {
	return !isCancelled() ? value() : static_cast<T>(std::forward<U>(v));
}

template <typename T>
template <class U>
inline T Cancelable<T>::value_or(U &&v) && {
	return !isCancelled() ? std::move(value()) : static_cast<T>(std::forward<U>(v));
}

template <typename T>
template <class U>
inline T Cancelable<T>::value_or(U const &v) const {
	return !isCancelled() ? value() : static_cast<T>(v);
}
#endif
