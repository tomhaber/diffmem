/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DICTCPP_H
#define DICTCPP_H

#include <Dict.hpp>
#include <unordered_map>

class DIFFMEM_EXPORT DictCpp : public Dict {
public:
	template <typename... Args>
	DictCpp(Args &&... args) {
		setMany(std::forward<Args>(args)...);
	}

public:
	bool exists(const std::string &name) const;
	Vecd getNumericVector(const std::string &name) const;
	Matrixd getNumericMatrix(const std::string &name) const;
	Veci getIntegerVector(const std::string &name) const;
	Matrixi getIntegerMatrix(const std::string &name) const;
	Vecb getBooleanVector(const std::string &name) const;
	Matrixb getBooleanMatrix(const std::string &name) const;
	std::string getString(const std::string &name) const;
	int getInteger(const std::string &name) const;
	double getNumeric(const std::string &name) const;
	std::shared_ptr<Dict> getDict(const std::string &name) const;

	void setNumericVector(const std::string &name, ConstRefVecd &value);
	void setNumericMatrix(const std::string &name, ConstRefMatrixd &value);
	void setIntegerVector(const std::string &name, ConstRefVeci &value);
	void setIntegerMatrix(const std::string &name, ConstRefMatrixi &value);
	void setBooleanVector(const std::string &name, ConstRefVecb &value);
	void setBooleanMatrix(const std::string &name, ConstRefMatrixb &value);
	void setString(const std::string &name, const std::string &value);
	void setInteger(const std::string &name, int value);
	void setNumeric(const std::string &name, double value);
	void setDict(const std::string &name, std::shared_ptr<DictCpp> &&value);

private:
	void set(const std::string &name, Vecd &&value);
	void set(const std::string &name, Matrixd &&value);
	void set(const std::string &name, Veci &&value);
	void set(const std::string &name, Matrixi &&value);
	void set(const std::string &name, Vecb &&value);
	void set(const std::string &name, Matrixb &&value);
	void set(const std::string &name, int value);
	void set(const std::string &name, std::string &&value);
	void set(const std::string &name, double value);

	void set(const std::string &name, const Vecd &value);
	void set(const std::string &name, const Matrixd &value);
	void set(const std::string &name, const Veci &value);
	void set(const std::string &name, const Matrixi &value);
	void set(const std::string &name, const Vecb &value);
	void set(const std::string &name, const Matrixb &value);
	void set(const std::string &name, const std::string &value);

	void setMany() {}
 	template <typename N, typename V, typename... Args>
	 void setMany(N && name, V && value, Args && ...args) {
		set(std::forward<N>(name), std::forward<V>(value));
		setMany(std::forward<Args>(args)...);
	}

private:
	std::unordered_map<std::string, Vecd> numericVectorMap;
	std::unordered_map<std::string, Matrixd> numericMatrixMap;
	std::unordered_map<std::string, Veci> integerVectorMap;
	std::unordered_map<std::string, Matrixi> integerMatrixMap;
	std::unordered_map<std::string, Vecb> booleanVectorMap;
	std::unordered_map<std::string, Matrixb> booleanMatrixMap;
	std::unordered_map<std::string, std::string> stringMap;
	std::unordered_map<std::string, int> integerMap;
	std::unordered_map<std::string, double> numericMap;
	std::unordered_map<std::string, std::shared_ptr<DictCpp>> dictMap;
};

#endif /* DICTCPP_H */
