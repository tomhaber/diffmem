/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef POINTWISELIKELIHOODMODEL_H
#define POINTWISELIKELIHOODMODEL_H

#include <LikelihoodModel.hpp>

class DIFFMEM_EXPORT PointwiseLikelihoodModel : public LikelihoodModel {
public:
	double logpdf(ConstRefVecd &tau, ConstRefMatrixd &u) const override;
	double logpdfGradTau(ConstRefVecd &tau, ConstRefMatrixd &u, RefVecd gradTau) const override;

	using LikelihoodModel::logpdfGradTau;
	virtual double logProportions(ConstRefVecd &tau) const { return 0.0; }
	virtual double logProportionalPdf(ConstRefVecd &tau, int i, ConstRefVecd &u) const = 0;
	virtual double logpdfGradTau(ConstRefVecd &tau, int i, ConstRefVecd &u, RefVecd gradTau) const = 0;

public:
	void logpdfGrad(ConstRefVecd &tau, ConstRefMatrixd &u, ConstRefMatrixd &s,
			RefVecd grad) const override;
	void logpdfHess(ConstRefVecd &tau, ConstRefMatrixd &u, ConstRefMatrixd &s,
			RefVecd grad, RefMatrixd hess) const override;

	virtual void logpdfGradU(ConstRefVecd &tau, int i, ConstRefVecd &u, RefVecd gradU) const = 0;
	virtual void logpdfHessU(ConstRefVecd &tau, int i, ConstRefVecd &u, RefVecd gradU, RefMatrixd hessU) const = 0;
};
#endif
