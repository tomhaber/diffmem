/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DICT_H_
#define DICT_H_

#include <MatrixVector.hpp>
#include <memory>
#include <string>

class missingkey_error : public std::runtime_error {
	public:
		missingkey_error(const std::string & name) :
			std::runtime_error(std::string("Missing key, expected \"") + name + "\"") {
		}
};

class DIFFMEM_EXPORT Dict {
	public:
		virtual ~Dict() {}

	public:
		virtual bool exists(const std::string & name) const = 0;
		virtual Vecd getNumericVector(const std::string & name) const = 0;
		virtual Matrixd getNumericMatrix(const std::string & name) const = 0;
		virtual Veci getIntegerVector(const std::string & name) const = 0;
		virtual Matrixi getIntegerMatrix(const std::string & name) const = 0;
		virtual Vecb getBooleanVector(const std::string & name) const = 0;
		virtual Matrixb getBooleanMatrix(const std::string & name) const = 0;
		virtual std::string getString(const std::string & name) const = 0;
		virtual int getInteger(const std::string & name) const = 0;
		virtual double getNumeric(const std::string & name) const = 0;
		virtual std::shared_ptr<Dict> getDict(const std::string & name) const = 0;

	public:
		std::string getStringDefault(const std::string & name, const std::string & def) const {
			if( ! exists(name) ) return def;
			return getString(name);
		}

		int getIntegerDefault(const std::string & name, int def) const {
			if( ! exists(name) ) return def;
			return getInteger(name);
		}

		double getNumericDefault(const std::string & name, double def) const {
			if( ! exists(name) ) return def;
			return getNumeric(name);
		}
};
#endif
