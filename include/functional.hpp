/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef FUNCTIONAL_H
#define FUNCTIONAL_H

#include <iostream>
#include <type_traits>
#include <typeinfo>
#ifndef _MSC_VER
#   include <cxxabi.h>
#endif
#include <memory>
#include <string>
#include <cstdlib>

template <class T>
std::string type_name() {
	typedef typename std::remove_reference<T>::type TR;
	std::unique_ptr<char, void (*)(void *)> own(
#ifndef _MSC_VER
			abi::__cxa_demangle(typeid(TR).name(), nullptr,
					nullptr, nullptr),
#else
			nullptr,
#endif
			std::free);
	std::string r = own != nullptr ? own.get() : typeid(TR).name();
	if(std::is_const<TR>::value)
		r += " const";
	if(std::is_volatile<TR>::value)
		r += " volatile";
	if(std::is_lvalue_reference<T>::value)
		r += "&";
	else if(std::is_rvalue_reference<T>::value)
		r += "&&";
	return r;
}

namespace details {
	template <typename T, typename S=std::ostream>
	class is_streamable {
		template <typename SS, typename TT>
		static auto test(int)
				-> decltype(std::declval<SS &>() << std::declval<TT>(), std::true_type());

		template <typename, typename>
		static auto test(...) -> std::false_type;

	public:
		static const bool value = decltype(test<S, T>(0))::value;
	};

	template <typename T>
	struct TypeOf {
		typedef T type;
	};

	template <class T>
	struct is_stream {
	enum { value = std::is_same<T, std::ostream>::value || std::is_same<T, std::ostringstream>::value };
	};

	template <typename S, typename T>
	S &print_helper(S &s, TypeOf<T>) {
		s << type_name<T>();
		return s;
	}

	template <typename S, typename T>
	std::enable_if_t<is_stream<S>::value && is_streamable<T, S>::value, S&>
	print_helper(S &s, T &&v) {
		s << v;
		return s;
	}

	template <typename S, typename T>
	std::enable_if_t<is_stream<S>::value && !is_streamable<T, S>::value, S&>
	print_helper(S &s, T &&v) {
		s << "***";
		return s;
	}
}

template <typename T>
details::TypeOf<T> type_of(T) {
	return {};
}

template <typename S, typename T>
std::enable_if_t<details::is_stream<S>::value, T>
print(S &s, T &&v) {
	details::print_helper(s, v);
	s << std::endl;
	return std::forward<T>(v);
}

template <typename S, typename T, typename... Args>
std::enable_if_t<details::is_stream<S>::value, T>
print(S &s, T &&v, Args &&... args) {
	details::print_helper(s, v) << " ";
	print(s, std::forward<Args>(args)...);
	return std::forward<T>(v);
}


template <typename T, typename... Args>
std::enable_if_t<!details::is_stream<T>::value, T>
print(T &&v, Args &&... args) {
	return print(std::cout, std::forward<T>(v), std::forward<Args>(args)...);
}

template <class T>
class constant_function {
public:
	typedef T value_type;
	constexpr constant_function() : value() {}
	constexpr constant_function(const T &v) : value(v) {}
	constexpr constant_function(T && v) : value(std::move(v)) {}
	constexpr operator T() const { return value; }

	template <typename... Args>
	constexpr T operator()(Args &&...) const {
		return value;
	}

private:
	T value;
};

template <class T, typename DT=typename std::remove_reference<T>::type>
constexpr constant_function<DT> constant(T &&v) {
	return constant_function<DT>(std::forward<T>(v));
}
#endif
