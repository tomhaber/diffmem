/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MASKED_DISTRIBUTION_H
#define MASKED_DISTRIBUTION_H

#include <Distribution.hpp>
#include <LocalMatrix.hpp>

template <int N = Eigen::Dynamic>
class DIFFMEM_EXPORT MaskedDistribution final : public Distribution {
	public:
		MaskedDistribution(std::shared_ptr<Distribution> dist, const VecNd<N> &vals, const VecNb<N> &mask)
				: dist(dist), vals(vals), mask(mask) {
			Require(dist->numberOfDimensions() == vals.size());
			Require(dist->numberOfDimensions() == mask.size());
		}
		template <typename std::enable_if<N == Eigen::Dynamic>::type *x=nullptr>
		MaskedDistribution(const Dict &dict);

	public:
		int numberOfDimensions() const override {
			return mask.count();
		}

		double logpdf(ConstRefVecd &x) const override {
			LOCAL_VECTOR(Vecd, p, vals.size());
			Require(x.size() == mask.count());

			int idx = 0;
			for(int i = 0; i < p.size(); ++i)
				p[i] = mask[i] ? x[idx++] : vals[i];

			return dist->logpdf(p);
		}

		double logpdf(ConstRefVecd &x, RefVecd grad) const override {
			LOCAL_VECTOR(Vecd, p, vals.size());
			LOCAL_VECTOR(Vecd, g, vals.size());
			Require(x.size() == mask.count());
			Require(grad.size() == mask.count());

			for(int i = 0, idx = 0; i < p.size(); ++i)
				p[i] = mask[i] ? x[idx++] : vals[i];

			double fp = dist->logpdf(p, g);

			for(int i = 0, idx = 0; i < g.size(); ++i)
				if(mask[i])
					grad[idx++] = g[i];
			return fp;
		}

		double logpdf(ConstRefVecd &x, RefVecd grad, RefMatrixd hess) const override {
			LOCAL_VECTOR(Vecd, p, vals.size());
			LOCAL_VECTOR(Vecd, g, vals.size());
			LOCAL_MATRIX(Matrixd, H, vals.size(), vals.size());

			Require(x.size() == mask.count());
			Require(grad.size() == mask.count());
			Require(H.rows() == mask.count() && H.cols() == mask.count());

			int idx = 0;
			for(int i = 0; i < p.size(); ++i)
				p[i] = mask[i] ? x[idx++] : vals[i];

			double fp = dist->logpdf(p, g);

			for(int i = 0, idx_i = 0; i < g.size(); ++i) {
				if(mask[i]) {
					grad[idx_i] = g[i];

					for(int j = 0, idx_j = 0; j < g.size(); ++j) {
						if(mask[j]) {
							hess(idx_i, idx_j) = H(i, j);
							idx_j++;
						}
					}

					idx_i++;
				}
			}

			return fp;
		}

	private:
		std::shared_ptr<Distribution> dist;
		VecNd<N> vals;
		VecNb<N> mask;	// true => keep, false => remove
};

template <int N = Eigen::Dynamic>
auto mask_distribution(std::shared_ptr<Distribution> dist, const VecNd<N> &vals, const VecNb<N> &mask) {
	return std::make_shared<MaskedDistribution<N>>(std::move(dist), vals, mask);
}

auto mask_distribution(std::shared_ptr<Distribution> dist, ConstRefVecd &vals, ConstRefVecb &mask) {
	return std::make_shared<MaskedDistribution<>>(std::move(dist), vals, mask);
}
#endif
