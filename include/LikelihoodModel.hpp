/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LIKELIHOODMODEL_H
#define LIKELIHOODMODEL_H

#include <common.hpp>
#include <MatrixVector.hpp>
#include <StructuralModel.hpp>
#include <Dict.hpp>

class DIFFMEM_EXPORT LikelihoodModel {
public:
	virtual ~LikelihoodModel() {
	}

public:
	virtual int numberOfObservations() const = 0;
	virtual int numberOfMeasurements() const = 0;
	virtual int numberOfSigmas() const = 0;
	virtual int numberOfTaus() const = 0;

public:
	virtual void transsigma(ConstRefVecd &tau, RefVecd sigma) const;
	virtual void transtau(ConstRefVecd &sigma, RefVecd tau) const;

public:
	virtual double logpdf(ConstRefVecd &tau, ConstRefMatrixd &u) const = 0;
	virtual double logpdfGradTau(ConstRefVecd &tau, ConstRefMatrixd &u, RefVecd gradTau) const = 0;

public:
	virtual double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, const StructuralModel &sm) const = 0;
	virtual double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, const StructuralModel &sm,
			RefVecd gradPhi) const  = 0;
	virtual double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, const StructuralModel &sm,
			RefVecd gradPhi, RefMatrixd hessPhi) const = 0;
	virtual double logpdfGradTau(ConstRefVecd &phi, ConstRefVecd &tau, const StructuralModel &sm,
			RefVecd gradTau) const  = 0;

public:
	virtual void logpdfGrad(ConstRefVecd &tau, ConstRefMatrixd &u, ConstRefMatrixd &s,
			RefVecd grad) const = 0;
	virtual void logpdfHess(ConstRefVecd &tau, ConstRefMatrixd &u, ConstRefMatrixd &s,
			RefVecd grad, RefMatrixd hess) const = 0;

	virtual void updateTau(RefVecd tau, ConstRefVecd &gradTau, int N, double alpha) const = 0;
};

#define LIKELIHOODMODEL_DISPATCH \
public: \
	double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, const StructuralModel &sm) const override { \
		return sm.logpdf(phi, tau, measurements.time(), *this); \
	} \
	double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, const StructuralModel &sm, \
			RefVecd gradPhi) const override { \
		return sm.logpdf(phi, tau, measurements.time(), *this, gradPhi); \
	} \
	double logpdf(ConstRefVecd &phi, ConstRefVecd &tau, const StructuralModel &sm, \
			RefVecd gradPhi, RefMatrixd hessPhi) const override { \
		return sm.logpdf(phi, tau, measurements.time(), *this, gradPhi, hessPhi); \
	} \
	double logpdfGradTau(ConstRefVecd &phi, ConstRefVecd &tau, const StructuralModel &sm, \
			RefVecd gradTau) const override { \
		return sm.logpdfGradTau(phi, tau, measurements.time(), *this, gradTau); \
	}
#endif
