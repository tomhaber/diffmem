/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef EXCEPTION_H_
#define EXCEPTION_H_

#include <common.hpp>
#include <stdexcept>
#include <system_error>

#include <string>
#include <fmt/format.h>
#include <fmt/string.h>
#include <fmt/ostream.h>

class assertion_error : public std::runtime_error {
	public:
		assertion_error(const char *msg) : std::runtime_error(msg) {}
		assertion_error(const std::string &msg) : std::runtime_error(msg.c_str()) {}
};

class notyetimplemented_error : public std::runtime_error {
	public:
		notyetimplemented_error(const char *msg) : std::runtime_error(msg) {}
};

class model_error : public std::exception {
	public:
		model_error(const char *msg) : msg(msg) {}
		model_error(const std::string & msg) : msg(msg) {}

#if __cplusplus > 199711L
		virtual const char *what() const noexcept { return msg.c_str(); }
#else
		virtual const char *what() const { return msg.c_str(); }
#endif

	private:
		std::string msg;
};

class interrupted_exception : public std::exception {
	public:
		interrupted_exception() {}

#if __cplusplus > 199711L
		virtual const char *what() const noexcept { return "Interrupted"; }
#else
		virtual const char *what() const { return "Interrupted"; }
#endif
};

namespace details {
	inline std::string to_string() {
		return {};
	}

	template <size_t N, class... Args>
	std::string to_string(const char (&fmt)[N], Args &&... args) {
		return fmt::format(fmt, std::forward<Args>(args)...);
	}
}

[[noreturn]] inline void notYetImplemented(const char* msg) {
	throw notyetimplemented_error(msg);
}

template <class... Args>
[[noreturn]] inline void notYetImplemented(Args&&... args) {
	notYetImplemented(details::to_string(std::forward<Args>(args)...).c_str());
}

template <class Ex>
[[noreturn]] inline void throwException(const char* msg) {
	throw Ex(msg);
}

template <class Ex, class... Args>
[[noreturn]] NOINLINE void throwException(Args&&... args) {
	throwException<Ex>(details::to_string(std::forward<Args>(args)...).c_str());
}

template <size_t N, class... Args>
[[noreturn]] NOINLINE void throwDomainError(const char *function, const char (&fmt)[N], Args&&... args) {
	fmt::MemoryWriter out;
	out << function << ": ";
	out.write(fmt, std::forward<Args>(args)...);
	throwException<std::domain_error>(out.c_str());
}

template <size_t N, class... Args>
[[noreturn]] NOINLINE void throwAssertionError(const char *file, int line, const char *function, const char (&fmt)[N], Args &&... args) {
	fmt::MemoryWriter out;
	out.write("Assertion failed in {:s} at {:s}:{:d}:\n", function, file, line);
	out.write(fmt, std::forward<Args>(args)...);
	throwException<assertion_error>(out.c_str());
}

[[noreturn]] inline void throwSystemErrorExplicit(int err, const char* msg) {
	throw std::system_error(err, std::system_category(), msg);
}

template <class... Args>
[[noreturn]] NOINLINE void throwSystemErrorExplicit(int err, Args&&... args) {
	throwSystemErrorExplicit(
			err, details::to_string(std::forward<Args>(args)...).c_str());
}

// Helper to throw std::system_error from errno and components of a string
template <class... Args>
[[noreturn]] NOINLINE void throwSystemError(Args&&... args) {
	throwSystemErrorExplicit(errno, std::forward<Args>(args)...);
}

// Check a Posix return code (0 on success, error number on error), throw
// on error.
template<class ... Args>
void checkPosixError(int err, Args&&... args) {
	if( err != 0 ) {
		throwSystemErrorExplicit(err, std::forward<Args>(args)...);
	}
}

#ifdef __unix__
// Check a Linux kernel-style return code (>= 0 on success, negative error
// number on error), throw on error.
template<class ... Args>
void checkKernelError(ssize_t ret, Args&&... args) {
	if( ret < 0 ) {
		throwSystemErrorExplicit(-ret, std::forward<Args>(args)...);
	}
}

// Check a traditional Unix return code (-1 and sets errno on error), throw
// on error.
template<class ... Args>
void checkUnixError(ssize_t ret, Args&&... args) {
	if( ret == -1 ) {
		throwSystemError(std::forward<Args>(args)...);
	}
}
#endif

DIFFMEM_EXPORT std::string errnoStr(int err);
#endif
