/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SMALLVECTOR_H
#define SMALLVECTOR_H

#include <cstddef>
#include <cstdlib>
#include <initializer_list>
#include <type_traits>
#include <iterator>
#include <cassert>
#include <math/next_power_2.hpp>
#include <Likely.hpp>

namespace details {
	template<size_t alignment, size_t size>
	struct AlignedCharArrayHelper {
		alignas(alignment) char buffer[size];
	};

	template <typename T>
	struct AlignedCharArray : AlignedCharArrayHelper<alignof(T), sizeof(T)> {};

	template <typename T, size_t N>
	struct SmallVectorStorage {
		details::AlignedCharArray<T> remainingElements[N-1];
	};

	template <typename T>
	struct SmallVectorStorage<T, 0> {};

	template <typename T>
	struct SmallVectorStorage<T, 1> {};
}

template <typename T>
class SmallVectorImpl {
	public:
		SmallVectorImpl(size_t size) : begin_(reinterpret_cast<T*>(&elements)), end_(begin_), capacity_(begin_ + size) {}

//		SmallVectorImpl(const SmallVectorImpl &other) = delete;

		const SmallVectorImpl &operator=(const SmallVectorImpl &other) {
			if(this == &other) return *this;

			size_t mySize = size();
			size_t otherSize = other.size();
			if(mySize >= otherSize) {
				iterator newEnd = (otherSize != 0) ?
						std::copy(other.begin(), other.end(), begin()) :
						begin();

				destroy_range(newEnd, end());
				end_ = newEnd;
				return *this;
			}

			if(capacity() < otherSize) {
				clear();
				grow(otherSize);
			}

			std::copy(other.begin(), other.begin() + mySize, this->begin());
			end_ = std::uninitialized_copy(other.begin() + mySize, other.end(), this->begin() + mySize);
			return *this;
		}

//		SmallVectorImpl(SmallVectorImpl &&other) = delete;

		const SmallVectorImpl &operator=(SmallVectorImpl &&other) {
			if(this == &other) return *this;

			if(!other.isSmall()) {
				destroy_range(begin(), end());
				if(!isSmall())
					free(begin());

				this->begin_ = other.begin_;
				this->end_ = other.end_;
				this->capacity_ = other.capacity_;

				other.begin_ = (T*)&other.elements;
				other.end_ = other.capacity_ = other.begin_;
				return *this;
			}

			size_t mySize = size();
			size_t otherSize = other.size();
			if(mySize >= otherSize) {
				iterator newEnd = (otherSize != 0) ?
						std::move(other.begin(), other.end(), begin()) :
						begin();

				destroy_range(newEnd, end());
				end_ = newEnd;
			} else {
				if(capacity() < otherSize) {
					clear();
					grow(otherSize);
				}

				std::move(other.begin(), other.begin() + mySize, begin());
				uninitialized_move(other.begin() + mySize, other.end(), begin() + mySize);
				end_ = begin() + otherSize;
			}

			other.clear();
			return *this;
		}

		~SmallVectorImpl() {
			// Destroy the constructed elements in the vector.
			destroy_range(begin(), end());
			if(!isSmall())
				free(begin());
		}

	public:
		void clear() {
			destroy_range(begin(), end());
			end_ = begin_;
		}

	public:
		/// This returns size()*sizeof(T).
		size_t size_in_bytes() const { return size_t((char *)end_ - (char *)begin_); }

		/// capacity_in_bytes - This returns capacity()*sizeof(T).
		size_t capacity_in_bytes() const {
			return size_t((char *)capacity_ - (char *)begin_);
		}

	public:
		using size_type = size_t;
		using difference_type = ptrdiff_t;
		using value_type = T;
		using iterator = T *;
		using const_iterator = const T *;

		using const_reverse_iterator = std::reverse_iterator<const_iterator>;
		using reverse_iterator = std::reverse_iterator<iterator>;

		using reference = T &;
		using const_reference = const T &;
		using pointer = T *;
		using const_pointer = const T *;

		// forward iterator creation methods.
		iterator begin() { return (iterator)begin_; }
		const_iterator begin() const { return (const_iterator)begin_; }
		iterator end() { return (iterator)end_; }
		const_iterator end() const { return (const_iterator)end_; }

		// reverse iterator creation methods.
		reverse_iterator rbegin() { return reverse_iterator(end()); }
		const_reverse_iterator rbegin() const { return const_reverse_iterator(end()); }
		reverse_iterator rend() { return reverse_iterator(begin()); }
		const_reverse_iterator rend() const { return const_reverse_iterator(begin()); }

	public:
		bool empty() const { return begin_ == end_; }
		size_type size() const { return end_ - begin_;}
		size_type capacity() const { return capacity_ - begin_; }

		pointer data() { return pointer(begin()); }
		const_pointer data() const { return const_pointer(begin()); }

		reference operator[](size_type idx) {
			assert(idx < size());
			return begin()[idx];
		}

		const_reference operator[](size_type idx) const {
			assert(idx < size());
			return begin()[idx];
		}

		reference front() {
			assert(!empty());
			return begin()[0];
		}
		const_reference front() const {
			assert(!empty());
			return begin()[0];
		}

		reference back() {
			assert(!empty());
			return end()[-1];
		}
		const_reference back() const {
			assert(!empty());
			return end()[-1];
		}

	public:
		void reserve(size_t newSize) {
			if(capacity() < newSize)
				grow(newSize);
		}

		void resize(size_type newSize) {
			if(newSize < size()) {
				destroy_range(begin() + newSize, end());
				end_ = (begin() + newSize);
			} else if(newSize > size()) {
				if(capacity() < newSize)
					grow(newSize);

				for(auto I = end(), E = begin() + newSize; I != E; ++I)
					new(&*I) T();

				end_ = (begin() + newSize);
			}
		}

		void resize(size_type newSize, const T & val) {
			if(newSize < size()) {
				destroy_range(begin() + newSize, end());
				end_ = (begin() + newSize);
			} else if(newSize > size()) {
				if(capacity() < newSize)
					grow(newSize);

				std::uninitialized_fill(end(), begin() + newSize, val);

				end_ = (begin() + newSize);
			}
		}

		void push_back(const T &elt) {
			if(UNLIKELY(end_ >= capacity_))
				grow();
			::new(end_) T(elt);
			end_++;
		}

		void push_back(T &&elt) {
			if(UNLIKELY(end_ >= capacity_))
				grow();
			::new(end_) T(::std::move(elt));
			end_++;
		}

		void pop_back() {
			end_--;
			end_->~T();
		}

		template <typename It,
				typename = typename std::enable_if<std::is_convertible<
						typename std::iterator_traits<It>::iterator_category,
						std::input_iterator_tag>::value>::type>
		void append(It in_start, It in_end) {
			size_type count = std::distance(in_start, in_end);
			if(count > size_type(capacity_ - end_))
				grow(size() + count);

			std::uninitialized_copy(in_start, in_end, end());
			end_ = end_ + count;
		}

		void append(size_type count, const T & elt) {
			if(count > size_type(capacity_ - end_))
				grow(size() + count);

			// Copy the new elements over.
			std::uninitialized_fill_n(end(), count, elt);
			end_ = end_ + count;
		}

		void append(std::initializer_list<T> IL) {
			 append(IL.begin(), IL.end());
		}

		void assign(size_type numElts, const T & elt) {
			clear();
			if(capacity() < numElts)
				grow(numElts);
			end_ = begin_ + numElts;
			std::uninitialized_fill(begin(), end(), elt);
		}

		template <typename It,
				typename = typename std::enable_if<std::is_convertible<
						typename std::iterator_traits<It>::iterator_category,
						std::input_iterator_tag>::value>::type>
		void assign(It in_start, It in_end) {
			clear();
			append(in_start, in_end);
		}

		void assign(std::initializer_list<T> IL) {
			clear();
			append(IL);
		}

		iterator erase(const_iterator CI) {
			iterator I = const_cast<iterator>(CI);

			assert(I >= begin() && "Iterator to erase is out of bounds.");
			assert(I < end() && "Erasing at past-the-end iterator.");

			iterator I2 = I;
			std::move(I + 1, end(), I);
			pop_back();
			return (I2);
		}

		iterator erase(const_iterator CS, const_iterator CE) {
			// Just cast away constness because this is a non-const member function.
			iterator S = const_cast<iterator>(CS);
			iterator E = const_cast<iterator>(CE);

			assert(S >= begin() && "Range to erase is out of bounds.");
			assert(S <= E && "Trying to erase invalid range.");
			assert(E <= end() && "Trying to erase past the end.");

			iterator I2 = S;
			iterator I = std::move(E, end(), S);
			destroy_range(I, end());
			end_ = I;
			return (I2);
		}

		iterator insert(iterator I, T &&elt) {
			if(I == end()) {
				push_back(::std::move(elt));
				return end() - 1;
			}

			assert(I >= begin() && "Insertion iterator is out of bounds.");
			assert(I <= end() && "Inserting past the end of the vector.");

			if(end_ >= capacity_) {
				size_t idx = I - begin();
				grow();
				I = begin() + idx;
			}

			::new(end_) T(::std::move(back()));
			std::move_backward(I, end() - 1, end());
			end_++;

			// update reference when moving element already in the vector
			T *ptr = &elt;
			if(I <= ptr && ptr < end_)
				ptr++;

			*I = ::std::move(*ptr);
			return I;
		}

		iterator insert(iterator I, const T &elt) {
			if(I == end()) {
				push_back(elt);
				return end() - 1;
			}

			assert(I >= begin() && "Insertion iterator is out of bounds.");
			assert(I <= end() && "Inserting past the end of the vector.");

			if(end_ >= capacity_) {
				size_t idx = I - begin();
				grow();
				I = begin() + idx;
			}

			::new(end_) T(::std::move(back()));
			std::move_backward(I, end() - 1, end());
			end_++;

			// update reference when moving element already in the vector
			T *ptr = &elt;
			if(I <= ptr && ptr < end_)
				ptr++;

			*I = *ptr;
			return I;
		}

		template <typename It,
				typename = typename std::enable_if<std::is_convertible<
						typename std::iterator_traits<It>::iterator_category,
						std::input_iterator_tag>::value>::type>
		iterator insert(iterator I, It from, It to) {
			size_t idx = I - begin();

			if(I == end()) {
				append(from, to);
				return begin() + idx;
			}

			assert(I >= begin() && "Insertion iterator is out of bounds.");
			assert(I <= end() && "Inserting past the end of the vector.");

			size_t count = std::distance(from, to);

			reserve(size() + count);

			// Uninvalidate the iterator.
			I = begin() + idx;

			if(size_t(end() - I) >= count) {
				T *oldEnd = end();
				append(std::move_iterator<iterator>(end() - count),
						std::move_iterator<iterator>(end()));

				// Copy the existing elements that get replaced.
				std::move_backward(I, oldEnd - count, oldEnd);

				std::copy(from, to, I);
			} else {
				T *oldEnd = end();
				end_ = end_ + count;
				size_t numOverwritten = oldEnd - I;
				uninitialized_move(I, oldEnd, end() - numOverwritten);

				// Replace the overwritten part.
				for(T *J = I; numOverwritten > 0; --numOverwritten) {
					*J = *from;
					++J;
					++from;
				}

				std::uninitialized_copy(from, to, oldEnd);
			}
			return I;
		}

		void insert(iterator I, std::initializer_list<T> IL) {
			insert(I, IL.begin(), IL.end());
		}

		template <typename... ArgTypes>
		void emplace_back(ArgTypes &&... Args) {
			if(UNLIKELY(end_ >= capacity_))
				grow();

			::new(end_) T(std::forward<ArgTypes>(Args)...);
			end_++;
		}

		bool isSmall() const { return begin_ == reinterpret_cast<const T *>(&elements); }

	protected:
		static void destroy_range(T *S, T *E) {
			while(S != E) {
				--E;
				E->~T();
			}
		}

		template <typename It1, typename It2>
		static void uninitialized_move(It1 I, It1 E, It2 Dest) {
			std::uninitialized_copy(
					std::make_move_iterator(I), std::make_move_iterator(E), Dest);
		}

		void grow(size_t minSize=0) {
			size_t curCapacity = capacity();
			size_t curSize = size();

			// Always grow, even from zero.
			size_t newCapacity = size_t(math::nextPowerOf2(curCapacity + 2));
			if(newCapacity < minSize)
				newCapacity = minSize;

			T *newElts = static_cast<T *>(malloc(newCapacity * sizeof(T)));

			// Move the elements over.
			this->uninitialized_move(begin(), end(), newElts);

			// Destroy the original elements.
			destroy_range(begin(), end());

			// If this wasn't grown from the inline copy, deallocate the old space.
			if(!isSmall())
				free(begin());

			this->begin_ = newElts;
			this->end_ = newElts + curSize;
			this->capacity_ = begin() + newCapacity;
		}

	protected:
		T *begin_, *end_, *capacity_;
		details::AlignedCharArray<T> elements;
};

template <typename T, size_t N>
class SmallVector : public SmallVectorImpl<T> {
	public:
		SmallVector() : SmallVectorImpl<T>(N) {}
		SmallVector(size_t size, const T &value = T()) : SmallVector() {
			this->assign(size, value);
		}
		SmallVector(std::initializer_list<T> list) : SmallVector(N) {
			this->assign(list);
		}

		template <typename It,
				typename = typename std::enable_if<std::is_convertible<
						typename std::iterator_traits<It>::iterator_category,
						std::input_iterator_tag>::value>::type>
		SmallVector(It S, It E) : SmallVector() {
			this->assign(S, E);
		}

		SmallVector(const SmallVector &other) : SmallVector() {
			if( ! other.empty() )
				operator =(other);
		}

		const SmallVector &operator=(const SmallVector &other) {
			SmallVectorImpl<T>::operator=(other);
			return *this;
		}

		SmallVector(SmallVector &&other) : SmallVector() {
			if( ! other.empty() )
				operator=(::std::move(other));
		}

		const SmallVector &operator=(SmallVector &&other) {
			SmallVectorImpl<T>::operator=(std::move(other));
			return *this;
		}

		template <size_t M>
		SmallVector(const SmallVector<T,M> &other) : SmallVector() {
			if( ! other.empty() )
				operator =(other);
		}

		template <size_t M>
		const SmallVector &operator=(const SmallVector<T,M> &other) {
			SmallVectorImpl<T>::operator=(other);
			return *this;
		}

		template <size_t M>
		SmallVector(SmallVector<T,M> &&other) : SmallVector() {
			if( ! other.empty() )
				operator=(::std::move(other));
		}

		template <size_t M>
		const SmallVector &operator=(SmallVector<T,M> &&other) {
			SmallVectorImpl<T>::operator=(std::move(other));
			return *this;
		}

	private:
		details::SmallVectorStorage<T, N> storage;
};
#endif

