/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MASKED_FUNCTION_H
#define MASKED_FUNCTION_H

#include <Function.hpp>
#include <LocalMatrix.hpp>

template <int N = Eigen::Dynamic>
class DIFFMEM_EXPORT MaskedFunction final : public Function {
	public:
		MaskedFunction(Function &func, const VecNd<N> &vals, const VecNb<N> &mask)
				: func(func), vals(vals), mask(mask) {
		}

	public:
		double operator()(ConstRefVecd &x) const override {
			LOCAL_VECTOR(Vecd, p, vals.size());
			Require(x.size() == mask.count());

			int idx = 0;
			for(int i = 0; i < p.size(); ++i)
				p[i] = mask[i] ? x[idx++] : vals[i];

			return func(p);
		}

		double operator()(ConstRefVecd &x, RefVecd grad) const override {
			LOCAL_VECTOR(Vecd, p, vals.size());
			LOCAL_VECTOR(Vecd, g, vals.size());
			Require(x.size() == mask.count());
			Require(grad.size() == mask.count());

			for(int i = 0, idx = 0; i < p.size(); ++i)
				p[i] = mask[i] ? x[idx++] : vals[i];

			double fp = func(p, g);

			for(int i = 0, idx = 0; i < g.size(); ++i)
				if(mask[i])
					grad[idx++] = g[i];
			return fp;
		}

		double operator()(ConstRefVecd &x, RefVecd grad, RefMatrixd hess) const override {
			LOCAL_VECTOR(Vecd, p, vals.size());
			LOCAL_VECTOR(Vecd, g, vals.size());
			LOCAL_MATRIX(Matrixd, H, vals.size(), vals.size());

			Require(x.size() == mask.count());
			Require(grad.size() == mask.count());
			Require(H.rows() == mask.count() && H.cols() == mask.count());

			int idx = 0;
			for(int i = 0; i < p.size(); ++i)
				p[i] = mask[i] ? x[idx++] : vals[i];

			double fp = func(p, g);

			for(int i = 0, idx_i = 0; i < g.size(); ++i) {
				if(mask[i]) {
					grad[idx_i] = g[i];

					for(int j = 0, idx_j = 0; j < g.size(); ++j) {
						if(mask[j]) {
							hess(idx_i, idx_j) = H(i, j);
							idx_j++;
						}
					}

					idx_i++;
				}
			}

			return fp;
		}

	private:
		Function &func;
		VecNd<N> vals;
		VecNb<N> mask;	// true => keep, false => remove
};
#endif
