#ifndef DISTRIBUTION_SAMPLER_H
#define DISTRIBUTION_SAMPLER_H

#include <MatrixVector.hpp>
#include <memory>

class Random;

class DIFFMEM_EXPORT DistributionSampler {
public:
	virtual ~DistributionSampler() {}

public:
	virtual void reset() {}
	virtual void sample(Random & rng, RefMatrixd samples) = 0;
};

template <typename Distr>
class BasicSampler : public DistributionSampler {
public:
	BasicSampler(std::shared_ptr<const Distr> dist) : dist(std::move(dist)) {}

	void sample(Random & rng, RefMatrixd samples) override {
		for(int k = 0; k < samples.cols(); k++) {
			dist->sample(rng, samples.col(k));
		}
	}

private:
	std::shared_ptr<const Distr> dist;
};

template <typename Distr>
auto make_basic_sampler(std::shared_ptr<Distr> dist) {
	return std::make_unique<BasicSampler<Distr>>(std::move(dist));
}
#endif //DISTRIBUTION_SAMPLER_H
