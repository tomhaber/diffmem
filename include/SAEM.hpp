/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SAEM_H
#define SAEM_H

#include <MEModel.hpp>
#include <OptimizationTrace.hpp>
#include <mcmc/ConditionalMESampler.hpp>
#include <memory>

class Random;
class DIFFMEM_EXPORT SAEM {
	public:
		struct Options {
			int K1{250};
			int K2{100};
			int burnin{50};
			int thinning{32};
			MEOptimizationTraceCallback trace{nullptr};

			void validate() {
				Require(burnin < K1, "Burnin should be smaller than K1");
				Require(thinning > 0, "Thinning should be larger than 0");
			}

			double stepsize(int iteration) const {
				if( iteration < burnin )
					return 0.0;
				else if( iteration < K1 )
					return 1.0;
				else return double(iteration - K1) / double(K2);
			}

			int numberOfWarmupIterations() const { return burnin; }
			int numberOfIterations() const { return K1+K2; }

			Options() {}
		};

	public:
		SAEM(MEModel & model, const Options & options = Options());

	public:
		void optimize(Random & rng);
		void step(Random & rng, int iteration);
		void warmup(Random & rng);

	private:
		MEModel & model;
		std::unique_ptr<ConditionalMEDistribution> distribution;
		std::unique_ptr<mcmc::ConditionalMESampler> sampler;
		Options options;
		const int N;

	private:
		Matrixd BInvPhi, mu, eta;

	private:
		Matrixd S0, S1, S2;

	friend class ErrorHelper;
};
#endif
