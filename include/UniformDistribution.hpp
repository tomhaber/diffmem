/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef UNIFORMDISTRIBUTION_H
#define UNIFORMDISTRIBUTION_H

#include <Distribution.hpp>
#include <DistributionSampler.hpp>
#include <Random.hpp>
#include <math/number.hpp>
#include <stats/uniform.hpp>

class UniformDistribution : public Distribution {
public:
	UniformDistribution(const Dict &d);

	UniformDistribution(double lb, double ub)
		: UniformDistribution(VecNd<1>(lb), VecNd<1>(ub)) {
	}

	UniformDistribution(ConstRefVecd &lb, ConstRefVecd &ub)
			: lb_(lb), ub_(ub) {
		logpdf_ = (ub - lb).array().log().sum();
	}

	int numberOfDimensions() const override { return lb_.size(); }

public:
	double logpdf(ConstRefVecd &x) const override {
		for(int i = 0; i < x.size(); ++i) {
			if(x(i) < lb_(i) || x(i) > ub_(i))
				return math::negInf();
		}

		return logpdf_;
	}

	void sample(Random &rng, RefVecd s) const {
		Require(s.size() == lb_.size());
		for(int i = 0; i < lb_.size(); ++i) {
			s[i] = stats::Uniform::sample(rng, lb_(i), ub_(i));
		}
	}

	std::unique_ptr<DistributionSampler> sampler(const Dict &dict) const override;

private:
	Vecd lb_;
	Vecd ub_;
	double logpdf_;
};
#endif /* UNIFORMDISTRIBUTION_H */
