/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef NELDERMEAD_H
#define NELDERMEAD_H

#include <MatrixVector.hpp>
#include <Function.hpp>
#include <OptimizationTrace.hpp>

class DIFFMEM_EXPORT NelderMeadState {
	public:
		void init(ConstRefVecd & x, const Function & func, double usualDelta, double zeroDelta);
		void updateIndices();

	public:
		double getBest() const {
			return values[best];
		}
		double getWorst() const {
			return values[worst];
		}
		double getNearWorst() const {
			return values[near_worst];
		}

		void getBest(Vecd *v, double *val) const {
			*v = points.col(best);
			*val = values[best];
		}

	public:
		double tryMove(const Function & func, double fac);
		void shrink(const Function & func);

	private:
		Matrixd points;
		Vecd values;
		int best, worst, near_worst;
		Vecd centroid, tmp;
};

class DIFFMEM_EXPORT NelderMead {
	public:
		struct Options {
			double fTol{1e-8};
			double alpha{-1.0}; // reflection factor
			double beta{0.5}; 	// contraction factor
			double gamma{2.0}; 	// expansion factor
			double usualDelta{5.0e-2};
			double zeroDelta{0.25e-3};
			int maxiters{1000};
			OptimizationTraceCallback trace{nullptr};

			Options() {}
		};

	public:
		NelderMead(const Function & d, ConstRefVecd & initial_x, const Options & options = Options());

	public:
		int optimize();
		int optimize(const Options & opts);
		bool step();

	public:
		bool isConverged() const;
		const Vecd & optimum() const;
		double value() const;

	private:
		const Function & func;
		Options options;

	private:
		NelderMeadState state;
		Vecd x;
		double fVal;
};

inline const Vecd & NelderMead::optimum() const {
	return x;
}

inline double NelderMead::value() const {
	return fVal;
}

inline int NelderMead::optimize(const Options & options) {
	this->options = options;
	return optimize();
}
#endif
