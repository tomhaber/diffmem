/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PARALLEL_REDUCE_SUM_H
#define PARALLEL_REDUCE_SUM_H

#include <tbb/parallel_for.h>
#include <tbb/parallel_reduce.h>
#include <tbb/blocked_range.h>
#include <tbb/combinable.h>
#include <function_traits.hpp>
#include <Cancelable.hpp>

template <typename T>
struct isSimple : std::is_arithmetic<T> {};

namespace details {
	template <typename U, typename T>
	struct CombinableHelper {
		template <typename OtherT>
		CombinableHelper(OtherT && init) : sum(std::forward<OtherT>(init)) {}
		U tmp;
		T sum;
	};

	template <typename FT>
	struct reduce_helper;

	template <typename It, typename T>
	struct reduce_helper<void(It, T&)> {
		template <typename Range, typename F>
		static std::enable_if_t<std::is_convertible<typename Range::const_iterator, It>::value, T>
		run(const Range &range, F &&f, const T &zero) {
			using CT = details::CombinableHelper<T, T>;

			tbb::combinable<CT> sums([&zero] { return zero; });
			tbb::parallel_for(
				range,
				[&f, &sums](Range &r) {
					CT &ct = sums.local();
					for(typename Range::const_iterator i = r.begin(); i != r.end(); ++i) {
						std::forward<F>(f)(i, ct.tmp);
						ct.sum += ct.tmp;
					}
				}
			);

			T val{zero};
			sums.combine_each(
				[&val](const CT &x) {
					val += x.sum;
				}
			);
			return val;
		}
	};

	template <typename It, typename T>
	struct reduce_helper<void(It, Cancelable<T>&)> {
		template <typename Range, typename F>
		static std::enable_if_t<std::is_convertible<typename Range::const_iterator, It>::value, Cancelable<T>>
		run(const Range &range, F &&f, const Cancelable<T> &zero) {
			using CT = details::CombinableHelper<Cancelable<T>, Cancelable<T>>;

			tbb::combinable<CT> sums([&zero] { return zero; });
			tbb::task_group_context ctx;
			tbb::parallel_for(
				range,
				[&f, &sums](Range &r) {
					CT &ct = sums.local();
					for(typename Range::const_iterator i = r.begin(); i != r.end() && !ct.sum.isCancelled(); ++i) {
						std::forward<F>(f)(i, ct.tmp);
						ct.sum += ct.tmp;
					}

					if(ct.sum.isCancelled())
						tbb::task::self().cancel_group_execution();
				}, ctx
			);

			if(!ctx.is_group_execution_cancelled()) {
				Cancelable<T> val{zero};
				sums.combine_each(
					[&val](const CT &x) {
						val += x.sum;
					});
				return val;
			} else {
				return cancelled;
			}
		}
	};

	template <typename T, typename It>
	struct reduce_helper<Cancelable<T>(It)> {
		template <typename Range, typename F, typename U>
		static std::enable_if_t<
			std::is_convertible<typename Range::const_iterator, It>::value && !isSimple<T>::value, Cancelable<T>>
		run(const Range &range, F &&f, const U &zero) {
			tbb::combinable<Cancelable<T>> sums([&zero] { return zero; });

			tbb::task_group_context ctx;
			tbb::parallel_for(
				range,
				[&f, &sums](Range &r) {
					Cancelable<T> &sum = sums.local();
					for(typename Range::const_iterator i = r.begin(); i != r.end() && !sum.isCancelled(); ++i) {
						sum += std::forward<F>(f)(i);
					}

					if(sum.isCancelled())
						tbb::task::self().cancel_group_execution();
				}, ctx
			);

			if(!ctx.is_group_execution_cancelled()) {
				Cancelable<T> val = zero;
				sums.combine_each(
					[&val](const Cancelable<T> &x) {
						val += x;
					});
				return val;
			} else {
				return cancelled;
			}
		}

		template <typename Range, typename F, typename U>
		static std::enable_if_t<
			std::is_convertible<typename Range::const_iterator, It>::value && isSimple<T>::value, Cancelable<T>>
		run(const Range &range, F &&f, const U &zero) {
			tbb::task_group_context ctx;
			Cancelable<T> val = tbb::parallel_reduce(
				range,
				Cancelable<T>(zero),
				[&f](Range &r, Cancelable<T> sum) {
					for(typename Range::const_iterator i = r.begin(); i != r.end() && !sum.isCancelled(); ++i) {
						sum += std::forward<F>(f)(i);
					}

					if(sum.isCancelled())
						tbb::task::self().cancel_group_execution();

					return sum;
				},
				std::plus<Cancelable<T>>(),
				__TBB_DEFAULT_PARTITIONER(),
				ctx
			);

			if(ctx.is_group_execution_cancelled())
				val = cancelled;
			return val;
		}
	};

	template <typename T, typename It>
	struct reduce_helper<T(It)> {
		template <typename Range, typename F, typename U>
		static std::enable_if_t<
			std::is_convertible<typename Range::const_iterator, It>::value && !isSimple<T>::value, T>
		run(const Range &range, F &&f, const U &zero) {
			tbb::combinable<T> sums([&zero] { return zero; });
			tbb::parallel_for(
				range,
				[&f, &sums](Range &r) {
					T &sum = sums.local();
					for(typename Range::const_iterator i = r.begin(); i != r.end(); ++i) {
						sum += std::forward<F>(f)(i);
					}
				}
			);

			T val = zero;
			sums.combine_each(
				[&val](const T &x) {
					val += x;
				}
			);
			return val;
		}

		template <typename Range, typename F, typename U>
		static std::enable_if_t<
			std::is_convertible<typename Range::const_iterator, It>::value && isSimple<T>::value, T>
		run(const Range &range, F &&f, const U &zero) {
			return tbb::parallel_reduce(
				range,
				zero,
				[&f](Range &r, T sum) {
					for(typename Range::const_iterator i = r.begin(); i != r.end(); ++i) {
						sum += std::forward<F>(f)(i);
					}
					return sum;
				},
				std::plus<T>()
			);
		}
	};

	template <typename T, typename It, typename U>
	struct reduce_helper<Cancelable<T>(It, U&)> {
		template <typename Range, typename F>
		static std::enable_if_t<
			std::is_convertible<typename Range::const_iterator, It>::value, Cancelable<T>>
		run(const Range &range, F &&f, const Cancelable<T> &zero) {
			using CT = details::CombinableHelper<U, Cancelable<T>>;

			tbb::combinable<CT> sums([&zero] { return zero; });
			tbb::task_group_context ctx;
			tbb::parallel_for(
				range,
				[&f, &sums](Range &r) {
					CT &ct = sums.local();
					for(typename Range::const_iterator i = r.begin(); i != r.end() && !ct.sum.isCancelled(); ++i) {
						ct.sum += std::forward<F>(f)(i, ct.tmp);
					}

					if(ct.sum.isCancelled())
						tbb::task::self().cancel_group_execution();
				}, ctx
			);

			if(!ctx.is_group_execution_cancelled()) {
				Cancelable<T> val = zero;
				sums.combine_each(
					[&val](const Cancelable<T> &x) {
						val += x.sum;
					});
				return val;
			} else {
				return cancelled;
			}
		}
	};

	template <typename DT, typename It, typename U>
	struct reduce_helper<DT(It, U&)> {
		template <typename Range, typename F>
		static std::enable_if_t<
			std::is_convertible<typename Range::const_iterator, It>::value, std::decay_t<DT>>
		run(const Range &range, F &&f, const std::decay_t<DT> &zero) {
			using T = std::decay_t<DT>;
			using CT = details::CombinableHelper<U, T>;

			tbb::combinable<CT> sums([&zero] { return zero; });
			tbb::parallel_for(
				range,
				[&f, &sums](Range &r) {
					CT &ct = sums.local();
					for(typename Range::const_iterator i = r.begin(); i != r.end(); ++i) {
						ct.sum += std::forward<F>(f)(i, ct.tmp);
					}
				}
			);

			T val{zero};
			sums.combine_each(
				[&val](const CT &x) {
					val += x.sum;
				}
			);
			return val;
		}
	};
}

template <typename Range, typename F, typename U>
auto parallel_reduce_sum(const Range &range, F &&f, const U &zero) {
	return details::reduce_helper<typename function_traits<F>::function_type>::run(range, std::forward<F>(f), zero);
}

template <typename Index, typename F, typename U>
auto parallel_reduce_sum(Index first, Index last, F &&f, const U &zero) {
	return parallel_reduce_sum(tbb::blocked_range<Index>(first, last), std::forward<F>(f), zero);
}
#endif
