/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef RANDOM_H
#define RANDOM_H

#include <cbrng.hpp>
#include <MatrixVector.hpp>

#include <random>
#include <vector>
#include <algorithm>

class ParallelRandom;

template <typename Distribution>
class RandomDistribution;

class DIFFMEM_EXPORT Random {
public:
	Random() {}
	Random(unsigned long seed);
	template <typename T>
	Random(std::initializer_list<T> l);
	Random(const Random &r);

public:
	std::vector<int> randperm(int n);

	template <typename T>
	void randperm(int n, T indices[]);

	template <typename T>
	void randperm(std::vector<T> &v);

public:
	void jump(size_t s);
	ParallelRandom split(size_t nstreams, size_t num_random = UINT_MAX);

public:
	template <typename Distribution, typename... Args>
	typename Distribution::result_type rand(Distribution &dist, Args &&...args) {
		return dist(r, std::forward<Args>(args)...);
	}

	template <typename Distribution>
	RandomDistribution<Distribution> build(Distribution && dist);

private:
#ifdef USE_PHILOX
	cbrng<philox<2, uint64_t, 7>, uint32_t> r;
#else
	cbrng<ars<uint64_t, 7>, uint32_t> r;
#endif
};

template <typename Distribution>
class RandomDistribution {
public:
	typedef typename Distribution::result_type result_type;
	typedef typename Distribution::param_type param_type;
	RandomDistribution(Random &rng, Distribution dist) : rng(rng), dist(std::move(dist)) {}

public:
	result_type operator()() { return rng.rand(dist); }
	result_type operator()(const param_type &value) { return rng.rand(dist, value); }

	result_type min() const { return dist.min(); }
	result_type max() const { return dist.max(); }

private:
	Random &rng;
	Distribution dist;
};

inline Random::Random(unsigned long seed) : r(seed) {
}

template <typename T>
inline Random::Random(std::initializer_list<T> l) : r(l) {
}

inline Random::Random(const Random &rng)
		: r(rng.r) {
}

inline std::vector<int> Random::randperm(int n) {
	std::vector<int> perm(n);

	for(int i = 0; i < n; i++)
		perm[i] = i;

	std::shuffle(perm.begin(), perm.end(), r);
	return perm;
}

template <typename T>
void Random::randperm(std::vector<T> &v) {
	std::random_shuffle(v.begin(), v.end(), r);
}

template <typename T>
void Random::randperm(int n, T indices[]) {
	std::random_shuffle(indices, indices + n, r);
}

inline void Random::jump(size_t s) {
	r.discard(s);
}

template <typename Distribution>
RandomDistribution<Distribution> Random::build(Distribution &&dist) {
	return RandomDistribution<Distribution>(*this, std::forward<Distribution>(dist));
}

namespace internal {

	template <typename Distribution>
	struct random_dist_op {
		typedef typename Distribution::result_type result_type;
		mutable RandomDistribution<Distribution> dist;

		random_dist_op(RandomDistribution<Distribution> dist) : dist(dist) {}

		template <typename Index>
		inline result_type operator()(Index, Index = 0) const {
			return dist();
		}
	};
}

class ParallelRandom {
public:
	ParallelRandom(Random &orig_rng, size_t nstreams, size_t nrandom)
			: rng(orig_rng), nstreams(nstreams), nrandom(nrandom) {
		for(size_t i = 0; i < nstreams; ++i)
			orig_rng.jump(nrandom);
	}

	Random operator[](size_t id) const {
		Random ret = rng;
		for(size_t i = 0; i < id; ++i)
			ret.jump(nrandom);
		return ret;
	}

	Random operator()(size_t id) const {
		return (*this)[id];
	}

private:
	Random rng;
	size_t nstreams, nrandom;
};

inline ParallelRandom Random::split(size_t nstreams, size_t num_random) {
	return ParallelRandom(*this, nstreams, num_random);
}

template <typename Derived, typename Distribution>
inline Eigen::CwiseNullaryOp<internal::random_dist_op<Distribution>, Derived>
generate_random(RandomDistribution<Distribution> rd, typename Derived::Index m, typename Derived::Index n=1) {
	return {m, n, internal::random_dist_op<Distribution>(rd)};
}
#endif
