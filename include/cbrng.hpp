/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Implementation based on the paper "Parallel random numbers: as easy as 1, 2, 3"
 * and code from John K. Salmon et al. https://www.thesalmons.org/john/random123/
 */

#ifndef CBRNG_H
#define CBRNG_H

#include <random>
#include <stdint.h>
#include <bigint.hpp>
#include <Exception.hpp>
#include <random>

#if defined(__i386__) || defined(__x86_64__)
#	include <wmmintrin.h>
#endif

template <unsigned _N, typename T>
struct philox_constants {
};

template <>
struct philox_constants<2, uint64_t> {
	static const uint64_t M0 = UINT64_C(0xD2B74407B1CE6E93);
	static const uint64_t W0 = UINT64_C(0x9E3779B97F4A7C15);
};

template <>
struct philox_constants<2, uint32_t> {
	static const uint32_t M0 = UINT32_C(0xD256D193);
	static const uint32_t W0 = UINT32_C(0x9E3779B9);
};

template <>
struct philox_constants<4, uint64_t> {
	static const uint64_t M0 = UINT64_C(0xD2E7470EE14C6C93);
	static const uint64_t M1 = UINT64_C(0xCA5A826395121157);
	static const uint64_t W0 = UINT64_C(0x9E3779B97F4A7C15); /* golden ratio */
	static const uint64_t W1 = UINT64_C(0xBB67AE8584CAA73B); /* sqrt(3)-1 */
};

template <>
struct philox_constants<4, uint32_t> {
	static const uint32_t M0 = UINT32_C(0xD2511F53);
	static const uint32_t M1 = UINT32_C(0xCD9E8D57);
	static const uint32_t W0 = UINT64_C(0x9E3779B9); /* golden ratio */
	static const uint32_t W1 = UINT64_C(0xBB67AE85); /* sqrt(3)-1 */
};

template <size_t N, typename T>
struct threefry_constants {
};

template <>
struct threefry_constants<2, uint32_t> {
	static constexpr uint32_t KS_PARITY = UINT32_C(0x1BD11BDA);
	static constexpr unsigned Rotations[8] = {13, 15, 26, 6, 17, 29, 16, 24};
};

template <>
struct threefry_constants<4, uint32_t> {
	static constexpr uint32_t KS_PARITY = UINT32_C(0x1BD11BDA);
	static constexpr unsigned Rotations0[8] = {10, 11, 13, 23, 6, 17, 25, 18};
	static constexpr unsigned Rotations1[8] = {26, 21, 27, 5, 20, 11, 10, 20};
};

template <>
struct threefry_constants<2, uint64_t> {
	static constexpr uint64_t KS_PARITY = UINT64_C(0x1BD11BDAA9FC1A22);
	static constexpr unsigned Rotations[8] = {16, 42, 12, 31, 16, 32, 24, 21};
};

template <>
struct threefry_constants<4, uint64_t> {
	static constexpr uint64_t KS_PARITY = UINT64_C(0x1BD11BDAA9FC1A22);
	static constexpr unsigned Rotations0[8] = {14, 52, 23, 5, 25, 46, 58, 32};
	static constexpr unsigned Rotations1[8] = {16, 57, 40, 37, 33, 12, 22, 32};
};

namespace details {
	template <typename T>
	T rotl(T x, unsigned s) {
		static_assert( std::is_unsigned<T>::value && std::is_integral<T>::value, "");
		return (x << s) | (x >> (std::numeric_limits<T>::digits - s));
	}

	template <typename T>
	struct dbl_uint_t : std::false_type {
	};

	template <>
	struct dbl_uint_t<uint32_t> : std::true_type {
		typedef uint64_t type;
	};

#ifndef UINT128MAX
	template <>
	struct dbl_uint_t<uint64_t> : std::true_type {
		typedef __uint128_t type;
	};
#endif

	template <typename T>
	inline typename std::enable_if_t<dbl_uint_t<T>::value, T>
	mulhilo(T a, T b, T &hip) {
		typedef typename dbl_uint_t<T>::type DblT;
		DblT product = ((DblT)a) * ((DblT)b);
		hip = product >> std::numeric_limits<T>::digits;
		return (T)product;
	}

	template <typename T>
	inline typename std::enable_if_t<!dbl_uint_t<T>::value, T>
	mulhilo(T a, T b, T &hip) {
		const unsigned WHALF = std::numeric_limits<T>::digits/2;
		const T LOMASK = ((T)(~(T)0)) >> WHALF;

		T lo = a*b;
		T ahi = a>>WHALF;
		T alo = a& LOMASK;
		T bhi = b>>WHALF;
		T blo = b& LOMASK;

		T ahbl = ahi*blo;
		T albh = alo*bhi;

		T ahbl_albh = ((ahbl&LOMASK) + (albh&LOMASK));
		T hi = (ahi*bhi) + (ahbl>>WHALF) +  (albh>>WHALF);
		hi += ahbl_albh >> WHALF;

		/* carry from the sum with alo*blo */
		hi += ((lo >> WHALF) < (ahbl_albh&LOMASK));
		hip = hi;
		return lo;
	}
}

template <size_t N, typename T=uint64_t, size_t R=20>
class threefry {
};

template <typename T, size_t R>
class threefry<2,T,R> {
public:
	typedef bigint<2,T> key_type;
	typedef bigint<2,T> counter_type;
	typedef threefry_constants<2, T> Constants;

	constexpr threefry(key_type k = key_type()) : k(k) {}
	constexpr void setkey(key_type _k) {
		k = _k;
	}

	counter_type operator()(counter_type c) const {
		T ks[3];
		ks[2] = Constants::KS_PARITY;
		ks[0] = k[0]; ks[2] ^= k[0]; c[0] += k[0];
		ks[1] = k[1]; ks[2] ^= k[1]; c[1] += k[1];

		for(unsigned r = 0; r < R;) {
			c[0] += c[1];
			c[1] = details::rotl(c[1], Constants::Rotations[r % 8]);
			c[1] ^= c[0];
			++r;
			if((r & 3) == 0) {
				unsigned r4 = r >> 2;
				c[0] += ks[r4 % 3];
				c[1] += ks[(r4 + 1) % 3] + r4;
			}
		}
		return c;
	}

private:
	key_type k;
};

template <typename T, size_t R>
class threefry<4,T,R> {
public:
	typedef bigint<4,T> key_type;
	typedef bigint<4,T> counter_type;
	typedef threefry_constants<4, T> Constants;

	constexpr threefry(key_type k_ = key_type()) : k(k_) {}
	constexpr void setkey(key_type k_) {
		k = k_;
	}

	counter_type operator()(counter_type c) const {
		T ks[5];
		ks[4] = Constants::KS_PARITY;
		ks[0] = k[0]; ks[4] ^= k[0]; c[0] += k[0];
		ks[1] = k[1]; ks[4] ^= k[1]; c[1] += k[1];
		ks[2] = k[2]; ks[4] ^= k[2]; c[2] += k[2];
		ks[3] = k[3]; ks[4] ^= k[3]; c[3] += k[3];

		// clang does not seem to unroll this loop correctly
	//	boost::mpl::for_each<boost::mpl::range_c<unsigned, 0, R>>(
		//	[&c, &ks](unsigned r) {
		for(unsigned r = 0; r < R; ++r) {
				if((r & 1) == 0) {
					c[0] += c[1]; c[1] = details::rotl(c[1],Constants::Rotations0[r%8]); c[1] ^= c[0];
					c[2] += c[3]; c[3] = details::rotl(c[3],Constants::Rotations1[r%8]); c[3] ^= c[2];
				} else {
					c[0] += c[3]; c[3] = details::rotl(c[3],Constants::Rotations0[r%8]); c[3] ^= c[0];
					c[2] += c[1]; c[1] = details::rotl(c[1],Constants::Rotations1[r%8]); c[1] ^= c[2];
				}
				unsigned r1 = r+1;
				if((r1 & 3) == 0) {
					unsigned r4 = r1>>2;
					c[0] += ks[(r4+0)%5];
					c[1] += ks[(r4+1)%5];
					c[2] += ks[(r4+2)%5];
					c[3] += ks[(r4+3)%5] + r4;
				}
			}
		//);
		return c;
	}

private:
	key_type k;
};

template <size_t N, typename T=uint64_t, size_t R=10>
struct philox{
};

template <typename T, size_t R>
class philox<2,T,R> {
public:
	typedef bigint<1,T> key_type;
	typedef bigint<2,T> counter_type;
	typedef philox_constants<2, T> Constants;

	constexpr philox(key_type k = key_type()) : k(k) {}
	constexpr void setkey(key_type _k) {
		k = _k;
	}

	counter_type operator()(counter_type c) const {
		key_type key = k;
		for(unsigned r = 0; r < R; ++r) {
			T hi;
			T lo = details::mulhilo(Constants::M0, c[0], hi);
			c[0] = hi^key[0]^c[1];
			c[1] = lo;
			key[0] += Constants::W0;
		}
		return c;
	}

private:
	key_type k;
};

template <typename T, size_t R>
class philox<4,T,R> {
public:
	typedef bigint<2,T> key_type;
	typedef bigint<4,T> counter_type;
	typedef philox_constants<4, T> Constants;

	constexpr philox(key_type k = key_type()) : k(k) {}
	constexpr void setkey(key_type _k) {
		k = _k;
	}

	counter_type operator()(counter_type c) const {
		key_type key = k;
		for(unsigned r = 0; r < R; ++r) {
			T hi0;
			T hi1;
			T lo0 = details::mulhilo(Constants::M0, c[0], hi0);
			T lo1 = details::mulhilo(Constants::M1, c[2], hi1);
			c[0] = hi1 ^ c[1] ^ key[0];
			c[1] = lo1;
			c[2] = hi0 ^ c[3] ^ key[1];
			c[3] = lo0;
			key[0] += Constants::W0;
			key[1] += Constants::W1;
		}
		return c;
	}

private:
	key_type k;
};

#ifdef __AES__
template <size_t R>
__m128i ars1xm128i(__m128i c, __m128i k) {
	__m128i kweyl = _mm_set_epi64x(
		0xBB67AE8584CAA73BUL, /* sqrt(3) - 1.0 */
		0x9E3779B97F4A7C15UL /* golden ratio */
	);

	__m128i v = _mm_xor_si128(c, k);
	for(size_t r = 1; r < R; ++r) {
		k = _mm_add_epi64(k, kweyl);
		v = _mm_aesenc_si128(v, k);
	}

	k = _mm_add_epi64(k, kweyl);
	v = _mm_aesenclast_si128(v, k);
	return v;
}
#else
DIFFMEM_EXPORT void ars4x32(int R, uint32_t *c, const uint32_t *k);
#endif

template <typename T=uint64_t, size_t R=20>
class ars {
};

template <size_t R>
class ars<uint32_t, R> {
public:
	typedef bigint<4, uint32_t> key_type;
	typedef bigint<4, uint32_t> counter_type;

	ars(key_type k = key_type()) : k(k) {}
	void setkey(key_type _k) {
		k = _k;
	}

	counter_type operator()(counter_type c) const {
#ifdef __AES__
		__m128i c128 = _mm_set_epi32(c[3], c[2], c[1], c[0]);
		__m128i k128 = _mm_set_epi32(k[3], k[2], k[1], k[0]);
		c128 = ars1xm128i<R>(c128, k128);
		_mm_storeu_si128((__m128i*)c.data(), c128);
#else
		ars4x32(R, c.data(), k.data());
#endif
		return c;
	}

private:
	key_type k;
};

template <size_t R>
class ars<uint64_t, R> {
public:
	typedef bigint<2, uint64_t> key_type;
	typedef bigint<2, uint64_t> counter_type;

	ars(key_type k = key_type()) : k(k) {}
	void setkey(key_type _k) {
		k = _k;
	}

	counter_type operator()(counter_type c) const {
#ifdef __AES__
		__m128i c128 = _mm_set_epi64x(c[1], c[0]);
		__m128i k128 = _mm_set_epi64x(k[1], k[0]);
		c128 = ars1xm128i<R>(c128, k128);
		_mm_storeu_si128((__m128i*)c.data(), c128);
#else
		ars4x32(R, (uint32_t*)c.data(), (uint32_t*)k.data());
#endif
		return c;
	}

private:
	key_type k;
};

template <typename Prf, typename T=uint32_t>
class cbrng {
public:
	typedef typename Prf::counter_type counter_type;
	typedef typename Prf::key_type key_type;
	typedef T result_type;

public:
	cbrng() : b() {
		reset(counter_type());
	}

	template <typename U>
	cbrng(U val) : b(key_type(val)) {
		reset(counter_type());
	}

	template <typename U>
	cbrng(std::initializer_list<U> l) : b(key_type(l)) {
		reset(counter_type());
	}

public:
	void reset(counter_type newc) {
		c = newc;
		v = b(c);
		next = 0;
	}

	result_type operator()() {
		if(next == counter_type::template count<result_type>()) {
			v = b(++c);
			next = 0;
		}

		return v.template at<result_type>(next++);
	}

	void discard(size_t skip) {
		constexpr unsigned Nresult = counter_type::template count<result_type>();
		unsigned newnext = next + (skip % Nresult);
		skip /= Nresult;
		if(newnext > Nresult) {
			newnext -= Nresult;
			skip++;
		}

		c += skip;
		next = newnext;
		v = b(c);
	}

	static constexpr result_type min() {
		return std::numeric_limits<result_type>::min();
	}

	static constexpr result_type max() {
		return std::numeric_limits<result_type>::max();
	}

private:
	Prf b;
	counter_type c, v;
	unsigned next;
};
#endif
