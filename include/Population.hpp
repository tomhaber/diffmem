/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef POPULATION_H_
#define POPULATION_H_

#include <vector>
#include <string>
#include <Individual.hpp>

class DIFFMEM_EXPORT Population {
	public:
		Population() {}

	public:
		void add(Individual && ind);

		DEPRECATED("use beta/eta version instead") void evalU(ConstRefMatrixd & psis, ConstRefVecd & t, RefMatrixd u) const;
		DEPRECATED("use beta/eta version instead") void evalU(ConstRefVeci & indices, ConstRefMatrixd & psis, ConstRefVecd & t, RefMatrixd u) const;
		DEPRECATED("use beta/eta version instead") void evalSens(ConstRefMatrixd & psis, ConstRefVecd & t, RefMatrixd s) const;
		DEPRECATED("use beta/eta version instead") void evalSens(ConstRefVeci & indices, ConstRefMatrixd & psis, ConstRefVecd & t, RefMatrixd s) const;

		void evalU(ConstRefVecd & beta, ConstRefMatrixd & etas, ConstRefVecd & t, RefMatrixd u) const;
		void evalU(ConstRefVecd & beta, ConstRefVeci & indices, ConstRefMatrixd & etas, ConstRefVecd & t, RefMatrixd u) const;
		void evalSens(ConstRefVecd & beta, ConstRefMatrixd & etas, ConstRefVecd & t, RefMatrixd s) const;
		void evalSens(ConstRefVecd & beta, ConstRefVeci & indices, ConstRefMatrixd & etas, ConstRefVecd & t, RefMatrixd s) const;

	public:
		int numberOfRandom() const;
		int numberOfBetas() const;
		int numberOfParameters() const;
		int numberOfSigmas() const;
		int numberOfTaus() const;
		int numberOfObservations() const;
		int numberOfIndividuals() const { return static_cast<int>(subjects.size()); }

		void transsigma(ConstRefVecd & tau, RefVecd sigma) const;
		void transtau(ConstRefVecd & sigma, RefVecd tau) const;
		void updateTau(RefVecd tau, ConstRefVecd & gradTau, int N, double alpha) const;

		int numberOfMeasurements() const;

		const Individual & subject(int id) const { return subjects[id]; }

	private:
		std::vector<Individual> subjects;
};
#endif
