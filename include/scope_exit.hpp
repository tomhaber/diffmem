/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCOPE_EXIT_H_
#define SCOPE_EXIT_H_

#include <common.hpp>
#include <utility>

template<typename Function>
class scope_exit_invoker {
	public:
		explicit scope_exit_invoker(Function&& func) :
				_func(std::move(func)), dismissed(false) {
		}

		scope_exit_invoker(scope_exit_invoker&& src) :
				_func(std::move(src._func)), dismissed(src.dismissed) {
			src.dismissed = true;
		}

		scope_exit_invoker(const scope_exit_invoker&) = delete;
		scope_exit_invoker& operator=(const scope_exit_invoker&) = delete;
		scope_exit_invoker& operator=(scope_exit_invoker&&) = delete;

		~scope_exit_invoker() {
			if( !dismissed )
				_func();
		}

		void dismiss() {
			dismissed = true;
		}

	private:
		Function _func;
		bool dismissed;
};

template<typename Function>
scope_exit_invoker<Function> on_scope_exit(Function && func) {
	return scope_exit_invoker<Function>(std::forward<Function>(func));
}

namespace details {
	enum class ScopeExit {};
	template<typename Function>
	scope_exit_invoker<Function> operator +(ScopeExit, Function && func) {
		return scope_exit_invoker<Function>(std::forward<Function>(func));
	}
}

#define SCOPE_EXIT \
	auto ANONYMOUS_VARIABLE(SCOPE_EXIT) = details::ScopeExit() + [&]()
#endif
