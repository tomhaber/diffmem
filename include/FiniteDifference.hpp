/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef FINITEDIFFERENCE_H
#define FINITEDIFFERENCE_H

#include <MatrixVector.hpp>
#include <cmath>

class DIFFMEM_EXPORT FiniteDifference {
	public:
		static double epsilon(double x, double rel_eps=1e-8, double cutoff=1e-12) {
			return std::max( rel_eps * std::abs(x), cutoff );
		}

	public:
		template <typename Func>
		static void computeGradient(const Func &f, ConstRefVecd &x, RefVecd grad,
				const double rel_eps = 1e-8, const double cutoff_eps = 1e-12) {
			const int DIM = x.size();
			Require(grad.size() == DIM);

			Vecd xx(DIM);
			for(int i = 0; i < DIM; i++) {
				const double eps = epsilon(x[i], rel_eps, cutoff_eps);

				xx = x;
				xx[i] += eps;
				const double fp = f(xx);

				xx[i] -= 2*eps;
				const double fm = f(xx);

				grad[i] = (fp - fm) / (2.0 * eps);
			}
		}

		template <typename Func>
		static void computeJacobian(const Func &f, ConstRefVecd &x, RefVecd fx, RefMatrixd jac,
				const double rel_eps = 1e-8, const double cutoff_eps = 1e-12) {
			const int IDIM = x.size();
			const int ODIM = fx.size();
			Require(jac.rows() == ODIM && jac.cols() == IDIM);

			f(x, fx);

			Vecd xx(IDIM), fxx(ODIM);
			for(int i = 0; i < IDIM; i++) {
				const double eps = epsilon(x[i], rel_eps, cutoff_eps);

				xx = x;
				xx[i] += eps;
				f(xx, fxx);

				jac.col(i) = (fxx - fx) / eps;
			}
		}

		template <typename Func>
		static double computeHessian(const Func &f, ConstRefVecd &x, RefMatrixd hessian,
				const int order = 2, const double rel_eps = 1e-8, const double cutoff_eps = 1e-6) {
			const int DIM = x.size();
			Require(hessian.rows() == DIM && hessian.cols() == DIM);

			Vecd xx = x;
			const double fnow = f(xx);

			if( order == 2 ) {
				for(int i = 0; i < DIM; i++) {
					const double eps_i = epsilon(x[i], rel_eps, cutoff_eps);
					for(int j = 0; j < i; j++) {
						const double eps_j = epsilon(x[j], rel_eps, cutoff_eps);
						///////////////////////////
						xx[i] = x[i] + eps_i;
						xx[j] = x[j] + eps_j;
						const double  fpp = f(xx);
						///////////////////////////
						xx[i] = x[i] + eps_i;
						xx[j] = x[j] - eps_j;
						const double  fpm = f(xx);
						///////////////////////////
						xx[i] = x[i] - eps_i;
						xx[j] = x[j] + eps_j;
						const double  fmp = f(xx);
						///////////////////////////
						xx[i] = x[i] - eps_i;
						xx[j] = x[j] - eps_j;
						const double  fmm = f(xx);
						///////////////////////////
						hessian(i,j) = ((fpp - fmp)/(2*eps_i) - (fpm - fmm)/(2*eps_i))/(2*eps_j);
						xx[j] = x[j];
					}

					///////////////////////////
					xx[i] = x[i] + eps_i;
					const double fpp = f(xx);
					///////////////////////////
					xx[i] = x[i] - eps_i;
					const double fmm = f(xx);
					///////////////////////////
					hessian(i,i) = (fpp - 2.0*fnow + fmm) / (eps_i*eps_i);
				}
			} else if( order == 1 ) {
				for(int i = 0; i < DIM; i++) {
					const double eps_i = epsilon(x[i], rel_eps, cutoff_eps);

					xx[i] += eps_i;
					const double f_ipj = f(xx);

					for(int j = 0; j < i; j++) {
						const double eps_j = epsilon(x[j], rel_eps, cutoff_eps);

						xx[j] += eps_j;
						const double f_ipjp = f(xx);

						xx[i] -= eps_i;
						const double f_ijp = f(xx);

						hessian(i, j) = (f_ipjp - f_ipj - f_ijp + fnow) / (eps_i * eps_j);

						// back to original i,j
						xx[j] -= eps_j;
						xx[i] += eps_i;
					}

					xx[i] -= 2.0 * eps_i;
					const double f_imj = f(xx);
					hessian(i,i) = (f_ipj - 2.0*fnow + f_imj)/(eps_i*eps_i);

					xx[i] += eps_i;
				}
			}

			for(int i = 0; i < DIM; i++)
				for(int j = 0; j < i; j++)
					hessian(j, i) = hessian(i, j);

			return fnow;
		}

};
#endif
