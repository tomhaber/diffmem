/*
 * Copyright (c) 2016, Hasselt University
 * All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef DYNAMICLIBRARY_H
#define DYNAMICLIBRARY_H

#include <Exception.hpp>
#include <string>

#ifdef WIN32
#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>
#	include <winbase.h>
#endif

/*!
  Throws when something went wrong with a dynamic library.
*/
class  DynamicLibraryException : public std::runtime_error {
	public:
		DynamicLibraryException() : std::runtime_error("DynamicLibrary Exception") {}
		DynamicLibraryException(const char *s) : std::runtime_error(s) {}
		DynamicLibraryException(const std::string & s) : std::runtime_error(s) {}
};

class DynamicLibrary {
	public:
		DynamicLibrary() : handle(nullptr) {}
		DynamicLibrary(const std::string & name);
		DynamicLibrary(DynamicLibrary && lib) noexcept;
		DynamicLibrary & operator =(DynamicLibrary&&) noexcept;

		DynamicLibrary(const DynamicLibrary & lib) = delete;
		DynamicLibrary & operator =(DynamicLibrary&) = delete;
		~DynamicLibrary();

	public:
		void open(const std::string & name);
		void close();

	public:
		void *procAddress(const char *name);
		template <class T> T procAddress(const char *name);

	private:
#ifdef WIN32
		typedef HMODULE LibraryHandle;
#else
		typedef void * LibraryHandle;
#endif

		LibraryHandle handle{nullptr};
		std::string libname;
};

template <class T>
inline T DynamicLibrary::procAddress(const char *name) {
	return (T)procAddress(name);
}
#endif
