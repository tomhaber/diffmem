/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "R_Dict.hpp"
#include "serialize.hpp"

bool R_Dict::exists(const std::string & name) const {
	return list.containsElementNamed(name.c_str());
}

Vecd R_Dict::getNumericVector(const std::string & name) const {
	if( ! exists(name) ) throw missingkey_error(name);

	Rcpp::NumericVector v = Rcpp::as<Rcpp::NumericVector>( list[name] );
	return MapVecd(v.begin(), v.size());
}

Matrixd R_Dict::getNumericMatrix(const std::string & name) const {
	if( ! exists(name) ) throw missingkey_error(name);

	SEXP x = list[name];
	if( ::Rf_isMatrix(x) ) {
		Rcpp::NumericMatrix m = Rcpp::as<Rcpp::NumericMatrix>( x );
		return MapMatrixd(m.begin(), m.rows(), m.cols());
	} else {
		Rcpp::NumericVector v = Rcpp::as<Rcpp::NumericVector>( x );
		return MapMatrixd(v.begin(), v.size(), 1);
	}
}

Veci R_Dict::getIntegerVector(const std::string & name) const {
	if( ! exists(name) ) throw missingkey_error(name);

	Rcpp::IntegerVector v = Rcpp::as<Rcpp::IntegerVector>( list[name] );
	return MapVeci(v.begin(), v.size());
}

Matrixi R_Dict::getIntegerMatrix(const std::string & name) const {
	if( ! exists(name) ) throw missingkey_error(name);

	SEXP x = list[name];
	if( ::Rf_isMatrix(x) ) {
		Rcpp::IntegerMatrix m = Rcpp::as<Rcpp::IntegerMatrix>( x );
		return MapMatrixi(m.begin(), m.rows(), m.cols());
	} else {
		Rcpp::IntegerVector v = Rcpp::as<Rcpp::IntegerVector>( x );
		return MapMatrixi(v.begin(), v.size(), 1);
	}
}

Vecb R_Dict::getBooleanVector(const std::string & name) const {
	if( ! exists(name) ) throw missingkey_error(name);

	Rcpp::LogicalVector v = Rcpp::as<Rcpp::LogicalVector>( list[name] );
	Vecb w( v.size() );
	for(int i = 0; i < v.size(); ++i)
		w[i] = (v[i] == TRUE);
	return w;
}

Matrixb R_Dict::getBooleanMatrix(const std::string & name) const {
	if( ! exists(name) ) throw missingkey_error(name);

	SEXP x = list[name];
	if( ::Rf_isMatrix(x) ) {
		Rcpp::LogicalMatrix m = Rcpp::as<Rcpp::LogicalMatrix>( x );
		Matrixb w( m.rows(), m.cols() );
		for(int j = 0; j < m.cols(); ++j)
			for(int i = 0; i < m.rows(); ++i)
				w(i, j) = (m(i, j) == TRUE);
		return w;
	} else {
		Rcpp::LogicalVector v = Rcpp::as<Rcpp::LogicalVector>(x);
		Matrixb w( v.size(), 1 );
		for(int i = 0; i < v.size(); ++i)
			w(i, 1) = (v[i] == TRUE);

		return w;
	}
}

std::string R_Dict::getString(const std::string & name) const {
	if( ! exists(name) ) throw missingkey_error(name);
	SEXP v = list[name];
	if( ! ::Rf_isValidString(v) ) {
		v = serializeToRaw(v);
		return std::string(reinterpret_cast<const char *>(RAW(v)), LENGTH(v));
	}
	return Rcpp::as<std::string>(v);
}

int R_Dict::getInteger(const std::string & name) const {
	if( ! exists(name) ) throw missingkey_error(name);
	return Rcpp::as<int>( list[name] );
}

double R_Dict::getNumeric(const std::string & name) const {
	if( ! exists(name) ) throw missingkey_error(name);
	return Rcpp::as<double>( list[name] );
}

std::shared_ptr<Dict> R_Dict::getDict(const std::string & name) const {
	if( ! exists(name) ) throw missingkey_error(name);
	Rcpp::List l = Rcpp::as<Rcpp::List>( list[name] );
	return std::make_unique<R_Dict>( l );
}
