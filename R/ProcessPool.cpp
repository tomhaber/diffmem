/*
	This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ProcessPool.hpp"
#include <Error.hpp>
#include <scope_exit.hpp>

#include <sys/wait.h>
#include <fcntl.h>

#include <R_ext/RStartup.h>
#include <Rembedded.h>
#include <Rcpp.h>
#include "message.hpp"
#include <shared_instance.hpp>

const char *programName = "REmbedded";

#define error(msg) throw(Rcpp::exception(msg,__FILE__,__LINE__));

SEXP asRaw(const char *data, size_t len) {
	SEXP rv = Rf_allocVector(RAWSXP, len);
	unsigned char *rvb = RAW(rv);
	memcpy(rvb, data, len);
	return rv;
}

ConstMapMatrixd toEigenMatrix(SEXP mat) {
	Rcpp::NumericMatrix m(mat);
	return ConstMapMatrixd(m.begin(), m.rows(), m.cols());
}

ConstMapVecd toEigenVector(SEXP vec) {
	Rcpp::NumericVector v(vec);
	return ConstMapVecd(v.begin(), v.size());
}

static void my_send(int fd, const serialization::Buffer &msg) {
	unsigned int len = msg.size();
	const unsigned char *b = msg.data();

	if( write(fd, &len, sizeof(len)) != sizeof(len) ) {
		error("write error, closing");
	}

	unsigned int i = 0;
	while( i < len ) {
		int n = write(fd, b + i, len - i);
		if( n < 1 ) {
			error("write error, closing");
		}
		i += n;
	}
}

static bool my_recv(int fd, serialization::Buffer &msg) {
	unsigned int len = 0;
	int n = read(fd, &len, sizeof(len));

	if( n == 0 )
		return false;

	if( n != sizeof(len) )
		error("read error, closing");

	msg.resize(len);

	unsigned int i = 0;
	while( i < len ) {
		n = read(fd, &msg[i], (len - i));
		if( n < 1 )
			error("read error, closing");

		i += n;
	}

	return true;
}

Process::Process(pid_t pid, int inFd, int outFd) : pid(pid), inFd(inFd), outFd(outFd) {
}

Process::~Process() {
	if(pid > 0) {
		::close(inFd);
		::close(outFd);
		wait();
		pid = -1;
	}
}

void Process::send(const serialization::Buffer &msg) {
	my_send(outFd, msg);
}

bool Process::recv(serialization::Buffer &msg) {
	msg.clear();
	return my_recv(inFd, msg);
}

int Process::wait() {
	Require( pid > 0 );

	int status;
	pid_t found;
	do {
		found = ::waitpid(pid, &status, 0);
	} while( found == -1 && errno == EINTR );

	if( found == -1 )
		throwSystemError("waitpid failed on {}", pid);

	Require(found == pid);
	return status;
}

void Process::closeAndForget() {
	::close(inFd);
	::close(outFd);
	pid = -1;
}

void ProcessPool::createPipe(int & outFd, int & inFd) {
	int pipe[2];
	int r;

#ifdef HAVE_PIPE2
	r = ::pipe2(pipe, O_CLOEXEC);
	checkUnixError(r, "pipe2");
#else
	r = ::pipe(pipe);
	checkUnixError(r, "pipe");
	r = fcntl(pipe[0], F_SETFD, FD_CLOEXEC);
	checkUnixError(r, "set FD_CLOEXEC");
	r = fcntl(pipe[1], F_SETFD, FD_CLOEXEC);
	checkUnixError(r, "set FD_CLOEXEC");
#endif

	inFd = pipe[0];
	outFd = pipe[1];
}

void ProcessPool::spawn() {
	struct IOPipe {
		int inFd;
		int outFd;
	};

	struct IOPipe parent;
	struct IOPipe child;

	createPipe(parent.outFd, child.inFd);
	createPipe(child.outFd, parent.inFd);

	auto pipes_guard = on_scope_exit([&parent] {
		::close(parent.inFd);
		::close(parent.outFd);
	});
	auto pipes_clean = on_scope_exit([&child] {
		::close(child.inFd);
		::close(child.outFd);
	});

	int r;

	// Block all signals
	sigset_t allBlocked;
	sigset_t oldSignals;

	r = sigfillset(&allBlocked);
	checkUnixError(r, "sigfillset");

	r = pthread_sigmask(SIG_SETMASK, &allBlocked, &oldSignals);
	checkPosixError(r, "pthread_sigmask");
	auto signals_guard = on_scope_exit([oldSignals] {
		// Restore signal mask
		int r = pthread_sigmask(SIG_SETMASK, &oldSignals, nullptr);
		if( r != 0 )
			std::cerr << "pthread_sigmask: " << errnoStr(r);// shouldn't fail
	});

	pid_t pid = fork();

	if( pid == 0 ) {
		::close(parent.inFd);
		::close(parent.outFd);

		for(auto &proc : pool)
			proc.closeAndForget();
		pool.clear();

		// Default handler for all (still blocked) signals
		for(int sig = 1; sig < NSIG; ++sig)
			::signal(sig, SIG_DFL);

		{
			// Unblock signals; restore signal mask.
			int r = pthread_sigmask(SIG_SETMASK, &oldSignals, nullptr);
			if( r != 0 )
				;// XXX childError(r);
		}

		if( R_NilValue == NULL ) {
			const char *R_argv[] = {(char*)programName, "--gui=none", "--no-save", "--no-readline", "--silent", "--vanilla", "--slave"};
			int R_argc = sizeof(R_argv) / sizeof(R_argv[0]);
			Rf_initEmbeddedR(R_argc, (char**)R_argv);
		}

		if(true) {
			SEXP suppressMessagesSymbol = Rf_install("suppressMessages");
			SEXP requireSymbol = Rf_install("require");
			Rf_eval(Rf_lang2(suppressMessagesSymbol,
							Rf_lang2(requireSymbol, Rf_mkString("Rcpp"))),
					R_GlobalEnv);
		}

		Process process(pid, child.inFd, child.outFd);
		waitAndExecute(process);
		exit(0);
	}

	checkUnixError(pid);

	pool.push_back( Process(pid, parent.inFd, parent.outFd) );
	pipes_guard.dismiss();
}

ProcessPool::ProcessPool(size_t maxprocesses) : maxprocesses(maxprocesses) {
	for(size_t i = 0; i < maxprocesses; ++i)
		spawn();
	for(auto &x : pool)
		ready.push_back(&x);
}

void ProcessPool::release(Process &proc) {
	std::unique_lock<std::mutex> lk(mtx);
	ready.push_back(&proc);
	condition.notify_one();
}

Process &ProcessPool::findReady() {
	std::unique_lock<std::mutex> lk(mtx);
	if(ready.empty()) {
		if(pool.size() < maxprocesses) {
			lk.unlock();
			spawn();
			return pool.back();
		}

		while(ready.empty())
			condition.wait(lk);
	}

	Process *myProcess = ready.back();
	ready.pop_back();

	return *myProcess;
}

void ProcessPool::runTask(const serialization::Buffer & cmd, serialization::Buffer & reply) {
	Process & proc = findReady();
	proc.send(cmd);
	if( ! proc.recv(reply) )
		error("unexpected end of input");
	release(proc);
}

void ProcessPool::runEverywhere(const serialization::Buffer &cmd) {
	for(auto &proc : pool) {
		proc.send(cmd);
	}

	serialization::Buffer reply;
	execute(cmd, reply);

	for(auto &proc : pool) {
		if( ! proc.recv(reply) )
			error("unexpected end of input");
		unpack_reply(reply);
	}
}

namespace Rcpp {
	template<typename Archive>
	typename std::enable_if<cereal::traits::is_output_serializable<cereal::BinaryData<double>, Archive>::value, void>::type
	CEREAL_SAVE_FUNCTION_NAME(Archive &ar, const NumericVector & v) {
		ar(cereal::make_size_tag(static_cast<cereal::size_type>(v.size())));
		ar(cereal::make_size_tag(static_cast<cereal::size_type>(1)));
		ar(cereal::binary_data(v.begin(), v.size() * sizeof(double)));
	}

	template<typename Archive>
	typename std::enable_if<cereal::traits::is_output_serializable<cereal::BinaryData<double>, Archive>::value, void>::type
	CEREAL_SAVE_FUNCTION_NAME(Archive &ar, const NumericMatrix & m) {
		ar(cereal::make_size_tag(static_cast<cereal::size_type>(m.rows())));
		ar(cereal::make_size_tag(static_cast<cereal::size_type>(m.cols())));
		ar(cereal::binary_data(m.begin(), m.size() * sizeof(double)));
	}

	template<typename Archive>
	typename std::enable_if<cereal::traits::is_input_serializable<cereal::BinaryData<double>, Archive>::value, void>::type
	CEREAL_LOAD_FUNCTION_NAME(Archive &ar, NumericVector & v) {
		cereal::size_type rows, cols;
		ar(cereal::make_size_tag(rows));
		ar(cereal::make_size_tag(cols));
		assert(cols == 1);
		v = NumericVector(rows);
		ar(cereal::binary_data(v.begin(), static_cast<std::size_t>(v.size()) * sizeof(double)));
	}

	template<typename Archive>
	typename std::enable_if<cereal::traits::is_input_serializable<cereal::BinaryData<double>, Archive>::value, void>::type
	CEREAL_LOAD_FUNCTION_NAME(Archive &ar, NumericMatrix & m) {
		cereal::size_type rows, cols;
		ar(cereal::make_size_tag(rows));
		ar(cereal::make_size_tag(cols));
		m = NumericMatrix(rows, cols);
		ar(cereal::binary_data(m.begin(), static_cast<std::size_t>(m.size()) * sizeof(double)));
	}
}

void ProcessPool::waitAndExecute(Process & proc) {
	serialization::Buffer msg, reply;
	do {
		reply.clear();
		msg.clear();

		if( ! proc.recv(msg) )
			return;

		execute(msg, reply);
		proc.send(reply);
	} while( true );
}

void ProcessPool::execute(const serialization::Buffer &msg, serialization::Buffer &reply) {
	const Rcpp::LogicalVector TRUE(true);
	const Rcpp::Symbol symGrad("grad");
	const Rcpp::Symbol symHessian("hessian");

	try {
		serialization::BufferInputArchive archive(msg);
		enum Command type;
		unpack(archive, type);

		switch( type ) {
			case Command::REGISTER_FUNCTION: {
					serialization::call(
						[](const std::string &name, const std::string &func) {
							SEXP raw = asRaw(func.data(), func.size());
							Rcpp::Language call(
									"<-", Rcpp::Symbol(name), Rcpp::Language("unserialize", raw, R_NilValue));

							return call.eval();
						}, archive);
					pack_message(reply, Response::SUCCESS);
				}
				break;

			case Command::UNREGISTER_FUNCTION: {
					serialization::call(
							[](const std::string &name) {
								Rcpp::Language call("rm", Rcpp::Symbol(name));
								return call.eval();
							}, archive);
					pack_message(reply, Response::SUCCESS);
				}
				break;

			case Command::EVAL_U: {
					SEXP res = serialization::call(
						[](const std::string &name, const Rcpp::NumericVector &psi, const Rcpp::NumericVector & t) {
							Rcpp::Language call(name, psi, t);
							return call.eval();
						}, archive);

					Rcpp::NumericMatrix u(res);
					pack_message(reply, Response::SUCCESS, u);
				}
				break;

			case Command::LOGPDF: {
					SEXP res = serialization::call(
						[](const std::string &name, const Rcpp::NumericVector &x) {
							Rcpp::Language call(name, x);
							return call.eval();
						}, archive);

					double lp = Rcpp::as<double>(res);
					pack_message(reply, Response::SUCCESS, lp);
				}
				break;

			case Command::LOGPDF_GRAD: {
					SEXP res = serialization::call(
						[&TRUE](const std::string &name, const Rcpp::NumericVector &x) {
							Rcpp::Language call(name, x, TRUE);
							return call.eval();
						}, archive);

					SEXP g_ = Rf_getAttrib(res, symGrad);
					if( g_ == R_NilValue )
						error("missing gradient")

					double lp = Rcpp::as<double>(res);
					Rcpp::NumericVector g(g_);
					pack_message(reply, Response::SUCCESS, lp, g);
				}
				break;

			case Command::LOGPDF_HESS: {
					SEXP res = serialization::call(
						[&TRUE](const std::string &name, const Rcpp::NumericVector &x) {
							Rcpp::Language call(name, x, TRUE, TRUE);
							return call.eval();
						}, archive);

					SEXP g_ = Rf_getAttrib(res, symGrad);
					if( g_ == R_NilValue )
						error("missing gradient")

					SEXP H_ = Rf_getAttrib(res, symHessian);
					if( H_ == R_NilValue )
						error("missing hessian")

					double lp = Rcpp::as<double>(res);
					Rcpp::NumericVector g(g_);
					Rcpp::NumericMatrix H(H_);
					pack_message(reply, Response::SUCCESS, lp, g, H);
				}
				break;

			default:
				error("unknown message type");
		}
	} catch( std::exception & ex ) {
		pack_message(reply, Response::FAILURE, std::string(ex.what()));
	}
}

std::shared_ptr<ProcessPool> ProcessPool::instance() {
	return get_shared_instance<ProcessPool>();
}
