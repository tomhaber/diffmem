/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PROCESSPOOL_H
#define PROCESSPOOL_H

#include <vector>
#include <functional>
#include <unistd.h>
#include <message.hpp>

#include <mutex>
#include <atomic>
#include <condition_variable>

class Process {
	public:
		Process(pid_t pid, int inFd, int outFd);
		~Process();

		// not copiable but movable
		Process(const Process&) = delete;
		Process& operator=(const Process&) = delete;
		Process(Process&& other) { *this = std::move(other); }
		Process& operator=(Process&& other) {
			pid = other.pid;
			inFd = other.inFd;
			outFd = other.outFd;
			other.pid = -1;
			return *this;
		}

	public:
		pid_t getpid() const { return pid; }
		void send(const serialization::Buffer & msg);
		bool recv(serialization::Buffer & reply);
		int wait();
		void closeAndForget();

	private:
		pid_t pid;
		int inFd, outFd;
};

class ProcessPool {
	public:
		ProcessPool(size_t maxprocesses=10);

	public:
		static std::shared_ptr<ProcessPool> instance();
		void runTask(const serialization::Buffer &cmd, serialization::Buffer &reply);
		void runEverywhere(const serialization::Buffer &cmd);

	private:
		void spawn();
		void createPipe(int & outFd, int & intFd);
		void waitAndExecute(Process & proc);
		void execute(const serialization::Buffer &msg, serialization::Buffer &reply);
		Process & findReady();
		void release(Process & proc);

	private:
		std::condition_variable condition;
		std::mutex mtx;
		std::vector<Process*> ready;
		std::vector<Process> pool;
		int maxprocesses;
};
#endif
