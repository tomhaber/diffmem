/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <RcppEigen.hpp>
#include <tbb/parallel_for.h>

namespace {

	inline void checkInterruptFn(void * /*dummy*/) {
		R_CheckUserInterrupt();
	}

}

// Check for interrupts and throw exception if one is pending
inline void checkUserInterrupt() {
	if(R_ToplevelExec(checkInterruptFn, NULL) == FALSE)
		throw interrupted_exception();
}

#include <Rcpp.h>
#include <Init.hpp>
#include <R_Dict.hpp>
#include <ModelFactory.hpp>
#include <StructuralModel.hpp>
#include <LikelihoodModel.hpp>
#include <Individual.hpp>
#include <MEModel.hpp>
#include <Random.hpp>
#include <SAEM.hpp>
#include <Distribution.hpp>
#include <SMLMDistribution.hpp>
#include <DistributionSampler.hpp>
#include <ProductDistribution.hpp>
#include <MaskedDistribution.hpp>
#include <BFGS.hpp>
#include <NelderMead.hpp>
#include <PatternSearch.hpp>
#include <ParticleSwarmOptimization.hpp>
#include <mcmc/MHSampler.hpp>
#include <mcmc/MESampler.hpp>
#include <mcmc/HamiltonianSampler.hpp>
#include <mcmc/ProposalFactory.hpp>
#include <mcmc/SamplerFactory.hpp>

namespace {
	template<typename> struct Void { typedef void type; };

	template<typename T, typename Sfinae = void>
	struct has_get_method : std::false_type {};

	template<typename T>
	struct has_get_method<
	    T
	    , typename Void<
	        decltype( std::declval<T&>().get() )
	    >::type
	>: std::true_type {};

	template <typename T>
	typename std::enable_if<has_get_method<Rcpp::XPtr<T>>::value, T*>::type getter(Rcpp::XPtr<T> & xptr) {
		return xptr.get();
	}

	template <typename T>
	typename std::enable_if<!has_get_method<Rcpp::XPtr<T>>::value, T*>::type getter(Rcpp::XPtr<T> & xptr) {
		return xptr;
	}
}

class R_StructuralModel {
	public:
		R_StructuralModel(const std::string & name) {
			R_Dict dict;
			model = ModelFactory::instance().createStructuralModel(name, dict);
		}

		R_StructuralModel(const std::string & name, Rcpp::List dict_) {
			R_Dict dict(dict_);
			model = ModelFactory::instance().createStructuralModel(name, dict);
		}

	public:
		int numberOfObservations() {
			return model->numberOfObservations();
		}

		int numberOfParameters() {
			return model->numberOfParameters();
		}

		SEXP transpsi(SEXP phiSexp) {
			if( ::Rf_isMatrix(phiSexp) ) {
				Rcpp::NumericMatrix phi_(phiSexp);
				Require( phi_.rows() == model->numberOfParameters(), "wrong number of rows in phi" );
				Rcpp::NumericMatrix psi_(phi_.rows(), phi_.cols());

				ConstMapMatrixd phi(phi_.begin(), phi_.rows(), phi_.cols());
				MapMatrixd psi(psi_.begin(), psi_.rows(), psi_.cols());
				for(int i = 0; i < phi.cols(); ++i) {
					model->transpsi(phi.col(i), psi.col(i));
				}

				return psi_;
			} else if( ::Rf_isNewList(phiSexp) ) {
				Rcpp::ListOf<Rcpp::NumericVector> phi_(phiSexp);
				Rcpp::List psi_;

				for(int i = 0; i < phi_.size(); ++i) {
					Require( phi_[i].size() == model->numberOfParameters(), "wrong number of elements in phi" );
					ConstMapVecd phi(phi_[i].begin(), phi_[i].size());

					Rcpp::NumericVector v( phi.size() );
					MapVecd psi(v.begin(), v.size());
					model->transpsi(phi, psi);
					psi_.push_back(v);
				}

				return psi_;
			} else {
				Rcpp::NumericVector phi_(phiSexp);
				Require( phi_.size() == model->numberOfParameters(), "wrong number of elements in phi" );
				Rcpp::NumericVector psi_(phi_.size());

				typedef Eigen::Map< const Eigen::Matrix<double, Eigen::Dynamic, 1> > ConstMapVecd;
				ConstMapVecd phi(phi_.begin(), phi_.size());
				MapVecd psi(psi_.begin(), psi_.size());
				model->transpsi(phi, psi);
				return psi_;
			}
		}

		SEXP transphi(SEXP psiSexp) {
			if( ::Rf_isMatrix(psiSexp) ) {
				Rcpp::NumericMatrix psi_(psiSexp);
				Require( psi_.rows() == model->numberOfParameters(), "wrong number of rows in psi" );
				Rcpp::NumericMatrix phi_(psi_.rows(), psi_.cols());

				ConstMapMatrixd psi(psi_.begin(), psi_.rows(), psi_.cols());
				MapMatrixd phi(phi_.begin(), phi_.rows(), phi_.cols());
				for(int i = 0; i < psi.cols(); ++i) {
					model->transphi(psi.col(i), phi.col(i));
				}

				return phi_;
			} else if( ::Rf_isNewList(psiSexp) ) {
				Rcpp::ListOf<Rcpp::NumericVector> psi_(psiSexp);
				Rcpp::List phi_;

				for(int i = 0; i < psi_.size(); ++i) {
					Require( psi_[i].size() == model->numberOfParameters(), "wrong number of elements in psi" );
					ConstMapVecd psi(psi_[i].begin(), psi_[i].size());

					Rcpp::NumericVector v( psi.size() );
					MapVecd phi(v.begin(), v.size());
					model->transphi(psi, phi);
					phi_.push_back(v);
				}

				return phi_;
			} else {
				Rcpp::NumericVector psi_(psiSexp);
				Require( psi_.size() == model->numberOfParameters(), "wrong number of elements in psi" );
				Rcpp::NumericVector phi_(psi_.size());

				ConstMapVecd psi(psi_.begin(), psi_.size());
				MapVecd phi(phi_.begin(), phi_.size());
				model->transphi(psi, phi);
				return phi_;
			}
		}

		SEXP dtranspsi(SEXP phiSexp) {
			if( ::Rf_isMatrix(phiSexp) ) {
				Rcpp::NumericMatrix phi_(phiSexp);
				Require( phi_.rows() == model->numberOfParameters(), "wrong number of rows in phi" );
				Rcpp::NumericMatrix dphi_(phi_.rows(), phi_.cols());

				ConstMapMatrixd phi(phi_.begin(), phi_.rows(), phi_.cols());
				MapMatrixd dphi(dphi_.begin(), dphi_.rows(), dphi_.cols());
				for(int i = 0; i < phi.cols(); ++i) {
					model->dtranspsi(phi.col(i), dphi.col(i));
				}

				return dphi_;
			} else if( ::Rf_isNewList(phiSexp) ) {
				Rcpp::ListOf<Rcpp::NumericVector> phi_(phiSexp);
				Rcpp::List dphi_;

				for(int i = 0; i < phi_.size(); ++i) {
					Require( phi_[i].size() == model->numberOfParameters(), "wrong number of elements in phi" );
					ConstMapVecd phi(phi_[i].begin(), phi_[i].size());

					Rcpp::NumericVector v( phi.size() );
					MapVecd dphi(v.begin(), v.size());
					model->dtranspsi(phi, dphi);
					dphi_.push_back(v);
				}

				return dphi_;
			} else {
				Rcpp::NumericVector phi_(phiSexp);
				Require( phi_.size() == model->numberOfParameters(), "wrong number of elements in phi" );
				Rcpp::NumericVector dphi_(phi_.size());

				typedef Eigen::Map< const Eigen::Matrix<double, Eigen::Dynamic, 1> > ConstMapVecd;
				ConstMapVecd phi(phi_.begin(), phi_.size());
				MapVecd dphi(dphi_.begin(), dphi_.size());
				model->dtranspsi(phi, dphi);
				return dphi_;
			}
		}

		Rcpp::NumericMatrix evalSensitivity(Rcpp::NumericVector psi_, Rcpp::NumericVector t_) {
			ConstMapVecd psi(psi_.begin(), psi_.size());
			Require( psi_.size() == model->numberOfParameters(), "wrong number of elements in psi" );

			ConstMapVecd t(t_.begin(), t_.size());

			const int numberOfSensitivities = numberOfObservations() * (psi_.size() + 1);
			Rcpp::NumericMatrix sens_( numberOfSensitivities, t_.size() );
			MapMatrixd sens(sens_.begin(), sens_.rows(), sens_.cols());

			model->evalSens(psi, t, sens);
			return sens_;
		}

		Rcpp::NumericMatrix evalU(Rcpp::NumericVector psi_, Rcpp::NumericVector t_) {
			ConstMapVecd psi(psi_.begin(), psi_.size());
			Require( psi_.size() == model->numberOfParameters(), "wrong number of elements in psi" );

			Rcpp::NumericMatrix u_( numberOfObservations(), t_.size() );
			ConstMapVecd t(t_.begin(), t_.size());
			MapMatrixd u(u_.begin(), u_.rows(), u_.cols());

			model->evalU(psi, t, u);
			return u_;
		}

	public:
		StructuralModel & getModel() const { return *model; }
		std::shared_ptr<::StructuralModel> getModelPtr() { return model; }

	private:
		std::shared_ptr<::StructuralModel> model;
};

class R_LikelihoodModel {
	public:
		R_LikelihoodModel(const std::string & name) {
			R_Dict dict;
			model = ModelFactory::instance().createLikelihoodModel(name, dict);
		}

		R_LikelihoodModel(const std::string & name, Rcpp::List dict_) {
			R_Dict dict(dict_);
			model = ModelFactory::instance().createLikelihoodModel(name, dict);
		}

	public:
		int numberOfObservations() {
			return model->numberOfObservations();
		}

		int numberOfSigmas() {
			return model->numberOfSigmas();
		}

		int numberOfTaus() {
			return model->numberOfTaus();
		}

		SEXP transsigma(SEXP tauSexp) {
			if( ::Rf_isMatrix(tauSexp) ) {
				Rcpp::NumericMatrix tau_(tauSexp);
				Require( tau_.rows() == model->numberOfTaus(), "wrong number of rows in tau" );
				Rcpp::NumericMatrix sigma_( model->numberOfSigmas(), tau_.cols() );

				ConstMapMatrixd tau(tau_.begin(), tau_.rows(), tau_.cols());
				MapMatrixd sigma(sigma_.begin(), sigma_.rows(), sigma_.cols());
				for(int i = 0; i < tau.cols(); ++i) {
					model->transsigma(tau.col(i), sigma.col(i));
				}

				return sigma_;
			} else if( ::Rf_isNewList(tauSexp) ) {
				Rcpp::ListOf<Rcpp::NumericVector> tau_(tauSexp);
				Rcpp::List sigma_;

				for(int i = 0; i < tau_.size(); ++i) {
					Require( tau_[i].size() == model->numberOfTaus(), "wrong number of elements in tau" );
					ConstMapVecd tau(tau_[i].begin(), tau_[i].size());

					Rcpp::NumericVector v( model->numberOfSigmas() );
					MapVecd sigma(v.begin(), v.size());
					model->transsigma(tau, sigma);
					sigma_.push_back(v);
				}

				return sigma_;
			} else {
				Rcpp::NumericVector tau_(tauSexp);
				Require( tau_.size() == model->numberOfTaus(), "wrong number of elements in tau" );
				Rcpp::NumericVector sigma_( model->numberOfSigmas() );

				typedef Eigen::Map< const Eigen::Matrix<double, Eigen::Dynamic, 1> > ConstMapVecd;
				ConstMapVecd tau(tau_.begin(), tau_.size());
				MapVecd sigma(sigma_.begin(), sigma_.size());
				model->transsigma(tau, sigma);
				return sigma_;
			}
		}

		SEXP transtau(SEXP sigmaSexp) {
			if( ::Rf_isMatrix(sigmaSexp) ) {
				Rcpp::NumericMatrix sigma_(sigmaSexp);
				Require( sigma_.rows() == model->numberOfSigmas(), "wrong number of rows in sigma" );
				Rcpp::NumericMatrix tau_(model->numberOfTaus(), sigma_.cols());

				ConstMapMatrixd sigma(sigma_.begin(), sigma_.rows(), sigma_.cols());
				MapMatrixd tau(tau_.begin(), tau_.rows(), tau_.cols());
				for(int i = 0; i < sigma.cols(); ++i) {
					model->transtau(sigma.col(i), tau.col(i));
				}

				return tau_;
			} else if( ::Rf_isNewList(sigmaSexp) ) {
				Rcpp::ListOf<Rcpp::NumericVector> sigma_(sigmaSexp);
				Rcpp::List tau_;

				for(int i = 0; i < sigma_.size(); ++i) {
					Require( sigma_[i].size() == model->numberOfSigmas(), "wrong number of elements in sigma" );
					ConstMapVecd sigma(sigma_[i].begin(), sigma_[i].size());

					Rcpp::NumericVector v( model->numberOfTaus() );
					MapVecd tau(v.begin(), v.size());
					model->transtau(sigma, tau);
					tau_.push_back(v);
				}

				return tau_;
			} else {
				Rcpp::NumericVector sigma_(sigmaSexp);
				Require( sigma_.size() == model->numberOfSigmas(), "wrong number of elements in sigma" );
				Rcpp::NumericVector tau_(model->numberOfTaus());

				ConstMapVecd sigma(sigma_.begin(), sigma_.size());
				MapVecd tau(tau_.begin(), tau_.size());
				model->transtau(sigma, tau);
				return tau_;
			}
		}

		double logpdf_u(Rcpp::NumericVector tau_, Rcpp::NumericMatrix u_) {
			Require( tau_.size() == model->numberOfTaus(), "wrong number of elements in tau" );
			ConstMapVecd tau(tau_.begin(), tau_.size());

			ConstMapMatrixd u(u_.begin(), u_.rows(), u_.cols());
			return model->logpdf(tau, u);
		}

	public:
		SEXP logpdf(SEXP psiSexp, SEXP tauSexp,
				R_StructuralModel & sm, bool gradPsi, bool hessPsi) {
			if(::Rf_isMatrix(psiSexp) && ::Rf_isMatrix(tauSexp)) {
				Rcpp::NumericMatrix psi_(psiSexp);
				Rcpp::NumericMatrix tau_(tauSexp);
				if(gradPsi || hessPsi)
					throw std::runtime_error("gradient and hessians are not supported in vectorized mode");

				Require( psi_.rows() == sm.numberOfParameters(), "wrong number of rows in psi" );
				ConstMapMatrixd psi(psi_.begin(), psi_.rows(), psi_.cols());

				Require( tau_.rows() == model->numberOfTaus(), "wrong number of rows in tau" );
				ConstMapMatrixd tau(tau_.begin(), tau_.rows(), tau_.cols());

				std::vector<double> result(psi.cols());
				tbb::parallel_for(0, psi.cols(), [&](int i) {
					try {
						result[i] = model->logpdf(psi.col(i), tau.col(i), sm.getModel());
					} catch( model_error & e ) {
						result[i] = math::negInf();
					}
				});

				return Rcpp::wrap(result);
			} else if(::Rf_isMatrix(psiSexp)) {
				Rcpp::NumericMatrix psi_(psiSexp);
				Rcpp::NumericVector tau_(tauSexp);

				if(gradPsi || hessPsi)
					throw std::runtime_error("gradient and hessians are not supported in vectorized mode");

				Require( psi_.rows() == sm.numberOfParameters(), "wrong number of rows in psi" );
				ConstMapMatrixd psi(psi_.begin(), psi_.rows(), psi_.cols());

				Require( tau_.size() == model->numberOfTaus(), "wrong number of elements in tau" );
				ConstMapVecd tau(tau_.begin(), tau_.size());

				std::vector<double> result(psi.cols());
				tbb::parallel_for(0, psi.cols(), [&](int i) {
					try {
						result[i] = model->logpdf(psi.col(i), tau, sm.getModel());
					} catch( model_error & e ) {
						result[i] = math::negInf();
					}
				});

				return Rcpp::wrap(result);
			} else if(::Rf_isMatrix(tauSexp)) {
				Rcpp::NumericVector psi_(psiSexp);
				Rcpp::NumericMatrix tau_(tauSexp);

				if(gradPsi || hessPsi)
					throw std::runtime_error("gradient and hessians are not supported in vectorized mode");

				Require( psi_.size() == sm.numberOfParameters(), "wrong number of elements in psi" );
				ConstMapVecd psi(psi_.begin(), psi_.size());

				Require( tau_.rows() == model->numberOfTaus(), "wrong number of rows in tau" );
				ConstMapMatrixd tau(tau_.begin(), tau_.rows(), tau_.cols());

				std::vector<double> result(tau.cols());
				tbb::parallel_for(0, tau.cols(), [&](int i) {
					try {
						result[i] = model->logpdf(psi, tau.col(i), sm.getModel());
					} catch( model_error & e ) {
						result[i] = math::negInf();
					}
				});

				return Rcpp::wrap(result);
			} else {
				Rcpp::NumericVector psi_(psiSexp);
				Rcpp::NumericVector tau_(tauSexp);

				Require( psi_.size() == sm.numberOfParameters(), "wrong number of elements in psi" );
				ConstMapVecd psi(psi_.begin(), psi_.size());

				Require( tau_.size() == model->numberOfTaus(), "wrong number of elements in tau" );
				ConstMapVecd tau(tau_.begin(), tau_.size());

				if( hessPsi ) {
					const int N = psi.size();
					Rcpp::NumericMatrix H_( N, N );
					Rcpp::NumericVector g_( N );

					MapMatrixd H(H_.begin(), H_.rows(), H_.cols());
					MapVecd g(g_.begin(), g_.size());
					double ll = model->logpdf(psi, tau, sm.getModel(), g, H);

					Rcpp::NumericVector ret = Rcpp::wrap(ll);
					ret.attr("grad") = g_;
					ret.attr("hess") = H_;
					return ret;
				} else if( gradPsi ) {
					Rcpp::NumericVector g_( psi.size() );
					MapVecd g(g_.begin(), g_.size());
					double ll = model->logpdf(psi, tau, sm.getModel(), g);

					Rcpp::NumericVector ret = Rcpp::wrap(ll);
					ret.attr("grad") = g_;
					return ret;
				} else {
					return Rcpp::wrap( model->logpdf(psi, tau, sm.getModel()) );
				}
			}
		}

		SEXP logpdfGradTau(Rcpp::NumericVector psi_, Rcpp::NumericVector tau_,
				R_StructuralModel & sm) {

			Require( psi_.size() == sm.numberOfParameters(), "wrong number of elements in psi" );
			ConstMapVecd psi(psi_.begin(), psi_.size());

			Require( tau_.size() == model->numberOfTaus(), "wrong number of elements in tau" );
			ConstMapVecd tau(tau_.begin(), tau_.size());

			Rcpp::NumericVector g_( tau.size() );
			MapVecd g(g_.begin(), g_.size());

			double ll = model->logpdfGradTau(psi, tau, sm.getModel(), g);

			Rcpp::NumericVector ret = Rcpp::wrap(ll);
			ret.attr("gradTau") = g_;
			return ret;
		}


	public:
		LikelihoodModel & getModel() const { return *model; }
		std::shared_ptr<::LikelihoodModel> getModelPtr() { return model; }

	private:
		std::shared_ptr<::LikelihoodModel> model;
};

class R_Individual {
	public:
		R_Individual(int id, R_StructuralModel & sm, R_LikelihoodModel & lm, Rcpp::List data_) {
			R_Dict data(data_);

			individual = Individual(id, sm.getModelPtr(), lm.getModelPtr(),
						data.getNumericMatrix("A"),
						data.getNumericMatrix("B"));
		}

		R_Individual(int id, R_StructuralModel & sm, R_LikelihoodModel & lm,
				Rcpp::NumericMatrix A_, Rcpp::NumericMatrix B_) {

			ConstMapMatrixd A(A_.begin(), A_.rows(), A_.cols());
			ConstMapMatrixd B(B_.begin(), B_.rows(), B_.cols());
			individual = Individual(id, sm.getModelPtr(), lm.getModelPtr(), A, B);
		}

	public:
		int numberOfBetas() const { return individual.numberOfBetas(); }
		int numberOfParameters() const { return individual.numberOfParameters(); }
		int numberOfRandom() const { return individual.numberOfRandom(); }
		int numberOfTaus() const { return individual.numberOfTaus(); }
		int numberOfObservations() const { return individual.numberOfObservations(); }
		int numberOfMeasurements() const { return individual.numberOfMeasurements(); }

		Rcpp::NumericVector toParameter(Rcpp::NumericVector beta_, Rcpp::NumericVector eta_) {
			Require( beta_.size() == numberOfBetas(), "wrong number of elements in beta" );
			ConstMapVecd beta(beta_.begin(), beta_.size());

			Require( eta_.size() == numberOfRandom(), "wrong number of elements in eta" );
			ConstMapVecd eta(eta_.begin(), eta_.size());

			Rcpp::NumericVector phi_( numberOfParameters() );
			MapVecd phi(phi_.begin(), phi_.size());

			individual.toParameter(beta, eta, phi);
			return phi_;
		}


		SEXP logpdf(Rcpp::NumericVector phi_, Rcpp::NumericVector tau_, bool gradPhi, bool hessPsi) {
			Require( phi_.size() == numberOfParameters(), "wrong number of elements in phi" );
			ConstMapVecd phi(phi_.begin(), phi_.size());

			Require( tau_.size() == numberOfTaus(), "wrong number of elements in tau" );
			ConstMapVecd tau(tau_.begin(), tau_.size());

			if( hessPsi ) {
				const int N = phi.size();
				Rcpp::NumericMatrix H_( N, N );
				Rcpp::NumericVector g_( N );

				MapMatrixd H(H_.begin(), H_.rows(), H_.cols());
				MapVecd g(g_.begin(), g_.size());
				double ll = individual.logpdf(phi, tau, g, H);

				Rcpp::NumericVector ret = Rcpp::wrap(ll);
				ret.attr("grad") = g_;
				ret.attr("hess") = H_;
				return ret;
			} else if( gradPhi ) {
				Rcpp::NumericVector g_( phi.size() );
				MapVecd g(g_.begin(), g_.size());
				double ll = individual.logpdf(phi, tau, g);

				Rcpp::NumericVector ret = Rcpp::wrap(ll);
				ret.attr("grad") = g_;
				return ret;
			} else {
				return Rcpp::wrap( individual.logpdf(phi, tau) );
			}
		}

		SEXP logpdfGradTau(Rcpp::NumericVector phi_, Rcpp::NumericVector tau_) {
			Require( phi_.size() == numberOfParameters(), "wrong number of elements in phi" );
			ConstMapVecd phi(phi_.begin(), phi_.size());

			Require( tau_.size() == numberOfTaus(), "wrong number of elements in tau" );
			ConstMapVecd tau(tau_.begin(), tau_.size());

			Rcpp::NumericVector g_( tau.size() );
			MapVecd g(g_.begin(), g_.size());

			double ll = individual.error(phi, tau, g);

			Rcpp::NumericVector ret = Rcpp::wrap(ll);
			ret.attr("gradTau") = g_;
			return ret;
		}

	public:
		const Individual & get() const { return individual; }

	private:
		Individual individual;
		friend class R_Population;
};

class R_Population {
	public:
		R_Population() {}

	public:
		void add(R_Individual & ind) {
			population.add( std::move(ind.individual) );
		}

	public:
		SEXP numberOfRandom() const {
			return Rcpp::wrap( population.numberOfRandom() );
		}

		SEXP numberOfBetas() const {
			return Rcpp::wrap( population.numberOfBetas() );
		}

		SEXP numberOfParameters() const {
			return Rcpp::wrap( population.numberOfParameters() );
		}

		SEXP numberOfTaus() const {
			return Rcpp::wrap( population.numberOfTaus() );
		}

		SEXP numberOfIndividuals() const  {
			return Rcpp::wrap( population.numberOfIndividuals() );
		}

	private:
		Population population;
		friend class R_MEModel;
};

class R_MEModel {
	public:
		R_MEModel(R_Population & pop, Rcpp::List args_) {
			R_Dict args(args_);
			model = std::make_unique<MEModel>(
				pop.population,
				args.getNumericVector("beta"),
				args.getBooleanVector("estimated"),
				args.getNumericMatrix("omega"),
				args.getBooleanMatrix("cov_model"),
				args.getNumericVector("sigma"),
				args.getBooleanVector("sigma_model"));
		}

		Rcpp::NumericVector beta() const {
			return Rcpp::wrap( model->beta() );
		}

		Rcpp::NumericMatrix omega() const {
			return Rcpp::wrap( model->omega() );
		}

		Rcpp::NumericVector sigma() const {
			return Rcpp::wrap( model->sigma() );
		}

		MEModel & get() { return *model; }

	private:
		std::unique_ptr<MEModel> model;
};

class R_ConstMEModel {
	public:
		R_ConstMEModel(const MEModel & model) : model(model) {}

	public:
		Rcpp::NumericVector beta() const {
			return Rcpp::wrap( model.beta() );
		}

		Rcpp::NumericMatrix omega() const {
			return Rcpp::wrap( model.omega() );
		}

		Rcpp::NumericVector sigma() const {
			return Rcpp::wrap( model.sigma() );
		}

	private:
		const MEModel &model;
};

Random &get_rng(Rcpp::List control) {
	static Random rng;
	static Random rng_seeded;

	if( control.containsElementNamed("seed") ) {
		auto seed = Rcpp::as<unsigned long>( control["seed"] );
		rng_seeded = Random(seed);
		return rng_seeded;
	}

	return rng;
}

class R_Distribution {
	public:
		R_Distribution(const std::string & name) {
			R_Dict dict;
			distr = ModelFactory::instance().createDistribution(name, dict);
		}

		R_Distribution(const std::string & name, Rcpp::List data_) {
			R_Dict dict( data_ );
			distr = ModelFactory::instance().createDistribution(name, dict);
		}

		R_Distribution(std::shared_ptr<Distribution> && d) : distr(std::move(d)) {
		}

		R_Distribution(std::shared_ptr<Distribution> & d) : distr(d) {
		}

	public:
		int numberOfDimensions() const { return distr->numberOfDimensions(); }

		SEXP logpdf(SEXP xSexp, bool gradX, bool hessX) {
			if( ::Rf_isMatrix(xSexp) ) {
				Rcpp::NumericMatrix x_(xSexp);
				if(gradX || hessX)
					throw std::runtime_error("gradient and hessians are not supported in vectorized mode");

				Require( x_.rows() == distr->numberOfDimensions(), "wrong number of rows in x" );
				ConstMapMatrixd x(x_.begin(), x_.rows(), x_.cols());

				std::vector<double> result(x.cols());
				tbb::parallel_for(0, x.cols(), [&](int i) {
					try {
						result[i] = distr->logpdf(x.col(i));
					} catch( model_error & e ) {
						result[i] = math::negInf();
					}
				});

				return Rcpp::wrap(result);
			} else {
				Rcpp::NumericVector x_(xSexp);
				ConstMapVecd x(x_.begin(), x_.size());
				Require( x_.size() == distr->numberOfDimensions(), "wrong number of elements in x" );

				if( hessX ) {
					const int N = x.size();
					Rcpp::NumericMatrix H_( N, N );
					Rcpp::NumericVector g_( N );

					MapMatrixd H(H_.begin(), H_.rows(), H_.cols());
					MapVecd g(g_.begin(), g_.size());
					double ll = distr->logpdf(x, g, H);

					Rcpp::NumericVector ret = Rcpp::wrap(ll);
					ret.attr("grad") = g_;
					ret.attr("hess") = H_;
					return ret;
				} else if( gradX ) {
					Rcpp::NumericVector g_( x.size() );
					MapVecd g(g_.begin(), g_.size());
					double ll = distr->logpdf(x, g);

					Rcpp::NumericVector ret = Rcpp::wrap(ll);
					ret.attr("grad") = g_;
					return ret;
				} else {
					return Rcpp::wrap( distr->logpdf(x) );
				}
			}
		}

		Rcpp::NumericMatrix sample(int nsamples, Rcpp::List params_) {
			R_Dict params(params_);
			auto sampler = distr->sampler(params);
			Rcpp::NumericMatrix s_(distr->numberOfDimensions(), nsamples);
			MapMatrixd s(s_.begin(), s_.rows(), s_.cols());
			sampler->sample(get_rng(params_), s);
			return s_;
		}

	public:
		Distribution & get() const { return *distr; }
		std::shared_ptr<Distribution> getPtr() const { return distr; }

	private:
		std::shared_ptr<Distribution> distr;
};

class IndividualDistribution : public Distribution {
public:
	IndividualDistribution(const Individual &subject, ConstRefVecd &beta, ConstRefVecd &tau, ConstRefMatrixd &omega)
			: mvn(omega), condme(mvn, subject, beta, tau) {}

public:
	int numberOfDimensions() const override {
		return condme.numberOfDimensions();
	}

public:
	double logpdf(ConstRefVecd & x) const override {
		return condme.logpdf(x);
	}

	double logpdf(ConstRefVecd & x, RefVecd grad) const override {
		return condme.logpdf(x, grad);
	}

	double logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const override {
		return condme.logpdf(x, grad, H);
	}

private:
	ZMMvNDistribution mvn;
	ConditionalIndividualDistribution condme;
};

R_Distribution make_individual(R_Individual &subject, Rcpp::NumericVector beta_,
		Rcpp::NumericVector tau_, Rcpp::NumericMatrix omega_) {
	ConstMapVecd beta(beta_.begin(), beta_.size());
	ConstMapVecd tau(tau_.begin(), tau_.size());
	ConstMapMatrixd omega(omega_.begin(), omega_.rows(), omega_.cols());
	return {std::make_shared<IndividualDistribution>(subject.get(), beta, tau, omega)};
}

R_Distribution make_smlm(R_StructuralModel &sm, R_LikelihoodModel &lm) {
	return {std::make_shared<SMLMDistribution>(sm.getModelPtr(), lm.getModelPtr())};
}

R_Distribution make_product(R_Distribution &ll, R_Distribution &prior) {
	return {std::make_shared<ProductDistribution<>>(ll.getPtr(), prior.getPtr())};
}

R_Distribution make_masked(R_Distribution &dist, Rcpp::NumericVector vals_, Rcpp::LogicalVector mask_) {
	ConstMapVecd vals(vals_.begin(), vals_.size());
	Vecb mask(mask_.size());
	for(int i = 0; i < mask_.size(); ++i)
		mask[i] = mask_[i] != 0;

	return {mask_distribution(dist.getPtr(), vals, mask)};
}

bool optimization_trace_helper(OptimizationState istate, OptimizationValues & vals, Rcpp::Function f) {
	std::string state;
	switch(istate) {
		case OptimizationState::Init:
			state = "init";
			break;

		case OptimizationState::Iter:
			state = "iter";
			break;

		case OptimizationState::Interrupt:
			state = "interrupt";
			break;

		case OptimizationState::Done:
			state = "done";
			break;
	}

	if( istate == OptimizationState::Interrupt )
		checkUserInterrupt();

	SEXP res = f(Rcpp::List::create( // values
			Rcpp::Named("state") = Rcpp::String(state), // state
			Rcpp::Named("iteration") = Rcpp::wrap(vals.iteration), // iteration number
			Rcpp::Named("x") = Rcpp::wrap(vals.x), // position
			Rcpp::Named("value") = Rcpp::wrap(vals.value), // function value
			Rcpp::Named("gradNorm") = Rcpp::wrap(vals.gradNorm)));

	return Rf_isLogical(res) ? Rcpp::as<bool>(res) : false;
}

OptimizationTraceCallback make_optimization_trace(Rcpp::Function f) {
	return [f](OptimizationState state, OptimizationValues vals) -> bool {
		return optimization_trace_helper(state, vals, f);
	};
}

bool me_optimization_trace_helper(OptimizationState istate, size_t iteration, const MEModel & m, Rcpp::Function f) {
	std::string state;
	switch(istate) {
		case OptimizationState::Init:
			state = "init";
			break;

		case OptimizationState::Iter:
			state = "iter";
			break;

		case OptimizationState::Interrupt:
			state = "interrupt";
			break;

		case OptimizationState::Done:
			state = "done";
			break;
	}

	if( istate == OptimizationState::Interrupt )
		checkUserInterrupt();

	SEXP res = f(Rcpp::String(state), // state
			Rcpp::wrap(iteration), // iteration number
			Rcpp::wrap(R_ConstMEModel(m))); // model

	return Rf_isLogical(res) ? Rcpp::as<bool>(res) : false;
}

MEOptimizationTraceCallback make_me_optimization_trace(Rcpp::Function f) {
	return [f](OptimizationState state, size_t iter, const MEModel &model) -> bool {
		return me_optimization_trace_helper(state, iter, model, f);
	};
}

int R_saem(R_MEModel & m, Rcpp::List control) {
	MEModel & model = m.get();

	SAEM::Options opts;

	if( control.containsElementNamed("K1") )
		opts.K1 = Rcpp::as<int>( control["K1"] );
	if( control.containsElementNamed("K2") )
		opts.K2 = Rcpp::as<int>( control["K2"] );
	if( control.containsElementNamed("burnin") )
		opts.burnin = Rcpp::as<int>( control["burnin"] );
	if( control.containsElementNamed("thinning") )
		opts.thinning = Rcpp::as<int>( control["thinning"] );
	if( control.containsElementNamed("trace") )
		opts.trace = make_me_optimization_trace( control["trace"] );
	opts.validate();

	SAEM algo(model, opts);
	algo.optimize(get_rng(control));
	return 0;
}

size_t sampling_trace_helper(mcmc::SamplingValues & vals, Rcpp::Function f) {
	checkUserInterrupt();

	SEXP res = f(Rcpp::List::create( // values
			Rcpp::Named("iteration") = Rcpp::wrap(vals.iteration), // iteration number
			Rcpp::Named("x") = Rcpp::wrap(vals.x), // position
			Rcpp::Named("value") = Rcpp::wrap(vals.value))); // function value

	return Rf_isInteger(res) ? Rcpp::as<size_t>(res) : 1;
}

mcmc::SamplingTraceCallback make_sampling_trace(Rcpp::Function f) {
	return [f](mcmc::SamplingValues vals) -> size_t {
		return sampling_trace_helper(vals, f);
	};
}

std::unique_ptr<mcmc::Proposal> createProposal(Rcpp::List sampler, Distribution & target) {
	R_Dict data(sampler);

	std::string type = data.getStringDefault("proposal", "MH");
	return mcmc::ProposalFactory::instance().createProposal(type, target, data);
}

Rcpp::NumericMatrix R_hmc(Rcpp::List control, int nsamples, Rcpp::NumericVector theta_, R_Distribution & prior) {
	double epsilon = 0.01;
	int L = 10;
	int nwarmup = 0;
	mcmc::SamplingTraceCallback trace;

	if( control.containsElementNamed("L") )
		L = Rcpp::as<int>( control["L"] );
	if( control.containsElementNamed("epsilon") )
		epsilon = Rcpp::as<double>( control["epsilon"] );
	if( control.containsElementNamed("warmup") )
		nwarmup = Rcpp::as<int>( control["warmup"] );
	if( control.containsElementNamed("trace") )
		trace = make_sampling_trace( control["trace"] );

	ConstMapVecd theta(theta_.begin(), theta_.size());
	const int N = theta.size();

	Rcpp::NumericMatrix samples_(N, nsamples);
	MapMatrixd samples(samples_.begin(), samples_.rows(), samples_.cols());

	mcmc::HamiltonianSampler sampler(prior.get(), epsilon, L, theta);
	sampler.setTrace(trace);
	Random &rng = get_rng(control);
	sampler.warmup(rng, nwarmup);
	sampler.sample(rng, samples);

	return samples_;
}

Rcpp::NumericMatrix R_sample(Rcpp::List control, int nsamples, Rcpp::NumericVector theta, R_Distribution & target) {
	int nwarmup = 0;
	bool adaptive = false;
	mcmc::SamplingTraceCallback trace;

	if( control.containsElementNamed("warmup") )
		nwarmup = Rcpp::as<int>( control["warmup"] );
	if( control.containsElementNamed("trace") )
		trace = make_sampling_trace( control["trace"] );
	if( control.containsElementNamed("always_adaptive") )
		adaptive = Rcpp::as<bool>( control["always_adaptive"] );

	control["initial"] = theta;
	const auto N = theta.size();

	Rcpp::NumericMatrix samples_(N, nsamples);
	MapMatrixd samples(samples_.begin(), samples_.rows(), samples_.cols());

	using namespace mcmc;

	R_Dict params(control);
	std::string name = params.getStringDefault("type", "MH");
	auto sampler = SamplerFactory::instance().createSampler(name, target.getPtr(), params);

	sampler->setTrace(trace);
	Random &rng = get_rng(control);
	sampler->warmup(rng, nwarmup);
	sampler->sample(rng, samples);

	return samples_;
}

Rcpp::NumericMatrix R_sample_me(Rcpp::List control, int nsamples,
		R_MEModel & memodel, R_Distribution & prior) {

	using mcmc::MESampler;
	int thinning = MESampler::kThinning;
	int nwarmup = 0;
	mcmc::SamplingTraceCallback trace{nullptr};

	if( control.containsElementNamed("thinning") )
		thinning = Rcpp::as<int>( control["thinning"] );
	if( control.containsElementNamed("warmup") )
		nwarmup = Rcpp::as<int>( control["warmup"] );
	if( control.containsElementNamed("trace") )
		trace = make_sampling_trace( control["trace"] );

#if 0
	MEDistribution target(memodel.get(), prior.get());
	const int N = target.numberOfDimensions();
	Vecd initial = target.generateInitial();
	double fx = target.logpdf(initial);
	std::cout << "initial " << initial.transpose() << " -> " << fx << std::endl;
	for(size_t i = 0; i < initial.size(); ++i) {
		Vecd pert = initial;
		pert[i] += .1;
		double fx_p = target.logpdf(pert);
		std::cout << "perturbed " << i << " -> " << fx_p << std::endl;
	}

	using namespace mcmc;
//	auto proposal = createProposal(control, target);
//	MHSampler sampler(std::move(proposal), initial);
//	sampler.setAdaptive(true);
	AffineSampler sampler(target, initial, 5000, 1.5);
#else
	MESampler sampler(memodel.get(), prior.get(), thinning);
	const int N = sampler.dimensions();
#endif

	Rcpp::NumericMatrix samples_(N, nsamples);
	MapMatrixd samples(samples_.begin(), samples_.rows(), samples_.cols());

	sampler.setTrace(trace);
	Random &rng = get_rng(control);
	sampler.warmup(rng, nwarmup);
	sampler.sample(rng, samples);

	return samples_;
}

Rcpp::List R_optimize(Rcpp::List list, R_Distribution &dist, Rcpp::NumericVector x_) {
	ConstMapVecd x(x_.begin(), x_.size());
	const int N = x.size();

	const Distribution &d = dist.get();

	OptimizationTraceCallback trace;
	if( list.containsElementNamed("trace") )
		trace = make_optimization_trace( list["trace"] );

	R_Dict data(list);
	std::string type = data.getStringDefault("type", "BFGS");
	if( type == "BFGS" ) {
		BFGS::Options opts;
		opts.fTol = data.getNumericDefault("fTol", 1e-8);
		opts.xTol = data.getNumericDefault("xTol", 1e-32);
		opts.gradTol = data.getNumericDefault("gradTol", 1e-8);
		opts.maxiters = data.getIntegerDefault("maxiters", 1000);
		opts.m = data.getIntegerDefault("m", 10);
		opts.trace = trace;
		BFGS algo(d, x, opts);
		int iters = algo.optimize();

		return Rcpp::List::create(Rcpp::Named("par") = Rcpp::wrap(algo.optimum()),
		                          Rcpp::Named("value") = Rcpp::wrap(-algo.value()),
		                          Rcpp::Named("convergence") = Rcpp::wrap(algo.isConverged()),
								  Rcpp::Named("iters") = Rcpp::wrap(iters));
	} else if( type == "PSO" ) {
		ParticleSwarmOptimization::Options opts;
		opts.numParticles = data.getIntegerDefault("numParticles", 20);
		opts.numEpochs = data.getIntegerDefault("numEpochs", 1000);
		opts.c1 = data.getNumericDefault("c1", 2.0);
		opts.c2 = data.getNumericDefault("c2", 2.0);
		opts.w_start = data.getNumericDefault("w_start", 0.95);
		opts.w_end = data.getNumericDefault("w_end", 0.4);
		opts.w_varyfor = data.getNumericDefault("w_varyfor", 1.0);
		opts.V_max = data.getNumericDefault("V_max", 100.0);
		opts.trace = trace;
		double sigma = data.getNumeric("sigma");

		ParticleSwarmOptimization algo(d, opts);
		algo.optimize(x, sigma);

		return Rcpp::List::create(Rcpp::Named("par") = Rcpp::wrap(algo.optimum()),
		                          Rcpp::Named("value") = Rcpp::wrap(-algo.value()));
	} else if( type == "NelderMead" ) {
		NelderMead::Options opts;
		opts.fTol = data.getNumericDefault("fTol", 1e-8);
		opts.alpha = data.getNumericDefault("alpha", -1.0);
		opts.beta = data.getNumericDefault("beta", 0.5);
		opts.gamma = data.getNumericDefault("gamma", 2.0);
		opts.usualDelta = data.getNumericDefault("usualDelta", 5.0e-2);
		opts.zeroDelta = data.getNumericDefault("zeroDelta", 0.25e-3);
		opts.maxiters = data.getIntegerDefault("maxiters", 1000);
		opts.trace = trace;

		NelderMead algo(d, x, opts);
		int iters = algo.optimize();

		return Rcpp::List::create(Rcpp::Named("par") = Rcpp::wrap(algo.optimum()),
		                          Rcpp::Named("value") = Rcpp::wrap(-algo.value()),
		                          Rcpp::Named("convergence") = Rcpp::wrap(algo.isConverged()),
								  Rcpp::Named("iters") = Rcpp::wrap(iters));
	} else if( type == "Pattern") {
		PatternSearch::Options opts;
		opts.fTol = data.getNumericDefault("fTol", 1e-6);
		opts.xTol = data.getNumericDefault("xTol", 1e-6);
		opts.meshTol = data.getNumericDefault("meshTol", 1e-6);
		opts.initialMeshSize = data.getNumericDefault("initialMeshSize", 1.0);
		opts.expansion = data.getNumericDefault("expansion", 2.0);
		opts.contraction = data.getNumericDefault("contraction", 0.5);
		opts.maxMeshSize = data.getNumericDefault("maxMeshSize", math::posInf());
		opts.maxEvals = data.getNumericDefault("maxEvals", 2000*N);
		opts.maxIters = data.getIntegerDefault("maxIters", 100*N);
		opts.trace = trace;

		std::string method = data.getStringDefault("method", "Positive2N");
		if( method == "Positive2N" )
			opts.method = PatternSearch::Positive2N;
		else if( method == "PositiveNp1" )
			opts.method = PatternSearch::PositiveNp1;
		else if( method == "Cross4N" )
			opts.method = PatternSearch::Cross4N;
		else
			throw std::runtime_error("unknown poll method for patternsearch: " + method);

		PatternSearch algo(d, x, opts);
		int iters = algo.optimize();

		return Rcpp::List::create(Rcpp::Named("par") = Rcpp::wrap(algo.optimum()),
		                          Rcpp::Named("value") = Rcpp::wrap(-algo.value()),
		                          Rcpp::Named("convergence") = Rcpp::wrap(algo.isConverged()),
								  Rcpp::Named("nevals") = Rcpp::wrap(algo.numberOfEvals()),
								  Rcpp::Named("meshsize") = Rcpp::wrap(algo.meshSize()),
								  Rcpp::Named("iters") = Rcpp::wrap(iters));
	} else {
		throw std::runtime_error("unknown optimization method: " + type);
	}
}

#include "PluginManager.hpp"

void loadPlugin(Rcpp::String str) {
	PluginManager::instance().loadPlugin(str);
}

void unloadPlugin(Rcpp::String str) {
	PluginManager::instance().unloadPlugin(str);
}

void R_setNumThreads(int nthreads) {
	diffmem::setNumThreads(nthreads);
}

RCPP_EXPOSED_CLASS(R_StructuralModel);
RCPP_EXPOSED_CLASS(R_LikelihoodModel);
RCPP_EXPOSED_CLASS(R_Individual);
RCPP_EXPOSED_CLASS(R_Population);
RCPP_EXPOSED_CLASS(R_MEModel);
RCPP_EXPOSED_CLASS(R_ConstMEModel);
RCPP_EXPOSED_CLASS(R_Distribution);

extern "C" {
	DIFFMEM_EXPORT SEXP RCPP_MODULE_BOOT(DiffMEM)();
}

RCPP_MODULE(DiffMEM) {
	diffmem::Options opts;
	diffmem::init(opts);

	Rcpp::function("setNumThreads", &R_setNumThreads);

	Rcpp::class_<R_StructuralModel>("StructuralModel")
		.constructor<std::string>()
		.constructor<std::string, Rcpp::List>()
		.method("numberOfObservations", &R_StructuralModel::numberOfObservations)
		.method("numberOfParameters", &R_StructuralModel::numberOfParameters)
		.method("transpsi", &R_StructuralModel::transpsi)
		.method("transphi", &R_StructuralModel::transphi)
		.method("dtranspsi", &R_StructuralModel::dtranspsi)
		.method("evalSensitivity", &R_StructuralModel::evalSensitivity)
		.method("evalU", &R_StructuralModel::evalU)
		;

	Rcpp::class_<R_LikelihoodModel>("LikelihoodModel")
		.constructor<std::string>()
		.constructor<std::string, Rcpp::List>()
		.method("numberOfObservations", &R_LikelihoodModel::numberOfObservations)
		.method("numberOfTaus", &R_LikelihoodModel::numberOfTaus)
		.method("numberOfSigmas", &R_LikelihoodModel::numberOfSigmas)
		.method("transsigma", &R_LikelihoodModel::transsigma)
		.method("transtau", &R_LikelihoodModel::transtau)
		.method("logpdf_u", &R_LikelihoodModel::logpdf_u)
		.method("logpdf", &R_LikelihoodModel::logpdf)
		.method("logpdfGradTau", &R_LikelihoodModel::logpdfGradTau)
		;

	Rcpp::class_<R_Individual>("Individual")
		.constructor<int, R_StructuralModel&, R_LikelihoodModel&, Rcpp::List>()
		.constructor<int, R_StructuralModel&, R_LikelihoodModel&,
					Rcpp::NumericMatrix, Rcpp::NumericMatrix>()
		.method("numberOfObservations", &R_Individual::numberOfObservations)
		.method("numberOfParameters", &R_Individual::numberOfParameters)
		.method("numberOfBetas", &R_Individual::numberOfBetas)
		.method("numberOfRandom", &R_Individual::numberOfRandom)
		.method("numberOfMeasurements", &R_Individual::numberOfMeasurements)
		.method("toParameter", &R_Individual::toParameter)
		.method("logpdf", &R_Individual::logpdf)
		.method("logpdfGradTau", &R_Individual::logpdfGradTau)
		;

	Rcpp::class_<R_Population>("Population")
		.constructor()
		.method("add", &R_Population::add)
		.method("numberOfParameters", &R_Population::numberOfParameters)
		.method("numberOfBetas", &R_Population::numberOfBetas)
		.method("numberOfRandom", &R_Population::numberOfRandom)
		.method("numberOfIndividuals", &R_Population::numberOfIndividuals)
		.method("numberOfTaus", &R_Population::numberOfTaus)
		;

	Rcpp::class_<R_MEModel>("MEModel")
		.constructor<R_Population&, Rcpp::List>()
		.method("beta", &R_MEModel::beta)
		.method("omega", &R_MEModel::omega)
		.method("sigma", &R_MEModel::sigma)
		;

	Rcpp::class_<R_ConstMEModel>("ConstMEModel")
		.method("beta", &R_ConstMEModel::beta)
		.method("omega", &R_ConstMEModel::omega)
		.method("sigma", &R_ConstMEModel::sigma)
		;

	Rcpp::class_<R_Distribution>("Distribution")
		.constructor<std::string>()
		.constructor<std::string, Rcpp::List>()
		.method("numberOfDimensions", &R_Distribution::numberOfDimensions)
		.method("sample", &R_Distribution::sample)
		.method("logpdf", &R_Distribution::logpdf)
		;

	Rcpp::function("saem", &R_saem);
	Rcpp::function("hmc", &R_hmc);
	Rcpp::function("sample", &R_sample);
	Rcpp::function("sample_me", &R_sample_me);
	Rcpp::function("optim", &R_optimize);
	Rcpp::function("make_smlm", &make_smlm);
	Rcpp::function("make_individual", &make_individual);
	Rcpp::function("make_product", &make_product);
	Rcpp::function("make_masked", &make_masked);
	Rcpp::function("loadPlugin", &loadPlugin);
	Rcpp::function("unloadPlugin", &unloadPlugin);
}
