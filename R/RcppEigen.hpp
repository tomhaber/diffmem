/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RCPP_EIGEN_H
#define RCPP_EIGEN_H

#include <RcppCommon.h>
#include <MatrixVector.hpp>

namespace Rcpp {

	template<typename Scalar, int Rows, int Cols, int Options, int MaxRows, int MaxCols>
	SEXP wrap(const Eigen::Matrix<Scalar, Rows, Cols, Options, MaxRows, MaxCols> & m) {
		SEXP ans = PROTECT(wrap(m.data(), m.data() + m.size()));
		if( Cols != 1 ) {
			SEXP dd = PROTECT(::Rf_allocVector(INTSXP, 2));
			int *d = INTEGER(dd);
			d[0] = m.rows();
			d[1] = m.cols();
			::Rf_setAttrib(ans, R_DimSymbol, dd);
			UNPROTECT(1);
		}
		UNPROTECT(1);

		return ans;
	}

	template<typename Scalar, int Rows, int Cols, int Options, int MaxRows, int MaxCols>
	SEXP wrap(const Eigen::Ref<const Eigen::Matrix<Scalar, Rows, Cols, Options, MaxRows, MaxCols>> & m) {
		SEXP ans = PROTECT(wrap(m.data(), m.data() + m.size()));
		if( Cols != 1 ) {
			SEXP dd = PROTECT(::Rf_allocVector(INTSXP, 2));
			int *d = INTEGER(dd);
			d[0] = m.rows();
			d[1] = m.cols();
			::Rf_setAttrib(ans, R_DimSymbol, dd);
			UNPROTECT(1);
		}
		UNPROTECT(1);

		return ans;
	}

}
#endif
