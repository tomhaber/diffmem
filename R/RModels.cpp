/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ProcessPool.hpp"
#include "picosha2.h"
#include "message.hpp"

#include <Exception.hpp>
#include <StructuralModel.hpp>
#include <LikelihoodModel.hpp>
#include <Distribution.hpp>
#include <ModelFactory.hpp>

std::string generateHash(const std::string & data) {
	unsigned char hash[32];
	picosha2::hash256(data.begin(), data.end(), hash, hash+32);

	std::string hash_hex_str;
	picosha2::hash256_hex_string(hash, hash+32, hash_hex_str);
	return hash_hex_str;
}

class RStructuralModel final : public StructuralModel {
	public:
		RStructuralModel(const Dict & dict) {
			nparams = dict.getInteger("nparams");
			nobservations = dict.getInteger("nobservations");
			std::string func = dict.getString("func");
			name = generateHash(func);

			pp = ProcessPool::instance();
			serialization::Buffer msg = pack_message(Command::REGISTER_FUNCTION, name, func);
			pp->runEverywhere(msg);
		}

		~RStructuralModel() {
			serialization::Buffer msg = pack_message(Command::UNREGISTER_FUNCTION, name);
			pp->runEverywhere(msg);
		}

	public:
		int numberOfParameters() const override { return nparams; }
		int numberOfObservations() const override { return nobservations; }

	public:
		void evalU(ConstRefVecd &psi, ConstRefVecd &t, RefMatrixd u) const override {
			Require( psi.size() == nparams );

			serialization::Buffer msg = pack_message(Command::EVAL_U, name, psi, t);
			pp->runTask(msg, msg);
			unpack_reply(msg, u);

			Require( u.rows() == nobservations );
		}

	private:
		int nparams;
		int nobservations;
		std::string name;
		std::shared_ptr<ProcessPool> pp;
};

class RDistribution final : public Distribution {
	public:
		RDistribution(const Dict & dict) {
			dim = dict.getInteger("dim");
			std::string func = dict.getString("func");
			name = generateHash(func);

			pp = ProcessPool::instance();

			serialization::Buffer msg = pack_message(Command::REGISTER_FUNCTION, name, func);
			pp->runEverywhere(msg);
		}

		~RDistribution() {
			serialization::Buffer msg = pack_message(Command::UNREGISTER_FUNCTION, name);
			pp->runEverywhere(msg);
		}

	public:
		int numberOfDimensions() const override { return dim; }

	public:
		double logpdf(ConstRefVecd & x) const override {
			Require( x.size() == dim );

			serialization::Buffer msg = pack_message(Command::LOGPDF, name, x);
			pp->runTask(msg, msg);

			double lp;
			unpack_reply(msg, lp);
			return lp;
		}

		double logpdf(ConstRefVecd & x, RefVecd grad) const override {
			Require( x.size() == dim );

			serialization::Buffer msg = pack_message(Command::LOGPDF_GRAD, name, x);
			pp->runTask(msg, msg);

			double lp;
			unpack_reply(msg, lp, grad);
			return lp;
		}

		double logpdf(ConstRefVecd & x, RefVecd grad, RefMatrixd H) const override {
			Require( x.size() == dim );

			serialization::Buffer msg = pack_message(Command::LOGPDF_HESS, name, x);
			pp->runTask(msg, msg);

			double lp;
			unpack_reply(msg, lp, grad, H);
			return lp;
		}

	private:
		int dim;
		std::string name;
		std::shared_ptr<ProcessPool> pp;
};

REGISTER_DISTRIBUTION(RDistribution, RDistribution);
REGISTER_STRUCTURAL_MODEL(RStructuralModel, RStructuralModel);
