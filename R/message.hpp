/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MESSAGE_H
#define MESSAGE_H

#include <MatrixVector.hpp>
#include <Serialization.hpp>

enum class Command : int { REGISTER_FUNCTION, UNREGISTER_FUNCTION, LOGPDF, LOGPDF_GRAD, LOGPDF_HESS, EVAL_U, EVAL_SENS };
enum class Response : int { SUCCESS, FAILURE };

template <typename... Args>
void pack_message(serialization::Buffer &msg, Command type, Args &&...args) {
	serialization::BufferOutputArchive ar(msg);
	serialization::pack(ar, type, std::forward<Args>(args)...);
}

template <typename... Args>
void pack_message(serialization::Buffer &msg, Response resp, Args &&...args) {
	serialization::BufferOutputArchive ar(msg);
	serialization::pack(ar, resp, std::forward<Args>(args)...);
}

template <typename... Args>
serialization::Buffer pack_message(Response resp, Args &&...args) {
	serialization::Buffer msg;
	pack_message(msg, resp, std::forward<Args>(args)...);
	return msg;
}

template <typename... Args>
serialization::Buffer pack_message(Command type, Args &&...args) {
	serialization::Buffer msg;
	pack_message(msg, type, std::forward<Args>(args)...);
	return msg;
}

template <typename... Args>
void unpack_reply(serialization::Buffer &msg, Args &&...args) {
	serialization::BufferInputArchive ar(msg);

	Response type;
	serialization::unpack(ar, type);
	if( type == Response::SUCCESS ) {
		serialization::unpack(ar, std::forward<Args>(args)...);
	} else {
		std::string error;
		serialization::unpack(ar, error);
		throw std::runtime_error(error.c_str());
	}
}
#endif
