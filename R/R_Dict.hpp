/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef R_DICT_H_
#define R_DICT_H_

#include <Rcpp.h>
#include <Dict.hpp>

class R_Dict final : public Dict {
	public:
		R_Dict() {}
		R_Dict(SEXP sexp) : list(sexp) {}
		R_Dict(Rcpp::List list) : list(list) {}

	public:
		bool exists(const std::string & name) const override;
		Vecd getNumericVector(const std::string & name) const override;
		Matrixd getNumericMatrix(const std::string & name) const override;
		Veci getIntegerVector(const std::string & name) const override;
		Matrixi getIntegerMatrix(const std::string & name) const override;
		Vecb getBooleanVector(const std::string & name) const override;
		Matrixb getBooleanMatrix(const std::string & name) const override;

		std::string getString(const std::string & name) const override;
		int getInteger(const std::string & name) const override;
		double getNumeric(const std::string & name) const override;
		std::shared_ptr<Dict> getDict(const std::string & name) const override;

	private:
		Rcpp::List list;
};
#endif
