# DiffMEM

DiffMEM is a freely available open-source package under active development for rapid optimization of non-linear mixed effect models using ordinary and partial differential equations (ODE/PDE). During parameter estimation, these ODE/PDE's are frequently integrated numerically as the values of the parameters change from individual to individual and iterations. This can be computationally expensive and results in long runtimes for model fitting that can become a serious limitation for building, testing and using such models. Through heavy use of parallism, extensive low-level optimizations and algorithmic innovation, DiffMEM is able to rapidly fit such complex models. This in turn allows for shorter duty-cycles while building a model, and enables adaptive/optimal trial design and model validation which typically require a lot of simulation-estimation steps.

## Installing

The DiffMEM source directory contains:

* DiffMEM library
* R interface
* Python interface

as well as a CMake script that will build all the components.

Before building, please make sure you have the following dependencies installed:

* [Eigen >= 3.2.8](http://eigen.tuxfamily.org)
* [Sundials CVODES](https://computation.llnl.gov/projects/sundials/cvodes)
* [Intel TBB](https://www.threadingbuildingblocks.org/)
* [Cereal](https://uscilab.github.io/cereal/)
* [fmt](https://github.com/fmtlib/fmt)

Perform the following steps in order to build DiffMEM

```sh
mkdir build # create build directory
cd build
cmake ../ # let cmake set up Makefiles
make
```

### Defining Models

DiffMEM is implemented as a toolbox of optimization routines and samplers and has three concepts of models:

* Distribution
* StructuralModel
* LikelihoodModel

A *Distribution* is the most basic model, containing only the ability to compute the logpdf at a position *x* in space.
New distributions can be implemented in C++ directly by extending from *Distribution* and registering the type
```c++
REGISTER_DISTRIBUTION(name, type);
```

A Distribution can be instantiated in Python or R by referring to it by its *name*. For example in Python:
```python
import pydiffmem.core as dm
import numpy as np

d = dm.Distribution("Rosenbrock3", a=1, b=100) # additional parameters are passed as a dict to the constructor
d.logpdf(np.array([1.,2.,3.])) # evaluate logpdf at position (1,2,3)
```

In addition, Distribution can be built by combining a *StructuralModel* and a *LikelihoodModel*:
the StructuralModel defines the underlying deterministic behaviour (given parameters) whereas
the LikelihoodModel defines the probability of observing data given this behaviour and additional parameters.
This distinction is made to allow reuse of both types of models instead of reimplementing it. Several models are
available within DiffMEM: including PK/PD models with dosing events and Gaussian/LogGaussian likelihood.

For example in R, we can create a two-compartment PK with turn-over PD model and a Gaussian likelihood model as follows:
```R
# define dosing vectors DTIME, AMT, ADDL, CMT, II
# define measurement vectors TIME and DV
sm <- new(m$StructuralModel, "PKPD", list(time=DTIME,amt=AMT,addl=ADDL,cmt=CMT,rate=II))
lm <- new(m$LikelihoodModel, "Gaussian", list(t=TIME, y=matrix(DV,nrow=1)))
d <- m$make_smlm(sm, lm) # construct a Distribution out of 'sm' and 'lm'

# Simulations can be done using 'sm'
t <- seq(0, 10)
u <- sm$evalU( c(ka, ke, Vf, kin, kout, IC50, 0, 0, 0), t)
```
---

An experimental feature allows the definition of StructuralModels and Distributions using R functions. In the background,
DiffMEM will spawn a set of R processes such that the evaluation of these functions can still proceed in parallel
(similar to the **parallel** package in R).

```
library(deSolve)

PKPDode <- function(t, y, parms) {
    dy1 <- -parms[1] * y[1]
    dy2 <- (parms[1] * y[1] - parms[2] * y[2])/parms[3]
    dy3 <- parms[4] * (1 - y[2]/(parms[5] + y[2])) - parms[6] * y[3]
    return(list(c(dy1, dy2, dy3)))
}

make_logpdf <- function() {
    pars <- c(ka=0.78, ke=1.04, Vf=9.6, kin=86.53, IC50=21.20, kout=0.17)
    xTime <- seq(from = 0, to = 25, by = 0.25)
    yini <- c(1250, 0, 300)

	# generate some data
    out <- ode(times = xTime, y = yini, func = PKPDode, parms = pars, method = "bdf")
    y <- out[,4]

    function(x, grad=F, hess=F) {
        out <- ode (times = xTime, y = yini, func = PKPDode, parms = x, method = "bdf")
        sum( dnorm(y, out[,4], 40, log=T) )
    }
}

d <- new(m$Distribution, "RDistribution", list(func=make_logpdf(), dim=6))
pars <- c(ka=1, ke=0.5, Vf=10, kin=exp(5)*exp(-1), IC50=exp(5), kout=exp(-1))
ans <- d$logpdf(pars, F, F) # evaluate single parameter

many_pars <- pars + matrix(rnorm(100*length(pars)), nrow=length(pars))
ans <- d$logpdf(many_pars, F, F) # evaluate many parameters in parallel
```

## Mixed-Effects Models
Individuals and populations can be built up using the previously described StructuralModels and LikelihoodModel components.
Covariates and the fixed/random effects structure need to be encoded using two matrices: A and B. The individual parameters
for the StructuralModels are modelled in DiffMEM as **phi_i = A_i * beta + B_i * eta_i**, where *beta* are the fixed effects
and *eta_i* the random effects.

The following R example shows how to create Individuals and a Population and finally how to create a MEModel.
The individuals are constructed using a *linear* structuralmodel, describing a simple function **y(t) = a*t^2 + b*t + c**,
and a gaussian likelihood. The *B* matrix describes random effects on all parameters except for *a*.

```R
X <- read.csv(file="simple.csv")
#  id       t       yr        y
#1  1 0.55556  0.49426  0.42657
#2  1 1.11110  1.36060  1.52050
#3  1 1.66670  2.75920  2.67120
#4  1 2.22220  4.69000  4.75420
#5  1 2.77780  7.15300  6.81220
#6  1 3.33330 10.14800 10.04200

subjects <- lapply(split(X, f=X$id), function(ind) {
    t <- ind[,"t"]
    y <- matrix(ind[,"y"], nrow=1)
    id <- ind[1, "id"]

    sm <- new(m$StructuralModel, "linear")
    lm <- new(m$LikelihoodModel, "gaussian", list(t=t, y=y))

	A <- diag(1, 3, 3)
	B <- matrix(c(0,0,1,0,0,1), ncol=2, byrow=T)

    new(m$Individual, id, sm, lm, list(t=t, y=y, A=A, B=B))
})

population <- new(m$Population)
for( ind in subjects )
    population$add(ind)

beta <- c(alpha=.7, beta=.5, lambda=-1.)
omega <- diag(rep(1,2))
sigma <- 1

mem <- new(m$MEModel, population,
     list(beta=beta,
			estimated=c(alpha=T, beta=T, lambda=T),
			omega=omega,
			cov_model=matrix(c(T,T,T,T), ncol=2),
			sigma=sigma,
			sigma_model=T))
```

### Optimization
DiffMEM currently includes a SAEM procedure for fitting mixed-effects models. The procedure will estimate (in parallel)
the *beta*, *omega* and *sigma* parameters.

```R
m$saem(mem, list(burnin=50, K1=200, K2=100))

# mem$beta()
# [1]  0.8606239  0.3734469 -1.2851465
# mem$omega()
#           [,1]     [,2]
# [1,] 0.9091364 0.514996
# [2,] 0.5149960 1.012454
# mem$sigma()
# [1] 0.1011258

```

## High-level Interface
We have made an (experimental) layer on top of the DiffMEM infrastructure which allows the definition of the previous models
using a high-level language. This avoids the need to write models in C++. The models are automatically translated,
automatically differentiated and compiled.

The high-level language tries to close to the mathematical description and uses a WinBUGS-like syntax for the definition of
statistical models. The following describes a two-compartment ODE pharmacokinetic structuralmodel:

```
$settings:
	modelname = theo
	isstiff = false
	withdosing = true

$params: ka, Cl, V

$ode: A, B
	A_0 = 0
	B_0 = 0

	d_A = -ka * A;
	d_B = ka * A - (Cl / V) * B;

$output:  Ct
	Ct = B / V
```

Distributions and LikelihoodModels can be similarly defined. The following example describes a 6 parameter prior distribution.

```
$params: basem, kout, ke, imax, b, basep, tau

	basem ~ lognormal(3.61, 0.002)
	tau ~ gamma(54.441, 24.476)
	ke ~ lognormal(-1.15, 0.514)
	kout ~ lognormal(1.93, 6.84)
	basep*basep ~ gamma(2.022, 1.186)
	imax ~ beta(19.574,  174.532)
	b ~ uniform(-10, 10)
```

## Model Fitting as a Web-Service
Additionally, a web-service has been built on top of DiffMEM. Given that software installation, hardware configuration and performance tuning
requires highly specialized knowledge, we want to shield the end-user as much as possible. The web-service allows this by separating the model
definition from the model fitting process. User are able to specify their models using the previously described high-level language, those can
then be submitted together with the data to the web-service. The backend, tuned specifically for the available hardware, can then perform the
parameter estimation or simulation while optimally using the hardware.

A demo of the web-service is available on request. Please email <tom.haber@uhasselt.be>

## Optimization Algorithms
DiffMEM contains several optimization algorithms which can find the maxima of a *Distribution* in serial or parallel.
All algorithms are easily accessible using the *optim* function in the Python and R interfaces.

Serial algorithms:

* BFGS
* NelderMead

Parallel algorithms:

* Pattern: pattern search
* PSO: particle swarm

For example, we can optimize the distribution created above:
```python
d = dm.Distribution("Rosenbrock3", a=1, b=100)
fit = dm.optim(d, np.array([1,2,3]), algo="NelderMead")
#{'par': array([[1.],
#       [1.],
#       [1.]]), 'value': -1.90476116837221e-17, 'convergence': True, 'iters': 182}

```

### BFGS
Broyden–Fletcher–Goldfarb–Shanno (BFGS) algorithm is an iterative method for solving unconstrained nonlinear optimization problems.
It uses function values and gradients to build up a picture of the surface to be optimized.

Options:

* xTol: Termination tolerance on x. The default value is 1e-32
* fTol: Termination tolerance on the function value. The default value is 1e-8
* gradTol: Termination tolerance on the norm of the gradient. The default value is 1e-8
* m: size of the BFGS memory. Default value is 10
* maxiters: The maximum number of iterations. Defaults to 1000

### Nelder-Mead
A robust but relatively slow optimization algorithm that uses only function values. It works reasonably well for
non-differentiable functions.

Options:

* fTol: Termination tolerance on the function value. The default value is 1e-8
* alpha: the reflection factor. Default is -1
* beta: the contraction factor. Default is .5
* gamma: the expansion factor. Default is 2.0
* maxiters: The maximum number of iterations. Defaults to 1000

### Patternsearch
Patternsearch looks for an optima based on an adaptive mesh that is aligned with the coordinate directions.
At each iteration, the pattern search polls the points in the current mesh. It computes the objective function at the mesh points
to see if there is one whose function value is less than the function value at the current point. You can specify the pattern that
defines the mesh by the **method** option. The default pattern *Positive2N* consists of the following 2N directions:

	[1 0 0...0]
	[0 1 0...0]
	...
	[0 0 0...1]
	[–1 0 0...0]
	[0 –1 0...0]
	[0 0 0...–1]

Alternatively, you can set method to *PositiveNP1*, the pattern consisting of the following N + 1 directions:

	[1 0 0...0]
	[0 1 0...0]
	...
	[0 0 0...1]
	[–1 –1 –1...–1]

or the *Cross4N* pattern, consisting of the following 2N directions and its negative:

	[1 0 0...0]
	[0 1 0...0]
	...
	[0 0 0...1]
	[1+alpha 0 0...0]
	[0 1+alpha 0...0]
	[0 0 0...1+alpha]

Options:

* xTol: Termination tolerance on x. The default value is 1e-32
* fTol: Termination tolerance on the function value. The default value is 1e-8
* meshTol: Tolerance on mesh size. Default value is 1e-6
* initialMeshSize: size of the initial mesh. Default value is 1.0
* expansion: Mesh expansion factor for successful iteration. Default value is 2.0
* contraction: Mesh contraction factor for unsuccessful iteration. Default value is 0.5
* maxMeshSize: Maximum mesh size used in a step. Default is Inf
* maxEvals: Maximum number of function evaluations: Default is 2000
* maxIters: Maximum number of iterations: Default is 200
* method: polling method used

### Particle swarm optimization
Particle swarm optimization solves a problem by having a population of candidate solutions, particles, and moving these particles
around in the search-space according to simple mathematical formulae over the particle's position and velocity. Each particle's movement
is influenced by its local best known position, but is also guided toward the best known positions in the search-space, which are updated
as better positions are found by other particles. This is expected to move the swarm toward the best solutions.

Particle swarm optimization can be initialized by either a bounding box or a position and variance. The initial set of particles
will then be generated either uniformly within the box or using a Gaussian distribution.

Options:

* numParticles: number of particles used. Default is 20
* numEpochs: number of particles updates performed. Default is 1000
* c1: Default is 2.0
* c2: Default is 2.0
* w_start: Default is 0.95
* w_end: Default is 0.4
* w_varyfor: Default is 1.0
* V_max: Default is 100.0

## Sampling
DiffMEM also contains several sampling algorithms which can be used to draw samples from a *Distribution* in serial or parallel.

Samplers:

* MH: Metropolis-Hasting with different proposal distributions
* Hamiltonian: Hamiltonian Monte-Carlo
* PopMCMC: population MCMC
* SMC: sequential Monte-Carlo

```python
d = dm.Distribution("Rosenbrock3", a=1, b=100)
# draw 10 samples from the distribution
samples = dm.sample(d, 10, warmup=1000, initial=np.array([1.,2.,3.]), type_="MH")
array([[1.24195551, 1.24195551, 1.24195551, 1.24195551, 1.24195551,
        1.24195551, 1.28637928, 1.28600196, 1.28600196, 1.28600196],
       [1.57576914, 1.57576914, 1.57576914, 1.57576914, 1.57576914,
        1.57576914, 1.64732902, 1.5951178 , 1.5951178 , 1.5951178 ],
       [2.60345656, 2.60345656, 2.60345656, 2.60345656, 2.60345656,
        2.60345656, 2.63100227, 2.65703332, 2.65703332, 2.65703332]])
```

DiffMEM has its own internal random number stream. When calling functions consuming random numbers (such as *sample*), the random stream will advance accordingly.
Calling the same function again will consume a different set of random numbers. The user can specify an additional *seed* parameter such that DiffMEM will create
a novel random stream for that function.

### Metropolis-Hasting
Proposal distributions can be chosen with the *proposal* option. Available proposals include:

* MH: default gaussian random walk distribution
* AMM: Adaptive Mixture-Metropolis
* DiagAMM: Adaptive Mixture-Metropolis with diagonal covariance estimate
* MALA: Metropolis Adjusted Langevin Algorithm
* mMALA: manifold Metropolis Adjusted Langevin Algorithm

### Hamiltonian Monte-Carlo
Options:

* epsilon: stepsize in leapfrog steps
* L: number of leapfrog steps


## Attribution

Please cite DiffMEM if you find this code useful in your research and contact us for collaboration

The BibTeX entry for the paper is:

	@misc{diffmem,
		author = {Tom Haber and Valdemar Melicher and Thomas Kovac and Balazs Nemeth and Johan Claes},
		title = {DiffMEM},
		url = {https://bitbucket.org/tomhaber/diffmem}
	}


## License

Copyright 2018 Universiteit Hasselt.

DiffMEM is free software made available under the 3-Clause BSD License. For details see
the LICENSE file.
