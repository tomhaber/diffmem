#===============================================
# Do the final processing for the package find.
#===============================================
macro(findpkg_finish PREFIX)
  # skip if already processed during this run
  if (NOT ${PREFIX}_FOUND)
    if (${PREFIX}_INCLUDE_DIR AND ${PREFIX}_LIBRARY)
      set(${PREFIX}_FOUND TRUE)
      set (${PREFIX}_INCLUDE_DIRS ${${PREFIX}_INCLUDE_DIR})
      set (${PREFIX}_LIBRARIES ${${PREFIX}_LIBRARY})
    else ()
      if (${PREFIX}_FIND_REQUIRED AND NOT ${PREFIX}_FIND_QUIETLY)
        message(FATAL_ERROR "Required library ${PREFIX} not found.")
      endif ()
    endif ()

   #mark the following variables as internal variables
   mark_as_advanced(${PREFIX}_INCLUDE_DIR
                    ${PREFIX}_LIBRARY
                    ${PREFIX}_LIBRARY_DEBUG
                    ${PREFIX}_LIBRARY_RELEASE)
  endif ()
endmacro(findpkg_finish)


#===============================================
# Generate debug names from given RELEASEease names
#===============================================
macro(get_debug_names PREFIX)
  foreach(i ${${PREFIX}})
    set(${PREFIX}_DEBUG ${${PREFIX}_DEBUG} ${i}d ${i}D ${i}_d ${i}_D ${i}_debug ${i})
  endforeach(i)
endmacro(get_debug_names)


macro(make_library_set PREFIX)
  if (${PREFIX}_RELEASE AND ${PREFIX}_DEBUG)
    set(${PREFIX} optimized ${${PREFIX}_RELEASE} debug ${${PREFIX}_DEBUG})
  elseif (${PREFIX}_RELEASE)
    set(${PREFIX} ${${PREFIX}_RELEASE})
  elseif (${PREFIX}_DEBUG)
    set(${PREFIX} ${${PREFIX}_DEBUG})
  endif ()
endmacro(make_library_set)


set(TRNG_LIBRARY_NAMES trng4)
get_debug_names(TRNG_LIBRARY_NAMES)

find_path(TRNG_INCLUDE_DIR trng/config.hpp
  HINTS
  $ENV{TRNG_DIR}
  PATH_SUFFIXES include
  PATHS
  ~/Library/Frameworks
  /Library/Frameworks
  /usr/local
  /usr
  /sw # Fink
  /opt/local # DarwinPorts
  /opt/csw # Blastwave
  /opt
)

find_library(TRNG_LIBRARY_RELEASE
             NAMES ${TRNG_LIBRARY_NAMES}
			 HINTS $ENV{TRNG_DIR}
			 PATH_SUFFIXES lib64 lib)
find_library(TRNG_LIBRARY_DEBUG
             NAMES ${TRNG_LIBRARY_NAMES_DEBUG}
			 HINTS $ENV{TRNG_DIR}
			 PATH_SUFFIXES lib64 lib)

make_library_set(TRNG_LIBRARY)

findpkg_finish(TRNG)
