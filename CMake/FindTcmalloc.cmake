# - Find TCMALLOC
# Find the native TCMALLOC includes and library
#
#  TCMALLOC_INCLUDE_DIR - where to find TCMALLOC.h, etc.
#  TCMALLOC_LIBRARIES   - List of libraries when using TCMALLOC.
#  TCMALLOC_FOUND       - True if TCMALLOC found.

find_path(TCMALLOC_INCLUDE_DIR google/tcmalloc.h)

if (USE_TCMALLOC)
  set(TCMALLOC_NAMES tcmalloc)
else ()
  set(TCMALLOC_NAMES tcmalloc_minimal tcmalloc)
endif ()

find_library(TCMALLOC_LIBRARY NAMES ${TCMALLOC_NAMES})

if (TCMALLOC_INCLUDE_DIR AND TCMALLOC_LIBRARY)
  set(TCMALLOC_FOUND TRUE)
  set( TCMALLOC_LIBRARIES ${TCMALLOC_LIBRARY} )
else ()
  set(TCMALLOC_FOUND FALSE)
  set( TCMALLOC_LIBRARIES )
endif ()

if (TCMALLOC_FOUND)
  message(STATUS "Found Tcmalloc: ${TCMALLOC_LIBRARY}")
else ()
  message(STATUS "Not Found Tcmalloc")
  if (TCMALLOC_FIND_REQUIRED)
    message(STATUS "Looked for Tcmalloc libraries named ${TCMALLOC_NAMES}.")
    message(FATAL_ERROR "Could NOT find Tcmalloc library")
  endif ()
endif ()

mark_as_advanced(
  TCMALLOC_LIBRARY
  TCMALLOC_INCLUDE_DIR
  )
