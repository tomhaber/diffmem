
#
# - This module locates an installed R distribution.
#
# Defines the following:
#  R_COMMAND           - Path to R command
#  R_HOME              - Path to 'R home', as reported by R
#  R_INCLUDE_DIRS      - Path to R include directory
#  R_LIBRARY_BASE      - Path to R library
#  R_LIBRARY_BLAS      - Path to Rblas / blas library
#  R_LIBRARY_LAPACK    - Path to Rlapack / lapack library
#  R_LIBRARY_READLINE  - Path to readline library
#  R_LIBRARIES         - Array of: R_LIBRARY_BASE, R_LIBRARY_BLAS, R_LIBRARY_LAPACK, R_LIBRARY_BASE [, R_LIBRARY_READLINE]
#
# Variable search order:
#   1. Attempt to locate and set R_COMMAND
#     - If unsuccessful, generate error and prompt user to manually set R_COMMAND
#   2. Use R_COMMAND to set R_HOME
#   3. Locate other libraries in the priority:
#     1. Within a user-built instance of R at R_HOME
#     2. Within an installed instance of R
#     3. Within external system libraries
#

if( NOT R_FOUND )
	set(TEMP_CMAKE_FIND_APPBUNDLE ${CMAKE_FIND_APPBUNDLE})
	set(CMAKE_FIND_APPBUNDLE "NEVER")
	find_program(R_COMMAND R DOC "R executable.")
	set(CMAKE_FIND_APPBUNDLE ${TEMP_CMAKE_FIND_APPBUNDLE})

	if(R_COMMAND)
		execute_process(WORKING_DIRECTORY .
										COMMAND ${R_COMMAND} RHOME
										OUTPUT_VARIABLE R_ROOT_DIR
										OUTPUT_STRIP_TRAILING_WHITESPACE)

		set(R_HOME ${R_ROOT_DIR} CACHE PATH "R home directory obtained from R RHOME")

		find_path(R_INCLUDE_DIR R.h
							HINTS ${R_ROOT_DIR}
							PATHS /usr /usr/local /usr/local/lib /usr/local/lib64 /usr/share
							PATH_SUFFIXES include R/include include/R
							DOC "Path to file R.h")

		find_library(R_LIBRARY_BASE R
							HINTS ${R_ROOT_DIR}/lib
							DOC "R library (example libR.a, libR.dylib, etc.).")

		find_library(R_LIBRARY_BLAS NAMES Rblas blas
							HINTS ${R_ROOT_DIR}/lib
							DOC "Rblas library (example libRblas.a, libRblas.dylib, etc.).")

		find_library(R_LIBRARY_LAPACK NAMES Rlapack lapack
							HINTS ${R_ROOT_DIR}/lib
							DOC "Rlapack library (example libRlapack.a, libRlapack.dylib, etc.).")

		find_library(R_LIBRARY_READLINE readline
							DOC "(Optional) system readline library. Only required if the R libraries were built with readline support.")

		# handle the QUIETLY and REQUIRED arguments and set R_FOUND to TRUE if all listed variables are TRUE
		include(FindPackageHandleStandardArgs)
		find_package_handle_standard_args(R DEFAULT_MSG R_INCLUDE_DIR R_LIBRARY_BASE R_LIBRARY_BLAS R_LIBRARY_LAPACK)
	else()
		if (R_FIND_REQUIRED AND NOT R_FIND_QUIETLY)
			message(SEND_ERROR "FindR.cmake requires the following variables to be set: R_COMMAND")
		endif()
	endif()

  if( R_LIBRARY_BASE AND R_INCLUDE_DIR )
		# Note: R_LIBRARY_BASE is added to R_LIBRARIES twice; this may be due to circular linking dependencies; needs further investigation
		if( R_LIBRARY_BLAS AND R_LIBRARY_LAPACK )
			set(R_LIBRARIES ${R_LIBRARY_BASE} ${R_LIBRARY_BLAS} ${R_LIBRARY_LAPACK} ${R_LIBRARY_BASE})
		else()
			set(R_LIBRARIES ${R_LIBRARY_BASE})
		endif()

		if(R_LIBRARY_READLINE)
			set(R_LIBRARIES ${R_LIBRARIES} ${R_LIBRARY_READLINE})
		endif()
		set(R_INCLUDE_DIRS ${R_INCLUDE_DIR})
		set(R_FOUND TRUE)
	else()
		if (R_FIND_REQUIRED AND NOT R_FIND_QUIETLY)
			message(FATAL_ERROR "Required library R not found.")
		endif ()
	endif()

  #mark the following variables as internal variables
  mark_as_advanced(R_INCLUDE_DIR
                   R_LIBRARY_BASE
                   R_LIBRARY_BLAS
                   R_LIBRARY_LAPACK)
endif()
