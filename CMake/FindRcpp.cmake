set(NUM_TRUNC_CHARS 2)

set(TEMP_CMAKE_FIND_APPBUNDLE ${CMAKE_FIND_APPBUNDLE})
set(CMAKE_FIND_APPBUNDLE "NEVER")
find_program(RSCRIPT_COMMAND Rscript DOC "Rscript executable.")
set(CMAKE_FIND_APPBUNDLE ${TEMP_CMAKE_FIND_APPBUNDLE})

if(RSCRIPT_COMMAND)
	execute_process(COMMAND ${RSCRIPT_COMMAND} -e "Rcpp:::CxxFlags()"
                  OUTPUT_VARIABLE RCPPINCL
                  OUTPUT_STRIP_TRAILING_WHITESPACE)
	string(SUBSTRING ${RCPPINCL} ${NUM_TRUNC_CHARS} -1 RCPP_INCLUDE_DIRS)

	execute_process(COMMAND Rscript -e "Rcpp:::LdFlags()"
                OUTPUT_VARIABLE RCPP_LIBRARY)
else()
  message(SEND_ERROR "FindRcpp.cmake requires the following variables to be set: RSCRIPT_COMMAND")
endif()
